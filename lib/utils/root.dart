import 'package:flutter/material.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/view/page-home/widget/home_widget.dart';
import 'package:provider/provider.dart';

class Root extends StatefulWidget {
  @override
  RootState createState() => RootState();
}

class RootState extends State<Root> {
  bool isHomeLoading = true;
  bool goingHome = true;

  Widget buildProgressIndicator() {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logo_umkm.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!isHomeLoading) {
      if (goingHome) {
        return Home();
      } else {
        return Home();
      }
    }
    final CHttp provider = Provider.of<CHttp>(context, listen: false);
    provider.auth.isLoggedIn().then((value) {
      if (value) {
        setState(() {
          isHomeLoading = false;
          goingHome = value;
        });
      } else {
        setState(() {
          goingHome = false;
          isHomeLoading = false;
        });
      }
    });
    return buildProgressIndicator();
  }
}

import 'package:dio/dio.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseAuth {
  Future<User> register(
    String _email,
    String _name,
    String _password,
    String _role,
    String _identityNumber,
    String _gender,
    String _phoneNumber,
    String _firebaseToken,
  );
  Future<User> login(String username, String password, String firebaseToken);
  Future<User> loadUser();
  Future<bool> isLoggedIn();
  void refreshToken();
  void logout();
  User currentUser;
}

class Auth implements BaseAuth {
  // final Dio _dio = Dio();
  User currentUser;
  bool _isVisited;

  Future<bool> isOnboardVisited() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _isVisited = prefs.getBool("visited");
    if (_isVisited == null) {
      _isVisited = false;
    }
    prefs.setBool("visited", true);
    return _isVisited;
  }

  @override
  Future<bool> isLoggedIn() async {
    bool isLoaded = false;
    User _user = await loadUser();
    isLoaded = _user == null ? false : true;
    return isLoaded;
  }

  @override
  Future<User> loadUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("id") == null) {
      currentUser = null;
    } else {
      User _user = User(
          accessToken: prefs.getString('accessToken'),
          refreshToken: prefs.getString('refreshToken'),
          data: UserClass(
            id: prefs.getString("id"),
            email: prefs.getString("email"),
            name: prefs.getString("name"),
            phoneNumber: prefs.getString("phone"),
            gender: prefs.getString("gender"),
            role: prefs.getString("role"),
            isActive: prefs.getBool("isActive"),
            identityNumber: prefs.getString("identityNumber"),
            refreshToken: prefs.getString("refreshToken"),
            avatarUrl: prefs.getString("avatarUrl"),
          ));
      currentUser = _user;
    }
    return currentUser;
  }

  @override
  Future<User> login(
      String _username, String _password, String _firebaseToken) async {
    final Dio _dio = Dio();
    // (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
    //     (HttpClient client) {
    //   client.badCertificateCallback =
    //       (X509Certificate cert, String host, int port) => true;
    // };
    print("user & pw");
    print(_username);
    print(_password);
    final String loginUrl = "$baseUrl/user-service/login";
    print(loginUrl);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      Response _res = await _dio.post(loginUrl,
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            "phone_number": _username,
            "password": _password,
            "firebase_token": _firebaseToken
          });
      print(_res);
      if (_res.statusCode == 200) {
        User _user = User.fromJson(_res.data);
        currentUser = _user;
        _writeData();
        print("id nya adalah");
        print(prefs.getString("id"));
      }
    } catch (err) {
      print(err);
      currentUser = null;
      this._deleteData();
    }
    return currentUser;
  }

  @override
  Future<User> register(
    String _email,
    String _name,
    String _password,
    String _role,
    String _identityNumber,
    String _gender,
    String _phoneNumber,
    String _firebaseToken,
  ) async {
    final Dio _dio = Dio();
    final String registerUrl = "$baseUrl/user-service/signup";
    print(registerUrl);
    print(_email);
    print(_name);
    print(_password);
    print(_role);
    print(_identityNumber);
    print(_gender);
    print(_phoneNumber);
    print(_firebaseToken);
    try {
      Response _res = await _dio.post(registerUrl,
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            "email": _email,
            "name": _name,
            "password": _password,
            "role": _role,
            "identity_number": _identityNumber,
            "gender": _gender,
            "phone_number": _phoneNumber,
            "firebase_token": _firebaseToken
          });
      print(_res);
      if (_res.statusCode != 200 && _res.statusCode != 201) {
        throw ("Error: ${_res.statusCode}, message ${_res.statusMessage}");
      }
      User _user = User.fromJson(_res.data);
      currentUser = _user;
      _writeData();
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      print(err.response.data);
      currentUser = null;
      this._deleteData();
    }
    return currentUser;
  }

  void _writeData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool("visited", true);
    prefs.setBool("isActive", true);
    prefs.setString("id", currentUser.data.id);
    prefs.setString("email", currentUser.data.email);
    prefs.setString("name", currentUser.data.name);
    prefs.setString("phone", currentUser.data.phoneNumber);
    prefs.setString("gender", currentUser.data.gender);
    prefs.setString("role", currentUser.data.role);
    prefs.setString("identityNumber", currentUser.data.identityNumber);
    prefs.setString("refreshToken", currentUser.refreshToken);
    prefs.setString("accessToken", currentUser.accessToken);
    prefs.setString("avatarUrl", currentUser.data.avatarUrl);
  }

  void _deleteData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("isActive", false);
    prefs.remove("id");
    prefs.remove("email");
    prefs.remove("name");
    prefs.remove("phone");
    prefs.remove("gender");
    prefs.remove("role");
    prefs.remove("identityNumber");
    prefs.remove("accessToken");
    prefs.remove("refreshToken");
    prefs.remove("avatarUrl");
    currentUser = null;
  }

  @override
  void logout() {
    _deleteData();
  }

  Future<User> updateUser(
    String _email,
    String _name,
    String _identityNumber, [
    String avatarUrl = '',
  ]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String _id = prefs.getString("id");
    if (_id == null) {
      print("kenapa id kosong?");
      currentUser = null;
      return currentUser;
    } else {
      final String _updateUrl = "$baseUrl/user-service/user/$_id";
      print(_updateUrl);
      print(_email);
      print(_name);
      print(_identityNumber);
      print(avatarUrl);
      print(currentUser.accessToken);
      print('ajeg');

      Map<String, dynamic> _data = {
        "email": _email,
        "name": _name,
        "identity_number": _identityNumber,
      };

      if (avatarUrl.trim().length > 0) {
        _data.addAll({"avatar_url": avatarUrl});
      }

      try {
        Dio _dio = Dio();
        Response _res = await _dio.put(
            'https://cxapi.connexist.com/user-service/user/$_id',
            options: Options(
                contentType: Headers.formUrlEncodedContentType,
                headers: {
                  'x-access-token': '${currentUser.accessToken}',
                }),
            data: _data);
        print(_res.data);
        UserClass _user = UserClass.fromJson(_res.data["data"]);
        currentUser = User(
            refreshToken: currentUser.refreshToken,
            accessToken: currentUser.accessToken,
            data: _user);

        _writeData();
        return currentUser;
      } catch (err) {
        print(err);
        return null;
      }
    }
  }

  @override
  Future<User> refreshToken() async {
    if (currentUser != null) {
      final String refreshUrl = "$baseUrl/user-service/refresh";
      try {
        Dio _dio = Dio();
        Response _res = await _dio.post(refreshUrl,
            options: Options(
              contentType: Headers.formUrlEncodedContentType,
            ),
            data: {
              'refreshToken': '${currentUser.refreshToken}',
            });
        if (_res.statusCode != 200 && _res.statusCode != 201) {
          throw ("Error: ${_res.statusCode}, message ${_res.statusMessage}");
        }
        User _user = User.fromJson(_res.data);
        currentUser = User(
            refreshToken: currentUser.refreshToken,
            accessToken: _user.accessToken,
            data: currentUser.data);
        _writeData();
      } catch (err) {
        print("gagal refresh $err, ${currentUser.refreshToken}");
        currentUser = null;
        this._deleteData();
      }
    }
    return currentUser;
  }
}

class User {
  User({
    this.data,
    this.accessToken,
    this.refreshToken,
  });

  UserClass data;
  String accessToken;
  String refreshToken;

  factory User.fromJson(Map<String, dynamic> json) => User(
        data: json["data"] == null ? null : UserClass.fromJson(json["data"]),
        accessToken: json["accessToken"] == null ? null : json["accessToken"],
        refreshToken:
            json["refreshToken"] == null ? null : json["refreshToken"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "accessToken": accessToken == null ? null : accessToken,
        "refreshToken": refreshToken == null ? null : refreshToken,
      };
}

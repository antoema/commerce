import 'package:pekik_app/model/search_history.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class DBHelper {
  static final DBHelper _instance = new DBHelper.internal();
  factory DBHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "search.db");
    var theDb = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
          create table Search ( 
          _id INTEGER PRIMARY KEY AUTOINCREMENT, 
          name TEXT NOT NULL UNIQUE)
          ''');
    });
    return theDb;
  }

  DBHelper.internal();

  Future<SearchHistory> insert(SearchHistory user) async {
    var dbClient = await db;
    user.id = await dbClient.insert('Search', user.toMap());
    return user;
  }

  Future<SearchHistory> getSerachHistory(int id) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query('Search',
        columns: ['_id', 'name'], where: '_id = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return SearchHistory.fromMap(maps.first);
    }
    return null;
  }

  Future<int> deleteSerachHistory(int id) async {
    var dbClient = await db;
    return await dbClient.delete('Search', where: '_id = ?', whereArgs: [id]);
  }

  Future<int> deleteAll() async {
    var dbClient = await db;
    return await dbClient.rawDelete('Delete from Search');
  }

  Future<int> updateSerachHistory(SearchHistory user) async {
    var dbClient = await db;
    return await dbClient
        .update('Search', user.toMap(), where: '_id = ?', whereArgs: [user.id]);
  }

  Future<List> getAllSerachHistory() async {
    List<SearchHistory> user = List();
    var dbClient = await db;
    List<Map> maps = await dbClient.query('Search',
        columns: ['_id', 'name'], orderBy: "_id DESC", limit: 5);
    if (maps.length > 0) {
      maps.forEach((f) {
        user.add(SearchHistory.fromMap(f));
      });
    }
    return user;
  }

  Future closeSerachHistory() async {
    var dbClient = await db;
    dbClient.close();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MyWebview extends StatefulWidget {
  MyWebview({
    Key key,
    this.onUrlChanged,
    this.onStateChanged,
    this.url,
    this.title,
    // this.navigationDelegate
  }) : super(key: key);

  final ValueChanged<String> onUrlChanged;
  final String url;
  final String title;
  final ValueChanged<WebViewStateChanged> onStateChanged;
  // final ValueChanged<NavigationRequest> navigationDelegate;
  final FlutterWebviewPlugin flutterWebviewPlugin = new FlutterWebviewPlugin();

  _MyWebviewState createState() => _MyWebviewState();
}

class _MyWebviewState extends State<MyWebview> {
  bool isLoading = true;
  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    super.initState();

    // widget.flutterWebviewPlugin.onUrlChanged.listen((String url) {
    //   widget.onUrlChanged(url);
    // });
    widget.flutterWebviewPlugin.onUrlChanged.listen((String url) {
      // navigationDelegate:
      // (NavigationRequest request) {
      //   if (request.url.contains("mailto:")) {
      //     _launchURL(request.url);
      //     return NavigationDecision.prevent;
      //   } else if (request.url.contains("tel:")) {
      //     _launchURL(request.url);
      //     return NavigationDecision.prevent;
      //   } else if (request.url.contains("https://wa.me/")) {
      //     _launchURL(request.url);
      //     return NavigationDecision.prevent;
      //   }
      //   return NavigationDecision.navigate;
      // };
      if (url.contains("mailto:")) {
        widget.flutterWebviewPlugin.stopLoading();
        _launchURL(url);
        return NavigationDecision.prevent;
      } else if (url.contains("tel:")) {
        widget.flutterWebviewPlugin.stopLoading();
        _launchURL(url);
        return NavigationDecision.prevent;
      } else if (url.contains("whatsapp:")) {
        widget.flutterWebviewPlugin.stopLoading();
        _launchURL(url);
        return NavigationDecision.prevent;
      }
      return NavigationDecision.navigate;
    });

    widget.flutterWebviewPlugin.onStateChanged.listen((event) {
      widget.onStateChanged(event);
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.flutterWebviewPlugin.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
        url: widget.url,
        appBar: new AppBar(
            elevation: 0,
            textTheme:
                TextTheme(caption: TextStyle(color: ColorPalette.themeIcon)),
            iconTheme: IconThemeData(color: ColorPalette.themeIcon),
            title: Text(
              widget.title,
              style: TextStyle(
                  color: ColorPalette.themeIcon,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Poppins'),
            ),
            backgroundColor: Colors.white),
        invalidUrlRegex: '/((mailto:\w+)|(tel:\w+)|(whatsapp:\w+))/g',
        withZoom: true,
        hidden: true,
        primary: true,
        resizeToAvoidBottomInset: true,
        initialChild: Stack(
          children: [
            Align(
                alignment: Alignment.topCenter,
                child: LinearProgressIndicator(
                  backgroundColor: ColorPalette.btnGreen.withOpacity(0.5),
                  valueColor:
                      AlwaysStoppedAnimation<Color>(ColorPalette.btnGreen),
                )),
          ],
        ),
        withLocalStorage: true,
        appCacheEnabled: true,
        supportMultipleWindows: true,
        withJavascript: true,
        clearCache: true,
        clearCookies: true,
        userAgent: mobileUA);
    // return Scaffold(
    //   appBar: AppBar(
    //     iconTheme: IconThemeData(color: Colors.white),
    //     title: Text(
    //       widget.title,
    //       style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    //     ),
    //     backgroundColor: ColorPalette.themeColor,
    //   ),
    //   body: Stack(children: [
    //     WebView(
    //       initialUrl: widget.url,
    //       javascriptMode: JavascriptMode.unrestricted,
    //       onPageFinished: (finish) {
    //         setState(() {
    //           isLoading = false;
    //         });
    //       },
    //       navigationDelegate: (NavigationRequest request) {
    //         if (request.url.contains("mailto:")) {
    //           _launchURL(request.url);
    //           return NavigationDecision.prevent;
    //         } else if (request.url.contains("tel:")) {
    //           _launchURL(request.url);
    //           return NavigationDecision.prevent;
    //         } else if (request.url.contains("https://wa.me/")) {
    //           _launchURL(request.url);
    //           return NavigationDecision.prevent;
    //         }
    //         return NavigationDecision.navigate;
    //       },
    //     ),
    //     isLoading
    //         ? Center(
    //             child: CircularProgressIndicator(),
    //           )
    //         : Stack(),
    //   ]),
    // );
  }
}

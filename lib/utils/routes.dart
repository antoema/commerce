import 'package:pekik_app/utils/root.dart';
import 'package:pekik_app/view/page-auth/page_buat_akun.dart';
import 'package:pekik_app/view/page-auth/page_login.dart';
import 'package:pekik_app/view/page-merchant/page_detail_transaksi_toko.dart';
import 'package:pekik_app/view/page-profile/page_setting_account.dart';
import 'package:pekik_app/view/page-home/page_home.dart';
import 'package:pekik_app/view/page-merchant/page_daftar_produk.dart';
import 'package:pekik_app/view/page-merchant/page_tambah_barang.dart';
import 'package:pekik_app/view/page-merchant/page_toko_saya.dart';
import 'package:pekik_app/view/page-merchant/page_transaksi_toko.dart';
import 'package:pekik_app/view/page-profile/page_profile.dart';
import 'package:pekik_app/view/page-public-product/page_public_detail_product.dart';
import 'package:pekik_app/view/page-public-product/page_public_search_product.dart';
import 'package:pekik_app/view/page-transaction/page_select_delivery.dart';
import 'package:pekik_app/view/page-transaction/page_select_payment.dart';

final appRoutes = {
  '/': (context) => Root(),
  '/home': (context) => HomePage(),
  '/login': (context) => LoginPage(),
  '/buat-akun': (context) => BuatAkunPage(),
  '/profile': (context) => ProfilePage(),
  '/edit-profile': (context) => ProfilePage(),
  '/setting-account': (context) => SettingAccountPage(),
  '/toko-saya': (context) => TokoSayaPage(),
  '/tambah-barang': (context) => TambahBarangPage(),
  '/daftar-produk': (context) => DaftarProdukPage(),
  '/detail-produk': (context) => PublicDetailProductPage(),
  '/select-delivery': (context) => SelectDeliveryPage(),
  '/select-payment': (context) => SelectPaymentPage(),
  '/pesanan': (context) => TransaksiTokoPage(),
  '/search': (context) => PublicSearchProductPage(),
  '/detail-transaksi': (context) => DetailTransaksiTokoPage(),
};

// To parse this JSON data, do
//
//     final regionModel = regionModelFromJson(jsonString);

import 'dart:convert';

RegionModel regionModelFromJson(String str) =>
    RegionModel.fromJson(json.decode(str));

String regionModelToJson(RegionModel data) => json.encode(data.toJson());

class RegionModel {
  RegionModel({
    this.data,
  });

  List<DataRegionModel> data;

  factory RegionModel.fromJson(Map<String, dynamic> json) => RegionModel(
        data: json["data"] == null
            ? null
            : List<DataRegionModel>.from(
                json["data"].map((x) => DataRegionModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataRegionModel {
  DataRegionModel({
    this.cityId,
    this.provinceId,
    this.province,
    this.type,
    this.cityName,
    this.postalCode,
  });

  String cityId;
  String provinceId;
  String province;
  String type;
  String cityName;
  String postalCode;

  factory DataRegionModel.fromJson(Map<String, dynamic> json) =>
      DataRegionModel(
        cityId: json["city_id"] == null ? null : json["city_id"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        province: json["province"] == null ? null : json["province"],
        type: json["type"] == null ? null : json["type"],
        cityName: json["city_name"] == null ? null : json["city_name"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
      );

  Map<String, dynamic> toJson() => {
        "city_id": cityId == null ? null : cityId,
        "province_id": provinceId == null ? null : provinceId,
        "province": province == null ? null : province,
        "type": type == null ? null : type,
        "city_name": cityName == null ? null : cityName,
        "postal_code": postalCode == null ? null : postalCode,
      };
}

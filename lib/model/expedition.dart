// To parse this JSON data, do
//
//     final expeditionModel = expeditionModelFromJson(jsonString);

import 'dart:convert';

ExpeditionModel expeditionModelFromJson(String str) =>
    ExpeditionModel.fromJson(json.decode(str));

String expeditionModelToJson(ExpeditionModel data) =>
    json.encode(data.toJson());

class ExpeditionModel {
  ExpeditionModel({
    this.data,
  });

  List<DataExpedition> data;

  factory ExpeditionModel.fromJson(Map<String, dynamic> json) =>
      ExpeditionModel(
        data: json["data"] == null
            ? null
            : List<DataExpedition>.from(
                json["data"].map((x) => DataExpedition.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataExpedition {
  DataExpedition({
    this.code,
    this.name,
    this.costs,
  });

  String code;
  String name;
  List<ExpeditionCost> costs;

  factory DataExpedition.fromJson(Map<String, dynamic> json) => DataExpedition(
        code: json["code"] == null ? null : json["code"],
        name: json["name"] == null ? null : json["name"],
        costs: json["costs"] == null
            ? null
            : List<ExpeditionCost>.from(
                json["costs"].map((x) => ExpeditionCost.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "name": name == null ? null : name,
        "costs": costs == null
            ? null
            : List<dynamic>.from(costs.map((x) => x.toJson())),
      };
}

class ExpeditionCost {
  ExpeditionCost({
    this.service,
    this.description,
    this.cost,
  });

  String service;
  String description;
  List<CostCost> cost;

  factory ExpeditionCost.fromJson(Map<String, dynamic> json) => ExpeditionCost(
        service: json["service"] == null ? null : json["service"],
        description: json["description"] == null ? null : json["description"],
        cost: json["cost"] == null
            ? null
            : List<CostCost>.from(
                json["cost"].map((x) => CostCost.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "service": service == null ? null : service,
        "description": description == null ? null : description,
        "cost": cost == null
            ? null
            : List<dynamic>.from(cost.map((x) => x.toJson())),
      };
}

class CostCost {
  CostCost({
    this.value,
    this.etd,
    this.note,
  });

  int value;
  String etd;
  String note;

  factory CostCost.fromJson(Map<String, dynamic> json) => CostCost(
        value: json["value"] == null ? null : json["value"],
        etd: json["etd"] == null ? null : json["etd"],
        note: json["note"] == null ? null : json["note"],
      );

  Map<String, dynamic> toJson() => {
        "value": value == null ? null : value,
        "etd": etd == null ? null : etd,
        "note": note == null ? null : note,
      };
}

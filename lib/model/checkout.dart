// To parse this JSON data, do
//
//     final checkoutModel = checkoutModelFromJson(jsonString);

import 'dart:convert';

CheckoutModel checkoutModelFromJson(String str) =>
    CheckoutModel.fromJson(json.decode(str));

String checkoutModelToJson(CheckoutModel data) => json.encode(data.toJson());

class CheckoutModel {
  CheckoutModel({
    this.data,
  });

  DataCheckoutModel data;

  factory CheckoutModel.fromJson(Map<String, dynamic> json) => CheckoutModel(
        data: json["data"] == null
            ? null
            : DataCheckoutModel.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataCheckoutModel {
  DataCheckoutModel({
    this.shippingAddress,
    this.totalPrice,
    this.expeditionCost,
    this.totalAmount,
    this.carts,
  });

  ShippingAddress shippingAddress;
  int totalPrice;
  int expeditionCost;
  int totalAmount;
  List<CartCheckoutModel> carts;

  factory DataCheckoutModel.fromJson(Map<String, dynamic> json) =>
      DataCheckoutModel(
        shippingAddress: json["shipping_address"] == null
            ? null
            : ShippingAddress.fromJson(json["shipping_address"]),
        totalPrice: json["total_price"] == null ? null : json["total_price"],
        expeditionCost:
            json["expedition_cost"] == null ? null : json["expedition_cost"],
        totalAmount: json["total_amount"] == null ? null : json["total_amount"],
        carts: json["carts"] == null
            ? null
            : List<CartCheckoutModel>.from(
                json["carts"].map((x) => CartCheckoutModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "shipping_address":
            shippingAddress == null ? null : shippingAddress.toJson(),
        "total_price": totalPrice == null ? null : totalPrice,
        "expedition_cost": expeditionCost == null ? null : expeditionCost,
        "total_amount": totalAmount == null ? null : totalAmount,
        "carts": carts == null
            ? null
            : List<dynamic>.from(carts.map((x) => x.toJson())),
      };
}

class CartCheckoutModel {
  CartCheckoutModel({
    this.id,
    this.userId,
    this.tenantId,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.expedition,
    this.priceChanged,
    this.stokNotEnough,
    this.items,
    this.tenant,
  });

  String id;
  String userId;
  String tenantId;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  Expedition expedition;
  bool priceChanged;
  bool stokNotEnough;
  List<ItemCheckoutModel> items;
  TenantCheckoutModel tenant;

  factory CartCheckoutModel.fromJson(Map<String, dynamic> json) =>
      CartCheckoutModel(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        tenantId: json["tenant_id"] == null ? null : json["tenant_id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        expedition: json["expedition"] == null
            ? null
            : Expedition.fromJson(json["expedition"]),
        priceChanged:
            json["price_changed"] == null ? null : json["price_changed"],
        stokNotEnough:
            json["stok_not_enough"] == null ? null : json["stok_not_enough"],
        items: json["items"] == null
            ? null
            : List<ItemCheckoutModel>.from(
                json["items"].map((x) => ItemCheckoutModel.fromJson(x))),
        tenant: json["tenant"] == null
            ? null
            : TenantCheckoutModel.fromJson(json["tenant"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "tenant_id": tenantId == null ? null : tenantId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "expedition": expedition == null ? null : expedition.toJson(),
        "price_changed": priceChanged == null ? null : priceChanged,
        "stok_not_enough": stokNotEnough == null ? null : stokNotEnough,
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
        "tenant": tenant == null ? null : tenant.toJson(),
      };
}

class Expedition {
  Expedition({
    this.id,
    this.cartId,
    this.expeditionCode,
    this.expeditionName,
    this.expeditionService,
    this.expeditionCost,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String cartId;
  String expeditionCode;
  String expeditionName;
  String expeditionService;
  int expeditionCost;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory Expedition.fromJson(Map<String, dynamic> json) => Expedition(
        id: json["_id"] == null ? null : json["_id"],
        cartId: json["cart_id"] == null ? null : json["cart_id"],
        expeditionCode:
            json["expedition_code"] == null ? null : json["expedition_code"],
        expeditionName:
            json["expedition_name"] == null ? null : json["expedition_name"],
        expeditionService: json["expedition_service"] == null
            ? null
            : json["expedition_service"],
        expeditionCost:
            json["expedition_cost"] == null ? null : json["expedition_cost"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "cart_id": cartId == null ? null : cartId,
        "expedition_code": expeditionCode == null ? null : expeditionCode,
        "expedition_name": expeditionName == null ? null : expeditionName,
        "expedition_service":
            expeditionService == null ? null : expeditionService,
        "expedition_cost": expeditionCost == null ? null : expeditionCost,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}

class ItemCheckoutModel {
  ItemCheckoutModel({
    this.id,
    this.cartId,
    this.productId,
    this.productName,
    this.productImage,
    this.price,
    this.qty,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.minimumOrder,
    this.stock,
  });

  String id;
  String cartId;
  String productId;
  String productName;
  String productImage;
  int price;
  int qty;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  int minimumOrder;
  int stock;

  factory ItemCheckoutModel.fromJson(Map<String, dynamic> json) =>
      ItemCheckoutModel(
        id: json["_id"] == null ? null : json["_id"],
        cartId: json["cart_id"] == null ? null : json["cart_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productImage:
            json["product_image"] == null ? null : json["product_image"],
        price: json["price"] == null ? null : json["price"],
        qty: json["qty"] == null ? null : json["qty"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        minimumOrder:
            json["minimum_order"] == null ? null : json["minimum_order"],
        stock: json["stock"] == null ? null : json["stock"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "cart_id": cartId == null ? null : cartId,
        "product_id": productId == null ? null : productId,
        "product_name": productName == null ? null : productName,
        "product_image": productImage == null ? null : productImage,
        "price": price == null ? null : price,
        "qty": qty == null ? null : qty,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "minimum_order": minimumOrder == null ? null : minimumOrder,
        "stock": stock == null ? null : stock,
      };
}

class TenantCheckoutModel {
  TenantCheckoutModel({
    this.isActive,
    this.tenantType,
    this.id,
    this.tenantName,
    this.tenantAddress,
    this.tenantPhone,
    this.tenantLogo,
    this.tenantLat,
    this.tenantLong,
    this.postalCode,
    this.owner,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.city,
    this.cityId,
    this.province,
    this.provinceId,
    this.subDistrict,
    this.subDistrictId,
  });

  bool isActive;
  String tenantType;
  String id;
  String tenantName;
  String tenantAddress;
  String tenantPhone;
  String tenantLogo;
  String tenantLat;
  String tenantLong;
  String postalCode;
  String owner;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String city;
  String cityId;
  String province;
  String provinceId;
  String subDistrict;
  String subDistrictId;

  factory TenantCheckoutModel.fromJson(Map<String, dynamic> json) =>
      TenantCheckoutModel(
        isActive: json["is_active"] == null ? null : json["is_active"],
        tenantType: json["tenant_type"] == null ? null : json["tenant_type"],
        id: json["_id"] == null ? null : json["_id"],
        tenantName: json["tenant_name"] == null ? null : json["tenant_name"],
        tenantAddress:
            json["tenant_address"] == null ? null : json["tenant_address"],
        tenantPhone: json["tenant_phone"] == null ? null : json["tenant_phone"],
        tenantLogo: json["tenant_logo"] == null ? null : json["tenant_logo"],
        tenantLat: json["tenant_lat"] == null ? null : json["tenant_lat"],
        tenantLong: json["tenant_long"] == null ? null : json["tenant_long"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        owner: json["owner"] == null ? null : json["owner"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        city: json["city"] == null ? null : json["city"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        province: json["province"] == null ? null : json["province"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "tenant_type": tenantType == null ? null : tenantType,
        "_id": id == null ? null : id,
        "tenant_name": tenantName == null ? null : tenantName,
        "tenant_address": tenantAddress == null ? null : tenantAddress,
        "tenant_phone": tenantPhone == null ? null : tenantPhone,
        "tenant_logo": tenantLogo == null ? null : tenantLogo,
        "tenant_lat": tenantLat == null ? null : tenantLat,
        "tenant_long": tenantLong == null ? null : tenantLong,
        "postal_code": postalCode == null ? null : postalCode,
        "owner": owner == null ? null : owner,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "city": city == null ? null : city,
        "city_id": cityId == null ? null : cityId,
        "province": province == null ? null : province,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district": subDistrict == null ? null : subDistrict,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

class ShippingAddress {
  ShippingAddress({
    this.id,
    this.userId,
    this.name,
    this.province,
    this.city,
    this.subDistrict,
    this.postalCode,
    this.address,
    this.latitude,
    this.longitude,
    this.defaultLocation,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.cityId,
    this.provinceId,
    this.subDistrictId,
    this.phoneNumber,
  });

  String id;
  String userId;
  String name;
  String province;
  String city;
  String subDistrict;
  int postalCode;
  String address;
  double latitude;
  double longitude;
  bool defaultLocation;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String cityId;
  String provinceId;
  String subDistrictId;
  String phoneNumber;

  factory ShippingAddress.fromJson(Map<String, dynamic> json) =>
      ShippingAddress(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        name: json["name"] == null ? null : json["name"],
        province: json["province"] == null ? null : json["province"],
        city: json["city"] == null ? null : json["city"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        address: json["address"] == null ? null : json["address"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        defaultLocation:
            json["default_location"] == null ? null : json["default_location"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "name": name == null ? null : name,
        "province": province == null ? null : province,
        "city": city == null ? null : city,
        "sub_district": subDistrict == null ? null : subDistrict,
        "postal_code": postalCode == null ? null : postalCode,
        "address": address == null ? null : address,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "default_location": defaultLocation == null ? null : defaultLocation,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "city_id": cityId == null ? null : cityId,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
        "phone_number": phoneNumber == null ? null : phoneNumber,
      };
}

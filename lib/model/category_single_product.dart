// To parse this JSON data, do
//
//     final categorySingleProductModel = categorySingleProductModelFromJson(jsonString);

import 'dart:convert';

CategorySingleProductModel categorySingleProductModelFromJson(String str) =>
    CategorySingleProductModel.fromJson(json.decode(str));

String categorySingleProductModelToJson(CategorySingleProductModel data) =>
    json.encode(data.toJson());

class CategorySingleProductModel {
  CategorySingleProductModel({
    this.data,
  });

  DataCategorySingleProduct data;

  factory CategorySingleProductModel.fromJson(Map<String, dynamic> json) =>
      CategorySingleProductModel(
        data: json["data"] == null
            ? null
            : DataCategorySingleProduct.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataCategorySingleProduct {
  DataCategorySingleProduct({
    this.id,
    this.categoryName,
    this.categoryDesc,
    this.categoryImage,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String categoryName;
  String categoryDesc;
  String categoryImage;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory DataCategorySingleProduct.fromJson(Map<String, dynamic> json) =>
      DataCategorySingleProduct(
        id: json["_id"] == null ? null : json["_id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        categoryDesc:
            json["category_desc"] == null ? null : json["category_desc"],
        categoryImage:
            json["category_image"] == null ? null : json["category_image"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "category_desc": categoryDesc == null ? null : categoryDesc,
        "category_image": categoryImage == null ? null : categoryImage,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}

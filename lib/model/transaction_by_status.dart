// To parse this JSON data, do
//
//     final transactionByStatusModel = transactionByStatusModelFromJson(jsonString);

import 'dart:convert';

TransactionByStatusModel transactionByStatusModelFromJson(String str) =>
    TransactionByStatusModel.fromJson(json.decode(str));

String transactionByStatusModelToJson(TransactionByStatusModel data) =>
    json.encode(data.toJson());

class TransactionByStatusModel {
  TransactionByStatusModel({
    this.data,
  });

  List<DataTransactionByStatusModel> data;

  factory TransactionByStatusModel.fromJson(Map<String, dynamic> json) =>
      TransactionByStatusModel(
        data: json["data"] == null
            ? null
            : List<DataTransactionByStatusModel>.from(json["data"]
                .map((x) => DataTransactionByStatusModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataTransactionByStatusModel {
  DataTransactionByStatusModel({
    this.id,
    this.userId,
    this.tenantId,
    this.totalPrice,
    this.totalAmount,
    this.status,
    this.invoiceNumber,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.items,
  });

  String id;
  String userId;
  String tenantId;
  int totalPrice;
  int totalAmount;
  String status;
  String invoiceNumber;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  List<Item> items;

  factory DataTransactionByStatusModel.fromJson(Map<String, dynamic> json) =>
      DataTransactionByStatusModel(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        tenantId: json["tenant_id"] == null ? null : json["tenant_id"],
        totalPrice: json["total_price"] == null ? null : json["total_price"],
        totalAmount: json["total_amount"] == null ? null : json["total_amount"],
        status: json["status"] == null ? null : json["status"],
        invoiceNumber:
            json["invoice_number"] == null ? null : json["invoice_number"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        items: json["items"] == null
            ? null
            : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "tenant_id": tenantId == null ? null : tenantId,
        "total_price": totalPrice == null ? null : totalPrice,
        "total_amount": totalAmount == null ? null : totalAmount,
        "status": status == null ? null : status,
        "invoice_number": invoiceNumber == null ? null : invoiceNumber,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Item {
  Item({
    this.id,
    this.productId,
    this.productName,
    this.productImage,
    this.price,
    this.qty,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.transactionId,
  });

  String id;
  String productId;
  String productName;
  String productImage;
  int price;
  int qty;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String transactionId;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["_id"] == null ? null : json["_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productImage:
            json["product_image"] == null ? null : json["product_image"],
        price: json["price"] == null ? null : json["price"],
        qty: json["qty"] == null ? null : json["qty"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "product_id": productId == null ? null : productId,
        "product_name": productName == null ? null : productName,
        "product_image": productImage == null ? null : productImage,
        "price": price == null ? null : price,
        "qty": qty == null ? null : qty,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "transaction_id": transactionId == null ? null : transactionId,
      };
}

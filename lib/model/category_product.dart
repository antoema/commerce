// To parse this JSON data, do
//
//     final categoryProductModel = categoryProductModelFromJson(jsonString);

import 'dart:convert';

CategoryProductModel categoryProductModelFromJson(String str) =>
    CategoryProductModel.fromJson(json.decode(str));

String categoryProductModelToJson(CategoryProductModel data) =>
    json.encode(data.toJson());

class CategoryProductModel {
  CategoryProductModel({
    this.data,
  });

  List<ResultCategoryProduct> data;

  factory CategoryProductModel.fromJson(Map<String, dynamic> json) =>
      CategoryProductModel(
        data: json["data"] == null
            ? null
            : List<ResultCategoryProduct>.from(
                json["data"].map((x) => ResultCategoryProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ResultCategoryProduct {
  ResultCategoryProduct({
    this.categoryName,
    this.id,
    this.categoryType,
    this.categoryDesc,
    this.categoryImage,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String categoryType;
  String id;
  String categoryName;
  String categoryDesc;
  String categoryImage;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory ResultCategoryProduct.fromJson(Map<String, dynamic> json) =>
      ResultCategoryProduct(
        categoryType:
            json["category_type"] == null ? null : json["category_type"],
        id: json["_id"] == null ? null : json["_id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        categoryDesc:
            json["category_desc"] == null ? null : json["category_desc"],
        categoryImage:
            json["category_image"] == null ? null : json["category_image"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "category_type": categoryType == null ? null : categoryType,
        "_id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "category_desc": categoryDesc == null ? null : categoryDesc,
        "category_image": categoryImage == null ? null : categoryImage,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}

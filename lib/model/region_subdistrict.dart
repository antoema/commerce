// To parse this JSON data, do
//
//     final regionSubdistrictModel = regionSubdistrictModelFromJson(jsonString);

import 'dart:convert';

RegionSubdistrictModel regionSubdistrictModelFromJson(String str) =>
    RegionSubdistrictModel.fromJson(json.decode(str));

String regionSubdistrictModelToJson(RegionSubdistrictModel data) =>
    json.encode(data.toJson());

class RegionSubdistrictModel {
  RegionSubdistrictModel({
    this.data,
  });

  List<DataSubdistrictModel> data;

  factory RegionSubdistrictModel.fromJson(Map<String, dynamic> json) =>
      RegionSubdistrictModel(
        data: json["data"] == null
            ? null
            : List<DataSubdistrictModel>.from(
                json["data"].map((x) => DataSubdistrictModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataSubdistrictModel {
  DataSubdistrictModel({
    this.subdistrictId,
    this.provinceId,
    this.province,
    this.cityId,
    this.city,
    this.type,
    this.subdistrictName,
  });

  String subdistrictId;
  String provinceId;
  String province;
  String cityId;
  String city;
  String type;
  String subdistrictName;

  factory DataSubdistrictModel.fromJson(Map<String, dynamic> json) =>
      DataSubdistrictModel(
        subdistrictId:
            json["subdistrict_id"] == null ? null : json["subdistrict_id"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        province: json["province"] == null ? null : json["province"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        city: json["city"] == null ? null : json["city"],
        type: json["type"] == null ? null : json["type"],
        subdistrictName:
            json["subdistrict_name"] == null ? null : json["subdistrict_name"],
      );

  Map<String, dynamic> toJson() => {
        "subdistrict_id": subdistrictId == null ? null : subdistrictId,
        "province_id": provinceId == null ? null : provinceId,
        "province": province == null ? null : province,
        "city_id": cityId == null ? null : cityId,
        "city": city == null ? null : city,
        "type": type == null ? null : type,
        "subdistrict_name": subdistrictName == null ? null : subdistrictName,
      };
}

// To parse this JSON data, do
//
//     final productModel = productModelFromJson(jsonString);

import 'dart:convert';

ProductModel productModelFromJson(String str) =>
    ProductModel.fromJson(json.decode(str));

String productModelToJson(ProductModel data) => json.encode(data.toJson());

class ProductModel {
  ProductModel({
    this.data,
  });

  List<ResultProduct> data;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        data: json["data"] == null
            ? null
            : List<ResultProduct>.from(
                json["data"].map((x) => ResultProduct.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ResultProduct {
  ResultProduct({
    this.isActive,
    this.id,
    this.productCode,
    this.productName,
    this.productDesc,
    this.productImage,
    this.productPrice,
    this.productDimensions,
    this.productWeight,
    this.productMeasurementUnit,
    this.minimumOrder,
    this.productStock,
    this.tenantId,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.productCategoryId,
    this.tenant,
    this.category,
  });

  bool isActive;
  String id;
  String productCode;
  String productName;
  String productDesc;
  String productImage;
  int productPrice;
  String productDimensions;
  int productWeight;
  String productMeasurementUnit;
  int minimumOrder;
  int productStock;
  String tenantId;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String productCategoryId;
  Tenant tenant;
  Category category;

  factory ResultProduct.fromJson(Map<String, dynamic> json) => ResultProduct(
        isActive: json["is_active"] == null ? null : json["is_active"],
        id: json["_id"] == null ? null : json["_id"],
        productCode: json["product_code"] == null ? null : json["product_code"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productDesc: json["product_desc"] == null ? null : json["product_desc"],
        productImage:
            json["product_image"] == null ? null : json["product_image"],
        productPrice:
            json["product_price"] == null ? null : json["product_price"],
        productDimensions: json["product_dimensions"] == null
            ? null
            : json["product_dimensions"],
        productWeight:
            json["product_weight"] == null ? null : json["product_weight"],
        productMeasurementUnit: json["product_measurement_unit"] == null
            ? null
            : json["product_measurement_unit"],
        minimumOrder:
            json["minimum_order"] == null ? null : json["minimum_order"],
        productStock:
            json["product_stock"] == null ? null : json["product_stock"],
        tenantId: json["tenant_id"] == null ? null : json["tenant_id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        productCategoryId: json["product_category_id"] == null
            ? null
            : json["product_category_id"],
        tenant: json["tenant"] == null ? null : Tenant.fromJson(json["tenant"]),
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "_id": id == null ? null : id,
        "product_code": productCode == null ? null : productCode,
        "product_name": productName == null ? null : productName,
        "product_desc": productDesc == null ? null : productDesc,
        "product_image": productImage == null ? null : productImage,
        "product_price": productPrice == null ? null : productPrice,
        "product_dimensions":
            productDimensions == null ? null : productDimensions,
        "product_weight": productWeight == null ? null : productWeight,
        "product_measurement_unit":
            productMeasurementUnit == null ? null : productMeasurementUnit,
        "minimum_order": minimumOrder == null ? null : minimumOrder,
        "product_stock": productStock == null ? null : productStock,
        "tenant_id": tenantId == null ? null : tenantId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "product_category_id":
            productCategoryId == null ? null : productCategoryId,
        "tenant": tenant == null ? null : tenant.toJson(),
        "category": category == null ? null : category.toJson(),
      };
}

class Category {
  Category({
    this.id,
    this.categoryName,
    this.categoryDesc,
    this.categoryImage,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String id;
  String categoryName;
  String categoryDesc;
  String categoryImage;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["_id"] == null ? null : json["_id"],
        categoryName:
            json["category_name"] == null ? null : json["category_name"],
        categoryDesc:
            json["category_desc"] == null ? null : json["category_desc"],
        categoryImage:
            json["category_image"] == null ? null : json["category_image"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "category_name": categoryName == null ? null : categoryName,
        "category_desc": categoryDesc == null ? null : categoryDesc,
        "category_image": categoryImage == null ? null : categoryImage,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}

class Tenant {
  Tenant({
    this.isActive,
    this.tenantType,
    this.id,
    this.tenantName,
    this.tenantAddress,
    this.tenantPhone,
    this.tenantLogo,
    this.tenantLat,
    this.tenantLong,
    this.postalCode,
    this.owner,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.city,
    this.cityId,
    this.province,
    this.provinceId,
    this.subDistrict,
    this.subDistrictId,
  });

  bool isActive;
  String tenantType;
  String id;
  String tenantName;
  String tenantAddress;
  String tenantPhone;
  String tenantLogo;
  String tenantLat;
  String tenantLong;
  String postalCode;
  String owner;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String city;
  String cityId;
  String province;
  String provinceId;
  String subDistrict;
  String subDistrictId;

  factory Tenant.fromJson(Map<String, dynamic> json) => Tenant(
        isActive: json["is_active"] == null ? null : json["is_active"],
        tenantType: json["tenant_type"] == null ? null : json["tenant_type"],
        id: json["_id"] == null ? null : json["_id"],
        tenantName: json["tenant_name"] == null ? null : json["tenant_name"],
        tenantAddress:
            json["tenant_address"] == null ? null : json["tenant_address"],
        tenantPhone: json["tenant_phone"] == null ? null : json["tenant_phone"],
        tenantLogo: json["tenant_logo"] == null ? null : json["tenant_logo"],
        tenantLat: json["tenant_lat"] == null ? null : json["tenant_lat"],
        tenantLong: json["tenant_long"] == null ? null : json["tenant_long"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        owner: json["owner"] == null ? null : json["owner"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        city: json["city"] == null ? null : json["city"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        province: json["province"] == null ? null : json["province"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "tenant_type": tenantType == null ? null : tenantType,
        "_id": id == null ? null : id,
        "tenant_name": tenantName == null ? null : tenantName,
        "tenant_address": tenantAddress == null ? null : tenantAddress,
        "tenant_phone": tenantPhone == null ? null : tenantPhone,
        "tenant_logo": tenantLogo == null ? null : tenantLogo,
        "tenant_lat": tenantLat == null ? null : tenantLat,
        "tenant_long": tenantLong == null ? null : tenantLong,
        "postal_code": postalCode == null ? null : postalCode,
        "owner": owner == null ? null : owner,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "city": city == null ? null : city,
        "city_id": cityId == null ? null : cityId,
        "province": province == null ? null : province,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district": subDistrict == null ? null : subDistrict,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

// To parse this JSON data, do
//
//     final paymentModel = paymentModelFromJson(jsonString);

import 'dart:convert';

PaymentModel paymentModelFromJson(String str) =>
    PaymentModel.fromJson(json.decode(str));

String paymentModelToJson(PaymentModel data) => json.encode(data.toJson());

class PaymentModel {
  PaymentModel({
    this.data,
  });

  DataPayment data;

  factory PaymentModel.fromJson(Map<String, dynamic> json) => PaymentModel(
        data: json["data"] == null ? null : DataPayment.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataPayment {
  DataPayment({
    this.paymentGateway,
    this.paymentChannel,
    this.paymentCode,
    this.paymentRefId,
    this.paymentGuide,
    this.paymentGuideUrl,
    this.paymentGuide2Url,
    this.paymentAdminFee,
    this.internalAdminFee,
    this.paymentStatus,
    this.faspayStatus,
    this.merchantName,
    this.type,
    this.amount,
    this.billingUid,
    this.billingNo,
    this.billingPhone,
    this.billingEmail,
    this.billingName,
    this.billingAddress,
    this.accountNumber,
    this.refNo,
    this.description,
    this.currency,
    this.paymentDate,
    this.createdAt,
    this.items,
  });

  String paymentGateway;
  String paymentChannel;
  String paymentCode;
  String paymentRefId;
  String paymentGuide;
  String paymentGuideUrl;
  String paymentGuide2Url;
  int paymentAdminFee;
  int internalAdminFee;
  String paymentStatus;
  String faspayStatus;
  String merchantName;
  int type;
  int amount;
  String billingUid;
  String billingNo;
  String billingPhone;
  String billingEmail;
  String billingName;
  String billingAddress;
  String accountNumber;
  String refNo;
  String description;
  String currency;
  dynamic paymentDate;
  DateTime createdAt;
  List<ItemDataPayment> items;

  factory DataPayment.fromJson(Map<String, dynamic> json) => DataPayment(
        paymentGateway:
            json["payment_gateway"] == null ? null : json["payment_gateway"],
        paymentChannel:
            json["payment_channel"] == null ? null : json["payment_channel"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
        paymentRefId:
            json["payment_ref_id"] == null ? null : json["payment_ref_id"],
        paymentGuide:
            json["payment_guide"] == null ? null : json["payment_guide"],
        paymentGuideUrl: json["payment_guide_url"] == null
            ? null
            : json["payment_guide_url"],
        paymentGuide2Url: json["payment_guide2_url"] == null
            ? null
            : json["payment_guide2_url"],
        paymentAdminFee: json["payment_admin_fee"] == null
            ? null
            : json["payment_admin_fee"],
        internalAdminFee: json["internal_admin_fee"] == null
            ? null
            : json["internal_admin_fee"],
        paymentStatus:
            json["payment_status"] == null ? null : json["payment_status"],
        faspayStatus:
            json["faspay_status"] == null ? null : json["faspay_status"],
        merchantName:
            json["merchant_name"] == null ? null : json["merchant_name"],
        type: json["type"] == null ? null : json["type"],
        amount: json["amount"] == null ? null : json["amount"],
        billingUid: json["billing_uid"] == null ? null : json["billing_uid"],
        billingNo: json["billing_no"] == null ? null : json["billing_no"],
        billingPhone:
            json["billing_phone"] == null ? null : json["billing_phone"],
        billingEmail:
            json["billing_email"] == null ? null : json["billing_email"],
        billingName: json["billing_name"] == null ? null : json["billing_name"],
        billingAddress:
            json["billing_address"] == null ? null : json["billing_address"],
        accountNumber:
            json["account_number"] == null ? null : json["account_number"],
        refNo: json["ref_no"] == null ? null : json["ref_no"],
        description: json["description"] == null ? null : json["description"],
        currency: json["currency"] == null ? null : json["currency"],
        paymentDate: json["payment_date"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        items: json["items"] == null
            ? null
            : List<ItemDataPayment>.from(
                json["items"].map((x) => ItemDataPayment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "payment_gateway": paymentGateway == null ? null : paymentGateway,
        "payment_channel": paymentChannel == null ? null : paymentChannel,
        "payment_code": paymentCode == null ? null : paymentCode,
        "payment_ref_id": paymentRefId == null ? null : paymentRefId,
        "payment_guide": paymentGuide == null ? null : paymentGuide,
        "payment_guide_url": paymentGuideUrl == null ? null : paymentGuideUrl,
        "payment_guide2_url":
            paymentGuide2Url == null ? null : paymentGuide2Url,
        "payment_admin_fee": paymentAdminFee == null ? null : paymentAdminFee,
        "internal_admin_fee":
            internalAdminFee == null ? null : internalAdminFee,
        "payment_status": paymentStatus == null ? null : paymentStatus,
        "faspay_status": faspayStatus == null ? null : faspayStatus,
        "merchant_name": merchantName == null ? null : merchantName,
        "type": type == null ? null : type,
        "amount": amount == null ? null : amount,
        "billing_uid": billingUid == null ? null : billingUid,
        "billing_no": billingNo == null ? null : billingNo,
        "billing_phone": billingPhone == null ? null : billingPhone,
        "billing_email": billingEmail == null ? null : billingEmail,
        "billing_name": billingName == null ? null : billingName,
        "billing_address": billingAddress == null ? null : billingAddress,
        "account_number": accountNumber == null ? null : accountNumber,
        "ref_no": refNo == null ? null : refNo,
        "description": description == null ? null : description,
        "currency": currency == null ? null : currency,
        "payment_date": paymentDate,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class ItemDataPayment {
  ItemDataPayment({
    this.productId,
    this.sku,
    this.qty,
    this.price,
    this.description,
    this.currency,
    this.status,
    this.createdAt,
  });

  String productId;
  String sku;
  int qty;
  int price;
  String description;
  String currency;
  int status;
  DateTime createdAt;

  factory ItemDataPayment.fromJson(Map<String, dynamic> json) =>
      ItemDataPayment(
        productId: json["product_id"] == null ? null : json["product_id"],
        sku: json["sku"] == null ? null : json["sku"],
        qty: json["qty"] == null ? null : json["qty"],
        price: json["price"] == null ? null : json["price"],
        description: json["description"] == null ? null : json["description"],
        currency: json["currency"] == null ? null : json["currency"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId == null ? null : productId,
        "sku": sku == null ? null : sku,
        "qty": qty == null ? null : qty,
        "price": price == null ? null : price,
        "description": description == null ? null : description,
        "currency": currency == null ? null : currency,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
      };
}

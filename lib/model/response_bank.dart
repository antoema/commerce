// To parse this JSON data, do
//
//     final responseBank = responseBankFromJson(jsonString);

import 'dart:convert';

ResponseBank responseBankFromJson(String str) =>
    ResponseBank.fromJson(json.decode(str));

String responseBankToJson(ResponseBank data) => json.encode(data.toJson());

class ResponseBank {
  ResponseBank({
    this.data,
  });

  DataResponseBank data;

  factory ResponseBank.fromJson(Map<String, dynamic> json) => ResponseBank(
        data: json["data"] == null
            ? null
            : DataResponseBank.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataResponseBank {
  DataResponseBank({
    this.kodeProduk,
    this.waktu,
    this.idpel1,
    this.namaPelanggan,
    this.namaBank,
    this.kodeBank,
    this.nominal,
    this.admin,
    this.uid,
    this.pin,
    this.ref1,
    this.ref2,
    this.ref3,
    this.status,
    this.ket,
    this.saldoTerpotong,
    this.sisaSaldo,
    this.urlStruk,
  });

  String kodeProduk;
  String waktu;
  String idpel1;
  String namaPelanggan;
  String namaBank;
  String kodeBank;
  String nominal;
  String admin;
  String uid;
  String pin;
  String ref1;
  String ref2;
  String ref3;
  String status;
  String ket;
  String saldoTerpotong;
  String sisaSaldo;
  String urlStruk;

  factory DataResponseBank.fromJson(Map<String, dynamic> json) =>
      DataResponseBank(
        kodeProduk: json["KODE_PRODUK"] == null ? null : json["KODE_PRODUK"],
        waktu: json["WAKTU"] == null ? null : json["WAKTU"],
        idpel1: json["IDPEL1"] == null ? null : json["IDPEL1"],
        namaPelanggan:
            json["NAMA_PELANGGAN"] == null ? null : json["NAMA_PELANGGAN"],
        namaBank: json["NAMA_BANK"] == null ? null : json["NAMA_BANK"],
        kodeBank: json["KODE_BANK"] == null ? null : json["KODE_BANK"],
        nominal: json["NOMINAL"] == null ? null : json["NOMINAL"],
        admin: json["ADMIN"] == null ? null : json["ADMIN"],
        uid: json["UID"] == null ? null : json["UID"],
        pin: json["PIN"] == null ? null : json["PIN"],
        ref1: json["REF1"] == null ? null : json["REF1"],
        ref2: json["REF2"] == null ? null : json["REF2"],
        ref3: json["REF3"] == null ? null : json["REF3"],
        status: json["STATUS"] == null ? null : json["STATUS"],
        ket: json["KET"] == null ? null : json["KET"],
        saldoTerpotong:
            json["SALDO_TERPOTONG"] == null ? null : json["SALDO_TERPOTONG"],
        sisaSaldo: json["SISA_SALDO"] == null ? null : json["SISA_SALDO"],
        urlStruk: json["URL_STRUK"] == null ? null : json["URL_STRUK"],
      );

  Map<String, dynamic> toJson() => {
        "KODE_PRODUK": kodeProduk == null ? null : kodeProduk,
        "WAKTU": waktu == null ? null : waktu,
        "IDPEL1": idpel1 == null ? null : idpel1,
        "NAMA_PELANGGAN": namaPelanggan == null ? null : namaPelanggan,
        "NAMA_BANK": namaBank == null ? null : namaBank,
        "KODE_BANK": kodeBank == null ? null : kodeBank,
        "NOMINAL": nominal == null ? null : nominal,
        "ADMIN": admin == null ? null : admin,
        "UID": uid == null ? null : uid,
        "PIN": pin == null ? null : pin,
        "REF1": ref1 == null ? null : ref1,
        "REF2": ref2 == null ? null : ref2,
        "REF3": ref3 == null ? null : ref3,
        "STATUS": status == null ? null : status,
        "KET": ket == null ? null : ket,
        "SALDO_TERPOTONG": saldoTerpotong == null ? null : saldoTerpotong,
        "SISA_SALDO": sisaSaldo == null ? null : sisaSaldo,
        "URL_STRUK": urlStruk == null ? null : urlStruk,
      };
}

// To parse this JSON data, do
//
//     final walletModel = walletModelFromJson(jsonString);

import 'dart:convert';

WalletModel walletModelFromJson(String str) =>
    WalletModel.fromJson(json.decode(str));

String walletModelToJson(WalletModel data) => json.encode(data.toJson());

class WalletModel {
  WalletModel({
    this.data,
  });

  DataWallet data;

  factory WalletModel.fromJson(Map<String, dynamic> json) => WalletModel(
        data: json["data"] == null ? null : DataWallet.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataWallet {
  DataWallet({
    this.isActive,
    this.id,
    this.userId,
    this.balance,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  bool isActive;
  String id;
  String userId;
  int balance;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  factory DataWallet.fromJson(Map<String, dynamic> json) => DataWallet(
        isActive: json["isActive"] == null ? null : json["isActive"],
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        balance: json["balance"] == null ? null : json["balance"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "isActive": isActive == null ? null : isActive,
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "balance": balance == null ? null : balance,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
      };
}

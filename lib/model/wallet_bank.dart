// To parse this JSON data, do
//
//     final walletBank = walletBankFromJson(jsonString);

import 'dart:convert';

WalletBank walletBankFromJson(String str) =>
    WalletBank.fromJson(json.decode(str));

String walletBankToJson(WalletBank data) => json.encode(data.toJson());

class WalletBank {
  WalletBank({
    this.data,
  });

  List<DataWalletBank> data;

  factory WalletBank.fromJson(Map<String, dynamic> json) => WalletBank(
        data: json["data"] == null
            ? null
            : List<DataWalletBank>.from(
                json["data"].map((x) => DataWalletBank.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataWalletBank {
  DataWalletBank({
    this.id,
    this.no,
    this.bankName,
    this.bankCode,
  });

  String id;
  String no;
  String bankName;
  String bankCode;

  factory DataWalletBank.fromJson(Map<String, dynamic> json) => DataWalletBank(
        id: json["_id"] == null ? null : json["_id"],
        no: json["no"] == null ? null : json["no"],
        bankName: json["bank_name"] == null ? null : json["bank_name"],
        bankCode: json["bank_code"] == null ? null : json["bank_code"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "no": no == null ? null : no,
        "bank_name": bankName == null ? null : bankName,
        "bank_code": bankCode == null ? null : bankCode,
      };
}

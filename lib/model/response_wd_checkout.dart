// To parse this JSON data, do
//
//     final walletModel = walletModelFromJson(jsonString);

import 'dart:convert';

ResponseWdCheckout responseWdCheckoutFromJson(String str) =>
    ResponseWdCheckout.fromJson(json.decode(str));

String responseWdCheckoutToJson(ResponseWdCheckout data) => json.encode(data.toJson());

class ResponseWdCheckout {
  ResponseWdCheckout({
    this.data,
  });

  DataResponseWdCheckout data;

  factory ResponseWdCheckout.fromJson(Map<String, dynamic> json) => ResponseWdCheckout(
    data: json["data"] == null ? null : DataResponseWdCheckout.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
  };
}

class DataResponseWdCheckout {
  DataResponseWdCheckout({
    this.nominal,
    this.admin,
    this.totalAmount,
  });

  int nominal;
  int admin;
  int totalAmount;

  factory DataResponseWdCheckout.fromJson(Map<String, dynamic> json) => DataResponseWdCheckout(
    nominal: json["nominal"] == null ? null : json["nominal"],
    admin: json["admin"] == null ? null : json["admin"],
    totalAmount: json["total_amount"] == null ? null : json["total_amount"],
  );

  Map<String, dynamic> toJson() => {
    "nominal": nominal == null ? null : nominal,
    "admin": admin == null ? null : admin,
    "total_amount": totalAmount == null ? null : totalAmount,
  };
}

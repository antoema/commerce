class UserClass {
  UserClass({
    this.isActive,
    this.role,
    this.id,
    this.email,
    this.name,
    this.identityNumber,
    this.gender,
    this.phoneNumber,
    this.password,
    this.accessToken,
    this.v,
    this.avatarUrl,
    this.refreshToken,
  });

  bool isActive;
  String role;
  String id;
  String email;
  String name;
  String identityNumber;
  String gender;
  String phoneNumber;
  String password;
  String accessToken;
  int v;
  String avatarUrl;
  String refreshToken;

  factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
        isActive: json["is_active"] == null ? null : json["is_active"],
        role: json["role"] == null ? null : json["role"],
        id: json["_id"] == null ? null : json["_id"],
        email: json["email"] == null ? null : json["email"],
        name: json["name"] == null ? null : json["name"],
        identityNumber:
            json["identity_number"] == null ? null : json["identity_number"],
        gender: json["gender"] == null ? null : json["gender"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        password: json["password"] == null ? null : json["password"],
        accessToken: json["accessToken"] == null ? null : json["accessToken"],
        v: json["__v"] == null ? null : json["__v"],
        avatarUrl: json["avatar_url"] == null ? null : json["avatar_url"],
        refreshToken:
            json["refreshToken"] == null ? null : json["refreshToken"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "role": role == null ? null : role,
        "_id": id == null ? null : id,
        "email": email == null ? null : email,
        "name": name == null ? null : name,
        "identity_number": identityNumber == null ? null : identityNumber,
        "gender": gender == null ? null : gender,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "password": password == null ? null : password,
        "accessToken": accessToken == null ? null : accessToken,
        "__v": v == null ? null : v,
        "avatar_url": avatarUrl == null ? null : avatarUrl,
        "refreshToken": refreshToken == null ? null : refreshToken,
      };
}

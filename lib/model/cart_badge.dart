// To parse this JSON data, do
//
//     final cartBadgesModel = cartBadgesModelFromJson(jsonString);

import 'dart:convert';

CartBadgesModel cartBadgesModelFromJson(String str) =>
    CartBadgesModel.fromJson(json.decode(str));

String cartBadgesModelToJson(CartBadgesModel data) =>
    json.encode(data.toJson());

class CartBadgesModel {
  CartBadgesModel({
    this.data,
  });

  DataCartBadges data;

  factory CartBadgesModel.fromJson(Map<String, dynamic> json) =>
      CartBadgesModel(
        data:
            json["data"] == null ? null : DataCartBadges.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataCartBadges {
  DataCartBadges({
    this.shoppingCartBadges,
  });

  int shoppingCartBadges;

  factory DataCartBadges.fromJson(Map<String, dynamic> json) => DataCartBadges(
        shoppingCartBadges: json["shopping_cart_badges"] == null
            ? null
            : json["shopping_cart_badges"],
      );

  Map<String, dynamic> toJson() => {
        "shopping_cart_badges":
            shoppingCartBadges == null ? null : shoppingCartBadges,
      };
}

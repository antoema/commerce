// To parse this JSON data, do
//
//     final cartModel = cartModelFromJson(jsonString);

import 'dart:convert';

CartModel cartModelFromJson(String str) => CartModel.fromJson(json.decode(str));

String cartModelToJson(CartModel data) => json.encode(data.toJson());

class CartModel {
  CartModel({
    this.data,
  });

  DataCartModel data;

  factory CartModel.fromJson(Map<String, dynamic> json) => CartModel(
        data:
            json["data"] == null ? null : DataCartModel.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataCartModel {
  DataCartModel({
    this.totalPrice,
    this.carts,
  });

  int totalPrice;
  List<Cart> carts;

  factory DataCartModel.fromJson(Map<String, dynamic> json) => DataCartModel(
        totalPrice: json["total_price"] == null ? null : json["total_price"],
        carts: json["carts"] == null
            ? null
            : List<Cart>.from(json["carts"].map((x) => Cart.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "total_price": totalPrice == null ? null : totalPrice,
        "carts": carts == null
            ? null
            : List<dynamic>.from(carts.map((x) => x.toJson())),
      };
}

class Cart {
  Cart({
    this.id,
    this.userId,
    this.tenantId,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.priceChanged,
    this.stokNotEnough,
    this.items,
    this.tenant,
  });

  String id;
  String userId;
  String tenantId;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  bool priceChanged;
  bool stokNotEnough;
  List<ItemCartModel> items;
  TenantCartModel tenant;

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        tenantId: json["tenant_id"] == null ? null : json["tenant_id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        priceChanged:
            json["price_changed"] == null ? null : json["price_changed"],
        stokNotEnough:
            json["stok_not_enough"] == null ? null : json["stok_not_enough"],
        items: json["items"] == null
            ? null
            : List<ItemCartModel>.from(
                json["items"].map((x) => ItemCartModel.fromJson(x))),
        tenant: json["tenant"] == null
            ? null
            : TenantCartModel.fromJson(json["tenant"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "tenant_id": tenantId == null ? null : tenantId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "price_changed": priceChanged == null ? null : priceChanged,
        "stok_not_enough": stokNotEnough == null ? null : stokNotEnough,
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
        "tenant": tenant == null ? null : tenant.toJson(),
      };
}

class ItemCartModel {
  ItemCartModel({
    this.id,
    this.cartId,
    this.productId,
    this.productName,
    this.productImage,
    this.price,
    this.qty,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.minimumOrder,
    this.stock,
  });

  String id;
  String cartId;
  String productId;
  String productName;
  String productImage;
  int price;
  int qty;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  int minimumOrder;
  int stock;

  factory ItemCartModel.fromJson(Map<String, dynamic> json) => ItemCartModel(
        id: json["_id"] == null ? null : json["_id"],
        cartId: json["cart_id"] == null ? null : json["cart_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productImage:
            json["product_image"] == null ? null : json["product_image"],
        price: json["price"] == null ? null : json["price"],
        qty: json["qty"] == null ? null : json["qty"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        minimumOrder:
            json["minimum_order"] == null ? null : json["minimum_order"],
        stock: json["stock"] == null ? null : json["stock"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "cart_id": cartId == null ? null : cartId,
        "product_id": productId == null ? null : productId,
        "product_name": productName == null ? null : productName,
        "product_image": productImage == null ? null : productImage,
        "price": price == null ? null : price,
        "qty": qty == null ? null : qty,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "minimum_order": v == null ? null : stock,
        "stock": v == null ? null : stock,
      };
}

class TenantCartModel {
  TenantCartModel({
    this.isActive,
    this.tenantType,
    this.id,
    this.tenantName,
    this.tenantAddress,
    this.tenantPhone,
    this.tenantLogo,
    this.tenantLat,
    this.tenantLong,
    this.postalCode,
    this.owner,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.city,
    this.cityId,
    this.province,
    this.provinceId,
    this.subDistrict,
    this.subDistrictId,
  });

  bool isActive;
  String tenantType;
  String id;
  String tenantName;
  String tenantAddress;
  String tenantPhone;
  String tenantLogo;
  String tenantLat;
  String tenantLong;
  String postalCode;
  String owner;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String city;
  String cityId;
  String province;
  String provinceId;
  String subDistrict;
  String subDistrictId;

  factory TenantCartModel.fromJson(Map<String, dynamic> json) =>
      TenantCartModel(
        isActive: json["is_active"] == null ? null : json["is_active"],
        tenantType: json["tenant_type"] == null ? null : json["tenant_type"],
        id: json["_id"] == null ? null : json["_id"],
        tenantName: json["tenant_name"] == null ? null : json["tenant_name"],
        tenantAddress:
            json["tenant_address"] == null ? null : json["tenant_address"],
        tenantPhone: json["tenant_phone"] == null ? null : json["tenant_phone"],
        tenantLogo: json["tenant_logo"] == null ? null : json["tenant_logo"],
        tenantLat: json["tenant_lat"] == null ? null : json["tenant_lat"],
        tenantLong: json["tenant_long"] == null ? null : json["tenant_long"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        owner: json["owner"] == null ? null : json["owner"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        city: json["city"] == null ? null : json["city"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        province: json["province"] == null ? null : json["province"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "tenant_type": tenantType == null ? null : tenantType,
        "_id": id == null ? null : id,
        "tenant_name": tenantName == null ? null : tenantName,
        "tenant_address": tenantAddress == null ? null : tenantAddress,
        "tenant_phone": tenantPhone == null ? null : tenantPhone,
        "tenant_logo": tenantLogo == null ? null : tenantLogo,
        "tenant_lat": tenantLat == null ? null : tenantLat,
        "tenant_long": tenantLong == null ? null : tenantLong,
        "postal_code": postalCode == null ? null : postalCode,
        "owner": owner == null ? null : owner,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "city": city == null ? null : city,
        "city_id": cityId == null ? null : cityId,
        "province": province == null ? null : province,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district": subDistrict == null ? null : subDistrict,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

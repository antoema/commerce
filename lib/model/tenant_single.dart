// To parse this JSON data, do
//
//     final tenantSingleModel = tenantSingleModelFromJson(jsonString);

import 'dart:convert';

TenantSingleModel tenantSingleModelFromJson(String str) =>
    TenantSingleModel.fromJson(json.decode(str));

String tenantSingleModelToJson(TenantSingleModel data) =>
    json.encode(data.toJson());

class TenantSingleModel {
  TenantSingleModel({
    this.data,
  });

  List<DataTenantSingle> data;

  factory TenantSingleModel.fromJson(Map<String, dynamic> json) =>
      TenantSingleModel(
        data: json["data"] == null
            ? null
            : List<DataTenantSingle>.from(
                json["data"].map((x) => DataTenantSingle.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataTenantSingle {
  DataTenantSingle({
    this.isActive,
    this.tenantType,
    this.id,
    this.tenantName,
    this.tenantAddress,
    this.tenantPhone,
    this.tenantLogo,
    this.tenantLat,
    this.tenantLong,
    this.postalCode,
    this.owner,
    this.v,
    this.createdAt,
    this.updatedAt,
    this.city,
    this.cityId,
    this.province,
    this.provinceId,
    this.subDistrict,
    this.subDistrictId,
  });

  bool isActive;
  String tenantType;
  String id;
  String tenantName;
  String tenantAddress;
  String tenantPhone;
  String tenantLogo;
  String tenantLat;
  String tenantLong;
  String postalCode;
  String owner;
  int v;
  DateTime createdAt;
  DateTime updatedAt;
  String city;
  String cityId;
  String province;
  String provinceId;
  String subDistrict;
  String subDistrictId;

  factory DataTenantSingle.fromJson(Map<String, dynamic> json) =>
      DataTenantSingle(
        isActive: json["is_active"] == null ? null : json["is_active"],
        tenantType: json["tenant_type"] == null ? null : json["tenant_type"],
        id: json["_id"] == null ? null : json["_id"],
        tenantName: json["tenant_name"] == null ? null : json["tenant_name"],
        tenantAddress:
            json["tenant_address"] == null ? null : json["tenant_address"],
        tenantPhone: json["tenant_phone"] == null ? null : json["tenant_phone"],
        tenantLogo: json["tenant_logo"] == null ? null : json["tenant_logo"],
        tenantLat: json["tenant_lat"] == null ? null : json["tenant_lat"],
        tenantLong: json["tenant_long"] == null ? null : json["tenant_long"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        owner: json["owner"] == null ? null : json["owner"],
        v: json["__v"] == null ? null : json["__v"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        city: json["city"] == null ? null : json["city"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        province: json["province"] == null ? null : json["province"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "tenant_type": tenantType == null ? null : tenantType,
        "_id": id == null ? null : id,
        "tenant_name": tenantName == null ? null : tenantName,
        "tenant_address": tenantAddress == null ? null : tenantAddress,
        "tenant_phone": tenantPhone == null ? null : tenantPhone,
        "tenant_logo": tenantLogo == null ? null : tenantLogo,
        "tenant_lat": tenantLat == null ? null : tenantLat,
        "tenant_long": tenantLong == null ? null : tenantLong,
        "postal_code": postalCode == null ? null : postalCode,
        "owner": owner == null ? null : owner,
        "__v": v == null ? null : v,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "city": city == null ? null : city,
        "city_id": cityId == null ? null : cityId,
        "province": province == null ? null : province,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district": subDistrict == null ? null : subDistrict,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

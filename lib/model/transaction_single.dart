// To parse this JSON data, do
//
//     final transactionSingleModel = transactionSingleModelFromJson(jsonString);

import 'dart:convert';

TransactionSingleModel transactionSingleModelFromJson(String str) =>
    TransactionSingleModel.fromJson(json.decode(str));

String transactionSingleModelToJson(TransactionSingleModel data) =>
    json.encode(data.toJson());

class TransactionSingleModel {
  TransactionSingleModel({
    this.data,
  });

  DataTransactionSingle data;

  factory TransactionSingleModel.fromJson(Map<String, dynamic> json) =>
      TransactionSingleModel(
        data: json["data"] == null
            ? null
            : DataTransactionSingle.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataTransactionSingle {
  DataTransactionSingle({
    this.id,
    this.userId,
    this.tenantId,
    this.totalPrice,
    this.totalAmount,
    this.status,
    this.invoiceNumber,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.wayBill,
    this.payment,
    this.expedition,
    this.tenant,
    this.origin,
    this.destination,
    this.items,
  });

  String id;
  String userId;
  String tenantId;
  int totalPrice;
  int totalAmount;
  String status;
  String invoiceNumber;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String wayBill;
  PaymentTransactionSingle payment;
  ExpeditionTransactionSingle expedition;
  TenantTransactionSingle tenant;
  DestinationTransactionSingle origin;
  DestinationTransactionSingle destination;
  List<ItemTransactionSingle> items;

  factory DataTransactionSingle.fromJson(Map<String, dynamic> json) =>
      DataTransactionSingle(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        tenantId: json["tenant_id"] == null ? null : json["tenant_id"],
        totalPrice: json["total_price"] == null ? null : json["total_price"],
        totalAmount: json["total_amount"] == null ? null : json["total_amount"],
        status: json["status"] == null ? null : json["status"],
        invoiceNumber:
            json["invoice_number"] == null ? null : json["invoice_number"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        wayBill: json["way_bill"] == null ? null : json["way_bill"],
        payment: json["payment"] == null
            ? null
            : PaymentTransactionSingle.fromJson(json["payment"]),
        expedition: json["expedition"] == null
            ? null
            : ExpeditionTransactionSingle.fromJson(json["expedition"]),
        tenant: json["tenant"] == null
            ? null
            : TenantTransactionSingle.fromJson(json["tenant"]),
        origin: json["origin"] == null
            ? null
            : DestinationTransactionSingle.fromJson(json["origin"]),
        destination: json["destination"] == null
            ? null
            : DestinationTransactionSingle.fromJson(json["destination"]),
        items: json["items"] == null
            ? null
            : List<ItemTransactionSingle>.from(
                json["items"].map((x) => ItemTransactionSingle.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "tenant_id": tenantId == null ? null : tenantId,
        "total_price": totalPrice == null ? null : totalPrice,
        "total_amount": totalAmount == null ? null : totalAmount,
        "status": status == null ? null : status,
        "invoice_number": invoiceNumber == null ? null : invoiceNumber,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "way_bill": wayBill == null ? null : wayBill,
        "payment": payment == null ? null : payment.toJson(),
        "expedition": expedition == null ? null : expedition.toJson(),
        "tenant": tenant == null ? null : tenant.toJson(),
        "origin": origin == null ? null : origin.toJson(),
        "destination": destination == null ? null : destination.toJson(),
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class DestinationTransactionSingle {
  DestinationTransactionSingle({
    this.id,
    this.phoneNumber,
    this.name,
    this.province,
    this.city,
    this.subDistrict,
    this.provinceId,
    this.cityId,
    this.subDistrictId,
    this.postalCode,
    this.address,
    this.latitude,
    this.longitude,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.transactionId,
  });

  String id;
  String phoneNumber;
  String name;
  String province;
  String city;
  String subDistrict;
  String provinceId;
  String cityId;
  String subDistrictId;
  int postalCode;
  String address;
  double latitude;
  double longitude;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String transactionId;

  factory DestinationTransactionSingle.fromJson(Map<String, dynamic> json) =>
      DestinationTransactionSingle(
        id: json["_id"] == null ? null : json["_id"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        name: json["name"] == null ? null : json["name"],
        province: json["province"] == null ? null : json["province"],
        city: json["city"] == null ? null : json["city"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        address: json["address"] == null ? null : json["address"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "name": name == null ? null : name,
        "province": province == null ? null : province,
        "city": city == null ? null : city,
        "sub_district": subDistrict == null ? null : subDistrict,
        "province_id": provinceId == null ? null : provinceId,
        "city_id": cityId == null ? null : cityId,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
        "postal_code": postalCode == null ? null : postalCode,
        "address": address == null ? null : address,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "transaction_id": transactionId == null ? null : transactionId,
      };
}

class ExpeditionTransactionSingle {
  ExpeditionTransactionSingle({
    this.id,
    this.expeditionCode,
    this.expeditionName,
    this.expeditionService,
    this.expeditionCost,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.transactionId,
  });

  String id;
  String expeditionCode;
  String expeditionName;
  String expeditionService;
  int expeditionCost;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String transactionId;

  factory ExpeditionTransactionSingle.fromJson(Map<String, dynamic> json) =>
      ExpeditionTransactionSingle(
        id: json["_id"] == null ? null : json["_id"],
        expeditionCode:
            json["expedition_code"] == null ? null : json["expedition_code"],
        expeditionName:
            json["expedition_name"] == null ? null : json["expedition_name"],
        expeditionService: json["expedition_service"] == null
            ? null
            : json["expedition_service"],
        expeditionCost:
            json["expedition_cost"] == null ? null : json["expedition_cost"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "expedition_code": expeditionCode == null ? null : expeditionCode,
        "expedition_name": expeditionName == null ? null : expeditionName,
        "expedition_service":
            expeditionService == null ? null : expeditionService,
        "expedition_cost": expeditionCost == null ? null : expeditionCost,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "transaction_id": transactionId == null ? null : transactionId,
      };
}

class ItemTransactionSingle {
  ItemTransactionSingle({
    this.id,
    this.productId,
    this.productName,
    this.productImage,
    this.price,
    this.qty,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.transactionId,
  });

  String id;
  String productId;
  String productName;
  String productImage;
  int price;
  int qty;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String transactionId;

  factory ItemTransactionSingle.fromJson(Map<String, dynamic> json) =>
      ItemTransactionSingle(
        id: json["_id"] == null ? null : json["_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productImage:
            json["product_image"] == null ? null : json["product_image"],
        price: json["price"] == null ? null : json["price"],
        qty: json["qty"] == null ? null : json["qty"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "product_id": productId == null ? null : productId,
        "product_name": productName == null ? null : productName,
        "product_image": productImage == null ? null : productImage,
        "price": price == null ? null : price,
        "qty": qty == null ? null : qty,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "transaction_id": transactionId == null ? null : transactionId,
      };
}

class PaymentTransactionSingle {
  PaymentTransactionSingle({
    this.id,
    this.transactionId,
    this.paymentChannel,
    this.paymentId,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.paymentCode,
  });

  String id;
  String transactionId;
  String paymentChannel;
  String paymentId;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String paymentCode;

  factory PaymentTransactionSingle.fromJson(Map<String, dynamic> json) =>
      PaymentTransactionSingle(
        id: json["_id"] == null ? null : json["_id"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
        paymentChannel:
            json["payment_channel"] == null ? null : json["payment_channel"],
        paymentId: json["payment_id"] == null ? null : json["payment_id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "transaction_id": transactionId == null ? null : transactionId,
        "payment_channel": paymentChannel == null ? null : paymentChannel,
        "payment_id": paymentId == null ? null : paymentId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "payment_code": paymentCode == null ? null : paymentCode,
      };
}

class TenantTransactionSingle {
  TenantTransactionSingle({
    this.isActive,
    this.tenantType,
    this.id,
    this.tenantName,
    this.tenantAddress,
    this.tenantPhone,
    this.tenantLogo,
    this.tenantLat,
    this.tenantLong,
    this.postalCode,
    this.owner,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.city,
    this.cityId,
    this.province,
    this.provinceId,
    this.subDistrict,
    this.subDistrictId,
  });

  bool isActive;
  String tenantType;
  String id;
  String tenantName;
  String tenantAddress;
  String tenantPhone;
  String tenantLogo;
  String tenantLat;
  String tenantLong;
  String postalCode;
  String owner;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String city;
  String cityId;
  String province;
  String provinceId;
  String subDistrict;
  String subDistrictId;

  factory TenantTransactionSingle.fromJson(Map<String, dynamic> json) =>
      TenantTransactionSingle(
        isActive: json["is_active"] == null ? null : json["is_active"],
        tenantType: json["tenant_type"] == null ? null : json["tenant_type"],
        id: json["_id"] == null ? null : json["_id"],
        tenantName: json["tenant_name"] == null ? null : json["tenant_name"],
        tenantAddress:
            json["tenant_address"] == null ? null : json["tenant_address"],
        tenantPhone: json["tenant_phone"] == null ? null : json["tenant_phone"],
        tenantLogo: json["tenant_logo"] == null ? null : json["tenant_logo"],
        tenantLat: json["tenant_lat"] == null ? null : json["tenant_lat"],
        tenantLong: json["tenant_long"] == null ? null : json["tenant_long"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        owner: json["owner"] == null ? null : json["owner"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        city: json["city"] == null ? null : json["city"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        province: json["province"] == null ? null : json["province"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "tenant_type": tenantType == null ? null : tenantType,
        "_id": id == null ? null : id,
        "tenant_name": tenantName == null ? null : tenantName,
        "tenant_address": tenantAddress == null ? null : tenantAddress,
        "tenant_phone": tenantPhone == null ? null : tenantPhone,
        "tenant_logo": tenantLogo == null ? null : tenantLogo,
        "tenant_lat": tenantLat == null ? null : tenantLat,
        "tenant_long": tenantLong == null ? null : tenantLong,
        "postal_code": postalCode == null ? null : postalCode,
        "owner": owner == null ? null : owner,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "city": city == null ? null : city,
        "city_id": cityId == null ? null : cityId,
        "province": province == null ? null : province,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district": subDistrict == null ? null : subDistrict,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

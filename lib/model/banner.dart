// To parse this JSON data, do
//
//     final bannerModel = bannerModelFromJson(jsonString);

import 'dart:convert';

BannerModel bannerModelFromJson(String str) =>
    BannerModel.fromJson(json.decode(str));

String bannerModelToJson(BannerModel data) => json.encode(data.toJson());

class BannerModel {
  BannerModel({
    this.data,
  });

  List<DataBanner> data;

  factory BannerModel.fromJson(Map<String, dynamic> json) => BannerModel(
        data: json["data"] == null
            ? null
            : List<DataBanner>.from(
                json["data"].map((x) => DataBanner.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataBanner {
  DataBanner({
    this.mediaType,
    this.isInternalLaunch,
    this.id,
    this.title,
    this.desc,
    this.mediaUrl,
    this.launchUrl,
    this.v,
  });

  String mediaType;
  bool isInternalLaunch;
  String id;
  String title;
  String desc;
  String mediaUrl;
  String launchUrl;
  int v;

  factory DataBanner.fromJson(Map<String, dynamic> json) => DataBanner(
        mediaType: json["media_type"] == null ? null : json["media_type"],
        isInternalLaunch: json["is_internal_launch"] == null
            ? null
            : json["is_internal_launch"],
        id: json["_id"] == null ? null : json["_id"],
        title: json["title"] == null ? null : json["title"],
        desc: json["desc"] == null ? null : json["desc"],
        mediaUrl: json["media_url"] == null ? null : json["media_url"],
        launchUrl: json["launch_url"] == null ? null : json["launch_url"],
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "media_type": mediaType == null ? null : mediaType,
        "is_internal_launch":
            isInternalLaunch == null ? null : isInternalLaunch,
        "_id": id == null ? null : id,
        "title": title == null ? null : title,
        "desc": desc == null ? null : desc,
        "media_url": mediaUrl == null ? null : mediaUrl,
        "launch_url": launchUrl == null ? null : launchUrl,
        "__v": v == null ? null : v,
      };
}

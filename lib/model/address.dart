// To parse this JSON data, do
//
//     final addressModel = addressModelFromJson(jsonString);

import 'dart:convert';

AddressModel addressModelFromJson(String str) =>
    AddressModel.fromJson(json.decode(str));

String addressModelToJson(AddressModel data) => json.encode(data.toJson());

class AddressModel {
  AddressModel({
    this.data,
  });

  List<DataAddressModel> data;

  factory AddressModel.fromJson(Map<String, dynamic> json) => AddressModel(
        data: json["data"] == null
            ? null
            : List<DataAddressModel>.from(
                json["data"].map((x) => DataAddressModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataAddressModel {
  DataAddressModel({
    this.id,
    this.userId,
    this.name,
    this.province,
    this.city,
    this.subDistrict,
    this.postalCode,
    this.address,
    this.latitude,
    this.longitude,
    this.defaultLocation,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.phoneNumber,
    this.provinceId,
    this.cityId,
    this.subDistrictId,
  });

  String id;
  String userId;
  String name;
  String province;
  String city;
  String subDistrict;
  int postalCode;
  String address;
  double latitude;
  double longitude;
  bool defaultLocation;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String phoneNumber;
  String provinceId;
  String cityId;
  String subDistrictId;

  factory DataAddressModel.fromJson(Map<String, dynamic> json) =>
      DataAddressModel(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        name: json["name"] == null ? null : json["name"],
        province: json["province"] == null ? null : json["province"],
        city: json["city"] == null ? null : json["city"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        address: json["address"] == null ? null : json["address"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        defaultLocation:
            json["default_location"] == null ? null : json["default_location"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "name": name == null ? null : name,
        "province": province == null ? null : province,
        "city": city == null ? null : city,
        "sub_district": subDistrict == null ? null : subDistrict,
        "postal_code": postalCode == null ? null : postalCode,
        "address": address == null ? null : address,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "default_location": defaultLocation == null ? null : defaultLocation,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "province_id": provinceId == null ? null : provinceId,
        "city_id": cityId == null ? null : cityId,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

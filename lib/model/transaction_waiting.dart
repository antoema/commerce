// To parse this JSON data, do
//
//     final transactionWaitingModel = transactionWaitingModelFromJson(jsonString);

import 'dart:convert';

TransactionWaitingModel transactionWaitingModelFromJson(String str) =>
    TransactionWaitingModel.fromJson(json.decode(str));

String transactionWaitingModelToJson(TransactionWaitingModel data) =>
    json.encode(data.toJson());

class TransactionWaitingModel {
  TransactionWaitingModel({
    this.data,
  });

  DataTransactionWaiting data;

  factory TransactionWaitingModel.fromJson(Map<String, dynamic> json) =>
      TransactionWaitingModel(
        data: json["data"] == null
            ? null
            : DataTransactionWaiting.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
      };
}

class DataTransactionWaiting {
  DataTransactionWaiting({
    this.payment,
    this.totalPrice,
    this.expeditionCost,
    this.totalAmount,
    this.transaction,
  });

  PaymentTransactionWaiting payment;
  int totalPrice;
  int expeditionCost;
  int totalAmount;
  List<TransactionTransactionWaiting> transaction;

  factory DataTransactionWaiting.fromJson(Map<String, dynamic> json) =>
      DataTransactionWaiting(
        payment: json["payment"] == null
            ? null
            : PaymentTransactionWaiting.fromJson(json["payment"]),
        totalPrice: json["total_price"] == null ? null : json["total_price"],
        expeditionCost:
            json["expedition_cost"] == null ? null : json["expedition_cost"],
        totalAmount: json["total_amount"] == null ? null : json["total_amount"],
        transaction: json["transaction"] == null
            ? null
            : List<TransactionTransactionWaiting>.from(json["transaction"]
                .map((x) => TransactionTransactionWaiting.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "payment": payment == null ? null : payment.toJson(),
        "total_price": totalPrice == null ? null : totalPrice,
        "expedition_cost": expeditionCost == null ? null : expeditionCost,
        "total_amount": totalAmount == null ? null : totalAmount,
        "transaction": transaction == null
            ? null
            : List<dynamic>.from(transaction.map((x) => x.toJson())),
      };
}

class PaymentTransactionWaiting {
  PaymentTransactionWaiting({
    this.id,
    this.transactionId,
    this.paymentChannel,
    this.paymentId,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.paymentCode,
  });

  String id;
  String transactionId;
  String paymentChannel;
  String paymentId;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String paymentCode;

  factory PaymentTransactionWaiting.fromJson(Map<String, dynamic> json) =>
      PaymentTransactionWaiting(
        id: json["_id"] == null ? null : json["_id"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
        paymentChannel:
            json["payment_channel"] == null ? null : json["payment_channel"],
        paymentId: json["payment_id"] == null ? null : json["payment_id"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "transaction_id": transactionId == null ? null : transactionId,
        "payment_channel": paymentChannel == null ? null : paymentChannel,
        "payment_id": paymentId == null ? null : paymentId,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "payment_code": paymentCode == null ? null : paymentCode,
      };
}

class TransactionTransactionWaiting {
  TransactionTransactionWaiting({
    this.id,
    this.userId,
    this.tenantId,
    this.totalPrice,
    this.totalAmount,
    this.status,
    this.invoiceNumber,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.expedition,
    this.items,
    this.tenant,
  });

  String id;
  String userId;
  String tenantId;
  int totalPrice;
  int totalAmount;
  String status;
  String invoiceNumber;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  ExpeditionTransactionWaiting expedition;
  List<ItemTransactionWaiting> items;
  TenantTransactionWaiting tenant;

  factory TransactionTransactionWaiting.fromJson(Map<String, dynamic> json) =>
      TransactionTransactionWaiting(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        tenantId: json["tenant_id"] == null ? null : json["tenant_id"],
        totalPrice: json["total_price"] == null ? null : json["total_price"],
        totalAmount: json["total_amount"] == null ? null : json["total_amount"],
        status: json["status"] == null ? null : json["status"],
        invoiceNumber:
            json["invoice_number"] == null ? null : json["invoice_number"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        expedition: json["expedition"] == null
            ? null
            : ExpeditionTransactionWaiting.fromJson(json["expedition"]),
        items: json["items"] == null
            ? null
            : List<ItemTransactionWaiting>.from(
                json["items"].map((x) => ItemTransactionWaiting.fromJson(x))),
        tenant: json["tenant"] == null
            ? null
            : TenantTransactionWaiting.fromJson(json["tenant"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "tenant_id": tenantId == null ? null : tenantId,
        "total_price": totalPrice == null ? null : totalPrice,
        "total_amount": totalAmount == null ? null : totalAmount,
        "status": status == null ? null : status,
        "invoice_number": invoiceNumber == null ? null : invoiceNumber,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "expedition": expedition == null ? null : expedition.toJson(),
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
        "tenant": tenant == null ? null : tenant.toJson(),
      };
}

class ExpeditionTransactionWaiting {
  ExpeditionTransactionWaiting({
    this.id,
    this.expeditionCode,
    this.expeditionName,
    this.expeditionService,
    this.expeditionCost,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.transactionId,
  });

  String id;
  String expeditionCode;
  String expeditionName;
  String expeditionService;
  int expeditionCost;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String transactionId;

  factory ExpeditionTransactionWaiting.fromJson(Map<String, dynamic> json) =>
      ExpeditionTransactionWaiting(
        id: json["_id"] == null ? null : json["_id"],
        expeditionCode:
            json["expedition_code"] == null ? null : json["expedition_code"],
        expeditionName:
            json["expedition_name"] == null ? null : json["expedition_name"],
        expeditionService: json["expedition_service"] == null
            ? null
            : json["expedition_service"],
        expeditionCost:
            json["expedition_cost"] == null ? null : json["expedition_cost"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "expedition_code": expeditionCode == null ? null : expeditionCode,
        "expedition_name": expeditionName == null ? null : expeditionName,
        "expedition_service":
            expeditionService == null ? null : expeditionService,
        "expedition_cost": expeditionCost == null ? null : expeditionCost,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "transaction_id": transactionId == null ? null : transactionId,
      };
}

class ItemTransactionWaiting {
  ItemTransactionWaiting({
    this.id,
    this.productId,
    this.productName,
    this.productImage,
    this.price,
    this.qty,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.transactionId,
  });

  String id;
  String productId;
  String productName;
  String productImage;
  int price;
  int qty;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String transactionId;

  factory ItemTransactionWaiting.fromJson(Map<String, dynamic> json) =>
      ItemTransactionWaiting(
        id: json["_id"] == null ? null : json["_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        productName: json["product_name"] == null ? null : json["product_name"],
        productImage:
            json["product_image"] == null ? null : json["product_image"],
        price: json["price"] == null ? null : json["price"],
        qty: json["qty"] == null ? null : json["qty"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        transactionId:
            json["transaction_id"] == null ? null : json["transaction_id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "product_id": productId == null ? null : productId,
        "product_name": productName == null ? null : productName,
        "product_image": productImage == null ? null : productImage,
        "price": price == null ? null : price,
        "qty": qty == null ? null : qty,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "transaction_id": transactionId == null ? null : transactionId,
      };
}

class TenantTransactionWaiting {
  TenantTransactionWaiting({
    this.isActive,
    this.tenantType,
    this.id,
    this.tenantName,
    this.tenantAddress,
    this.tenantPhone,
    this.tenantLogo,
    this.tenantLat,
    this.tenantLong,
    this.postalCode,
    this.owner,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.city,
    this.cityId,
    this.province,
    this.provinceId,
    this.subDistrict,
    this.subDistrictId,
  });

  bool isActive;
  String tenantType;
  String id;
  String tenantName;
  String tenantAddress;
  String tenantPhone;
  String tenantLogo;
  String tenantLat;
  String tenantLong;
  String postalCode;
  String owner;
  DateTime createdAt;
  DateTime updatedAt;
  int v;
  String city;
  String cityId;
  String province;
  String provinceId;
  String subDistrict;
  String subDistrictId;

  factory TenantTransactionWaiting.fromJson(Map<String, dynamic> json) =>
      TenantTransactionWaiting(
        isActive: json["is_active"] == null ? null : json["is_active"],
        tenantType: json["tenant_type"] == null ? null : json["tenant_type"],
        id: json["_id"] == null ? null : json["_id"],
        tenantName: json["tenant_name"] == null ? null : json["tenant_name"],
        tenantAddress:
            json["tenant_address"] == null ? null : json["tenant_address"],
        tenantPhone: json["tenant_phone"] == null ? null : json["tenant_phone"],
        tenantLogo: json["tenant_logo"] == null ? null : json["tenant_logo"],
        tenantLat: json["tenant_lat"] == null ? null : json["tenant_lat"],
        tenantLong: json["tenant_long"] == null ? null : json["tenant_long"],
        postalCode: json["postal_code"] == null ? null : json["postal_code"],
        owner: json["owner"] == null ? null : json["owner"],
        createdAt: json["createdAt"] == null
            ? null
            : DateTime.parse(json["createdAt"]),
        updatedAt: json["updatedAt"] == null
            ? null
            : DateTime.parse(json["updatedAt"]),
        v: json["__v"] == null ? null : json["__v"],
        city: json["city"] == null ? null : json["city"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        province: json["province"] == null ? null : json["province"],
        provinceId: json["province_id"] == null ? null : json["province_id"],
        subDistrict: json["sub_district"] == null ? null : json["sub_district"],
        subDistrictId:
            json["sub_district_id"] == null ? null : json["sub_district_id"],
      );

  Map<String, dynamic> toJson() => {
        "is_active": isActive == null ? null : isActive,
        "tenant_type": tenantType == null ? null : tenantType,
        "_id": id == null ? null : id,
        "tenant_name": tenantName == null ? null : tenantName,
        "tenant_address": tenantAddress == null ? null : tenantAddress,
        "tenant_phone": tenantPhone == null ? null : tenantPhone,
        "tenant_logo": tenantLogo == null ? null : tenantLogo,
        "tenant_lat": tenantLat == null ? null : tenantLat,
        "tenant_long": tenantLong == null ? null : tenantLong,
        "postal_code": postalCode == null ? null : postalCode,
        "owner": owner == null ? null : owner,
        "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
        "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
        "__v": v == null ? null : v,
        "city": city == null ? null : city,
        "city_id": cityId == null ? null : cityId,
        "province": province == null ? null : province,
        "province_id": provinceId == null ? null : provinceId,
        "sub_district": subDistrict == null ? null : subDistrict,
        "sub_district_id": subDistrictId == null ? null : subDistrictId,
      };
}

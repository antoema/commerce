class SearchHistory {
  int id;
  String name;

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'name': name,
    };
    if (id != null) {
      map['_id'] = id;
    }
    return map;
  }

  SearchHistory();

  SearchHistory.fromMap(Map<String, dynamic> map) {
    id = map['_id'];
    name = map['name'];
  }
}

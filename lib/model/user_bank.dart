// To parse this JSON data, do
//
//     final userBank = userBankFromJson(jsonString);

import 'dart:convert';

UserBank userBankFromJson(String str) => UserBank.fromJson(json.decode(str));

String userBankToJson(UserBank data) => json.encode(data.toJson());

class UserBank {
  UserBank({
    this.data,
  });

  List<DataUserBank> data;

  factory UserBank.fromJson(Map<String, dynamic> json) => UserBank(
        data: json["data"] == null
            ? null
            : List<DataUserBank>.from(
                json["data"].map((x) => DataUserBank.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataUserBank {
  DataUserBank({
    this.id,
    this.userId,
    this.bankName,
    this.bankCode,
    this.bankNumber,
    this.accName,
    this.v,
  });

  String id;
  String userId;
  String bankName;
  String bankCode;
  String bankNumber;
  String accName;
  int v;

  factory DataUserBank.fromJson(Map<String, dynamic> json) => DataUserBank(
        id: json["_id"] == null ? null : json["_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        bankName: json["bank_name"] == null ? null : json["bank_name"],
        bankCode: json["bank_code"] == null ? null : json["bank_code"],
        bankNumber: json["bank_number"] == null ? null : json["bank_number"],
        accName: json["acc_name"] == null ? null : json["acc_name"],
        v: json["__v"] == null ? null : json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "bank_name": bankName == null ? null : bankName,
        "bank_code": bankCode == null ? null : bankCode,
        "bank_number": bankNumber == null ? null : bankNumber,
        "acc_name": accName == null ? null : accName,
        "__v": v == null ? null : v,
      };
}

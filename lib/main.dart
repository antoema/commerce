import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/view/splashscreen.dart';
import 'package:pekik_app/utils/routes.dart';
import 'package:provider/provider.dart';

import './providers.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: <String, WidgetBuilder>{
      '/MyApp': (BuildContext context) => MyApp(),
    },
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: appTitle,
        theme: ThemeData.from(
          colorScheme: const ColorScheme.light(),
        ).copyWith(
          textTheme: ThemeData.light().textTheme.apply(
                fontFamily: 'Poppins',
              ),
          accentColor: Colors.blueAccent,
          primaryColor: Colors.blueAccent,
          primaryTextTheme: ThemeData.light().textTheme.apply(
                fontFamily: 'Poppins',
              ),
          accentTextTheme: ThemeData.light().textTheme.apply(
                fontFamily: 'Poppins',
              ),
          pageTransitionsTheme: const PageTransitionsTheme(
            builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.android: ZoomPageTransitionsBuilder(),
              TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
            },
          ),
        ),
        initialRoute: '/',
        routes: appRoutes,
        // home: MyHomePage(title: 'Home Page'),
      ),
    );
  }
}

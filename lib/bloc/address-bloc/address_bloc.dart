import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/address.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/address_vm.dart';

part 'address_event.dart';
part 'address_state.dart';

class AddressBloc extends Bloc<AddressEvent, AddressState> {
  AddressBloc() : super(AddressInitial());

  @override
  Stream<AddressState> mapEventToState(
    AddressEvent event,
  ) async* {
    if (event is GetAddress) {
      try {
        AddressViewModel addressViewModel = AddressViewModel(http: event.http);
        yield AddressLoading();
        final dataAddress = await addressViewModel.fetchAddressAll();
        yield AddressLoaded(dataAddress);
        if (dataAddress == null) {
          yield AddressError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield AddressError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

part of 'address_bloc.dart';

@immutable
abstract class AddressEvent {
  const AddressEvent();
}

class GetAddress extends AddressEvent {
  final CHttp http;
  GetAddress({this.http});
}

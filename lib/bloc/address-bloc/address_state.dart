part of 'address_bloc.dart';

@immutable
abstract class AddressState {
  const AddressState();
}

class AddressInitial extends AddressState {
  const AddressInitial();

  List<Object> get props => [];
}

class AddressLoading extends AddressState {
  const AddressLoading();

  List<Object> get props => null;
}

class AddressLoaded extends AddressState {
  final AddressModel addressModel;
  AddressLoaded(this.addressModel);

  List<Object> get props => [addressModel];
}

class AddressError extends AddressState {
  final String message;
  AddressError(this.message);

  List<Object> get props => [message];
}

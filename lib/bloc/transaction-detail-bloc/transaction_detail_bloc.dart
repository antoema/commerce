import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/transaction_single.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'transaction_detail_event.dart';
part 'transaction_detail_state.dart';

class TransactionDetailBloc
    extends Bloc<TransactionDetailEvent, TransactionDetailState> {
  TransactionDetailBloc() : super(TransactionDetailInitial());

  @override
  Stream<TransactionDetailState> mapEventToState(
    TransactionDetailEvent event,
  ) async* {
    if (event is GetTransactionDetail) {
      try {
        TransactionViewModel transactionViewModel =
            TransactionViewModel(http: event.http);
        yield TransactionDetailLoading();
        final dataTransactionDetail =
            await transactionViewModel.fetchTransactionSingle(event.idTrx);
        yield TransactionDetailLoaded(dataTransactionDetail);
        if (dataTransactionDetail == null) {
          yield TransactionDetailError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield TransactionDetailError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

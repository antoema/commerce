part of 'transaction_detail_bloc.dart';

@immutable
abstract class TransactionDetailEvent {
  const TransactionDetailEvent();
}

class GetTransactionDetail extends TransactionDetailEvent {
  final CHttp http;
  final String idTrx;
  GetTransactionDetail({this.http, this.idTrx});
}

part of 'transaction_detail_bloc.dart';

@immutable
abstract class TransactionDetailState {
  const TransactionDetailState();
}

class TransactionDetailInitial extends TransactionDetailState {
  const TransactionDetailInitial();

  List<Object> get props => [];
}

class TransactionDetailLoading extends TransactionDetailState {
  const TransactionDetailLoading();

  List<Object> get props => null;
}

class TransactionDetailLoaded extends TransactionDetailState {
  final TransactionSingleModel transactionSingleModel;
  TransactionDetailLoaded(this.transactionSingleModel);

  List<Object> get props => [transactionSingleModel];
}

class TransactionDetailError extends TransactionDetailState {
  final String message;

  TransactionDetailError(this.message);

  List<Object> get props => [message];
}

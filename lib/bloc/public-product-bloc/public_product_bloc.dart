import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';

part 'public_product_event.dart';
part 'public_product_state.dart';

class PublicProductBloc extends Bloc<PublicProductEvent, PublicProductState> {
  PublicProductBloc() : super(PublicProductInitial());

  @override
  Stream<PublicProductState> mapEventToState(
    PublicProductEvent event,
  ) async* {
    if (event is GetPublicProduct) {
      try {
        PublicProductViewModel productViewModel = PublicProductViewModel();
        yield PublicProductLoading();
        final dataProduct = await productViewModel.fetchProductAll();
        yield PublicProductLoaded(dataProduct);
        if (dataProduct == null) {
          yield PublicProductError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        print(err);
        yield PublicProductError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

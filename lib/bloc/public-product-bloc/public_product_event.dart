part of 'public_product_bloc.dart';

@immutable
abstract class PublicProductEvent {
  const PublicProductEvent();
}

class GetPublicProduct extends PublicProductEvent {
  GetPublicProduct();
}

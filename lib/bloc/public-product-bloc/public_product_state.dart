part of 'public_product_bloc.dart';

@immutable
abstract class PublicProductState {
  const PublicProductState();
}

class PublicProductInitial extends PublicProductState {
  const PublicProductInitial();

  List<Object> get props => [];
}

class PublicProductLoading extends PublicProductState {
  const PublicProductLoading();
  List<Object> get props => null;
}

class PublicProductLoaded extends PublicProductState {
  final ProductModel productModel;

  PublicProductLoaded(this.productModel);
  List<Object> get props => [productModel];
}

class PublicProductError extends PublicProductState {
  final String message;

  PublicProductError(this.message);

  List<Object> get props => [message];
}

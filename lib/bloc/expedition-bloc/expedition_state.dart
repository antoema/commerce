part of 'expedition_bloc.dart';

@immutable
abstract class ExpeditionState {
  const ExpeditionState();
}

class ExpeditionInitial extends ExpeditionState {
  const ExpeditionInitial();

  List<Object> get props => [];
}

class ExpeditionLoading extends ExpeditionState {
  const ExpeditionLoading();

  List<Object> get props => null;
}

class ExpeditionLoaded extends ExpeditionState {
  final ExpeditionModel expeditionModel;
  ExpeditionLoaded(this.expeditionModel);

  List<Object> get props => [expeditionModel];
}

class ExpeditionError extends ExpeditionState {
  final String message;

  ExpeditionError(this.message);

  List<Object> get props => [message];
}

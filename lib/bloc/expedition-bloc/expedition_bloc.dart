import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/expedition.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'expedition_event.dart';
part 'expedition_state.dart';

class ExpeditionBloc extends Bloc<ExpeditionEvent, ExpeditionState> {
  ExpeditionBloc() : super(ExpeditionInitial());

  @override
  Stream<ExpeditionState> mapEventToState(
    ExpeditionEvent event,
  ) async* {
    if (event is GetExpedition) {
      try {
        TransactionViewModel transactionViewModel =
            TransactionViewModel(http: event.http);
        yield ExpeditionLoading();
        final dataExpedition =
            await transactionViewModel.fetchExpedition(event.cartId);
        yield ExpeditionLoaded(dataExpedition);
        if (dataExpedition == null) {
          yield ExpeditionError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield ExpeditionError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

part of 'expedition_bloc.dart';

@immutable
abstract class ExpeditionEvent {
  const ExpeditionEvent();
}

class GetExpedition extends ExpeditionEvent {
  final CHttp http;
  final String cartId;
  GetExpedition({this.http, this.cartId});
}

part of 'public_detail_product_bloc.dart';

@immutable
abstract class PublicDetailProductEvent {
  const PublicDetailProductEvent();
}

class GetPublicDetailProduct extends PublicDetailProductEvent {
  final String idProduct;
  GetPublicDetailProduct({this.idProduct});
}

part of 'public_detail_product_bloc.dart';

@immutable
abstract class PublicDetailProductState {
  const PublicDetailProductState();
}

class PublicDetailProductInitial extends PublicDetailProductState {
  const PublicDetailProductInitial();
  List<Object> get props => [];
}

class PublicDetailProductLoading extends PublicDetailProductState {
  const PublicDetailProductLoading();
  List<Object> get props => null;
}

class PublicDetailProductLoaded extends PublicDetailProductState {
  final ProductSingleModel productModel;

  PublicDetailProductLoaded(this.productModel);
  List<Object> get props => [productModel];
}

class PublicDetailProductError extends PublicDetailProductState {
  final String message;

  PublicDetailProductError(this.message);

  List<Object> get props => [message];
}

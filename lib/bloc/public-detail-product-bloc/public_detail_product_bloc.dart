import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/product_single.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';

part 'public_detail_product_event.dart';
part 'public_detail_product_state.dart';

class PublicDetailProductBloc
    extends Bloc<PublicDetailProductEvent, PublicDetailProductState> {
  PublicDetailProductBloc() : super(PublicDetailProductInitial());

  @override
  Stream<PublicDetailProductState> mapEventToState(
    PublicDetailProductEvent event,
  ) async* {
    if (event is GetPublicDetailProduct) {
      try {
        PublicProductViewModel productViewModel = PublicProductViewModel();
        yield PublicDetailProductLoading();
        final dataProduct =
            await productViewModel.fetchSingleProduct(event.idProduct);
        yield PublicDetailProductLoaded(dataProduct);
        if (dataProduct == null) {
          yield PublicDetailProductError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield PublicDetailProductError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

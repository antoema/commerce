part of 'user_bank_bloc.dart';

@immutable
abstract class UserBankState {
  const UserBankState();
}

class UserBankInitial extends UserBankState {
  const UserBankInitial();
  List<Object> get props => [];
}

class UserBankLoading extends UserBankState {
  const UserBankLoading();

  List<Object> get props => null;
}

class UserBankLoaded extends UserBankState {
  final UserBank userBank;
  UserBankLoaded(this.userBank);

  List<Object> get props => [userBank];
}

class UserBankError extends UserBankState {
  final String message;
  UserBankError(this.message);

  List<Object> get props => [message];
}

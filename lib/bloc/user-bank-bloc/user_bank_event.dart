part of 'user_bank_bloc.dart';

@immutable
abstract class UserBankEvent {
  const UserBankEvent();
}

class GetUserBank extends UserBankEvent {
  final CHttp http;
  GetUserBank({this.http});
}

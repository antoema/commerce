import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/user_bank.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/wallet_vm.dart';

part 'user_bank_event.dart';
part 'user_bank_state.dart';

class UserBankBloc extends Bloc<UserBankEvent, UserBankState> {
  UserBankBloc() : super(UserBankInitial());

  @override
  Stream<UserBankState> mapEventToState(
    UserBankEvent event,
  ) async* {
    if (event is GetUserBank) {
      try {
        WalletViewModel walletViewModel = WalletViewModel(http: event.http);
        yield UserBankLoading();
        final dataUserBank = await walletViewModel.fetchUserBank();
        yield UserBankLoaded(dataUserBank);
        if (dataUserBank == null) {
          yield UserBankError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield UserBankError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

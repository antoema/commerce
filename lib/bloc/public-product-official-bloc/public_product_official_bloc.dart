import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';

part 'public_product_official_event.dart';
part 'public_product_official_state.dart';

class PublicProductOfficialBloc
    extends Bloc<PublicProductOfficialEvent, PublicProductOfficialState> {
  PublicProductOfficialBloc() : super(PublicProductOfficialInitial());

  @override
  Stream<PublicProductOfficialState> mapEventToState(
    PublicProductOfficialEvent event,
  ) async* {
    if (event is GetPublicProductOfficial) {
      try {
        PublicProductViewModel productViewModel = PublicProductViewModel();
        yield PublicProductOfficialLoading();
        final dataProduct = await productViewModel.fetchProductFilter(
            event.tenantType, event.tenantId, event.idCategory, event.qName);
        yield PublicProductOfficialLoaded(dataProduct);
        if (dataProduct == null) {
          yield PublicProductOfficialError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield PublicProductOfficialError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

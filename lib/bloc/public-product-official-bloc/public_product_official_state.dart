part of 'public_product_official_bloc.dart';

@immutable
abstract class PublicProductOfficialState {
  const PublicProductOfficialState();
}

class PublicProductOfficialInitial extends PublicProductOfficialState {
  const PublicProductOfficialInitial();
  List<Object> get props => [];
}

class PublicProductOfficialLoading extends PublicProductOfficialState {
  const PublicProductOfficialLoading();
  List<Object> get props => null;
}

class PublicProductOfficialLoaded extends PublicProductOfficialState {
  final ProductModel productModel;

  PublicProductOfficialLoaded(this.productModel);
  List<Object> get props => [productModel];
}

class PublicProductOfficialError extends PublicProductOfficialState {
  final String message;

  PublicProductOfficialError(this.message);

  List<Object> get props => [message];
}

part of 'public_product_official_bloc.dart';

@immutable
abstract class PublicProductOfficialEvent {
  const PublicProductOfficialEvent();
}

class GetPublicProductOfficial extends PublicProductOfficialEvent {
  final String tenantType;
  final String tenantId;
  final String idCategory;
  final String qName;
  GetPublicProductOfficial(
      {this.tenantType, this.tenantId, this.idCategory, this.qName});
}

part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {
  const CartEvent();
}

class GetCart extends CartEvent {
  final CHttp http;
  GetCart({this.http});
}

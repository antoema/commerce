part of 'cart_bloc.dart';

@immutable
abstract class CartState {
  const CartState();
}

class CartInitial extends CartState {
  const CartInitial();
  List<Object> get props => [];
}

class CartLoading extends CartState {
  const CartLoading();

  List<Object> get props => null;
}

class CartLoaded extends CartState {
  final CartModel cartModel;
  CartLoaded(this.cartModel);

  List<Object> get props => [cartModel];
}

class CartError extends CartState {
  final String message;

  CartError(this.message);

  List<Object> get props => [message];
}

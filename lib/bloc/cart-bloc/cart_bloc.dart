import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/cart.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartInitial());

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is GetCart) {
      try {
        TransactionViewModel transactionViewModel =
            TransactionViewModel(http: event.http);
        yield CartLoading();
        final dataCart = await transactionViewModel.fetchCartAll();
        yield CartLoaded(dataCart);
        if (dataCart == null) {
          yield CartError('Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield CartError('Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

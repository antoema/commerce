part of 'merchant_bloc.dart';

@immutable
abstract class MerchantEvent {
  const MerchantEvent();
}

class GetMerchant extends MerchantEvent {
  final CHttp http;
  GetMerchant({this.http});
}

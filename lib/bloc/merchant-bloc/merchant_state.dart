part of 'merchant_bloc.dart';

@immutable
abstract class MerchantState {
  const MerchantState();
}

class MerchantInitial extends MerchantState {
  const MerchantInitial();

  List<Object> get props => [];
}

class MerchantLoading extends MerchantState {
  const MerchantLoading();
  List<Object> get props => null;
}

class MerchantLoaded extends MerchantState {
  final MerchantModel merchantModel;

  MerchantLoaded(this.merchantModel);
  List<Object> get props => [merchantModel];
}

class MerchantError extends MerchantState {
  final String message;

  MerchantError(this.message);

  List<Object> get props => [message];
}

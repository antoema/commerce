import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/merchant.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/merchant_vm.dart';

part 'merchant_event.dart';
part 'merchant_state.dart';

class MerchantBloc extends Bloc<MerchantEvent, MerchantState> {
  MerchantBloc() : super(MerchantInitial());

  @override
  Stream<MerchantState> mapEventToState(
    MerchantEvent event,
  ) async* {
    if (event is GetMerchant) {
      try {
        MerchantViewModel merchantViewModel =
            MerchantViewModel(http: event.http);
        yield MerchantLoading();
        final dataMerchant = await merchantViewModel.fetchMerchant();
        yield MerchantLoaded(dataMerchant);
        if (dataMerchant == null) {
          yield MerchantError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield MerchantError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

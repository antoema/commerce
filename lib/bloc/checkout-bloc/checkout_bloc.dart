import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/checkout.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'checkout_event.dart';
part 'checkout_state.dart';

class CheckoutBloc extends Bloc<CheckoutEvent, CheckoutState> {
  CheckoutBloc() : super(CheckoutInitial());

  @override
  Stream<CheckoutState> mapEventToState(
    CheckoutEvent event,
  ) async* {
    if (event is GetCheckout) {
      try {
        TransactionViewModel transactionViewModel =
            TransactionViewModel(http: event.http);
        yield CheckoutLoading();
        final dataCheckout = await transactionViewModel.fetchCheckout();
        yield CheckoutLoaded(dataCheckout);
        if (dataCheckout == null) {
          yield CheckoutError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield CheckoutError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

part of 'checkout_bloc.dart';

@immutable
abstract class CheckoutEvent {
  const CheckoutEvent();
}

class GetCheckout extends CheckoutEvent {
  final CHttp http;
  GetCheckout({this.http});
}

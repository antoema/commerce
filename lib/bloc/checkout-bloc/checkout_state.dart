part of 'checkout_bloc.dart';

@immutable
abstract class CheckoutState {
  const CheckoutState();
}

class CheckoutInitial extends CheckoutState {
  const CheckoutInitial();
  List<Object> get props => [];
}

class CheckoutLoading extends CheckoutState {
  const CheckoutLoading();

  List<Object> get props => null;
}

class CheckoutLoaded extends CheckoutState {
  final CheckoutModel checkoutModel;
  CheckoutLoaded(this.checkoutModel);

  List<Object> get props => [checkoutModel];
}

class CheckoutError extends CheckoutState {
  final String message;

  CheckoutError(this.message);

  List<Object> get props => [message];
}

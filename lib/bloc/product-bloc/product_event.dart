part of 'product_bloc.dart';

@immutable
abstract class ProductEvent {
  const ProductEvent();
}

class GetProduct extends ProductEvent {
  final CHttp http;
  GetProduct({this.http});
}

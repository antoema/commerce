import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/merchant_vm.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc() : super(ProductInitial());

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    if (event is GetProduct) {
      try {
        MerchantViewModel merchantViewModel =
            MerchantViewModel(http: event.http);
        yield ProductLoading();
        final dataProduct = await merchantViewModel.fetchProductAll();
        yield ProductLoaded(dataProduct);
        if (dataProduct == null) {
          yield ProductError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield ProductError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

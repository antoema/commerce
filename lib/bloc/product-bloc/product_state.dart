part of 'product_bloc.dart';

@immutable
abstract class ProductState {
  const ProductState();
}

class ProductInitial extends ProductState {
  const ProductInitial();

  List<Object> get props => [];
}

class ProductLoading extends ProductState {
  const ProductLoading();
  List<Object> get props => null;
}

class ProductLoaded extends ProductState {
  final ProductModel productModel;

  ProductLoaded(this.productModel);
  List<Object> get props => [productModel];
}

class ProductError extends ProductState {
  final String message;

  ProductError(this.message);

  List<Object> get props => [message];
}

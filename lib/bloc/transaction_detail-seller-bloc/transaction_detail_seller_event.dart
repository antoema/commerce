part of 'transaction_detail_seller_bloc.dart';

@immutable
abstract class TransactionDetailSellerEvent {
  const TransactionDetailSellerEvent();
}

class GetTransactionDetailSeller extends TransactionDetailSellerEvent {
  final CHttp http;
  final String idTrx;
  GetTransactionDetailSeller({this.http, this.idTrx});
}

part of 'transaction_detail_seller_bloc.dart';

@immutable
abstract class TransactionDetailSellerState {
  const TransactionDetailSellerState();
}

class TransactionDetailSellerInitial extends TransactionDetailSellerState {
  const TransactionDetailSellerInitial();

  List<Object> get props => [];
}

class TransactionDetailSellerLoading extends TransactionDetailSellerState {
  const TransactionDetailSellerLoading();

  List<Object> get props => null;
}

class TransactionDetailSellerLoaded extends TransactionDetailSellerState {
  final TransactionSingleModel transactionSingleModel;
  TransactionDetailSellerLoaded(this.transactionSingleModel);

  List<Object> get props => [transactionSingleModel];
}

class TransactionDetailSellerError extends TransactionDetailSellerState {
  final String message;

  TransactionDetailSellerError(this.message);

  List<Object> get props => [message];
}

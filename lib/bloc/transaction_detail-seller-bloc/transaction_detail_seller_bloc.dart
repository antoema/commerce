import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/transaction_single.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'transaction_detail_seller_event.dart';
part 'transaction_detail_seller_state.dart';

class TransactionDetailSellerBloc
    extends Bloc<TransactionDetailSellerEvent, TransactionDetailSellerState> {
  TransactionDetailSellerBloc() : super(TransactionDetailSellerInitial());

  @override
  Stream<TransactionDetailSellerState> mapEventToState(
    TransactionDetailSellerEvent event,
  ) async* {
    if (event is GetTransactionDetailSeller) {
      try {
        TransactionViewModel transactionViewModel =
            TransactionViewModel(http: event.http);
        yield TransactionDetailSellerLoading();
        final dataTransactionDetail = await transactionViewModel
            .fetchSellerTransactionSingle(event.idTrx);
        yield TransactionDetailSellerLoaded(dataTransactionDetail);
        if (dataTransactionDetail == null) {
          yield TransactionDetailSellerError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield TransactionDetailSellerError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

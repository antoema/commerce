import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/bank.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'bank_event.dart';
part 'bank_state.dart';

class BankBloc extends Bloc<BankEvent, BankState> {
  BankBloc() : super(BankInitial());

  @override
  Stream<BankState> mapEventToState(
    BankEvent event,
  ) async* {
    if (event is GetBank) {
      try {
        TransactionViewModel addressViewModel = TransactionViewModel();
        yield BankLoading();
        final dataBank = await addressViewModel.fetchBank();
        yield BankLoaded(dataBank);
        if (dataBank == null) {
          yield BankError('Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield BankError('Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

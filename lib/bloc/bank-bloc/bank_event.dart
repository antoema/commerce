part of 'bank_bloc.dart';

@immutable
abstract class BankEvent {
  const BankEvent();
}

class GetBank extends BankEvent {
  GetBank();
}

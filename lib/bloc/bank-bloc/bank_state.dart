part of 'bank_bloc.dart';

@immutable
abstract class BankState {
  const BankState();
}

class BankInitial extends BankState {
  const BankInitial();

  List<Object> get props => [];
}

class BankLoading extends BankState {
  const BankLoading();

  List<Object> get props => null;
}

class BankLoaded extends BankState {
  final BankModel bankModel;
  BankLoaded(this.bankModel);

  List<Object> get props => [bankModel];
}

class BankError extends BankState {
  final String message;
  BankError(this.message);

  List<Object> get props => [message];
}

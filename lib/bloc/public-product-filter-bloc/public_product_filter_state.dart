part of 'public_product_filter_bloc.dart';

@immutable
abstract class PublicProductFilterState {
  const PublicProductFilterState();
}

class PublicProductFilterInitial extends PublicProductFilterState {}

class PublicProductFilterLoading extends PublicProductFilterState {
  const PublicProductFilterLoading();
  List<Object> get props => null;
}

class PublicProductFilterLoaded extends PublicProductFilterState {
  final ProductModel productModel;

  PublicProductFilterLoaded(this.productModel);
  List<Object> get props => [productModel];
}

class PublicProductFilterError extends PublicProductFilterState {
  final String message;

  PublicProductFilterError(this.message);

  List<Object> get props => [message];
}

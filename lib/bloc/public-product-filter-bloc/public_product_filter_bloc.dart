import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';

part 'public_product_filter_event.dart';
part 'public_product_filter_state.dart';

class PublicProductFilterBloc
    extends Bloc<PublicProductFilterEvent, PublicProductFilterState> {
  PublicProductFilterBloc() : super(PublicProductFilterInitial());

  @override
  Stream<PublicProductFilterState> mapEventToState(
    PublicProductFilterEvent event,
  ) async* {
    if (event is GetPublicProductFilter) {
      try {
        PublicProductViewModel productViewModel = PublicProductViewModel();
        yield PublicProductFilterLoading();
        final dataProduct = await productViewModel.fetchProductHome(event.path);
        yield PublicProductFilterLoaded(dataProduct);
        if (dataProduct == null) {
          yield PublicProductFilterError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield PublicProductFilterError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

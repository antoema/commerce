part of 'public_product_filter_bloc.dart';

@immutable
abstract class PublicProductFilterEvent {
  const PublicProductFilterEvent();
}

class GetPublicProductFilter extends PublicProductFilterEvent {
  final String path;
  GetPublicProductFilter({this.path});
}

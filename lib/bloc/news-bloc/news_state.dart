part of 'news_bloc.dart';

@immutable
abstract class NewsState {
  const NewsState();
}

class NewsInitial extends NewsState {
  const NewsInitial();

  List<Object> get props => [];
}

class NewsLoading extends NewsState {
  const NewsLoading();

  List<Object> get props => null;
}

class NewsLoaded extends NewsState {}

class NewsError extends NewsState {
  final String message;

  NewsError(this.message);

  List<Object> get props => [message];
}

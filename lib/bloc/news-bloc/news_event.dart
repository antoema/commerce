part of 'news_bloc.dart';

@immutable
abstract class NewsEvent {
  const NewsEvent();
}

class GetNews extends NewsEvent {
  final CHttp http;
  GetNews({this.http});
}

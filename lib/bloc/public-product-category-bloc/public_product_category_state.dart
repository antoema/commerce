part of 'public_product_category_bloc.dart';

@immutable
abstract class PublicProductCategoryState {
  const PublicProductCategoryState();
}

class PublicProductCategoryInitial extends PublicProductCategoryState {
  const PublicProductCategoryInitial();

  List<Object> get props => [];
}

class PublicProductCategoryLoading extends PublicProductCategoryState {
  const PublicProductCategoryLoading();
  List<Object> get props => null;
}

class PublicProductCategoryLoaded extends PublicProductCategoryState {
  final CategoryProductModel categoryProductModel;

  PublicProductCategoryLoaded(this.categoryProductModel);
  List<Object> get props => [categoryProductModel];
}

class PublicProductCategoryError extends PublicProductCategoryState {
  final String message;

  PublicProductCategoryError(this.message);

  List<Object> get props => [message];
}

part of 'public_product_category_bloc.dart';

@immutable
abstract class PublicProductCategoryEvent {}

class GetPublicProductCategory extends PublicProductCategoryEvent {
  GetPublicProductCategory();
}

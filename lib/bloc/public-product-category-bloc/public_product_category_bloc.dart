import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/category_product.dart';

part 'public_product_category_event.dart';
part 'public_product_category_state.dart';

class PublicProductCategoryBloc
    extends Bloc<PublicProductCategoryEvent, PublicProductCategoryState> {
  PublicProductCategoryBloc() : super(PublicProductCategoryInitial());

  @override
  Stream<PublicProductCategoryState> mapEventToState(
    PublicProductCategoryEvent event,
  ) async* {}
}

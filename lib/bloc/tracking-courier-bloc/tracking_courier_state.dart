part of 'tracking_courier_bloc.dart';

@immutable
abstract class TrackingCourierState {
  const TrackingCourierState();
}

class TrackingCourierInitial extends TrackingCourierState {
  const TrackingCourierInitial();

  List<Object> get props => [];
}

class TrackingCourierLoading extends TrackingCourierState {
  const TrackingCourierLoading();

  List<Object> get props => null;
}

class TrackingCourierLoaded extends TrackingCourierState {
  final TrackingCourierModel trackingCourierModel;
  TrackingCourierLoaded(this.trackingCourierModel);

  List<Object> get props => [trackingCourierModel];
}

class TrackingCourierError extends TrackingCourierState {
  final String message;

  TrackingCourierError(this.message);

  List<Object> get props => [message];
}

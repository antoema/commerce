part of 'tracking_courier_bloc.dart';

@immutable
abstract class TrackingCourierEvent {
  const TrackingCourierEvent();
}

class GetTracking extends TrackingCourierEvent {
  final CHttp http;
  final String idTrx;

  GetTracking({this.http, this.idTrx});
}

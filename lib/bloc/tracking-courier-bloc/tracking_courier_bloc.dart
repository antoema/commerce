import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:pekik_app/model/tracking_courier.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';

part 'tracking_courier_event.dart';
part 'tracking_courier_state.dart';

class TrackingCourierBloc
    extends Bloc<TrackingCourierEvent, TrackingCourierState> {
  TrackingCourierBloc() : super(TrackingCourierInitial());

  @override
  Stream<TrackingCourierState> mapEventToState(
    TrackingCourierEvent event,
  ) async* {
    if (event is GetTracking) {
      try {
        TransactionViewModel transactionViewModel =
            TransactionViewModel(http: event.http);
        yield TrackingCourierLoading();
        final dataTracking =
            await transactionViewModel.fetchTrackingCourier(event.idTrx);
        yield TrackingCourierLoaded(dataTracking);
        if (dataTracking == null) {
          yield TrackingCourierError(
              'Terjadi kesalahan saat berkomunikasi dengan server');
        }
      } catch (err) {
        yield TrackingCourierError(
            'Terjadi kesalahan saat berkomunikasi dengan server');
      }
    }
  }
}

import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/view/page-home/widget/home_widget.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final formKey = new GlobalKey<FormState>();
  final int beginCount = 30;
  bool isLoading = true;
  bool isHidePassword = true;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  void togglePasswordVisibility() {
    setState(() {
      isHidePassword = !isHidePassword;
    });
  }

  var token;

  Future<String> printToken() async {
    print("token should be printed");
    token = await firebaseMessaging.getToken();
    print("token saya $token");

    return token;
  }

  bool validates() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void validateAndSave() {
    if (validates()) {
      setState(() {
        isLoading = false;
      });
      String _username = usernameController.text;
      String _password = passwordController.text;
      // if (_username.startsWith('0')) {
      //   _username = '+62' + _username.replaceFirst(new RegExp(r'0'), '');
      // } else {
      //   _username = _username;
      // }
      print("Nomor");
      print(_username);
      CHttp _chttp = Provider.of<CHttp>(context, listen: false);
      BaseAuth _auth = _chttp.auth;
      Future<User> _login = _auth.login(_username, _password, token);

      print(_login);

      _login.then((User _login) {
        if (_login != null) {
          setState(() {
            isLoading = true;
          });
          Fluttertoast.showToast(
              msg: "Login Berhasil",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: ColorPalette.btnGreen,
              textColor: Colors.white);
          return Navigator.pushNamed(context, "/");
        } else {
          setState(() {
            isLoading = true;
          });
          Fluttertoast.showToast(
              msg: "Login is invalid",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white);
          print("error login, toaster nya kemana ?");
        }
      });
    }
  }

  Future<bool> _onWillPop(BuildContext context) async {
    return Navigator.push(
      context,
      PageTransition(
        type: PageTransitionType.fade,
        child: Home(),
      ),
    );
  }

  @override
  void initState() {
    printToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return _onWillPop(context);
      },
      child: Scaffold(
        body: Form(
          key: formKey,
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        _onWillPop(context);
                      },
                      child: Container(
                          height: 45,
                          width: 45,
                          decoration: BoxDecoration(
                              color: Colors.black54,
                              borderRadius: BorderRadius.circular(45)),
                          child: Center(
                              child: Platform.isIOS
                                  ? Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.white,
                                      ),
                                    )
                                  : Icon(
                                      Icons.arrow_back,
                                      color: Colors.white,
                                    ))),
                    ),
                  ],
                ),
              ),
              Container(
                height: 300,
                width: double.infinity,
                padding: EdgeInsets.all(16),
                child: Image.asset('assets/logo_umkm.png'),
              ),
              Container(
                height: 450,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Stack(
                  children: [
                    Container(
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                        child: Image.asset(
                          "assets/background.png",
                          fit: BoxFit.cover,
                          width: double.infinity,
                          height: double.infinity,
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Silahkan Masuk",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        new Container(
                            margin: EdgeInsets.only(left: 16, right: 16),
                            child: TextFormField(
                              validator: (value) => value.isEmpty
                                  ? 'Nomor Handphone tidak boleh kosong'
                                  : null,
                              controller: usernameController,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(55.0),
                                    borderSide: BorderSide(),
                                  ),
                                  hintText: 'Nomor Handphone',
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Icon(
                                      Icons.call,
                                      color: Colors.grey[400],
                                    ),
                                  )),
                            )),
                        SizedBox(
                          height: 16,
                        ),
                        new Container(
                            margin:
                                EdgeInsets.only(top: 10.0, left: 16, right: 16),
                            child: TextFormField(
                              validator: (value) => value.isEmpty
                                  ? 'Kata Sandi tidak boleh kosong'
                                  : null,
                              controller: passwordController,
                              obscureText: isHidePassword,
                              autofocus: false,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                  fillColor: Colors.white,
                                  filled: true,
                                  suffixIcon: GestureDetector(
                                    onTap: () {
                                      togglePasswordVisibility();
                                    },
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Icon(
                                        isHidePassword
                                            ? Icons.visibility_off
                                            : Icons.visibility,
                                        color: isHidePassword
                                            ? Colors.grey[400]
                                            : Colors.grey[400],
                                      ),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(55.0),
                                    borderSide: BorderSide(),
                                  ),
                                  hintText: 'Kata Sandi',
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Icon(
                                      Icons.vpn_key,
                                      color: Colors.grey[400],
                                    ),
                                  )),
                            )),
                        Container(
                          margin:
                              EdgeInsets.only(left: 16, right: 16, top: 25.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(left: 5),
                                  height: 55.0,
                                  child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(55.0)),
                                      color: ColorPalette.btnAction,
                                      child: Center(
                                        child: isLoading
                                            ? Text(
                                                "MASUK",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )
                                            : SizedBox(
                                                height: 20.0,
                                                width: 20.0,
                                                child:
                                                    CircularProgressIndicator(
                                                  valueColor:
                                                      AlwaysStoppedAnimation<
                                                          Color>(Colors.black),
                                                ),
                                              ),
                                      ),
                                      onPressed: () {
                                        validateAndSave();
                                        // Navigator.push(context,
                                        //     MaterialPageRoute(
                                        //         builder: (context) {
                                        //   return Home();
                                        // }));
                                      }),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              style: TextStyle(fontFamily: 'Poppins'),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Buat Akun ? ',
                                    style: TextStyle(color: Colors.black)),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Navigator.pushNamed(
                                            context, '/buat-akun');
                                      },
                                    text: 'Daftar Sekarang',
                                    style: TextStyle(
                                        color: ColorPalette.btnRed,
                                        fontWeight: FontWeight.bold)),
                              ]),
                        ),
                        // Container(
                        //   margin: EdgeInsets.only(
                        //       left: 16, right: 16, top: 50.0, bottom: 16),
                        //   child: RichText(
                        //     textAlign: TextAlign.center,
                        //     text: TextSpan(
                        //         style: TextStyle(fontFamily: 'Poppins'),
                        //         children: <TextSpan>[
                        //           TextSpan(
                        //               text:
                        //                   'Dengan daftar akun, anda menyetujui aturan ',
                        //               style: TextStyle(color: Colors.white)),
                        //           TextSpan(
                        //               recognizer: TapGestureRecognizer()
                        //                 ..onTap = () {
                        //                   Navigator.of(context).push(
                        //                       MaterialPageRoute(
                        //                           builder:
                        //                               (BuildContext context) =>
                        //                                   MyWebview(
                        //                                     title:
                        //                                         "Panduan Layanan",
                        //                                     url:
                        //                                         "https://connexist.com/term-of-sevice",
                        //                                   )));
                        //                 },
                        //               text: 'Panduan Layanan ',
                        //               style: TextStyle(
                        //                   color: ColorPalette.btnRed,
                        //                   fontWeight: FontWeight.bold)),
                        //           TextSpan(
                        //               text: 'dan ',
                        //               style: TextStyle(color: Colors.white)),
                        //           TextSpan(
                        //               recognizer: TapGestureRecognizer()
                        //                 ..onTap = () {
                        //                   Navigator.of(context).push(
                        //                       MaterialPageRoute(
                        //                           builder:
                        //                               (BuildContext context) =>
                        //                                   MyWebview(
                        //                                     title:
                        //                                         "Kebijakan Privasi",
                        //                                     url:
                        //                                         "https://connexist.com/term-of-sevice",
                        //                                   )));
                        //                 },
                        //               text: 'Kebijakan Privasi ',
                        //               style: TextStyle(
                        //                   color: ColorPalette.btnRed,
                        //                   fontWeight: FontWeight.bold)),
                        //           TextSpan(
                        //               text: 'Kami',
                        //               style: TextStyle(color: Colors.white)),
                        //         ]),
                        //   ),
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/content_view.dart';
import 'package:provider/provider.dart';

class BuatAkunPage extends StatefulWidget {
  @override
  _BuatAkunPageState createState() => _BuatAkunPageState();
}

class _BuatAkunPageState extends State<BuatAkunPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  TextEditingController nomorHp = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController namaLengkap = TextEditingController();
  TextEditingController noKtp = TextEditingController();
  TextEditingController kataSandi = TextEditingController();
  bool _obscure = true;
  bool isLoading = true;
  int _value;
  String gender = "";
  var token;

  Future<String> printToken() async {
    print("token should be printed");
    token = await firebaseMessaging.getToken();
    print("token saya $token");

    return token;
  }

  @override
  void initState() {
    printToken();
    super.initState();
  }

  final regExp = RegExp(
      "^[a-zA-Z0-9.!#\$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*\$");

  validateAndSave(
      String _email,
      String _name,
      String _identityNumber,
      String _password,
      String _role,
      String _gender,
      String _phoneNumber,
      String _firebaseToken) {
    final FormState form = formKey.currentState;
    print(form);
    if (form.validate()) {
      print('Form is valid');
      setState(() {
        isLoading = false;
      });
      CHttp _chttp = Provider.of<CHttp>(context, listen: false);
      BaseAuth _auth = _chttp.auth;
      Future<User> _register = _auth.register(_email, _name, _password, _role,
          _identityNumber, _gender, _phoneNumber, _firebaseToken);
      _register.then((User value) {
        if (value != null) {
          setState(() {
            isLoading = true;
          });
          Fluttertoast.showToast(
              msg: "Proses Daftar Berhasil",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: ColorPalette.btnGreen,
              textColor: Colors.white);
          return Navigator.pushNamed(context, "/");
        } else {
          setState(() {
            isLoading = true;
          });
          Fluttertoast.showToast(
              msg: "Terjadi kesalahan saat mendaftar",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white);
          print("error login, toaster nya kemana ?");
        }
      });
      return true;
    } else {
      print('Form is invalid');
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
      },
      child: Scaffold(
        body: Stack(
          children: [
            Image.asset(
              "assets/background.png",
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
            ),
            ListView(
              children: [
                SizedBox(
                  height: 50,
                ),
                Center(
                  child: Text(
                    "REGISTRASI",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  child: Center(
                    child: Text(
                      'Silahkan Buat Akun Anda',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  margin: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Informasi Pribadi",
                          style: TextStyle(color: Colors.black, fontSize: 18.0),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Divider(
                          thickness: 2,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55.0)),
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Nama Lengkap tidak boleh kosong';
                              }
                              return null;
                            },
                            controller: namaLengkap,
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Icon(
                                  Icons.person,
                                  color: Colors.grey[400],
                                ),
                              ),
                              hintText: "Nama Lengkap",
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(55.0),
                                borderSide: BorderSide(),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55.0)),
                          child: TextFormField(
                            // validator: (value) {
                            //   if (value.isEmpty) {
                            //     return 'No KTP tidak boleh kosong';
                            //   }
                            //   return null;
                            // },
                            controller: noKtp,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Icon(
                                  Icons.credit_card,
                                  color: Colors.grey[400],
                                ),
                              ),
                              hintText: "No KTP",
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(55.0),
                                borderSide: BorderSide(),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _value = 1;
                                    gender = "Male";
                                    print(gender);
                                  });
                                },
                                child: Container(
                                  height: 55,
                                  width: double.infinity,
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(55),
                                    border: Border.all(color: Colors.grey),
                                    color: _value == 1
                                        ? ColorPalette.btnGreen
                                        : _value == null
                                            ? Colors.white
                                            : Colors.white,
                                  ),
                                  child: Center(
                                      child: Text("Laki-laki",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: _value == 1
                                                  ? Colors.white
                                                  : _value == null
                                                      ? Colors.black
                                                      : Colors.black))),
                                ),
                              ),
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _value = 0;
                                    gender = "Female";
                                    print(gender);
                                  });
                                },
                                child: Container(
                                  height: 55,
                                  width: double.infinity,
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(55),
                                    border: Border.all(color: Colors.grey),
                                    color: _value == 0
                                        ? ColorPalette.btnGreen
                                        : _value == null
                                            ? Colors.white
                                            : Colors.white,
                                  ),
                                  child: Center(
                                      child: Text("Perempuan",
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: _value == 0
                                                  ? Colors.white
                                                  : _value == null
                                                      ? Colors.black
                                                      : Colors.black))),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55.0)),
                          child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Alamat Email tidak boleh kosong';
                                } else if (!regExp.hasMatch(value)) {
                                  return "Tolong masukkan email yang benar";
                                }
                                return null;
                              },
                              keyboardType: TextInputType.emailAddress,
                              controller: email,
                              decoration: InputDecoration(
                                hintText: "Alamat Email",
                                fillColor: Colors.white,
                                filled: true,
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.alternate_email,
                                    color: Colors.grey[400],
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(55.0),
                                  borderSide: BorderSide(),
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Informasi Akun",
                          style: TextStyle(color: Colors.black, fontSize: 18.0),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Divider(
                          thickness: 2,
                          color: Colors.black,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55.0)),
                          child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Nomor HP tidak boleh kosong';
                                }
                                if (value.length < 9) {
                                  return 'Nomor HP kurang dari 12 karakter';
                                }
                                return null;
                              },
                              controller: nomorHp,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                hintText: "Nomor HP",
                                fillColor: Colors.white,
                                filled: true,
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.phone,
                                    color: Colors.grey[400],
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(55.0),
                                  borderSide: BorderSide(),
                                ),
                              )),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55.0)),
                          child: TextFormField(
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Kata Sandi tidak boleh kosong';
                                }
                                if (value.length < 6) {
                                  return 'Kata Sandi kurang dari 6 karakter';
                                }
                                return null;
                              },
                              obscureText: _obscure,
                              controller: kataSandi,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                suffixIcon: IconButton(
                                  icon: Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Icon(
                                      _obscure
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _obscure = !_obscure;
                                    });
                                  },
                                ),
                                hintText: "Kata Sandi",
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.vpn_key,
                                    color: Colors.grey[400],
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(55.0),
                                  borderSide: BorderSide(),
                                ),
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 20.0,
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 55.0,
                                margin: EdgeInsets.only(right: 8),
                                child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(55.0),
                                    ),
                                    color: ColorPalette.btnBack,
                                    child: Text(
                                      'KEMBALI',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 18),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                    }),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 55.0,
                                margin: EdgeInsets.only(left: 8),
                                child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(55.0),
                                    ),
                                    color: ColorPalette.btnAction,
                                    child: Center(
                                      child: isLoading
                                          ? Text('SUBMIT',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                  fontSize: 18))
                                          : SizedBox(
                                              height: 20.0,
                                              width: 20.0,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation<
                                                        Color>(Colors.black),
                                              ),
                                            ),
                                    ),
                                    onPressed: () async {
                                      print(email.text +
                                          '\n' +
                                          namaLengkap.text +
                                          '\n' +
                                          noKtp.text +
                                          '\n' +
                                          kataSandi.text +
                                          '\n' +
                                          'basic' +
                                          '\n' +
                                          gender +
                                          '\n' +
                                          nomorHp.text +
                                          '\n' +
                                          token);
                                      await validateAndSave(
                                          email.text,
                                          namaLengkap.text,
                                          noKtp.text,
                                          kataSandi.text,
                                          'basic',
                                          gender == null ? '' : gender,
                                          nomorHp.text,
                                          token);
                                    }),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 50.0, bottom: 30),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                style: TextStyle(fontFamily: 'Poppins'),
                                children: <TextSpan>[
                                  TextSpan(
                                      text:
                                          'Dengan daftar akun, anda menyetujui aturan ',
                                      style: TextStyle(color: Colors.white)),
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          MyWebview(
                                                            title:
                                                                "Panduan Layanan",
                                                            url:
                                                                "https://connexist.com/term-of-sevice",
                                                          )));
                                        },
                                      text: 'Panduan Layanan ',
                                      style: TextStyle(
                                          color: ColorPalette.btnRed,
                                          fontWeight: FontWeight.bold)),
                                  TextSpan(
                                      text: 'dan ',
                                      style: TextStyle(color: Colors.white)),
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          MyWebview(
                                                            title:
                                                                "Kebijakan Privasi",
                                                            url:
                                                                "https://connexist.com/term-of-sevice",
                                                          )));
                                        },
                                      text: 'Kebijakan Privasi ',
                                      style: TextStyle(
                                          color: ColorPalette.btnRed,
                                          fontWeight: FontWeight.bold)),
                                  TextSpan(
                                      text: 'Kami',
                                      style: TextStyle(color: Colors.white)),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

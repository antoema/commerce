import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  startTime() async {
    Duration duration = Duration(seconds: 2);
    return Timer(duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacementNamed('/MyApp');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Poppins'),
      home: Scaffold(
        body: SafeArea(
          child: Stack(children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  // gradient: LinearGradient(
                  //     begin: Alignment.topCenter,
                  //     end: Alignment.bottomCenter,
                  //     colors: [
                  //       Color(0xFF66BB6A),
                  //       Color(0xFFA5D6A7),
                  //       Color(0xFFC8E6C9),
                  //       Color(0xFFFCE4EC),
                  //       Color(0xFFF8BBD0),
                  //       Color(0xFFF48FB1)
                  //     ]),
                  // color: ColorPalette.themeColor,
                  // image: DecorationImage(
                  //   image: AssetImage("assets/background.png"),
                  // ),
                  ),
              child: Stack(
                children: [
                  // Image.asset(
                  //   "assets/background.png",
                  //   width: double.infinity,
                  //   height: double.infinity,
                  //   fit: BoxFit.cover,
                  // ),
                  Center(
                    child: Image.asset(
                      "assets/logo_umkm.png",
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Image.asset(
                    'assets/logo_cx.png',
                    width: 122,
                    height: 45,
                  )),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(bottom: 60),
                child: Text(
                  "poweredBy :",
                  style: TextStyle(color: Colors.black, fontSize: 12),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}

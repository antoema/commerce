import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';

class PPOBPage extends StatefulWidget {
  const PPOBPage({Key key}) : super(key: key);

  @override
  _PPOBPageState createState() => _PPOBPageState();
}

class _PPOBPageState extends State<PPOBPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Top-up & Tagihan",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

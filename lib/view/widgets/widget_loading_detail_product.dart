import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingDetailProductWidget extends StatelessWidget {
  const LoadingDetailProductWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: double.infinity, height: 350, color: Colors.white),
              Container(
                width: double.infinity,
                height: 20,
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white),
              ),
              Container(
                width: double.infinity,
                height: 20,
                margin: EdgeInsets.only(left: 16, right: 100, top: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white),
              ),
              Container(
                width: 120,
                height: 20,
                margin:
                    EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white),
              ),
            ],
          ),
        ),
        Divider(
          thickness: 15,
          color: Colors.grey[100],
        ),
        SizedBox(height: 16),
        Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Row(
            children: [
              Container(
                  width: 50,
                  height: 50,
                  margin: EdgeInsets.only(left: 16),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.white)),
              SizedBox(width: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 140,
                    height: 15,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white),
                  ),
                  SizedBox(height: 5),
                  Container(
                    width: 80,
                    height: 15,
                    margin: EdgeInsets.only(top: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white),
                  ),
                ],
              )
            ],
          ),
        ),
        SizedBox(height: 16),
        Divider(
          thickness: 15,
          color: Colors.grey[100],
        ),
        SizedBox(height: 16),
        Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 150,
                height: 15,
                margin: EdgeInsets.only(
                  left: 16,
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white),
              ),
              Container(
                width: 20,
                height: 20,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white),
              ),
            ],
          ),
        ),
        // SizedBox(height: 16),
        // Divider(
        //   thickness: 15,
        //   color: Colors.grey[100],
        // ),
        // SizedBox(height: 16),
        // Shimmer.fromColors(
        //   baseColor: Colors.grey[300],
        //   highlightColor: Colors.grey[100],
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Container(
        //         width: 150,
        //         height: 15,
        //         margin: EdgeInsets.only(left: 16, right: 16),
        //         decoration: BoxDecoration(
        //             borderRadius: BorderRadius.circular(8),
        //             color: Colors.white),
        //       ),
        //       SizedBox(height: 10),
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           Container(
        //             width: 120,
        //             height: 15,
        //             margin: EdgeInsets.only(left: 16),
        //             decoration: BoxDecoration(
        //                 borderRadius: BorderRadius.circular(8),
        //                 color: Colors.white),
        //           ),
        //           Container(
        //             width: 80,
        //             height: 15,
        //             margin: EdgeInsets.only(right: 16),
        //             decoration: BoxDecoration(
        //                 borderRadius: BorderRadius.circular(8),
        //                 color: Colors.white),
        //           ),
        //         ],
        //       ),
        //       SizedBox(height: 10),
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           Container(
        //             width: 100,
        //             height: 15,
        //             margin: EdgeInsets.only(left: 16),
        //             decoration: BoxDecoration(
        //                 borderRadius: BorderRadius.circular(8),
        //                 color: Colors.white),
        //           ),
        //           Container(
        //             width: 100,
        //             height: 15,
        //             margin: EdgeInsets.only(right: 16),
        //             decoration: BoxDecoration(
        //                 borderRadius: BorderRadius.circular(8),
        //                 color: Colors.white),
        //           ),
        //         ],
        //       ),
        //       SizedBox(height: 10),
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           Container(
        //             width: 80,
        //             height: 15,
        //             margin: EdgeInsets.only(left: 16),
        //             decoration: BoxDecoration(
        //                 borderRadius: BorderRadius.circular(8),
        //                 color: Colors.white),
        //           ),
        //           Container(
        //             width: 120,
        //             height: 15,
        //             margin: EdgeInsets.only(right: 16),
        //             decoration: BoxDecoration(
        //                 borderRadius: BorderRadius.circular(8),
        //                 color: Colors.white),
        //           ),
        //         ],
        //       ),
        //     ],
        //   ),
        // ),
        SizedBox(height: 16),
        Divider(
          thickness: 15,
          color: Colors.grey[100],
        ),
        SizedBox(height: 16),
        Container(
          height: 320.0,
          child: Column(
            children: [
              Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 180,
                      height: 15,
                      margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                    ),
                    Container(
                        height: 280.0,
                        padding:
                            EdgeInsets.only(left: 16, right: 7, bottom: 16),
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 3,
                          itemBuilder: (context, index) {
                            return Container(
                              width: 150,
                              margin: EdgeInsets.only(right: 7),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15)),
                            );
                          },
                        ))
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

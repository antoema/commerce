import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pekik_app/bloc/public-product-bloc/public_product_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/cart_badge.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-transaction/page_cart.dart';
import 'package:pekik_app/view/widgets/widget_loading_product.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class PublicProductPage extends StatefulWidget {
  @override
  _PublicProductPageState createState() => _PublicProductPageState();
}

class _PublicProductPageState extends State<PublicProductPage> {
  PublicProductBloc productBloc = PublicProductBloc();
  CartBadgesModel cartBadgesModel;
  bool isActive = true;

  @override
  void initState() {
    productBloc.add(GetPublicProduct());
    _getRequest();
    super.initState();
  }

  Future<void> onRefresh() async {
    setState(() {
      productBloc.add(GetPublicProduct());
    });
  }

  _getRequest() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    if (_auth.currentUser != null) {
      await transactionVM.fetchCartBadge().then((value) {
        if (value != null) {
          setState(() {
            cartBadgesModel = value;
            isActive = false;
          });
        } else {
          setState(() {
            isActive = true;
          });
        }
      });
    }
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        elevation: 1,
        backgroundColor: Colors.white,
        title: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/search")
                      .then((val) => val ? _getRequest() : null);
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  height: 45,
                  decoration: BoxDecoration(
                      color: Color(0xffF0F1F5),
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(width: 0.5, color: Color(0xffC4C6CC))),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.search,
                        color: ColorPalette.themeIcon,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Cari Produk atau Toko",
                        style: TextStyle(
                          color: Color(0xffC4C6CC),
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        actions: [
          // Padding(
          //   padding: EdgeInsets.only(left: 16),
          //   child: GestureDetector(
          //     onTap: () {
          //       Navigator.push(context, MaterialPageRoute(builder: (context) {
          //         return ComingSoonWidget();
          //       }));
          //     },
          //     child: Icon(
          //       Icons.chat_outlined,
          //       color: Colors.black,
          //     ),
          //   ),
          // ),
          isActive
              ? Padding(
                  padding: EdgeInsets.only(left: 12, right: 16),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return CartPage();
                      })).then((val) => val ? _getRequest() : null);
                    },
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.black,
                    ),
                  ),
                )
              : _shoppingCartBadge()
        ],
      ),
      body: _buildListProduct(),
    );
  }

  Widget _buildListProduct() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => productBloc,
        child: BlocListener<PublicProductBloc, PublicProductState>(
          listener: (context, state) {
            if (state is PublicProductError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<PublicProductBloc, PublicProductState>(
            builder: (context, state) {
              if (state is PublicProductInitial) {
                return ListView(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                        width: double.infinity,
                        height: 250,
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    WidgetLoadingProduct(),
                  ],
                );
              } else if (state is PublicProductLoading) {
                return ListView(
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        width: double.infinity,
                        height: 220,
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    WidgetLoadingProduct(),
                  ],
                );
              }
              if (state is PublicProductLoaded) {
                return _buildList(context, state.productModel);
              }
              if (state is PublicProductError) {
                return Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 16, bottom: 70),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, ProductModel productModel) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    if (productModel.data.length == 0) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/ic_empty.png",
                width: 128,
                height: 128,
              ),
              Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text("Produk belum ada data")),
            ],
          ),
        ),
      );
    }
    return ListView(
      children: [
        Image.asset(
          'assets/banner_shopping3.jpeg',
          width: double.infinity,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Produk',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ],
          ),
        ),
        GridView.builder(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 76),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: productModel.data.length,
            physics: ScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 8.0,
              crossAxisSpacing: 8.0,
              childAspectRatio: 3 / 5.2,
            ),
            itemBuilder: (context, index) {
              // Result results = _results[index];
              return Container(
                child: Card(
                  elevation: 1.5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(10.0),
                    onTap: () {
                      basket.addAll({"idProduct": productModel.data[index].id});
                      Navigator.pushNamed(context, "/detail-produk")
                          .then((val) => val ? _getRequest() : null);
                    },
                    child: Column(
                      children: <Widget>[
                        AspectRatio(
                          aspectRatio: 7 / 5.8,
                          child: Container(
                            // height: imageHeight,
                            // width: imageWidth,
                            child: ClipRRect(
                              child: CachedNetworkImage(
                                imageUrl: productModel.data[index].productImage
                                    .split(',')
                                    .first,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Center(
                                    child: Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[100],
                                  child: Container(
                                    color: Colors.white,
                                    width: double.infinity,
                                    height: double.infinity,
                                  ),
                                )),
                                errorWidget: (context, url, error) => Center(
                                    child: Image.asset(
                                  "assets/default_image.png",
                                  fit: BoxFit.cover,
                                  width: double.infinity,
                                )),
                              ),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10.0),
                                  topRight: Radius.circular(10.0)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(12.0),
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              productModel.data[index].productName.length > 35
                                  ? Text(
                                      productModel.data[index].productName
                                              .substring(0, 35) +
                                          "...",
                                      maxLines: 2,
                                      style: TextStyle(
                                          fontSize: 16.0, color: Colors.black),
                                    )
                                  : Text(
                                      productModel.data[index].productName,
                                      maxLines: 2,
                                      style: TextStyle(
                                          fontSize: 16.0, color: Colors.black),
                                    ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                ConnexistHelper.formatCurrency(productModel
                                    .data[index].productPrice
                                    .toDouble()),
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                "${productModel.data[index].tenant.city}",
                                maxLines: 1,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: ColorPalette.fontColor),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              productModel.data[index].tenant.tenantType ==
                                      'official'
                                  ? Row(
                                      children: [
                                        Image.asset(
                                          'assets/ic_official_store.png',
                                          width: 20,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "Official Store",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 14.0,
                                              color: ColorPalette.btnGreen),
                                        ),
                                      ],
                                    )
                                  : productModel.data[index].productStock < 1
                                      ? Text(
                                          "Stok Habis",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.red),
                                        )
                                      : productModel.data[index].productStock <
                                              3
                                          ? Text(
                                              "Stok Hampir Habis",
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontSize: 12.0,
                                                  color: Colors.red),
                                            )
                                          : Text(
                                              "Stok Tersedia",
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontSize: 12.0,
                                                  color: Colors.black),
                                            ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
      ],
    );
  }

  Widget _shoppingCartBadge() {
    return cartBadgesModel.data.shoppingCartBadges == 0
        ? Padding(
            padding: EdgeInsets.only(left: 12, right: 16),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CartPage();
                })).then((val) => val ? _getRequest() : null);
              },
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Colors.black,
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: Badge(
              position: BadgePosition.topEnd(top: 3, end: 6),
              animationDuration: Duration(milliseconds: 300),
              animationType: BadgeAnimationType.slide,
              badgeContent: Text(
                cartBadgesModel.data.shoppingCartBadges.toString(),
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
              child: IconButton(
                  icon: Icon(Icons.shopping_cart_outlined, color: Colors.black),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CartPage();
                    })).then((val) => val ? _getRequest() : null);
                  }),
            ),
          );
  }
}

import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/model/search_history.dart';
import 'package:pekik_app/model/tenant_single.dart';
import 'package:pekik_app/utils/db_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-home/widget/widget_category.dart';
import 'package:pekik_app/view/page-public-product/page_public_toko.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class PublicSearchProductPage extends StatefulWidget {
  const PublicSearchProductPage({Key key}) : super(key: key);

  @override
  _PublicSearchProductPageState createState() =>
      _PublicSearchProductPageState();
}

class _PublicSearchProductPageState extends State<PublicSearchProductPage>
    with TickerProviderStateMixin {
  String str;
  bool isDataNull = true;
  bool isLoading = false;
  List<ResultProduct> _data = [];
  List<DataTenantSingle> _dataTenant = [];
  TabController tabController;
  List<SearchHistory> items = new List();
  List<SearchHistory> values = [];

  List<String> typeSearch = [
    'minyak',
    'saus',
    'kecap',
    'beras',
    'susu',
    'garam',
    'terigu',
    'coffee',
    'pakaian',
    'celana',
  ];
  final _search = TextEditingController();
  void _getData(String str) async {
    setState(() {
      isLoading = true;
    });

    PublicProductViewModel productViewModel = PublicProductViewModel();
    await productViewModel.fetchProductFilter('', '', '', str).then((product) {
      setState(() {
        _data = [];
      });

      if (product.data != null) {
        setState(() {
          _data.addAll(product.data);

          return null;
        });
      } else {
        Fluttertoast.showToast(
            msg: "Terjadi kesalahan saat berkomunikasi dengan server",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red[400],
            textColor: Colors.white,
            fontSize: 14.0);
      }
    });
    await productViewModel.fetchTenantFilter('', str).then((tenant) {
      setState(() {
        _dataTenant = [];
      });
      if (tenant.data != null) {
        _dataTenant.addAll(tenant.data);
        return null;
      } else {
        Fluttertoast.showToast(
            msg: "Terjadi kesalahan saat berkomunikasi dengan server",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red[400],
            textColor: Colors.white,
            fontSize: 14.0);
      }
    });
    addSearchHistory(str);
    setState(() {
      isDataNull = false;
      isLoading = false;
    });
  }

  addSearchHistory(String str) {
    var searchName = SearchHistory();
    searchName.name = str;
    var dbHelper = DBHelper();
    dbHelper.insert(searchName);
  }

  deleteSearchHistory(int id) {
    var dbHelper = DBHelper();
    setState(() {
      dbHelper.deleteSerachHistory(id);
    });
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    _search.addListener(() {
      setState(() {});
    });
  }

  Future<List<SearchHistory>> _getDataName() async {
    var dbHelper = DBHelper();
    await dbHelper.getAllSerachHistory().then((value) {
      items = value;
    });

    return items;
  }

  getAllSearchName() {
    return FutureBuilder(
        future: _getDataName(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            values = snapshot.data;
            if (values.length == 0) {
              return Container();
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Terakhir dicari',
                        style: TextStyle(
                            // fontFamily: "Caveat",
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold
                            // color: kKepasarTeal
                            ),
                      ),
                      GestureDetector(
                        onTap: () {
                          var dbHelper = DBHelper();
                          setState(() {
                            dbHelper.deleteAll();
                          });
                        },
                        child: Text(
                          'Hapus Semua',
                          style: TextStyle(
                              // fontFamily: "Caveat",
                              fontSize: 14.0,
                              color: ColorPalette.btnRed),
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                  itemCount: values.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Row(
                        children: [
                          Icon(
                            Icons.history,
                            color: ColorPalette.fontColor,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                              child: GestureDetector(
                            onTap: () {
                              setState(() {
                                _search.text = values[index].name;
                              });
                              _getData(values[index].name);
                            },
                            child: Text(
                              values[index].name,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black),
                            ),
                          )),
                          GestureDetector(
                              onTap: () {
                                deleteSearchHistory(values[index].id);
                              },
                              child: Icon(
                                Icons.close,
                                color: ColorPalette.fontColor,
                              ))
                        ],
                      ),
                    );
                  },
                ),
              ],
            );
          }
          return Container();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          titleSpacing: 0,
          leading: GestureDetector(
            onTap: () {
              if (isDataNull == false) {
                setState(() {
                  isDataNull = true;
                });
              } else {
                Navigator.pop(context, true);
              }
            },
            child: Icon(
              Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  height: 45,
                  child: CupertinoTextField(
                    textAlignVertical: TextAlignVertical.center,
                    onSubmitted: (String str) {
                      if (str != '') {
                        setState(() {
                          str = str;
                        });
                        _getData(str);
                      }
                    },
                    controller: _search,
                    keyboardType: TextInputType.text,
                    autofocus: true,
                    placeholder: 'Cari Produk atau Toko',
                    placeholderStyle: TextStyle(
                        color: Color(0xffC4C6CC),
                        fontSize: 14.0,
                        fontFamily: 'Poppins'),
                    style: TextStyle(fontFamily: 'Poppins'),
                    prefix: Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Icon(
                        Icons.search,
                        color: Color(0xffC4C6CC),
                      ),
                    ),
                    suffix: _search.text == ''
                        ? Container()
                        : Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: GestureDetector(
                              onTap: () {
                                _search.text = '';
                                if (isDataNull == false) {
                                  setState(() {
                                    isDataNull = true;
                                  });
                                }
                              },
                              child: Icon(
                                Icons.close,
                                color: Color(0xffC4C6CC),
                              ),
                            ),
                          ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color(0xffF0F1F5),
                        border: Border.all(width: 1, color: Color(0xffC4C6CC))),
                  ),
                ),
              ),
              SizedBox(
                width: 16,
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
          getDataSearch(),
          isLoading
              ? Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      child: LinearProgressIndicator(
                    backgroundColor: ColorPalette.themeIcon.withOpacity(0.5),
                    valueColor:
                        AlwaysStoppedAnimation<Color>(ColorPalette.themeIcon),
                  )),
                )
              : Container()
        ],
      ),
    );
  }

  Widget getDataSearch() {
    return isDataNull
        ? Container(
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Paling banyak dicari',
                        style: TextStyle(
                            // fontFamily: "Caveat",
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold
                            // color: kKepasarTeal
                            ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  child: Wrap(
                    runSpacing: 8.0,
                    spacing: 8.0,
                    direction: Axis.horizontal,
                    children: [
                      ...typeSearch.map((name) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              _search.text = name;
                            });
                            _getData(name);
                          },
                          child: Container(
                              padding: EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Color(0xffF0F1F5),
                                  border: Border.all(
                                      color: Color(0xffC4C6CC), width: 1)),
                              child: Text(name,
                                  style: new TextStyle(
                                      color: ColorPalette.themeIcon,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold))),
                        );
                      })
                    ],
                  ),
                ),
                getAllSearchName(),
                Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Kategori',
                        style: TextStyle(
                            // fontFamily: "Caveat",
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold
                            // color: kKepasarTeal
                            ),
                      ),
                      // GestureDetector(
                      //   onTap: () {
                      //     Navigator.push(context,
                      //         MaterialPageRoute(builder: (context) {
                      //       return PublicAllCategoryPage();
                      //     }));
                      //   },
                      //   child: Text(
                      //     "Lihat Semua",
                      //     style: TextStyle(
                      //         // fontFamily: "Caveat",
                      //         fontSize: 14.0,
                      //         // fontWeight: FontWeight.bold,
                      //         color: ColorPalette.themeIcon),
                      //   ),
                      // ),
                    ],
                  ),
                ),
                CategoryWidget(),
              ],
            ),
          )
        : Column(
            children: [
              TabBar(
                // isScrollable: true,
                indicatorColor: ColorPalette.themeIcon,
                labelColor: Colors.black,
                tabs: [
                  Tab(text: "Produk"),
                  Tab(text: "Toko"),
                ],
                controller: tabController,
              ),
              Expanded(
                flex: 40,
                child: TabBarView(
                    controller: tabController,
                    children: <Widget>[_getProductSearch(), _getTokoSearch()]),
              ),
              // getToken()
            ],
          );
  }

  Widget _getProductSearch() {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    if (_data.length == 0) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/ic_empty.png",
                width: 128,
                height: 128,
              ),
              Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text("Ups, Produk yang anda cari belum ada.")),
            ],
          ),
        ),
      );
    }
    return GridView.builder(
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 10),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        // controller: controller,
        itemCount: _data.length,
        physics: ScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 3 / 5.1,
        ),
        itemBuilder: (context, index) {
          // Result results = _results[index];
          return Container(
            child: Card(
              elevation: 1.5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: InkWell(
                borderRadius: BorderRadius.circular(10.0),
                onTap: () {
                  basket.addAll({"idProduct": _data[index].id});
                  Navigator.pushNamed(context, "/detail-produk");
                },
                child: Column(
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: 7 / 5.8,
                      child: Container(
                        // height: imageHeight,
                        // width: imageWidth,
                        child: ClipRRect(
                          child: CachedNetworkImage(
                            imageUrl:
                                _data[index].productImage.split(',').first,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Center(
                                child: Shimmer.fromColors(
                              baseColor: Colors.grey[300],
                              highlightColor: Colors.grey[100],
                              child: Container(
                                color: Colors.white,
                                width: double.infinity,
                                height: double.infinity,
                              ),
                            )),
                            errorWidget: (context, url, error) => Center(
                                child: Image.asset(
                              "assets/default_image.png",
                              fit: BoxFit.cover,
                              width: double.infinity,
                            )),
                          ),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0)),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(12.0),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _data[index].productName.length > 35
                              ? Text(
                                  _data[index].productName.substring(0, 35) +
                                      "...",
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.black),
                                )
                              : Text(
                                  _data[index].productName,
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.black),
                                ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            ConnexistHelper.formatCurrency(
                                _data[index].productPrice.toDouble()),
                            style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            "${_data[index].tenant.city}",
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 12.0, color: ColorPalette.fontColor),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          _data[index].tenant.tenantType == 'official'
                              ? Row(
                                  children: [
                                    Image.asset(
                                      'assets/ic_official_store.png',
                                      width: 20,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      "Official Store",
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: ColorPalette.btnGreen),
                                    ),
                                  ],
                                )
                              : _data[index].productStock < 1
                                  ? Text(
                                      "Stok Habis",
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 12.0, color: Colors.red),
                                    )
                                  : _data[index].productStock < 3
                                      ? Text(
                                          "Stok Hampir Habis",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.red),
                                        )
                                      : Text(
                                          "Stok Tersedia",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.black),
                                        ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget _getTokoSearch() {
    if (_dataTenant.length == 0) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/ic_store.png",
                width: 128,
                height: 128,
              ),
              Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text("Ups, Toko yang anda cari belum ada.")),
            ],
          ),
        ),
      );
    }
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _dataTenant.length,
      padding: EdgeInsets.all(16),
      itemBuilder: (context, index) {
        return Card(
          elevation: 2,
          margin: EdgeInsets.only(bottom: 10),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: InkWell(
            borderRadius: BorderRadius.circular(10),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return PublicTokoPage(idTenant: _dataTenant[index].id);
              }));
            },
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: CachedNetworkImage(
                          imageUrl: _dataTenant[index].tenantLogo,
                          fit: BoxFit.cover,
                          placeholder: (context, url) => Center(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[300],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              color: Colors.white,
                              width: double.infinity,
                              height: double.infinity,
                            ),
                          )),
                          errorWidget: (context, url, error) =>
                              Image.asset("assets/default_toko.jpg"),
                        ),
                      )),
                  SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Text(_dataTenant[index].tenantName,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                            SizedBox(
                              width: 5,
                            ),
                            _dataTenant[index].tenantType == 'official'
                                ? Image.asset(
                                    'assets/ic_official_store.png',
                                    width: 20,
                                  )
                                : Container(),
                          ],
                        ),
                        SizedBox(height: 4),
                        Text(_dataTenant[index].city,
                            style:
                                TextStyle(color: Colors.black, fontSize: 14)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

import 'dart:io';

import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/cart_badge.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/model/tenant_single.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-transaction/page_cart.dart';
import 'package:pekik_app/view/widgets/widget_loading_product.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class PublicTokoPage extends StatefulWidget {
  final String idTenant;

  PublicTokoPage({
    Key key,
    @required this.idTenant,
  }) : super(key: key);

  @override
  _PublicTokoPageState createState() => _PublicTokoPageState();
}

class _PublicTokoPageState extends State<PublicTokoPage> {
  CartBadgesModel cartBadgesModel;
  bool isActive = true;

  @override
  void initState() {
    super.initState();
    _getRequest();
  }

  _getRequest() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    if (_auth.currentUser != null) {
      await transactionVM.fetchCartBadge().then((value) {
        if (value != null) {
          setState(() {
            cartBadgesModel = value;
            isActive = false;
          });
        } else {
          setState(() {
            isActive = true;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        titleSpacing: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Row(
          children: [
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/search")
                      .then((val) => val ? _getRequest() : null);
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  height: 45,
                  decoration: BoxDecoration(
                      color: Color(0xffF0F1F5),
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(width: 0.5, color: Color(0xffC4C6CC))),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.search,
                        color: ColorPalette.themeIcon,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Cari Produk atau Toko",
                        style: TextStyle(
                          color: Color(0xffC4C6CC),
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        actions: [
          isActive
              ? Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return CartPage();
                      })).then((val) => val ? _getRequest() : null);
                    },
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.black,
                    ),
                  ),
                )
              : _shoppingCartBadge()
        ],
      ),
      body: ListView(
        // shrinkWrap: true,
        children: [
          _getTenant(),
          Divider(
            thickness: 1,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Produk',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
              ],
            ),
          ),
          _getProduct()
        ],
      ),
    );
  }

  Widget _shoppingCartBadge() {
    return cartBadgesModel.data.shoppingCartBadges == 0
        ? Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CartPage();
                })).then((val) => val ? _getRequest() : null);
              },
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Colors.black,
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: Badge(
              position: BadgePosition.topEnd(top: 3, end: 6),
              animationDuration: Duration(milliseconds: 300),
              animationType: BadgeAnimationType.slide,
              badgeContent: Text(
                cartBadgesModel.data.shoppingCartBadges.toString(),
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
              child: IconButton(
                  icon: Icon(Icons.shopping_cart_outlined, color: Colors.black),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CartPage();
                    })).then((val) => val ? _getRequest() : null);
                  }),
            ),
          );
  }

  Widget _getTenant() {
    PublicProductViewModel productViewModel = PublicProductViewModel();
    return FutureBuilder(
      future: productViewModel.fetchTenantFilter(widget.idTenant, ''),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final TenantSingleModel dataTenantSingle = snapshot.data;

          return ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.all(16),
            itemCount: dataTenantSingle.data.length,
            itemBuilder: (context, index) {
              if (dataTenantSingle.data[index].tenantPhone.startsWith('0')) {
                dataTenantSingle.data[index].tenantPhone = '+62' +
                    dataTenantSingle.data[index].tenantPhone
                        .replaceFirst(new RegExp(r'0'), '');
              } else {
                dataTenantSingle.data[index].tenantPhone =
                    dataTenantSingle.data[index].tenantPhone;
              }
              print("Nomor");
              print(dataTenantSingle.data[index].tenantPhone);
              var whatsappUrl =
                  "whatsapp://send?phone=${dataTenantSingle.data[index].tenantPhone}";

              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          width: 65,
                          height: 65,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(45)),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(45),
                            child: CachedNetworkImage(
                              imageUrl: dataTenantSingle.data[index].tenantLogo,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Center(
                                  child: Shimmer.fromColors(
                                baseColor: Colors.grey[300],
                                highlightColor: Colors.grey[100],
                                child: Container(
                                  color: Colors.white,
                                  width: double.infinity,
                                  height: double.infinity,
                                ),
                              )),
                              errorWidget: (context, url, error) =>
                                  Image.asset("assets/default_toko.jpg"),
                            ),
                          )),
                      SizedBox(width: 15),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                dataTenantSingle.data[index].tenantType ==
                                        'official'
                                    ? Image.asset(
                                        'assets/ic_official_store.png',
                                        width: 20,
                                      )
                                    : Container(),
                                SizedBox(
                                  width:
                                      dataTenantSingle.data[index].tenantType ==
                                              'official'
                                          ? 5
                                          : 0,
                                ),
                                dataTenantSingle.data[index].tenantName.length >
                                        25
                                    ? Text(
                                        dataTenantSingle.data[index].tenantName
                                                .substring(0, 25) +
                                            "...",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold))
                                    : Text(
                                        dataTenantSingle.data[index].tenantName,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold)),
                              ],
                            ),
                            SizedBox(height: 2),
                            Text(dataTenantSingle.data[index].city,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14)),
                          ],
                        ),
                      ),
                      SizedBox(width: 15),
                      GestureDetector(
                        onTap: () {
                          showBarModalBottomSheet(
                            expand: false,
                            context: context,
                            backgroundColor: Colors.transparent,
                            builder: (context) => modalMoreInfo(
                                context,
                                dataTenantSingle.data[index].tenantName,
                                dataTenantSingle.data[index].tenantAddress),
                          );
                        },
                        child: Icon(
                          Icons.info,
                          color: ColorPalette.fontColor,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: GestureDetector(
                        onTap: () {
                          print(whatsappUrl);
                          launch(whatsappUrl);
                        },
                        child: Container(
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: ColorPalette.themeIcon),
                          child: Center(
                            child: Text(
                              'Chat',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                          child: GestureDetector(
                        onTap: () {
                          _launchMaps(dataTenantSingle.data[index].tenantLat,
                              dataTenantSingle.data[index].tenantLong);
                        },
                        child: Container(
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                  width: 1, color: ColorPalette.themeIcon),
                              color: Colors.grey[200]),
                          child: Center(
                            child: Text(
                              'Lokasi',
                              style: TextStyle(
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )),
                    ],
                  )
                ],
              );
            },
          );
        }
        return _loadingTenant();
      },
    );
  }

  Widget _loadingTenant() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Container(
                          height: 65,
                          width: 65,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(65),
                              color: Colors.white)),
                      SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              height: 15,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white)),
                          SizedBox(height: 8),
                          Container(
                              height: 15,
                              width: 150,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white)),
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white)),
              ],
            ),
            SizedBox(height: 25),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                      height: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white)),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Container(
                      height: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget modalMoreInfo(BuildContext context, String name, String alamat) {
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.25,
                margin: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text("Alamat Lengkap",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    SizedBox(height: 8),
                    Text(
                      name,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 5),
                    Text(
                      alamat,
                      maxLines: 4,
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                )),
          ]),
    );
  }

  Widget _getProduct() {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    PublicProductViewModel productViewModel = PublicProductViewModel();
    return FutureBuilder<ProductModel>(
      future: productViewModel.fetchProductFilter('', widget.idTenant, '', ''),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final ProductModel productModel = snapshot.data;
          if (productModel.data.length == 0) {
            return Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    Image.asset(
                      "assets/ic_empty.png",
                      width: 128,
                      height: 128,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text("Produk belum ada")),
                  ],
                ),
              ),
            );
          }
          return GridView.builder(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: productModel.data.length,
              physics: ScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
                childAspectRatio: 3 / 5.2,
              ),
              itemBuilder: (context, index) {
                // Result results = _results[index];
                return Container(
                  child: Card(
                    elevation: 1.5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(10.0),
                      onTap: () {
                        basket
                            .addAll({"idProduct": productModel.data[index].id});
                        Navigator.pushNamed(context, "/detail-produk")
                            .then((val) => val ? _getRequest() : null);
                      },
                      child: Column(
                        children: <Widget>[
                          AspectRatio(
                            aspectRatio: 7 / 5.8,
                            child: Container(
                              // height: imageHeight,
                              // width: imageWidth,
                              child: ClipRRect(
                                child: CachedNetworkImage(
                                  imageUrl: productModel
                                      .data[index].productImage
                                      .split(',')
                                      .first,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => Center(
                                      child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.grey[100],
                                    child: Container(
                                      color: Colors.white,
                                      width: double.infinity,
                                      height: double.infinity,
                                    ),
                                  )),
                                  errorWidget: (context, url, error) => Center(
                                      child: Image.asset(
                                    "assets/default_image.png",
                                    fit: BoxFit.cover,
                                    width: double.infinity,
                                  )),
                                ),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(12.0),
                            alignment: Alignment.centerLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                productModel.data[index].productName.length > 35
                                    ? Text(
                                        productModel.data[index].productName
                                                .substring(0, 35) +
                                            "...",
                                        maxLines: 2,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black),
                                      )
                                    : Text(
                                        productModel.data[index].productName,
                                        maxLines: 2,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.black),
                                      ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  ConnexistHelper.formatCurrency(productModel
                                      .data[index].productPrice
                                      .toDouble()),
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "${productModel.data[index].tenant.city}",
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 12.0, color: Colors.black),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                productModel.data[index].tenant.tenantType ==
                                        'official'
                                    ? Row(
                                        children: [
                                          Image.asset(
                                            'assets/ic_official_store.png',
                                            width: 20,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            "Official Store",
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 14.0,
                                                color: ColorPalette.btnGreen),
                                          ),
                                        ],
                                      )
                                    : productModel.data[index].productStock < 1
                                        ? Text(
                                            "Stok Habis",
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 12.0,
                                                color: Colors.red),
                                          )
                                        : productModel
                                                    .data[index].productStock <
                                                3
                                            ? Text(
                                                "Stok Hampir Habis",
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 12.0,
                                                    color: Colors.red),
                                              )
                                            : Text(
                                                "Stok Tersedia",
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 12.0,
                                                    color: Colors.black),
                                              ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              });
        }
        return WidgetLoadingProduct();
      },
    );
  }

  _launchMaps(String lat, String long) async {
    print(lat);
    print(long);
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$lat,$long';
    String appleUrl = 'https://maps.apple.com/?sll=$lat,$long';
    if (await canLaunch(Platform.isIOS ? appleUrl : googleUrl)) {
      await launch(Platform.isIOS ? appleUrl : googleUrl);
    } else {
      throw 'Could not launch url';
    }
  }
}

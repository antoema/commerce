import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/view/page-ppob/page_ppob.dart';
import 'package:pekik_app/view/page-product-by-category/page_product_by_category.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:shimmer/shimmer.dart';

class PublicAllCategoryPage extends StatefulWidget {
  const PublicAllCategoryPage({Key key}) : super(key: key);

  @override
  _PublicAllCategoryPageState createState() => _PublicAllCategoryPageState();
}

class _PublicAllCategoryPageState extends State<PublicAllCategoryPage> {
  @override
  Widget build(BuildContext context) {
    PublicProductViewModel productVM = PublicProductViewModel();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        title: Text(
          "Semua Kategori",
          style: TextStyle(
            color: ColorPalette.themeIcon,
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
      ),
      body: FutureBuilder(
        future: productVM.fetchCategoryPublic(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final CategoryProductModel categoryModel = snapshot.data;
            return GridView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.all(16),
              itemCount: categoryModel.data.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                childAspectRatio: 1 / 1.1,
              ),
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(width: 1, color: Color(0xffC4C6CC))),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(15),
                    onTap: () {
                      if (categoryModel.data[index].categoryType ==
                          'official') {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ProductByCategoryPage(
                              title: categoryModel.data[index].categoryName,
                              tenantType:
                                  categoryModel.data[index].categoryType,
                              tenantId: '',
                              idCategory: '');
                        }));
                      } else if (categoryModel.data[index].categoryType ==
                          'ppob') {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return PPOBPage();
                        }));
                      } else {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ProductByCategoryPage(
                              title: categoryModel.data[index].categoryName,
                              tenantType: '',
                              tenantId: '',
                              idCategory: categoryModel.data[index].id);
                        }));
                      }
                    },
                    child: Container(
                      // width: 53.0,
                      margin: EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 10.0, bottom: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              height: 45.0,
                              width: 45.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(45),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      categoryModel.data[index].categoryImage,
                                  fit: BoxFit.contain,
                                  width: double.infinity,
                                  height: double.infinity,
                                  placeholder: (context, url) => Center(
                                      child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.grey[100],
                                    child: Container(
                                      color: Colors.white,
                                      width: double.infinity,
                                      height: double.infinity,
                                    ),
                                  )),
                                  errorWidget: (context, url, error) => Center(
                                      child: Image.asset(
                                    "assets/default_image.png",
                                    fit: BoxFit.cover,
                                  )),
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Text(
                              categoryModel.data[index].categoryName,
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }
          return loadingList();
        },
      ),
    );
  }

  Widget loadingList() {
    return Container(
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: GridView.builder(
          padding: EdgeInsets.all(16),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 3 / 4,
          ),
          physics: ScrollPhysics(),
          shrinkWrap: true,
          itemCount: 6,
          itemBuilder: (context, index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  width: 80,
                  height: 12.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  width: 60,
                  height: 12.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

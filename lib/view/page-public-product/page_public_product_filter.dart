import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pekik_app/bloc/public-product-filter-bloc/public_product_filter_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/widgets/widget_loading_product.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class PublicProductFilterPage extends StatefulWidget {
  final String title;
  final String path;

  PublicProductFilterPage({
    Key key,
    @required this.title,
    @required this.path,
  }) : super(key: key);

  @override
  _PublicProductFilterPageState createState() =>
      _PublicProductFilterPageState();
}

class _PublicProductFilterPageState extends State<PublicProductFilterPage> {
  PublicProductFilterBloc productFilterBloc = PublicProductFilterBloc();

  @override
  void initState() {
    productFilterBloc.add(GetPublicProductFilter(path: widget.path));
    super.initState();
  }

  Future<void> onRefresh() async {
    setState(() {
      productFilterBloc.add(GetPublicProductFilter(path: widget.path));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: ColorPalette.themeIcon),
          title: Text(
            widget.title,
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
        ),
        body: _buildListProduct(),
      ),
    );
  }

  Widget _buildListProduct() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => productFilterBloc,
        child: BlocListener<PublicProductFilterBloc, PublicProductFilterState>(
          listener: (context, state) {
            if (state is PublicProductFilterError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<PublicProductFilterBloc, PublicProductFilterState>(
            builder: (context, state) {
              if (state is PublicProductFilterInitial) {
                return WidgetLoadingProduct();
              } else if (state is PublicProductFilterLoading) {
                return WidgetLoadingProduct();
              }
              if (state is PublicProductFilterLoaded) {
                return _buildList(context, state.productModel);
              }
              if (state is PublicProductFilterError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, ProductModel productModel) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    if (productModel.data.length == 0) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/ic_empty.png",
                width: 128,
                height: 128,
              ),
              Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text("Produk belum ada data")),
            ],
          ),
        ),
      );
    }
    return GridView.builder(
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 10),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: productModel.data.length,
        physics: ScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 8.0,
          crossAxisSpacing: 8.0,
          childAspectRatio: 3 / 5.2,
        ),
        itemBuilder: (context, index) {
          // Result results = _results[index];
          return Container(
            child: Card(
              elevation: 1.5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: InkWell(
                borderRadius: BorderRadius.circular(10.0),
                onTap: () {
                  basket.addAll({"idProduct": productModel.data[index].id});
                  Navigator.pushNamed(context, "/detail-produk");
                },
                child: Column(
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: 7 / 5.8,
                      child: Container(
                        // height: imageHeight,
                        // width: imageWidth,
                        child: ClipRRect(
                          child: CachedNetworkImage(
                            imageUrl: productModel.data[index].productImage
                                .split(',')
                                .first,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Center(
                                child: Shimmer.fromColors(
                              baseColor: Colors.grey[300],
                              highlightColor: Colors.grey[100],
                              child: Container(
                                color: Colors.white,
                                width: double.infinity,
                                height: double.infinity,
                              ),
                            )),
                            errorWidget: (context, url, error) => Center(
                                child: Image.asset(
                              "assets/default_image.png",
                              fit: BoxFit.cover,
                              width: double.infinity,
                            )),
                          ),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0)),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(12.0),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          productModel.data[index].productName.length > 35
                              ? Text(
                                  productModel.data[index].productName
                                          .substring(0, 35) +
                                      "...",
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.black),
                                )
                              : Text(
                                  productModel.data[index].productName,
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.black),
                                ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            ConnexistHelper.formatCurrency(productModel
                                .data[index].productPrice
                                .toDouble()),
                            style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 8),
                          Text(
                            "${productModel.data[index].tenant.city}",
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 14.0, color: ColorPalette.fontColor),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          productModel.data[index].tenant.tenantType ==
                                  'official'
                              ? Row(
                                  children: [
                                    Image.asset(
                                      'assets/ic_official_store.png',
                                      width: 20,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      "Official Store",
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: ColorPalette.btnGreen),
                                    ),
                                  ],
                                )
                              : productModel.data[index].productStock < 1
                                  ? Text(
                                      "Stok Habis",
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 12.0, color: Colors.red),
                                    )
                                  : productModel.data[index].productStock < 3
                                      ? Text(
                                          "Stok Hampir Habis",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.red),
                                        )
                                      : Text(
                                          "Stok Tersedia",
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.black),
                                        ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

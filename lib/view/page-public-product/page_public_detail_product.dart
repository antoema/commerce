import 'dart:io';

import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/bloc/public-detail-product-bloc/public_detail_product_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/cart_badge.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/model/product_single.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-product-by-category/page_product_by_category.dart';
import 'package:pekik_app/view/page-public-product/page_public_toko.dart';
import 'package:pekik_app/view/page-transaction/page_cart.dart';
// import 'package:pekik_app/view/widgets/widget_detail_image.dart';
import 'package:pekik_app/view/widgets/widget_loading_detail_product.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class PublicDetailProductPage extends StatefulWidget {
  const PublicDetailProductPage({Key key}) : super(key: key);

  @override
  _PublicDetailProductPageState createState() =>
      _PublicDetailProductPageState();
}

class _PublicDetailProductPageState extends State<PublicDetailProductPage> {
  ScrollController _scrollController;
  bool lastStatus = true;
  bool isLoading = true;
  CartBadgesModel cartBadgesModel;
  bool isActive = true;
  bool isStock = true;
  PublicDetailProductBloc detailProductBloc = PublicDetailProductBloc();
  int _current = 0;
  String _linkMessage;
  bool _isCreatingLink = false;

  _scrollListener() {
    if (isShrink != lastStatus) {
      setState(() {
        lastStatus = isShrink;
      });
    }
  }

  bool get isShrink {
    return _scrollController.hasClients &&
        _scrollController.offset > (350 - kToolbarHeight);
  }

  @override
  void initState() {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    detailProductBloc
        .add(GetPublicDetailProduct(idProduct: basket['idProduct']));
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      setState(() {
        _scrollListener();
      });
    });
    _getRequest();
    super.initState();
  }

  Future<void> onRefresh() async {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    setState(() {
      detailProductBloc
          .add(GetPublicDetailProduct(idProduct: basket['idProduct']));
    });
  }

  _getRequest() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    if (_auth.currentUser != null) {
      await transactionVM.fetchCartBadge().then((value) {
        if (value != null) {
          setState(() {
            cartBadgesModel = value;
            isActive = false;
          });
        } else {
          setState(() {
            isActive = true;
          });
        }
      });
    }
  }

  Future<void> _createDynamicLink(
      bool short,
      String tenantName,
      String productCode,
      String productName,
      String productImage,
      String desc,
      String price) async {
    print(productImage);
    setState(() {
      _isCreatingLink = true;
    });

    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://pekik.page.link',
      link: Uri.parse('https://connexist.com/$productCode'),
      socialMetaTagParameters: SocialMetaTagParameters(
          imageUrl: Uri.parse(
            productImage,
          ),
          title: productName,
          description: desc),
      androidParameters: AndroidParameters(
          packageName: 'com.connexist.pekik_app',
          minimumVersion: 0,
          fallbackUrl: Uri.parse(
              'https://play.google.com/store/apps/details?id=com.connexist.pekik_app')),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
          bundleId: 'com.connexist.pekik',
          minimumVersion: '0',
          fallbackUrl: Uri.parse(
              'https://apps.apple.com/id/app/pekik-jkt/id1571444736')),
    );

    Uri url;
    if (short) {
      final ShortDynamicLink shortLink = await parameters.buildShortLink();
      url = shortLink.shortUrl;
    } else {
      url = await parameters.buildUrl();
    }

    setState(() {
      _linkMessage = url.toString();
      _isCreatingLink = false;
    });

    share(productName, tenantName, price, _linkMessage);
  }

  Future<void> share(
      String productName, String tenantName, String price, String link) async {
    var harga = ConnexistHelper.formatCurrency(double.parse(price));
    await FlutterShare.share(
        title: 'Beli $productName',
        text:
            'Beli $productName $link di toko $tenantName $harga di Aplikasi Pekik ',
        linkUrl: '',
        chooserTitle: '');
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    print(basket['idProduct']);
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, true);
      },
      child: Scaffold(
        body: _buildProduct(),
      ),
    );
  }

  Widget _buildProduct() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => detailProductBloc,
        child: BlocListener<PublicDetailProductBloc, PublicDetailProductState>(
          listener: (context, state) {
            if (state is PublicDetailProductError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<PublicDetailProductBloc, PublicDetailProductState>(
            builder: (context, state) {
              if (state is PublicDetailProductInitial) {
                return LoadingDetailProductWidget();
              } else if (state is PublicDetailProductLoading) {
                return LoadingDetailProductWidget();
              }
              if (state is PublicDetailProductLoaded) {
                return _build(context, state.productModel);
              }
              if (state is PublicDetailProductError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _build(BuildContext context, ProductSingleModel productSingleModel) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    if (productSingleModel.data.tenant.tenantPhone.startsWith('0')) {
      productSingleModel.data.tenant.tenantPhone = '+62' +
          productSingleModel.data.tenant.tenantPhone
              .replaceFirst(new RegExp(r'0'), '');
    } else {
      productSingleModel.data.tenant.tenantPhone =
          productSingleModel.data.tenant.tenantPhone;
    }
    print("Nomor");
    print(productSingleModel.data.tenant.tenantPhone);
    var whatsappUrl =
        "whatsapp://send?phone=${productSingleModel.data.tenant.tenantPhone}";
    return Stack(
      children: [
        CustomScrollView(
          controller: _scrollController,
          slivers: <Widget>[
            SliverAppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(
                color: isShrink ? Colors.black : Colors.white,
              ),
              leading: GestureDetector(
                onTap: () => Navigator.pop(context, true),
                child: Center(
                  child: Platform.isIOS
                      ? Container(
                          margin: EdgeInsets.only(left: 8),
                          child: isShrink
                              ? Icon(Icons.arrow_back_ios)
                              : Icon(Icons.arrow_back_ios, color: Colors.white),
                        )
                      : isShrink
                          ? Icon(Icons.arrow_back, color: Colors.black)
                          : Icon(Icons.arrow_back, color: Colors.white),
                ),
              ),
              pinned: true,
              expandedHeight: 350.0,
              floating: true,
              title: isShrink ? _searchCard() : Container(),
              titleSpacing: 0,
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: Stack(
                        children: [
                          Center(
                              child: getImages(
                                  productSingleModel.data.productImage)),
                        ],
                      ),
                    ),
                    SafeArea(
                        child: Container(
                      height: 15,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            spreadRadius: 25,
                            blurRadius: 30,
                            offset: Offset(0, 4), // changes position of shadow
                          ),
                        ],
                        // color: Colors.black54
                      ),
                    )),
                  ],
                ),
              ),
              actions: [
                cartBadgesModel != null
                    ? _shoppingCartBadge()
                    : isShrink
                        ? Padding(
                            padding: const EdgeInsets.only(left: 16, right: 10),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return CartPage();
                                }));
                              },
                              child: Center(
                                  child: Icon(
                                Icons.shopping_cart_outlined,
                                color: Colors.black,
                              )),
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.only(left: 16, right: 10),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return CartPage();
                                }));
                              },
                              child: Center(
                                  child: Icon(
                                Icons.shopping_cart,
                                color: Colors.white,
                              )),
                            ),
                          ),
                GestureDetector(
                  onTap: () {
                    if (!_isCreatingLink) {
                      _createDynamicLink(
                        true,
                        productSingleModel.data.tenant.tenantName,
                        productSingleModel.data.id,
                        productSingleModel.data.productName,
                        productSingleModel.data.productImage.split(',').first,
                        productSingleModel.data.productDesc,
                        productSingleModel.data.productPrice.toString(),
                      );
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: Center(
                      child: Icon(
                        isShrink ? Icons.share_outlined : Icons.share,
                        color: isShrink ? Colors.black : Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
            SliverList(
                delegate: SliverChildListDelegate(<Widget>[
              Container(
                width: double.infinity,
                padding:
                    EdgeInsets.only(left: 16, right: 16, bottom: 10, top: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              ConnexistHelper.formatCurrency(productSingleModel
                                  .data.productPrice
                                  .toDouble()),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold)),
                          // Icon(Icons.favorite_border,
                          //     color: Colors.black, size: 30)
                        ],
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 8.0),
                      child: Text(productSingleModel.data.productName,
                          textAlign: TextAlign.start,
                          style: TextStyle(fontSize: 16.0)),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: 8.0),
                      child: Text(
                          productSingleModel.data.productStock < 1
                              ? "Stok Habis"
                              : "Stok  " +
                                  productSingleModel.data.productStock
                                      .toString(),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight:
                                  isStock ? FontWeight.normal : FontWeight.bold,
                              color: productSingleModel.data.productStock < 1
                                  ? Colors.red
                                  : isStock
                                      ? Colors.black
                                      : Colors.red)),
                    ),
                    // : Container(),
                  ],
                ),
              ),
              Divider(
                thickness: 15,
                color: Colors.grey[100],
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return PublicTokoPage(
                        idTenant: productSingleModel.data.tenantId);
                  })).then((val) => val ? _getRequest() : null);
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 16, bottom: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55)),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(55),
                            child: CachedNetworkImage(
                              imageUrl:
                                  productSingleModel.data.tenant.tenantLogo,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Center(
                                  child: Shimmer.fromColors(
                                baseColor: Colors.grey[300],
                                highlightColor: Colors.grey[100],
                                child: Container(
                                  color: Colors.white,
                                  width: double.infinity,
                                  height: double.infinity,
                                ),
                              )),
                              errorWidget: (context, url, error) =>
                                  Image.asset("assets/default_toko.jpg"),
                            ),
                          )),
                      SizedBox(width: 15),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                productSingleModel.data.tenant.tenantType ==
                                        'official'
                                    ? Image.asset(
                                        'assets/ic_official_store.png',
                                        width: 20,
                                      )
                                    : Container(),
                                SizedBox(
                                  width: productSingleModel
                                              .data.tenant.tenantType ==
                                          'official'
                                      ? 5
                                      : 0,
                                ),
                                productSingleModel
                                            .data.tenant.tenantName.length >
                                        28
                                    ? Text(
                                        productSingleModel
                                                .data.tenant.tenantName
                                                .substring(0, 28) +
                                            "...",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold))
                                    : Text(
                                        productSingleModel
                                            .data.tenant.tenantName,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold)),
                              ],
                            ),
                            SizedBox(height: 2),
                            Text(productSingleModel.data.tenant.city,
                                style: TextStyle(
                                    color: ColorPalette.fontColor,
                                    fontSize: 14)),
                          ],
                        ),
                      ),
                      SizedBox(width: 15),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 20,
                      )
                    ],
                  ),
                ),
              ),
              Divider(
                thickness: 15,
                color: Colors.grey[100],
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: Text(
                  "Informasi Produk",
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                child: Column(
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Berat",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          Container(
                            child: Text(
                                productSingleModel.data.productWeight
                                        .toString() +
                                    " Gram",
                                style: TextStyle(
                                    color: ColorPalette.fontColor,
                                    fontSize: 16)),
                          )
                        ]),
                    SizedBox(height: 10),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Dimensi",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          Container(
                            child: Text(
                                productSingleModel.data.productDimensions,
                                style: TextStyle(
                                    color: ColorPalette.fontColor,
                                    fontSize: 16)),
                          )
                        ]),
                    SizedBox(height: 10),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Pemesanan Min",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          Container(
                            child: Text(
                                productSingleModel.data.minimumOrder
                                        .toString() +
                                    " buah",
                                style: TextStyle(
                                    color: isStock
                                        ? ColorPalette.fontColor
                                        : Colors.red,
                                    fontWeight: isStock
                                        ? FontWeight.normal
                                        : FontWeight.bold,
                                    fontSize: 16)),
                          )
                        ]),
                    SizedBox(height: 10),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Kategori",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return ProductByCategoryPage(
                                    title: productSingleModel
                                        .data.category.categoryName,
                                    tenantType: '',
                                    tenantId: '',
                                    idCategory:
                                        productSingleModel.data.category.id);
                              }));
                            },
                            child: Text(
                                productSingleModel.data.category.categoryName,
                                style: TextStyle(
                                    fontSize: 16,
                                    color: ColorPalette.themeIcon)),
                          )
                        ]),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              Divider(
                thickness: 15,
                color: Colors.grey[100],
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: Text(
                  "Deskripsi",
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                child: productSingleModel.data.productDesc.length > 200
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            productSingleModel.data.productDesc
                                    .substring(0, 200) +
                                "...",
                            style: TextStyle(
                              fontSize: 16,
                              color: ColorPalette.fontColor,
                            ),
                          ),
                          SizedBox(height: 8),
                          GestureDetector(
                            onTap: () {
                              showBarModalBottomSheet(
                                expand: false,
                                context: context,
                                backgroundColor: Colors.transparent,
                                builder: (context) => _modalDeskripsi(
                                    context, productSingleModel),
                              );
                            },
                            child: Text(
                              "Baca Selengkapnya",
                              style: TextStyle(
                                fontSize: 16,
                                color: ColorPalette.themeIcon,
                              ),
                            ),
                          )
                        ],
                      )
                    : Text(
                        productSingleModel.data.productDesc,
                        style: TextStyle(
                          fontSize: 16,
                          color: ColorPalette.fontColor,
                        ),
                      ),
              ),
              Divider(
                thickness: 15,
                color: Colors.grey[100],
              ),
            ])),
            _getDataProdukByTenant(productSingleModel.data.tenantId),
            SliverToBoxAdapter(
                child: SizedBox(
              height: 80,
            )),
          ],
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: 70,
                width: double.infinity,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 4), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        print(whatsappUrl);
                        launch(whatsappUrl);
                      },
                      child: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border:
                                Border.all(width: 1, color: Colors.grey[400])),
                        child: Center(
                          child: Icon(
                            Icons.chat_outlined,
                            size: 30,
                            color: ColorPalette.themeIcon,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () async {
                          if (productSingleModel.data.productStock > 0) {
                            if (_auth.currentUser == null) {
                              Navigator.pushNamed(context, "/login");
                            } else {
                              if (productSingleModel.data.productStock <
                                  productSingleModel.data.minimumOrder) {
                                setState(() {
                                  isStock = false;
                                });
                                Fluttertoast.showToast(
                                    msg: "Stok Tidak Cukup",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white);
                              } else {
                                setState(() {
                                  isLoading = false;
                                });
                                CHttp _chttp =
                                    Provider.of<CHttp>(context, listen: false);
                                TransactionViewModel transactionVM =
                                    TransactionViewModel(http: _chttp);
                                await transactionVM
                                    .postCart(productSingleModel.data.id)
                                    .then((value) {
                                  if (value != null) {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    Fluttertoast.showToast(
                                        msg: "Tambah keranjang berhasil",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIos: 1,
                                        backgroundColor: ColorPalette.btnGreen,
                                        textColor: Colors.white);
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return CartPage();
                                    })).then(
                                        (val) => val ? _getRequest() : null);
                                  } else {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    Fluttertoast.showToast(
                                        msg:
                                            "Terjadi kesalahan saat berkomunikasi dengan server",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIos: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white);
                                  }
                                });
                              }
                            }
                          } else {
                            Fluttertoast.showToast(
                                msg: "Stok Habis",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          }
                        },
                        child: Container(
                          height: 60,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(60),
                              color: productSingleModel.data.productStock > 0
                                  ? ColorPalette.themeIcon
                                  : ColorPalette.fontColor),
                          child: Center(
                            child: isLoading
                                ? Text(
                                    'Tambah Keranjang',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  )
                                : SizedBox(
                                    height: 20.0,
                                    width: 20.0,
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.white),
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )))
      ],
    );
  }

  Widget getImages(String imageUrl) {
    return Container(
      child: Stack(
        children: [
          CarouselSlider(
            items: imageUrl.split(',').map((fileImage) {
              return GestureDetector(
                onTap: () {
                  _modalBottomimages(context, _current, imageUrl);
                },
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ClipRRect(
                      child: CachedNetworkImage(
                    imageUrl: fileImage,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Center(
                        child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        color: Colors.white,
                        width: double.infinity,
                        height: double.infinity,
                      ),
                    )),
                    errorWidget: (context, url, error) => Center(
                        child: Image.asset(
                      "assets/default_image.png",
                      fit: BoxFit.cover,
                      width: double.infinity,
                      height: double.infinity,
                    )),
                  )),
                ),
              );
            }).toList(),
            options: CarouselOptions(
                height: double.infinity,
                enableInfiniteScroll: false,
                aspectRatio: 16 / 9,
                autoPlay: false,
                viewportFraction: 1.0,
                onPageChanged: (index, reason) {
                  setState(() => _current = index);
                }),
          ),
          imageUrl.split(',').length != 1
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: imageUrl.split(',').map((url) {
                      int index = imageUrl.split(',').indexOf(url);
                      return Container(
                        width: 8.0,
                        height: 8.0,
                        margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 2.0),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _current == index
                                ? Colors.red[600]
                                : Colors.grey[300]),
                      );
                    }).toList(),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  void _modalBottomimages(BuildContext context, int number, String imageUrl) {
    PageController pageController = PageController(initialPage: number);

    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        enableDrag: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        context: context,
        builder: (BuildContext cn) {
          return Container(
            decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10))),
            child: Scaffold(
                backgroundColor: Colors.black,
                body: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: PhotoViewGallery.builder(
                        pageController: pageController,
                        itemCount: imageUrl.split(',').length,
                        loadingBuilder: (context, event) => Center(
                          child: Container(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        builder: (context, index) {
                          return PhotoViewGalleryPageOptions(
                            imageProvider:
                                NetworkImage(imageUrl.split(',')[index]),
                            initialScale: PhotoViewComputedScale.contained * 1,
                            heroAttributes: PhotoViewHeroAttributes(
                                tag: imageUrl.split(',')[index]),
                          );
                        },
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding:
                                EdgeInsets.only(left: 16, right: 16, top: 40),
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                            ),
                          )),
                    ),
                  ],
                )),
          );
        });
  }

  Widget _shoppingCartBadge() {
    return cartBadgesModel.data.shoppingCartBadges == 0
        ? Padding(
            padding: EdgeInsets.only(left: 16, right: 10),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CartPage();
                })).then((val) => val ? _getRequest() : null);
              },
              child: isShrink
                  ? Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.black,
                    )
                  : Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(left: 4, right: 2),
            child: Badge(
              position: BadgePosition.topEnd(top: 3, end: 6),
              animationDuration: Duration(milliseconds: 300),
              animationType: BadgeAnimationType.slide,
              badgeContent: Text(
                cartBadgesModel.data.shoppingCartBadges.toString(),
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
              child: IconButton(
                  icon: isShrink
                      ? Icon(
                          Icons.shopping_cart_outlined,
                          color: Colors.black,
                        )
                      : Icon(
                          Icons.shopping_cart,
                          color: Colors.white,
                        ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CartPage();
                    })).then((val) => val ? _getRequest() : null);
                  }),
            ),
          );
  }

  Widget _searchCard() => GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, "/search")
              .then((val) => val ? _getRequest() : null);
        },
        child: Container(
          height: 45.0,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Color(0xffF0F1F5),
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(9.0, 6.0, 9.0, 6.0),
                child: Icon(
                  Icons.search,
                  color: Color(0xffC4C6CC),
                ),
              ),
              Text(
                "Cari Produk atau Toko",
                style: TextStyle(
                  color: Color(0xffC4C6CC),
                  fontSize: 14.0,
                ),
              )
            ],
          ),
        ),
      );
  Widget _modalDeskripsi(
      BuildContext context, ProductSingleModel productSingleModel) {
    return SafeArea(
      bottom: false,
      child: ListView(
        shrinkWrap: true,
        controller: ModalScrollController.of(context),
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.90,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Text("Deskripsi Produk",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                ),
                SizedBox(height: 8),
                Divider(
                  thickness: 1,
                ),
                SizedBox(height: 8),
                Expanded(
                  flex: 40,
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(16),
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 70,
                              height: 70,
                              child: Stack(
                                children: [
                                  Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(12),
                                        child: CachedNetworkImage(
                                          imageUrl: productSingleModel
                                              .data.productImage
                                              .split(',')
                                              .first,
                                          fit: BoxFit.cover,
                                          placeholder: (context, url) => Center(
                                              child: Shimmer.fromColors(
                                            baseColor: Colors.grey[300],
                                            highlightColor: Colors.grey[100],
                                            child: Container(
                                              color: Colors.white,
                                              width: double.infinity,
                                              height: double.infinity,
                                            ),
                                          )),
                                          errorWidget: (context, url, error) =>
                                              Center(
                                                  child: Image.asset(
                                            "assets/default_image.png",
                                            fit: BoxFit.cover,
                                          )),
                                        ),
                                      )),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  productSingleModel.data.productName.length >
                                          90
                                      ? Text(
                                          productSingleModel.data.productName
                                                  .substring(0, 90) +
                                              "...",
                                          maxLines: 2,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                          ),
                                        )
                                      : Text(
                                          productSingleModel.data.productName,
                                          maxLines: 2,
                                          style: TextStyle(
                                            fontSize: 15.0,
                                          ),
                                        ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                      ConnexistHelper.formatCurrency(
                                          productSingleModel.data.productPrice
                                              .toDouble()),
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  productSingleModel.data.productStock == 0
                                      ? Text("Stok Habis ",
                                          style: TextStyle(
                                              fontSize: 14.0,
                                              color: ColorPalette.btnRed))
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text("STOK: ",
                                                style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: ColorPalette
                                                        .themeIcon)),
                                            SizedBox(
                                              height: 8,
                                            ),
                                            Text(
                                                productSingleModel
                                                    .data.productStock
                                                    .toString(),
                                                style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: ColorPalette
                                                        .themeIcon)),
                                          ],
                                        ),
                                ],
                              ),
                            ),
                          ]),
                      SizedBox(
                        height: 8,
                      ),
                      Divider(
                        thickness: 2,
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        productSingleModel.data.productDesc,
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _getDataProdukByTenant(String idTenant) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    PublicProductViewModel productViewModel = PublicProductViewModel();
    return SliverToBoxAdapter(
      child: FutureBuilder(
        future: productViewModel.fetchProductFilter('', idTenant, '', ''),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final ProductModel productModel = snapshot.data;
            var filter = productModel.data
                .where((element) => element.id != basket['idProduct'])
                .toList();
            return filter.length == 0
                ? Container()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 16, right: 16, top: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Lainnya di toko ini',
                              style: TextStyle(
                                  // fontFamily: "Caveat",
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold
                                  // color: kKepasarTeal
                                  ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return PublicTokoPage(idTenant: idTenant);
                                })).then((val) => val ? _getRequest() : null);
                              },
                              child: Text(
                                "Lihat Semua",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 300.0,
                        child: ListView.builder(
                          padding:
                              EdgeInsets.only(left: 16, right: 16, top: 16),
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemCount:
                              filter.length > 5 ? 5 + 1 : filter.length + 1,
                          itemBuilder: (context, index) {
                            if (filter.length == index) {
                              return Container(
                                width: 160,
                                child: Card(
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(10.0),
                                    onTap: () {
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return PublicTokoPage(
                                            idTenant: idTenant);
                                      })).then(
                                          (val) => val ? _getRequest() : null);
                                    },
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.5),
                                                spreadRadius: 3,
                                                blurRadius: 3,
                                                offset: Offset(0,
                                                    2), // changes position of shadow
                                              ),
                                            ],
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: ColorPalette.themeIcon,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 16,
                                        ),
                                        Text(
                                          'Lihat Semua',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            } else if (5 == index) {
                              return Container(
                                width: 160,
                                child: Card(
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: InkWell(
                                    borderRadius: BorderRadius.circular(10.0),
                                    onTap: () {
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return PublicTokoPage(
                                            idTenant: idTenant);
                                      })).then(
                                          (val) => val ? _getRequest() : null);
                                    },
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.5),
                                                spreadRadius: 3,
                                                blurRadius: 3,
                                                offset: Offset(0,
                                                    2), // changes position of shadow
                                              ),
                                            ],
                                            borderRadius:
                                                BorderRadius.circular(50),
                                          ),
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: ColorPalette.themeIcon,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 16,
                                        ),
                                        Text(
                                          'Lihat Semua',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.black),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }
                            return Container(
                              width: 160,
                              child: Card(
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(10.0),
                                  onTap: () {
                                    basket.addAll(
                                        {"idProduct": filter[index].id});
                                    Navigator.pushNamed(
                                            context, "/detail-produk")
                                        .then((val) =>
                                            val ? _getRequest() : null);
                                  },
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      AspectRatio(
                                        aspectRatio: 7 / 5.8,
                                        child: Container(
                                          // height: imageHeight,
                                          // width: imageWidth,
                                          child: ClipRRect(
                                            child: CachedNetworkImage(
                                              imageUrl: filter[index]
                                                  .productImage
                                                  .split(',')
                                                  .first,
                                              fit: BoxFit.cover,
                                              placeholder: (context, url) =>
                                                  Center(
                                                      child: Shimmer.fromColors(
                                                baseColor: Colors.grey[300],
                                                highlightColor:
                                                    Colors.grey[100],
                                                child: Container(
                                                  color: Colors.white,
                                                  width: double.infinity,
                                                  height: double.infinity,
                                                ),
                                              )),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Center(
                                                          child: Image.asset(
                                                "assets/default_image.png",
                                                fit: BoxFit.cover,
                                                width: double.infinity,
                                              )),
                                            ),
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(10.0),
                                                topRight:
                                                    Radius.circular(10.0)),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(12.0),
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${filter[index].productName}",
                                              maxLines: 2,
                                              style: TextStyle(
                                                fontSize: 16.0,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              ConnexistHelper.formatCurrency(
                                                  filter[index]
                                                      .productPrice
                                                      .toDouble()),
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              "${filter[index].tenant.city}",
                                              maxLines: 1,
                                              style: TextStyle(
                                                fontSize: 12.0,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            filter[index].tenant.tenantType ==
                                                    'official'
                                                ? Row(
                                                    children: [
                                                      Image.asset(
                                                        'assets/ic_official_store.png',
                                                        width: 20,
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        "Official Store",
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize: 14.0,
                                                            color: ColorPalette
                                                                .btnGreen),
                                                      ),
                                                    ],
                                                  )
                                                : filter[index].productStock < 1
                                                    ? Text(
                                                        "Stok Habis",
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize: 12.0,
                                                            color: Colors.red),
                                                      )
                                                    : filter[index]
                                                                .productStock <
                                                            3
                                                        ? Text(
                                                            "Stok Hampir Habis",
                                                            maxLines: 1,
                                                            style: TextStyle(
                                                                fontSize: 12.0,
                                                                color:
                                                                    Colors.red),
                                                          )
                                                        : Text(
                                                            "Stok Tersedia",
                                                            maxLines: 1,
                                                            style: TextStyle(
                                                                fontSize: 12.0,
                                                                color: Colors
                                                                    .black),
                                                          ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
          }
          return _loadingList();
        },
      ),
    );
  }

  Widget _loadingList() {
    return Container(
      height: 320,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 16),
            child: Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 150,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                  ),
                  Container(
                    height: 15,
                    width: 80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 280,
            child: ListView.builder(
              padding: EdgeInsets.all(16),
              physics: ScrollPhysics(),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: 6,
              itemBuilder: (context, index) {
                return Container(
                  width: 160,
                  child: Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 140,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(15),
                                  topLeft: Radius.circular(15),
                                ),
                                color: Colors.white),
                          ),
                          SizedBox(height: 10),
                          Container(
                            width: 150,
                            height: 15.0,
                            margin:
                                EdgeInsets.only(left: 8, right: 8, bottom: 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white),
                          ),
                          Container(
                            width: 100,
                            height: 15.0,
                            margin: EdgeInsets.only(left: 8, right: 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

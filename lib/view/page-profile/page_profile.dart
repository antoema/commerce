import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/content_view.dart';
import 'package:pekik_app/view/page-profile/page_edit_profile.dart';
import 'package:pekik_app/view/page-transaction/page_select_address.dart';
import 'package:pekik_app/view/page-wallet/page_bank_account.dart';
import 'package:pekik_app/view/page-wallet/page_wallet.dart';
import 'package:pekik_app/view/page_about.dart';
import 'package:provider/provider.dart';
import 'package:package_info/package_info.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    version: 'Unknown',
  );
  User userAuth;
  bool loading = true;
  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    CHttp chttp = Provider.of<CHttp>(context, listen: false);
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);

    if (loading) {
      chttp.auth.loadUser().then((user) {
        if (user != null) {
          setState(() {
            userAuth = user;
            loading = false;
          });
        }
      }).catchError((onError) {
        print(onError);
      });
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: ColorPalette.themeIcon),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text(
            "Profil",
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
          actions: [
            // Padding(
            //     padding: EdgeInsets.only(right: 20.0),
            //     child: GestureDetector(
            //         onTap: () {
            //           Navigator.pushNamed(context, '/setting-account');
            //         },
            //         child: Column(
            //             mainAxisAlignment: MainAxisAlignment.center,
            //             mainAxisSize: MainAxisSize.min,
            //             children: [
            //               Icon(
            //                 Icons.settings,
            //                 color: Colors.grey,
            //               )
            //             ]))),
          ],
        ),
        body: ListView(children: [
          // Container(
          //   margin: EdgeInsets.all(10),
          //   height: 250,
          //   width: double.infinity,
          //   child: Stack(children: [
          //     Card(
          //       elevation: 4,
          //       shape: RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(15)),
          //       child: Container(
          //           width: double.infinity,
          //           height: double.infinity,
          //           child: ClipRRect(
          //             borderRadius: BorderRadius.circular(15),
          //             child:
          //                 Image.asset('assets/ic_card.png', fit: BoxFit.fill),
          //           )),
          //     ),
          //     Align(
          //       alignment: Alignment.bottomLeft,
          //       child: Container(
          //         height: 80,
          //         margin:
          //             EdgeInsets.only(left: 16, right: 16, bottom: 10, top: 20),
          //         width: double.infinity,
          //         child: Column(
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Text(
          //               _auth.currentUser.data.name,
          //               maxLines: 2,
          //               textAlign: TextAlign.end,
          //               style: TextStyle(
          //                   fontWeight: FontWeight.bold,
          //                   fontSize: 18.0,
          //                   color: ColorPalette.themeColor),
          //             ),
          //           ],
          //         ),
          //       ),
          //     ),
          //   ]),
          // ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    width: 75,
                    height: 75,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(45)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(45),
                      child: CachedNetworkImage(
                        imageUrl: _auth.currentUser == null
                            ? ''
                            : _auth.currentUser.data.avatarUrl == null
                            ? ''
                            : _auth.currentUser.data.avatarUrl,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => Center(
                            child: Shimmer.fromColors(
                          baseColor: Colors.grey[300],
                          highlightColor: Colors.grey[100],
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            height: double.infinity,
                          ),
                        )),
                        errorWidget: (context, url, error) =>
                            Image.asset("assets/default_profile.png"),
                      ),
                    )),
                SizedBox(width: 15),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          _auth.currentUser.data.name.length > 25
                              ? Text(
                                  _auth.currentUser.data.name.substring(0, 25) +
                                      "...",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold))
                              : Text(_auth.currentUser.data.name,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                        ],
                      ),
                      SizedBox(height: 4),
                      Text(_auth.currentUser.data.phoneNumber,
                          style: TextStyle(color: Colors.black, fontSize: 14)),
                      Text(_auth.currentUser.data.email,
                          style: TextStyle(color: Colors.black, fontSize: 14)),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        new MaterialPageRoute(builder: (context) {
                      return EditProfilePage();
                    }));
                  },
                  child: Icon(
                    Icons.edit_outlined,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ],
            ),
          ),
          WalletPage(),
          Container(
            margin: EdgeInsets.only(left: 12, right: 12, top: 16),
            child: Card(
              elevation: 2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Pengaturan Akun",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return SelectAddressPage();
                        }));
                      },
                      child: Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.home_outlined,
                            color: Colors.grey[600],
                            size: 30,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Daftar Alamat",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  "Atur alamat pengiriman belanjaan anda",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return BankAccountPage();
                        }));
                      },
                      child: Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.credit_card,
                            color: Colors.grey[600],
                            size: 30,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Rekening Bank",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  "Tarik saldo ke rekening tujuan",
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Seputar Pekik JKT",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => AboutPage()));
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.info_outline,
                            color: Colors.grey[600],
                            size: 30,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Text("Tentang",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => MyWebview(
                                  title: "Term Of Service",
                                  url: "https://connexist.com/term-of-sevice",
                                )));
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.assignment_late_outlined,
                            color: Colors.grey[600],
                            size: 30,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Text("Term of Service",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => MyWebview(
                                  title: "Bantuan",
                                  url: "https://connexist.com/mobile-bantuan",
                                )
                            // BantuanPage()
                            ));
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.flag_outlined,
                            color: Colors.grey[600],
                            size: 30,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Text("Bantuan",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () => Platform.isIOS
                          ? _launchURL(
                              "https://apps.apple.com/id/app/pekik-jkt/id1571444736")
                          : _launchURL(
                              'https://play.google.com/store/apps/details?id=com.connexist.pekik_app'),
                      child: Row(
                        children: [
                          Icon(
                            Icons.stars_outlined,
                            color: Colors.grey[600],
                            size: 30,
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: Text("Ulas Aplikasi",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700)),
                          ),
                          Text(
                            "PEKIK JKT v ${_packageInfo.version}",
                            style: TextStyle(color: Colors.grey, fontSize: 14),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ]));
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

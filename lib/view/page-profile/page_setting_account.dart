import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/view/page-profile/page_edit_profile.dart';
import 'package:pekik_app/view/page-transaction/page_select_address.dart';

class SettingAccountPage extends StatefulWidget {
  @override
  _SettingAccountPageState createState() => _SettingAccountPageState();
}

class _SettingAccountPageState extends State<SettingAccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Pengaturan Akun",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.push(context, new MaterialPageRoute(builder: (context) {
                return EditProfilePage();
              }));
            },
            child: Container(
                margin: EdgeInsets.all(16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(
                          Icons.person_outlined,
                          color: Colors.black,
                          size: 30,
                        )),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Ubah Profil",
                            style: TextStyle(fontSize: 16),
                          ),
                          Text(
                            "Tambahkan informasi di akun Anda",
                            style: TextStyle(fontSize: 12, color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Icon(Icons.arrow_forward_ios)
                  ],
                )),
          ),
          // Divider(),
          // InkWell(
          //   onTap: () {},
          //   child: Container(
          //       padding: EdgeInsets.all(16),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.start,
          //         children: <Widget>[
          //           Container(
          //               margin: EdgeInsets.only(right: 10),
          //               child: Image.asset(
          //                 "assets/change_password.png",
          //                 width: 32,
          //                 height: 32,
          //               )),
          //           Container(
          //             width: 270,
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: <Widget>[
          //                 Text(
          //                   "Ubah Kata Sandi",
          //                   style: TextStyle(fontSize: 16),
          //                 ),
          //                 Text(
          //                   "Ubah kata sandi unik untuk melindungi akun Anda",
          //                   style: TextStyle(fontSize: 12, color: Colors.grey),
          //                 ),
          //               ],
          //             ),
          //           ),
          //           Expanded(
          //               child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.end,
          //             children: <Widget>[
          //               Icon(Icons.arrow_forward_ios),
          //             ],
          //           ))
          //         ],
          //       )),
          // ),
          // Divider(),
          Divider(),
          InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return SelectAddressPage();
              }));
            },
            child: Container(
                padding: EdgeInsets.all(16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(
                          Icons.home_outlined,
                          color: Colors.black,
                          size: 30,
                        )),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Daftar Alamat",
                            style: TextStyle(fontSize: 16),
                          ),
                          Text(
                            "Atur alamat pengiriman belanjaan anda",
                            style: TextStyle(fontSize: 12, color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Icon(Icons.arrow_forward_ios)
                  ],
                )),
          ),
          Divider(),
          // InkWell(
          //   onTap: () {
          //     Navigator.push(context, MaterialPageRoute(builder: (context) {
          //       return BankAccountPage();
          //     }));
          //   },
          //   child: Container(
          //       padding: EdgeInsets.all(16),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.start,
          //         children: <Widget>[
          //           Container(
          //               margin: EdgeInsets.only(right: 10),
          //               child: Icon(
          //                 Icons.credit_card,
          //                 color: Colors.black,
          //                 size: 30,
          //               )),
          //           Expanded(
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: <Widget>[
          //                 Text(
          //                   "Rekening Bank",
          //                   style: TextStyle(fontSize: 16),
          //                 ),
          //                 Text(
          //                   "Tarik saldo ke rekening tujuan",
          //                   style: TextStyle(fontSize: 12, color: Colors.grey),
          //                 ),
          //               ],
          //             ),
          //           ),
          //           SizedBox(
          //             width: 8,
          //           ),
          //           Icon(Icons.arrow_forward_ios)
          //         ],
          //       )),
          // ),
          // Divider(),
        ],
      ),
    );
  }
}

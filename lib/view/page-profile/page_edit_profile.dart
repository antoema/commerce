import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/view/page-home/widget/home_widget.dart';
import 'package:pekik_app/viewmodel/upload_image.dart';
import 'package:provider/provider.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  File f1;
  String _email;
  String _name;
  String _noktp;
  final formKey = new GlobalKey<FormState>();
  bool _isUploading = false;

  void _pickImage() async {
    final imageSource = await showDialog<ImageSource>(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(
                "Pilih sumber gambar",
                style:
                    TextStyle(fontWeight: FontWeight.bold, color: Colors.teal),
              ),
              actions: <Widget>[
                MaterialButton(
                  child: Text(
                    "Camera",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  onPressed: () => Navigator.pop(context, ImageSource.camera),
                ),
                MaterialButton(
                  child: Text(
                    "Gallery",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  onPressed: () => Navigator.pop(context, ImageSource.gallery),
                )
              ],
            ));

    if (imageSource != null) {
      final file =
          await ImagePicker.pickImage(source: imageSource, maxHeight: 720);
      if (file != null) {
        setState(() => f1 = file);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    CHttp _chttp = Provider.of(context, listen: false);
    Auth _auth = _chttp.auth;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Ubah Profil",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 10, bottom: 20),
                      child: Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Container(
                          padding: EdgeInsets.all(16),
                          child: Form(
                            key: formKey,
                            child: Column(
                              children: <Widget>[
                                Stack(children: [
                                  Center(
                                    child: Container(
                                        width: 150,
                                        height: 150,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(150),
                                          child: f1 == null
                                              ? CachedNetworkImage(
                                                  imageUrl: _auth.currentUser
                                                              .data.avatarUrl ==
                                                          null
                                                      ? ''
                                                      : _auth.currentUser.data
                                                          .avatarUrl,
                                                  fit: BoxFit.cover,
                                                  placeholder: (context, url) =>
                                                      CircularProgressIndicator(),
                                                  errorWidget: (context, url,
                                                          error) =>
                                                      Image.asset(
                                                          "assets/default_profile.png"),
                                                )
                                              : FadeInImage(
                                                  fit: BoxFit.cover,
                                                  image: FileImage(f1),
                                                  placeholder: AssetImage(
                                                      "assets/default_profile.png")),
                                        ),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                        )),
                                  ),
                                  Center(
                                    child: Container(
                                      width: 150,
                                      height: 150,
                                      child: Stack(
                                        children: <Widget>[
                                          Align(
                                            alignment: Alignment.bottomRight,
                                            child: GestureDetector(
                                              onTap: () {
                                                _pickImage();
                                              },
                                              child: Container(
                                                height: 50,
                                                width: 50,
                                                decoration: BoxDecoration(
                                                    boxShadow: const [
                                                      BoxShadow(blurRadius: 5),
                                                    ],
                                                    color:
                                                        ColorPalette.themeIcon,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            50)),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Icon(
                                                      Icons.photo_camera,
                                                      color: Colors.white,
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ]),
                                SizedBox(
                                  height: 30,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: TextFormField(
                                    initialValue: _auth.currentUser.data.name,
                                    decoration: InputDecoration(
                                      labelText: "Nama Lengkap",
                                      fillColor: Colors.white,
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(),
                                      ),
                                    ),
                                    onSaved: (newValue) => _name = newValue,
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                      fontFamily: "Poppins",
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: new TextFormField(
                                    initialValue:
                                        _auth.currentUser.data.identityNumber,
                                    decoration: new InputDecoration(
                                      labelText: "No KTP",
                                      fillColor: Colors.white,
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                            new BorderRadius.circular(5.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    onSaved: (newValue) => _noktp = newValue,
                                    keyboardType: TextInputType.number,
                                    style: new TextStyle(
                                      fontFamily: "Poppins",
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: new TextFormField(
                                    initialValue: _auth.currentUser.data.email,
                                    decoration: new InputDecoration(
                                      labelText: "Email",
                                      fillColor: Colors.white,
                                      border: new OutlineInputBorder(
                                        borderRadius:
                                            new BorderRadius.circular(5.0),
                                        borderSide: new BorderSide(),
                                      ),
                                    ),
                                    onSaved: (newValue) => _email = newValue,
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                      fontFamily: "Poppins",
                                    ),
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(bottom: 20.0)),
                                GestureDetector(
                                  onTap: () {
                                    // simpan gambar terlebih dahulu

                                    setState(() {
                                      _isUploading = true;
                                    });
                                    final form = formKey.currentState;
                                    if (form.validate()) {
                                      form.save();
                                      if (f1 != null) {
                                        ImageUploader.uploadImage(f1)
                                            .then((img) {
                                          String _imgUrl = img.data.path;
                                          return _auth.updateUser(
                                              _email, _name, _noktp, _imgUrl);
                                        }).then((userData) {
                                          if (userData != null) {
                                            Fluttertoast.showToast(
                                                msg: "Ubah Profil Sukses",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIos: 1,
                                                backgroundColor:
                                                    ColorPalette.btnGreen,
                                                textColor: Colors.white);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Home()));
                                          } else {
                                            Fluttertoast.showToast(
                                                msg:
                                                    "Terjadi kesalahan, mohon periksa kembali datanya",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIos: 1,
                                                backgroundColor: Colors.red,
                                                textColor: Colors.white);
                                          }
                                        });
                                      } else {
                                        print("object");
                                        _auth
                                            .updateUser(_email, _name, _noktp)
                                            .then((userData) {
                                          if (userData != null) {
                                            Fluttertoast.showToast(
                                                msg: "Ubah Profil Sukses",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIos: 1,
                                                backgroundColor:
                                                    ColorPalette.btnGreen,
                                                textColor: Colors.white);
                                            Navigator.push(context,
                                                new MaterialPageRoute(
                                                    builder: (context) {
                                              return Home();
                                            }));
                                          } else {
                                            Fluttertoast.showToast(
                                                msg:
                                                    "Terjadi kesalahan, mohon periksa kembali datanya",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIos: 1,
                                                backgroundColor: Colors.red,
                                                textColor: Colors.white);
                                            setState(() {
                                              _isUploading = false;
                                            });
                                          }
                                        });
                                      }
                                    }
                                  },
                                  child: Container(
                                    height: 55,
                                    margin: EdgeInsets.only(right: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(55),
                                      color: ColorPalette.themeIcon,
                                    ),
                                    child: Center(
                                      child: _isUploading
                                          ? Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                Text(
                                                  "Sedang update profile",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Container(
                                                    padding:
                                                        EdgeInsets.all(5.0),
                                                    child: Center(
                                                        child:
                                                            CircularProgressIndicator(
                                                      // backgroundColor: Colors.white,
                                                      valueColor:
                                                          AlwaysStoppedAnimation<
                                                                  Color>(
                                                              Colors.white),
                                                    ))),
                                              ],
                                            )
                                          : Text("Simpan",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold)),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ))
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}

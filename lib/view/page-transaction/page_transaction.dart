import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/bank.dart';
import 'package:pekik_app/model/cart_badge.dart';
import 'package:pekik_app/model/transaction_by_status.dart';
import 'package:pekik_app/model/transaction_waiting.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-transaction/page_cart.dart';
import 'package:pekik_app/view/page-transaction/page_detail_transaction.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class TransactionPage extends StatefulWidget {
  const TransactionPage({Key key}) : super(key: key);

  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage>
    with TickerProviderStateMixin {
  TabController tabController;
  CartBadgesModel cartBadgesModel;
  bool isActive = true;

  @override
  void initState() {
    _getRequest();
    super.initState();
    tabController = TabController(length: 6, vsync: this);
  }

  _getRequest() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    if (_auth.currentUser != null) {
      await transactionVM.fetchCartBadge().then((value) {
        if (value != null) {
          setState(() {
            cartBadgesModel = value;
            isActive = false;
          });
        } else {
          setState(() {
            isActive = true;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        elevation: 1,
        backgroundColor: Colors.white,
        title: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Text(
              "Transaksi",
              style: TextStyle(
                  color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        actions: [
          // Padding(
          //   padding: EdgeInsets.only(left: 16),
          //   child: GestureDetector(
          //     onTap: () {
          //       Navigator.push(context, MaterialPageRoute(builder: (context) {
          //         return ComingSoonWidget();
          //       }));
          //     },
          //     child: Icon(
          //       Icons.chat_outlined,
          //       color: Colors.black,
          //     ),
          //   ),
          // ),
          isActive
              ? Padding(
                  padding: EdgeInsets.only(left: 12, right: 16),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return CartPage();
                      })).then((val) => val ? _getRequest() : null);
                    },
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.black,
                    ),
                  ),
                )
              : _shoppingCartBadge()
        ],
      ),
      body: _auth.currentUser == null
          ? Padding(
              padding: const EdgeInsets.only(
                  left: 16, right: 16, bottom: 60, top: 16),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/ic_login.png',
                      width: 128,
                      height: 128,
                    ),
                    SizedBox(height: 25),
                    Container(
                      child: Text(
                        "Wah, Kamu belum masuk",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      child: Text(
                        "Silahkan masuk terlebih dahulu untuk melanjutkan transaksi",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16, color: Colors.grey[450]),
                      ),
                    ),
                    SizedBox(height: 25),
                    Container(
                        height: 65,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          // boxShadow: [
                          //   BoxShadow(
                          //     color: Colors.grey.withOpacity(0.5),
                          //     spreadRadius: 5,
                          //     blurRadius: 7,
                          //     offset: Offset(0, 4), // changes position of shadow
                          //   ),
                          // ],
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context, "/login");
                                },
                                child: Container(
                                  height: 55,
                                  margin: EdgeInsets.only(left: 25, right: 25),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(55),
                                      color: ColorPalette.themeIcon),
                                  child: Center(
                                    child: Text(
                                      "Masuk",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            )
          : Column(
              children: [
                TabBar(
                  isScrollable: true,
                  indicatorColor: ColorPalette.themeIcon,
                  labelColor: Colors.black,
                  tabs: [
                    Tab(text: "Belum Bayar"),
                    Tab(text: "Menunggu Konfirmasi"),
                    Tab(text: "Diproses"),
                    Tab(text: "Dikirim"),
                    Tab(text: "Tiba di Tujuan"),
                    Tab(text: "Dibatalkan"),
                  ],
                  controller: tabController,
                ),
                Expanded(
                  flex: 40,
                  child:
                      TabBarView(controller: tabController, children: <Widget>[
                    _waitingPayment(),
                    _transactionBuyerByStatus('received'),
                    _transactionBuyerByStatus('packing'),
                    _transactionBuyerByStatus('shipping'),
                    _transactionBuyerByStatus('delivered'),
                    _transactionBuyerByStatus('canceled'),
                  ]),
                ),
                // getToken()
              ],
            ),
    );
  }

  Widget _waitingPayment() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    return FutureBuilder<TransactionWaitingModel>(
      future: transactionVM.fetchTransactionWaiting(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final TransactionWaitingModel transactionModel = snapshot.data;
          if (transactionModel.data == null) {
            return _emptyTransaction();
          } else if (transactionModel.data.transaction.length == 0) {
            return _emptyTransaction();
          }
          return ListView(
            padding:
                const EdgeInsets.only(left: 16, right: 16, bottom: 70, top: 16),
            children: [
              Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 16, right: 16, top: 16, bottom: 8),
                      child: Text('Silahkan Lakukan Pembayaran',
                          style: TextStyle(
                            color: ColorPalette.fontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                          )),
                    ),
                    SizedBox(height: 5),
                    Divider(
                      thickness: 8,
                      color: Colors.grey[100],
                    ),
                    SizedBox(height: 8),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 16,
                        right: 16,
                        top: 16,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(transactionModel.data.payment.paymentChannel,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              )),
                          // ClipRRect(
                          //   child: CachedNetworkImage(
                          //     imageUrl: transactionUnpaid
                          //         .body[index].paymentChannel.logo,
                          //     height: 20,
                          //     fit: BoxFit.cover,
                          //     placeholder: (context, url) =>
                          //         Center(child: CircularProgressIndicator()),
                          //     errorWidget: (context, url, error) => Center(
                          //         child: Image.asset(
                          //       "assets/default_image.png",
                          //       height: 20,
                          //       fit: BoxFit.cover,
                          //     )),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Divider(
                      thickness: 2,
                      color: Colors.grey[100],
                    ),
                    SizedBox(height: 5),
                    Padding(
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Nomor Virtual Account",
                              style: TextStyle(
                                color: ColorPalette.fontColor,
                                fontSize: 14,
                              )),
                          SizedBox(height: 2),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(transactionModel.data.payment.paymentCode,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  )),
                              GestureDetector(
                                onTap: () {
                                  Clipboard.setData(ClipboardData(
                                          text: transactionModel
                                              .data.payment.paymentCode))
                                      .then((result) {
                                    showFloatingFlushbar(context);
                                  });
                                },
                                child: Text("Salin",
                                    style: TextStyle(
                                      color: ColorPalette.themeIcon,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    )),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Text("Total Pembayaran",
                              style: TextStyle(
                                color: ColorPalette.fontColor,
                                fontSize: 14,
                              )),
                          SizedBox(height: 2),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  ConnexistHelper.formatCurrency(
                                      transactionModel.data.totalAmount
                                          .toDouble()),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  )),
                              GestureDetector(
                                onTap: () {
                                  showBarModalBottomSheet(
                                    expand: false,
                                    context: context,
                                    backgroundColor: Colors.transparent,
                                    builder: (context) => modalDetailTagihan(
                                        context, transactionModel),
                                  );
                                },
                                child: Text("Lihat Detail",
                                    style: TextStyle(
                                      color: ColorPalette.themeIcon,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    )),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2,
                      color: Colors.grey[100],
                    ),
                    SizedBox(height: 5),
                    GestureDetector(
                      onTap: () {
                        showBarModalBottomSheet(
                          expand: false,
                          context: context,
                          backgroundColor: Colors.transparent,
                          builder: (context) => modalGuide(context,
                              transactionModel.data.payment.paymentChannel),
                        );
                      },
                      child: Center(
                        child: Text("Lihat Cara Pembayaran",
                            style: TextStyle(
                              color: ColorPalette.themeIcon,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            )),
                      ),
                    ),
                    SizedBox(height: 15),
                  ],
                ),
              ),
            ],
          );
        }

        return loadingListUnPaid();
      },
    );
  }

  Widget _transactionBuyerByStatus(String status) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    return FutureBuilder<TransactionByStatusModel>(
      future: transactionVM.fetchTransactionByStatus(status),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final TransactionByStatusModel transactionPaid = snapshot.data;
          if (transactionPaid == null) {
            return _emptyTransaction();
          } else if (transactionPaid.data.length == 0) {
            return _emptyTransaction();
          }

          return ListView.builder(
            padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 70),
            itemCount: transactionPaid.data.length,
            itemBuilder: (context, index) {
              return Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.only(bottom: 10),
                child: InkWell(
                  borderRadius: BorderRadius.circular(10),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return DetailTransactionPage(
                          idTrx: transactionPaid.data[index].id);
                    }));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(Icons.shopping_bag,
                                    color: ColorPalette.themeIcon),
                                SizedBox(width: 8),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Belanja",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        )),
                                    SizedBox(height: 2),
                                    Text(
                                        ConnexistHelper.formatDate(
                                            transactionPaid
                                                .data[index].createdAt),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                        )),
                                  ],
                                )
                              ],
                            )),
                            Text(
                                transactionPaid.data[index].status
                                    .toUpperCase(),
                                style: TextStyle(
                                  color: ColorPalette.themeIcon,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                ))
                          ],
                        ),
                        SizedBox(height: 5),
                        Divider(
                          thickness: 2,
                          color: Colors.grey[100],
                        ),
                        SizedBox(height: 5),
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 45,
                                height: 45,
                                child: Stack(
                                  children: [
                                    Container(
                                        width: double.infinity,
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: CachedNetworkImage(
                                            imageUrl: transactionPaid
                                                .data[index]
                                                .items
                                                .first
                                                .productImage
                                                .split(',')
                                                .first,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) =>
                                                Center(
                                                    child: Shimmer.fromColors(
                                              baseColor: Colors.grey[300],
                                              highlightColor: Colors.grey[100],
                                              child: Container(
                                                color: Colors.white,
                                                width: double.infinity,
                                                height: double.infinity,
                                              ),
                                            )),
                                            errorWidget:
                                                (context, url, error) => Center(
                                                    child: Image.asset(
                                              "assets/default_image.png",
                                              fit: BoxFit.cover,
                                            )),
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    transactionPaid.data[index].items.first
                                                .productName.length >
                                            75
                                        ? Text(
                                            transactionPaid.data[index].items
                                                    .first.productName
                                                    .substring(0, 80) +
                                                "...",
                                            maxLines: 2,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        : Text(
                                            transactionPaid.data[index].items
                                                .first.productName,
                                            maxLines: 2,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                        transactionPaid
                                                .data[index].items.first.qty
                                                .toString() +
                                            " barang",
                                        style: TextStyle(
                                          fontSize: 12.0,
                                          color: ColorPalette.fontColor,
                                        )),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 5),
                        transactionPaid.data[index].items.length > 1
                            ? Text(
                                "+" +
                                    (transactionPaid.data[index].items.length -
                                            1)
                                        .toString() +
                                    " produk lainnya",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: ColorPalette.fontColor,
                                ))
                            : Container(),
                        SizedBox(height: 20),
                        Text("Total Belanja",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                            )),
                        SizedBox(height: 5),
                        Text(
                            ConnexistHelper.formatCurrency(transactionPaid
                                .data[index].totalPrice
                                .toDouble()),
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                            )),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        }
        return loadingList();
      },
    );
  }

  Widget modalDetailTagihan(
      BuildContext context, TransactionWaitingModel stores) {
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                // margin: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Text("Detail Tagihan",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    Expanded(
                      flex: 40,
                      child: ListView(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        children: [
                          ...stores.data.transaction
                              .asMap()
                              .map((index, data) => MapEntry(
                                  index,
                                  Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(16.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                "Pesanan  " +
                                                    (index + 1).toString(),
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            SizedBox(height: 10),
                                            Row(
                                              children: [
                                                Container(
                                                    height: 30,
                                                    width: 30,
                                                    padding: EdgeInsets.all(2),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(40),
                                                        color: ColorPalette
                                                            .themeIcon),
                                                    child: Center(
                                                        child: Icon(Icons.store,
                                                            color:
                                                                Colors.white))),
                                                SizedBox(
                                                  width: 8,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Text(
                                                            data.tenant
                                                                .tenantName,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold)),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                        data.tenant.tenantType ==
                                                                'official'
                                                            ? Image.asset(
                                                                'assets/ic_official_store.png',
                                                                width: 18,
                                                              )
                                                            : Container(),
                                                      ],
                                                    ),
                                                    Text(data.tenant.city,
                                                        style: TextStyle(
                                                          color: ColorPalette
                                                              .fontColor,
                                                          fontSize: 12,
                                                        )),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 8),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text("No Invoice",
                                                    style: TextStyle(
                                                      color: ColorPalette
                                                          .fontColor,
                                                      fontSize: 12,
                                                    )),
                                                SizedBox(height: 4),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(data.invoiceNumber,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 14,
                                                        )),
                                                    // GestureDetector(
                                                    //   onTap: () {},
                                                    //   child: Text("Lihat",
                                                    //       style: TextStyle(
                                                    //         color: ColorPalette.themeIcon,
                                                    //         fontSize: 14,
                                                    //       )),
                                                    // ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 8,
                                            ),
                                            ...data.items.map((dataProduct) {
                                              return Container(
                                                margin: EdgeInsets.only(top: 8),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Container(
                                                      width: 60,
                                                      height: 60,
                                                      child: Stack(
                                                        children: [
                                                          Container(
                                                              width: double
                                                                  .infinity,
                                                              height: double
                                                                  .infinity,
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            12),
                                                              ),
                                                              child: ClipRRect(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            12),
                                                                child:
                                                                    CachedNetworkImage(
                                                                  imageUrl: dataProduct
                                                                      .productImage
                                                                      .split(
                                                                          ',')
                                                                      .first,
                                                                  fit: BoxFit
                                                                      .cover,
                                                                  placeholder: (context,
                                                                          url) =>
                                                                      Center(
                                                                          child:
                                                                              Shimmer.fromColors(
                                                                    baseColor:
                                                                        Colors.grey[
                                                                            300],
                                                                    highlightColor:
                                                                        Colors.grey[
                                                                            100],
                                                                    child:
                                                                        Container(
                                                                      color: Colors
                                                                          .white,
                                                                      width: double
                                                                          .infinity,
                                                                      height: double
                                                                          .infinity,
                                                                    ),
                                                                  )),
                                                                  errorWidget: (context,
                                                                          url,
                                                                          error) =>
                                                                      Center(
                                                                          child:
                                                                              Image.asset(
                                                                    "assets/default_image.png",
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  )),
                                                                ),
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 8,
                                                    ),
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          dataProduct.productName
                                                                      .length >
                                                                  75
                                                              ? Text(
                                                                  dataProduct
                                                                          .productName
                                                                          .substring(
                                                                              0,
                                                                              80) +
                                                                      "...",
                                                                  maxLines: 2,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        15.0,
                                                                  ),
                                                                )
                                                              : Text(
                                                                  dataProduct
                                                                      .productName,
                                                                  maxLines: 2,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        15.0,
                                                                  ),
                                                                ),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                          Text(
                                                              dataProduct.qty
                                                                      .toString() +
                                                                  ' pcs',
                                                              style: TextStyle(
                                                                fontSize: 12.0,
                                                                color: ColorPalette
                                                                    .fontColor,
                                                              )),
                                                          SizedBox(
                                                            height: 5,
                                                          ),
                                                          Text(
                                                              ConnexistHelper
                                                                  .formatCurrency(
                                                                      dataProduct
                                                                          .price
                                                                          .toDouble()),
                                                              style: TextStyle(
                                                                fontSize: 16.0,
                                                                color: Colors
                                                                    .black,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }).toList(),
                                            SizedBox(
                                              height: 25,
                                            ),
                                            GestureDetector(
                                              onTap: () {},
                                              child: Container(
                                                  width: double.infinity,
                                                  padding: EdgeInsets.all(16),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    border: Border.all(
                                                        color: Colors.grey),
                                                    color: Colors.white,
                                                  ),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Expanded(
                                                              child: Text(
                                                                  data.expedition
                                                                      .expeditionService,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        15.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ))),
                                                          // SizedBox(
                                                          //   width:
                                                          //       8,
                                                          // ),
                                                          // Icon(
                                                          //     Icons.arrow_forward_ios,
                                                          //     color: Colors.grey)
                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Divider(
                                                        thickness: 1,
                                                        color: Colors.grey[100],
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Expanded(
                                                              child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                Text(
                                                                    data.expedition
                                                                        .expeditionName,
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          15.0,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                                SizedBox(
                                                                  height: 5,
                                                                ),
                                                                Text(
                                                                    ConnexistHelper.formatCurrency(data
                                                                        .expedition
                                                                        .expeditionCost
                                                                        .toDouble()),
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            15.0,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: ColorPalette
                                                                            .fontColor)),
                                                              ])),
                                                        ],
                                                      ),
                                                    ],
                                                  )),
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Divider(
                                        thickness: 12,
                                        color: Colors.grey[100],
                                      ),
                                    ],
                                  )))
                              .values
                              .toList(),
                          Container(
                              width: double.infinity,
                              padding: EdgeInsets.only(
                                  left: 16, right: 16, top: 8, bottom: 88),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 10, bottom: 10),
                                      child: Text("Ringakasan Belanja",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 8),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Total Harga Barang",
                                                style: TextStyle(
                                                    color: Colors.black)),
                                            Container(
                                              child: Text(
                                                  ConnexistHelper
                                                      .formatCurrency(stores
                                                          .data.totalPrice
                                                          .toDouble()),
                                                  style: TextStyle(
                                                      color: Colors.black)),
                                            )
                                          ]),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 8),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Ongkos Kirim",
                                                style: TextStyle(
                                                    color: Colors.black)),
                                            Container(
                                              child: Text(
                                                  ConnexistHelper
                                                      .formatCurrency(stores
                                                          .data.expeditionCost
                                                          .toDouble()),
                                                  style: TextStyle(
                                                      color: Colors.black)),
                                            )
                                          ]),
                                    ),
                                    // Container(
                                    //   margin: EdgeInsets.only(top: 8),
                                    //   child: Row(
                                    //       mainAxisAlignment:
                                    //           MainAxisAlignment.spaceBetween,
                                    //       children: [
                                    //         Text("Biaya Admin",
                                    //             style: TextStyle(
                                    //                 color: Colors.black)),
                                    //         Container(
                                    //           child: Text(
                                    //               ConnexistHelper
                                    //                   .formatCurrency(adminfee),
                                    //               style: TextStyle(
                                    //                   color: Colors.black)),
                                    //         )
                                    //       ]),
                                    // ),
                                    SizedBox(height: 8),
                                    Divider(
                                      thickness: 2,
                                      color: Colors.grey[100],
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 8),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Total Pembayaran",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                )),
                                            Container(
                                              child: Text(
                                                  ConnexistHelper
                                                      .formatCurrency(stores
                                                          .data.totalAmount
                                                          .toDouble()),
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )
                                          ]),
                                    ),
                                  ])),
                        ],
                      ),
                    ),
                  ],
                )),
          ]),
    );
  }

  Widget modalGuide(BuildContext context, String paymentChannel) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionViewModel =
        TransactionViewModel(http: _chttp);
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Text("Cara Pembayaran",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    Expanded(
                      flex: 40,
                      child: FutureBuilder(
                        future: transactionViewModel.fetchBank(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            final BankModel bankModel = snapshot.data;
                            return ListView(
                              padding: EdgeInsets.all(16),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: [
                                ...bankModel.data
                                    .where((element) =>
                                        element.channel == paymentChannel)
                                    .map((payment) {
                                  return Html(data: payment.guide);
                                })
                              ],
                            );
                          }
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      ),
                    ),
                  ],
                )),
          ]),
    );
  }

  void showFloatingFlushbar(BuildContext context) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.blue, Colors.blueAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      // All of the previous Flushbars could be dismissed by swiping down
      // now we want to swipe to the sides
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      // The default curve is Curves.easeOut
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: 'This is a floating Flushbar',
      message: 'Copied to Clipboard',
      duration: Duration(seconds: 2),
    )..show(context);
  }

  Widget _emptyTransaction() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.receipt_long,
            size: 120,
            color: ColorPalette.btnYellow,
          ),
          SizedBox(height: 25),
          Container(
            child: Text(
              "Wah, Belum ada transaksi",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 5),
          Container(
            child: Text(
              "Yuk, isi transaksi dengan pesanan kesukaanmu",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16, color: Colors.grey[450]),
            ),
          ),
        ],
      ),
    );
  }

  Widget _shoppingCartBadge() {
    return cartBadgesModel.data.shoppingCartBadges == 0
        ? Padding(
            padding: EdgeInsets.only(left: 12, right: 16),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CartPage();
                })).then((val) => val ? _getRequest() : null);
              },
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Colors.black,
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: Badge(
              position: BadgePosition.topEnd(top: 3, end: 6),
              animationDuration: Duration(milliseconds: 300),
              animationType: BadgeAnimationType.slide,
              badgeContent: Text(
                cartBadgesModel.data.shoppingCartBadges.toString(),
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
              child: IconButton(
                  icon: Icon(Icons.shopping_cart_outlined, color: Colors.black),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CartPage();
                    })).then((val) => val ? _getRequest() : null);
                  }),
            ),
          );
  }

  Widget loadingListUnPaid() {
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.all(16),
        shrinkWrap: true,
        itemCount: 2,
        itemBuilder: (context, index) {
          return Card(
            elevation: 2,
            margin: EdgeInsets.only(bottom: 8),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(
                              left: 16, right: 16, bottom: 8, top: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  height: 10,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white)),
                              SizedBox(height: 5),
                              Container(
                                  height: 10,
                                  width: double.infinity,
                                  margin: EdgeInsets.only(right: 16),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white)),
                              SizedBox(height: 5),
                              Container(
                                  height: 10,
                                  width: double.infinity,
                                  margin: EdgeInsets.only(right: 32),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white)),
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(height: 5),
                Divider(
                  thickness: 8,
                  color: Colors.grey[100],
                ),
                SizedBox(height: 5),
                Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                height: 10,
                                width: 120,
                                margin: EdgeInsets.only(right: 16, left: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                            Container(
                                height: 10,
                                width: 80,
                                margin: EdgeInsets.only(right: 16, left: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                          ],
                        ),
                      ],
                    )),
                SizedBox(height: 5),
                Divider(thickness: 2, color: Colors.grey[100]),
                SizedBox(height: 5),
                Shimmer.fromColors(
                    baseColor: Colors.grey[300],
                    highlightColor: Colors.grey[100],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 100,
                          height: 10.0,
                          margin: EdgeInsets.only(right: 16, left: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.white),
                        ),
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                height: 10,
                                width: 120,
                                margin: EdgeInsets.only(right: 16, left: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                            Container(
                                height: 10,
                                width: 40,
                                margin: EdgeInsets.only(right: 16, left: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                          ],
                        ),
                        SizedBox(height: 15),
                        Container(
                          width: 80,
                          height: 10.0,
                          margin: EdgeInsets.only(right: 16, left: 16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.white),
                        ),
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                height: 10,
                                width: 100,
                                margin: EdgeInsets.only(right: 16, left: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                            Container(
                                height: 10,
                                width: 60,
                                margin: EdgeInsets.only(right: 16, left: 16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                          ],
                        ),
                        SizedBox(height: 8),
                      ],
                    )),
                SizedBox(height: 5),
                Divider(thickness: 2, color: Colors.grey[100]),
                SizedBox(height: 5),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Center(
                    child: Container(
                        height: 10,
                        width: 120,
                        margin: EdgeInsets.only(right: 16, left: 16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Colors.white)),
                  ),
                ),
                SizedBox(height: 14),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget loadingList() {
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.all(16),
        shrinkWrap: true,
        itemCount: 3,
        itemBuilder: (context, index) {
          return Card(
            elevation: 2,
            margin: EdgeInsets.only(bottom: 8),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Row(
                            children: [
                              Container(
                                  height: 25,
                                  width: 25,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white)),
                              SizedBox(width: 8),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      height: 10,
                                      width: 80,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: Colors.white)),
                                  SizedBox(height: 2),
                                  Container(
                                      height: 10,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: Colors.white)),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                            height: 10,
                            width: 80,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.white)),
                      ],
                    ),
                    SizedBox(height: 5),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            height: 45,
                            width: 45,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white)),
                        SizedBox(width: 8),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                height: 10,
                                width: 120,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                            SizedBox(height: 5),
                            Container(
                                height: 10,
                                width: 80,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 30),
                    Container(
                      width: 80,
                      height: 10.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: 60,
                      height: 10.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pekik_app/bloc/tracking-courier-bloc/tracking_courier_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/tracking_courier.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:provider/provider.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class TrackingCourierPage extends StatefulWidget {
  final String idTrx;
  final String status;

  TrackingCourierPage({Key key, @required this.idTrx, @required this.status})
      : super(key: key);

  @override
  _TrackingCourierPageState createState() => _TrackingCourierPageState();
}

class _TrackingCourierPageState extends State<TrackingCourierPage> {
  TrackingCourierBloc trackingCourierBloc = TrackingCourierBloc();
  int ticks;

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    trackingCourierBloc.add(GetTracking(http: _chttp, idTrx: widget.idTrx));

    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      trackingCourierBloc.add(GetTracking(http: _chttp, idTrx: widget.idTrx));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Detail Pengiriman",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildListTracking(),
    );
  }

  Widget _buildListTracking() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => trackingCourierBloc,
        child: BlocListener<TrackingCourierBloc, TrackingCourierState>(
          listener: (context, state) {
            if (state is TrackingCourierError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<TrackingCourierBloc, TrackingCourierState>(
            builder: (context, state) {
              if (state is TrackingCourierInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is TrackingCourierLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is TrackingCourierLoaded) {
                return _buildTracking(context, state.trackingCourierModel);
              }
              if (state is TrackingCourierError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildTracking(
      BuildContext context, TrackingCourierModel trackingCourierModel) {
    if (trackingCourierModel.data == null) {
      return _emptyTracking();
    } else if (trackingCourierModel.data.manifest.length == 0) {
      return _emptyTracking();
    }
    return ListView(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 8, right: 8),
          child: Timeline.builder(
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return TimelineModel(
                    Container(
                      margin: EdgeInsets.only(top: 8, bottom: 8),
                      child: Card(
                        elevation: 0,
                        // margin: EdgeInsets.symmetric(vertical: 16.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0)),
                        clipBehavior: Clip.antiAlias,
                        child: Padding(
                          padding: const EdgeInsets.all(0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              // Image.network(doodle.doodle),
                              const SizedBox(
                                height: 8.0,
                              ),
                              Text(trackingCourierModel
                                  .data.manifest[index].cityName),
                              const SizedBox(
                                height: 8.0,
                              ),
                              Text(trackingCourierModel
                                  .data.manifest[index].manifestDescription),
                              const SizedBox(
                                height: 8.0,
                              ),
                              Row(children: [
                                Text(DateFormat('dd MMMM yyyy').format(
                                    trackingCourierModel
                                        .data.manifest[index].manifestDate)),
                                Text(trackingCourierModel
                                    .data.manifest[index].manifestTime),
                              ]),
                              const SizedBox(
                                height: 8.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    iconBackground: ColorPalette.themeIcon,
                    position: TimelineItemPosition.right,
                    isFirst: index == 0,
                    isLast: index == trackingCourierModel.data.manifest.length,
                    // iconBackground: doodle.iconBackground,
                    icon: Icon(
                      Icons.local_shipping,
                      color: Colors.white,
                    ));
              },
              itemCount: trackingCourierModel.data.manifest.length,
              physics: BouncingScrollPhysics(),
              position: TimelinePosition.Left),
        )
      ],
    );
  }

  Widget _emptyTracking() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.local_shipping,
            size: 120,
            color: ColorPalette.btnYellow,
          ),
          SizedBox(height: 25),
          Container(
            child: Text(
              "Wah, Belum ada riwayat pengiriman",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  Widget spacer() {
    return Container(
      width: 2.0,
    );
  }

  Widget line(Color colors, double width) {
    return Container(
      color: colors,
      height: 2.0,
      width: width,
    );
  }
}

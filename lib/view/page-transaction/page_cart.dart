import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/bloc/cart-bloc/cart_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/cart.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-home/widget/widget_drawer.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  CartBloc cartBloc = CartBloc();

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    cartBloc.add(GetCart(http: _chttp));

    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      cartBloc.add(GetCart(http: _chttp));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);

    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: _auth.currentUser == null
            ? Stack(
                children: [
                  PreferredSize(
                    preferredSize: Size.fromHeight(50.0),
                    child: AppBar(
                      iconTheme: IconThemeData(color: ColorPalette.themeIcon),
                      backgroundColor: Colors.white,
                      elevation: 0.0,
                      title: Text(
                        "Keranjang",
                        style: TextStyle(
                            color: ColorPalette.themeIcon,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/ic_login.png',
                            width: 128,
                            height: 128,
                          ),
                          SizedBox(height: 25),
                          Container(
                            child: Text(
                              "Wah, Kamu belum masuk",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(height: 5),
                          Container(
                            child: Text(
                              "Silahkan masuk terlebih dahulu untuk melanjutkan transaksi",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 16, color: Colors.grey[450]),
                            ),
                          ),
                          SizedBox(height: 25),
                          Container(
                              height: 65,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                // boxShadow: [
                                //   BoxShadow(
                                //     color: Colors.grey.withOpacity(0.5),
                                //     spreadRadius: 5,
                                //     blurRadius: 7,
                                //     offset: Offset(0, 4), // changes position of shadow
                                //   ),
                                // ],
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(context, "/login");
                                      },
                                      child: Container(
                                        height: 55,
                                        margin: EdgeInsets.only(
                                            left: 25, right: 25),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(55),
                                            color: ColorPalette.themeIcon),
                                        child: Center(
                                          child: Text(
                                            "Masuk",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : _buildListCart(),
      ),
    );
  }

  Widget _buildListCart() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => cartBloc,
        child: BlocListener<CartBloc, CartState>(
          listener: (context, state) {
            if (state is CartError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<CartBloc, CartState>(
            builder: (context, state) {
              if (state is CartInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is CartLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is CartLoaded) {
                return _buildList(context, state.cartModel);
              }
              if (state is CartError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, CartModel cartModel) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVm = TransactionViewModel(http: _chttp);
    if (cartModel.data.carts.length == 0) {
      return Stack(
        children: [
          PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              iconTheme: IconThemeData(color: ColorPalette.themeIcon),
              backgroundColor: Colors.white,
              elevation: 0.0,
              title: Text(
                "Keranjang",
                style: TextStyle(
                    color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
              ),
              actions: [
                cartModel.data.carts.length == 0
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.only(left: 16),
                        child: GestureDetector(
                            onTap: () async {
                              _alert();
                            },
                            child: Center(
                                child: Icon(Icons.delete_forever,
                                    color: Colors.red))))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/empty_cart.png',
                    width: 128,
                    height: 128,
                    color: ColorPalette.btnYellow,
                  ),
                  SizedBox(height: 25),
                  Container(
                    child: Text(
                      "Wah, Keranjang pesananmu kosong",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(height: 5),
                  Container(
                    child: Text(
                      "Yuk, isi keranjangmu dengan pesanan kesukaanmu",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16, color: Colors.grey[450]),
                    ),
                  ),
                  SizedBox(height: 25),
                  Container(
                      height: 65,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        // boxShadow: [
                        //   BoxShadow(
                        //     color: Colors.grey.withOpacity(0.5),
                        //     spreadRadius: 5,
                        //     blurRadius: 7,
                        //     offset: Offset(0, 4), // changes position of shadow
                        //   ),
                        // ],
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context, true);
                              },
                              child: Container(
                                height: 55,
                                margin: EdgeInsets.only(left: 25, right: 25),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(55),
                                    color: ColorPalette.themeRed),
                                child: Center(
                                  child: Text(
                                    "Yuk Pesan Lagi",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          ),
        ],
      );
    }
    return Stack(
      children: [
        PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child: AppBar(
            iconTheme: IconThemeData(color: ColorPalette.themeIcon),
            backgroundColor: Colors.white,
            elevation: 0.0,
            title: Text(
              "Keranjang",
              style: TextStyle(
                  color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
            ),
            actions: [
              cartModel.data.carts.length == 0
                  ? Container()
                  : Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: GestureDetector(
                          onTap: () async {
                            _alert();
                          },
                          child: Center(
                              child: Icon(Icons.delete_forever,
                                  color: Colors.red))))
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              bottom: 70, top: MediaQuery.of(context).size.height * 0.1),
          child: ListView(
            padding: const EdgeInsets.all(16),
            children: [
              ...cartModel.data.carts.map((store) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                            height: 35,
                            width: 35,
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(35),
                                color: ColorPalette.themeIcon),
                            child: Center(
                                child: Icon(Icons.store, color: Colors.white))),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(store.tenant.tenantName,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  store.tenant.tenantType == 'official'
                                      ? Image.asset(
                                          'assets/ic_official_store.png',
                                          width: 18,
                                        )
                                      : Container(),
                                ],
                              ),
                              Text(store.tenant.city,
                                  style: TextStyle(
                                    color: ColorPalette.fontColor,
                                    fontSize: 12,
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ...store.items.map((item) {
                      return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 8,
                            ),
                            GestureDetector(
                              onTap: () {
                                basket.addAll({"idProduct": item.productId});
                                Navigator.pushNamed(context, "/detail-produk");
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: 70,
                                    height: 70,
                                    child: Stack(
                                      children: [
                                        Container(
                                            width: double.infinity,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                            ),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              child: CachedNetworkImage(
                                                imageUrl: item.productImage
                                                    .split(',')
                                                    .first,
                                                fit: BoxFit.cover,
                                                placeholder: (context, url) =>
                                                    Center(
                                                        child:
                                                            Shimmer.fromColors(
                                                  baseColor: Colors.grey[300],
                                                  highlightColor:
                                                      Colors.grey[100],
                                                  child: Container(
                                                    color: Colors.white,
                                                    width: double.infinity,
                                                    height: double.infinity,
                                                  ),
                                                )),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Center(
                                                            child: Image.asset(
                                                  "assets/default_image.png",
                                                  fit: BoxFit.cover,
                                                )),
                                              ),
                                            )),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        item.productName.length > 75
                                            ? Text(
                                                item.productName
                                                        .substring(0, 80) +
                                                    "...",
                                                maxLines: 2,
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                ),
                                              )
                                            : Text(
                                                item.productName,
                                                maxLines: 2,
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                ),
                                              ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Text(
                                            ConnexistHelper.formatCurrency(
                                                item.price.toDouble()),
                                            style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Container(
                                width: double.infinity,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                        onTap: () async {
                                          await transactionVm
                                              .postRemoveItemCart(
                                                  item.cartId, item.id)
                                              .then((product) {
                                            if (product != null) {
                                              onRefresh();
                                            } else {
                                              Fluttertoast.showToast(
                                                  msg:
                                                      "Terjadi kesalahan saat berkomunikasi dengan server",
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIos: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white);
                                            }
                                          });
                                        },
                                        child: Icon(Icons.delete,
                                            color: Colors.red)),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        if (item.minimumOrder != item.qty) {
                                          await transactionVm
                                              .postDecrementCart(
                                                  item.productId, item.cartId)
                                              .then((increment) {
                                            if (increment != null) {
                                              onRefresh();
                                            } else {
                                              Fluttertoast.showToast(
                                                  msg:
                                                      "Terjadi kesalahan saat berkomunikasi dengan server",
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIos: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white);
                                            }
                                          });
                                        }
                                      },
                                      child: Icon(Icons.do_not_disturb_on,
                                          color: item.minimumOrder == item.qty
                                              ? Colors.grey[350]
                                              : ColorPalette.btnGreen,
                                          size: 28),
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Container(
                                        width: 30,
                                        height: 25,
                                        child: Center(
                                            child: Text(item.qty.toString(),
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 16,
                                                )))),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        if (item.stock != item.qty) {
                                          await transactionVm
                                              .postIncrementCart(
                                                  item.productId, item.cartId)
                                              .then((increment) {
                                            if (increment != null) {
                                              onRefresh();
                                            } else {
                                              Fluttertoast.showToast(
                                                  msg:
                                                      "Terjadi kesalahan saat berkomunikasi dengan server",
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIos: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white);
                                            }
                                          });
                                        }
                                      },
                                      child: Icon(Icons.add_circle,
                                          color: item.stock == item.qty
                                              ? Colors.grey[350]
                                              : ColorPalette.btnGreen,
                                          size: 28),
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 3,
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            SizedBox(
                              height: 3,
                            ),
                          ]);
                    }).toList()
                  ],
                );
              }).toList()
            ],
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: 70,
                width: double.infinity,
                padding:
                    EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 4), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                          height: 60,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Total Harga",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: ColorPalette.fontColor,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Row(
                                  children: [
                                    Text(
                                        ConnexistHelper.formatCurrency(cartModel
                                            .data.totalPrice
                                            .toDouble()),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold)),
                                    // SizedBox(
                                    //   width: 4,
                                    // ),
                                    // Icon(Icons.expand_less, color: Colors.black)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/select-delivery');
                      },
                      child: Container(
                        width: 150,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: ColorPalette.themeIcon),
                        child: Center(
                          child: Text(
                            "Beli",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                )))
      ],
    );
  }

  void _alert() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVm = TransactionViewModel(http: _chttp);
    await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext cn) => new AlertDialog(
        contentTextStyle: TextStyle(
          fontFamily: 'Poppins',
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        content: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  left: Constants.padding,
                  top: Constants.avatarRadius + Constants.padding,
                  right: Constants.padding,
                  bottom: Constants.padding),
              margin: EdgeInsets.only(top: Constants.avatarRadius),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(Constants.padding),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "Apakah anda ingin menghapus semua produk yang ada dikeranjang?",
                    style: TextStyle(fontSize: 14, color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(cn).pop(true);
                            },
                            child: Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  border: Border.all(color: Colors.grey[400])),
                              child: Center(
                                child: Text(
                                  "Tidak",
                                  style:
                                      TextStyle(color: ColorPalette.btnGreen),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () async {
                              await transactionVm
                                  .postEmptyCart()
                                  .then((product) {
                                if (product != null) {
                                  onRefresh();
                                  Navigator.of(cn).pop(true);
                                  Fluttertoast.showToast(
                                      msg: "Barang telah dihapus semua",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIos: 1,
                                      backgroundColor: ColorPalette.btnGreen,
                                      textColor: Colors.white);
                                } else {
                                  Fluttertoast.showToast(
                                      msg:
                                          "Terjadi kesalahan saat berkomunikasi dengan server",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIos: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white);
                                }
                              });
                            },
                            child: Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: ColorPalette.btnGreen),
                              child: Center(
                                child: Text(
                                  "Ya",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              left: Constants.padding,
              right: Constants.padding,
              child: CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: Constants.avatarRadius,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(
                        Radius.circular(Constants.avatarRadius)),
                    child: Image.asset("assets/default_toko.jpg")),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

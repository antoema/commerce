import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/maps/google_maps_place_picker.dart';
import 'package:pekik_app/maps/src/models/pick_result.dart';
import 'package:pekik_app/model/region.dart';
import 'package:pekik_app/model/region_subdistrict.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/address_vm.dart';
import 'package:pekik_app/viewmodel/region_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class AddAddressPage extends StatefulWidget {
  const AddAddressPage({Key key}) : super(key: key);

  @override
  _AddAddressPageState createState() => _AddAddressPageState();
}

class _AddAddressPageState extends State<AddAddressPage> {
  final _detailAlamat = TextEditingController();
  final _namaAlamat = TextEditingController();
  final _noPonsel = TextEditingController();
  final _kodePos = TextEditingController();
  Map<String, dynamic> basket;
  String _currentAddress;
  final formKey = new GlobalKey<FormState>();
  static PickResult selectedPlace;
  double latitude = -6.1966192;
  double longitude = 106.7635515;

  bool validates() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  _getAddressFromLatLng(double _getLat, double _getLng) async {
    try {
      List<Placemark> p =
          await geolocator.placemarkFromCoordinates(_getLat, _getLng);
      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.locality}, ${place.administrativeArea}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    _namaAlamat.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  bool isCheck = true;
  bool isLoading = true;
  List<String> typeTempat = ['Rumah', 'Kantor', 'Apartement', 'Kos'];

  @override
  Widget build(BuildContext context) {
    basket = Provider.of(context, listen: false);
    final _getLocations =
        LatLng(basket['geo-position']['lat'], basket['geo-position']['long']);
    double _getLat = selectedPlace == null
        ? basket['geo-position']['lat']
        : selectedPlace.geometry.location.lat;
    double _getLng = selectedPlace == null
        ? basket['geo-position']['long']
        : selectedPlace.geometry.location.lng;
    LatLng _getLatLngFromPickers = LatLng(_getLat, _getLng);
    LatLng _posisiSaatini =
        selectedPlace != null ? _getLatLngFromPickers : _getLocations;
    List<Marker> markers = [];
    markers.add(Marker(
      markerId: MarkerId("1"),
      position: _posisiSaatini,
      icon: BitmapDescriptor.defaultMarker,
    ));
    var alamat = basket['geo-position']['addressView'];
    if (selectedPlace != null) {
      _getAddressFromLatLng(selectedPlace.geometry.location.lat,
          selectedPlace.geometry.location.lng);
    }
    var _alamatLengkap = selectedPlace == null ? alamat : _currentAddress ?? "";
    return WillPopScope(
      onWillPop: () {
        setState(() {
          basket.addAll({
            'nameProvince': null,
            'idProvince': null,
            'nameCity': null,
            'idCity': null,
            'nameSubdistrict': null,
            'idSubdistrict': null,
          });
          selectedPlace = null;
        });
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: ColorPalette.themeIcon),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text(
            "Tambah Alamat",
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.all(16),
              child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: double.infinity,
                        child: Card(
                            elevation: 3.0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 16,
                                        right: 16,
                                        bottom: 8,
                                        top: 16),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Text("Alamat",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14,
                                                    color: Colors.black)),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text("(Berdasarkan pinpoint)",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: ColorPalette
                                                        .fontColor)),
                                          ],
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.push(context,
                                                MaterialPageRoute(
                                                    builder: (context) {
                                              return PlacePicker(
                                                apiKey: apiKey,
                                                useCurrentLocation: true,
                                                onPlacePicked: (result) {
                                                  setState(() {
                                                    selectedPlace = result;
                                                    latitude = selectedPlace
                                                        .geometry.location.lat;
                                                    longitude = selectedPlace
                                                        .geometry.location.lng;
                                                    _getLat = selectedPlace ==
                                                            null
                                                        ? basket['geo-position']
                                                            ['lat']
                                                        : selectedPlace.geometry
                                                            .location.lat;
                                                    _getLng = selectedPlace ==
                                                            null
                                                        ? basket['geo-position']
                                                            ['long']
                                                        : selectedPlace.geometry
                                                            .location.lng
                                                            .toDouble();
                                                    _getLatLngFromPickers =
                                                        LatLng(
                                                            _getLat, _getLng);
                                                  });
                                                  Navigator.pop(context, true);
                                                },
                                                autocompleteLanguage: "id",
                                                initialPosition: null,
                                              );
                                            }));
                                          },
                                          child: Text("Ubah Lokasi",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color:
                                                      ColorPalette.themeIcon)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    thickness: 3,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 16,
                                        right: 16,
                                        bottom: 16,
                                        top: 8),
                                    child: Text(
                                        _alamatLengkap == null
                                            ? "Sedang mencari lokasi..."
                                            : _alamatLengkap,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 14)),
                                  ),
                                ]))),
                    SizedBox(
                      height: 15,
                    ),
                    Text("Detail Alamat",
                        style: TextStyle(
                          fontSize: 16.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      // color: Colors.white,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: new TextFormField(
                        controller: _detailAlamat,
                        decoration: new InputDecoration(
                          // labelText: "No Ponsel",
                          hintText: "Tulis detail alamat Anda",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Detail alamat tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.multiline,
                        maxLength: null,
                        maxLines: null,
                        style: new TextStyle(
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    Container(),
                    SizedBox(
                      height: 15,
                    ),
                    Text("Nama Alamat",
                        style: TextStyle(
                          fontSize: 16.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      // color: Colors.white,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: new TextFormField(
                        controller: _namaAlamat,
                        onTap: () {
                          setState(() {
                            isCheck = false;
                          });
                        },
                        decoration: new InputDecoration(
                          // labelText: "No Ponsel",
                          hintText: "Ex: Rumah",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Nama Alamat tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    isCheck
                        ? Container()
                        : Container(
                            height: 35,
                            margin: EdgeInsets.only(bottom: 15),
                            child: ListView(
                              shrinkWrap: true,
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              children: [
                                ...typeTempat
                                    .map((e) => GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              _namaAlamat.text = e;
                                              isCheck = true;
                                            });
                                          },
                                          child: Container(
                                              height: 20,
                                              padding: EdgeInsets.all(8),
                                              margin: EdgeInsets.only(right: 8),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(35),
                                                  color: Colors.white,
                                                  border: Border.all(
                                                      color: Colors.grey[350])),
                                              child: Center(
                                                  child: Text(e,
                                                      style: new TextStyle(
                                                        color: Colors.grey[600],
                                                        fontSize: 14,
                                                      )))),
                                        ))
                                    .toList()
                              ],
                            )),
                    Text("Provinsi",
                        style: TextStyle(
                          fontSize: 16.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        showBarModalBottomSheet(
                          expand: false,
                          context: context,
                          backgroundColor: Colors.transparent,
                          builder: (context) =>
                              modalRegion(context, "province"),
                        );
                      },
                      child: Container(
                          height: 60,
                          width: double.infinity,
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.grey),
                              color: Colors.grey[100]),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  basket['nameProvince'] == null
                                      ? "Masukan Provinsi Anda"
                                      : basket['nameProvince'],
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.grey[600])),
                            ],
                          )),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text("Kota",
                        style: TextStyle(
                          fontSize: 16.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        if (basket['idProvince'] == null) {
                          Fluttertoast.showToast(
                              msg: "Pilih Provinsi Anda Terlebih Dahulu",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white);
                        } else {
                          showBarModalBottomSheet(
                            expand: false,
                            context: context,
                            backgroundColor: Colors.transparent,
                            builder: (context) => modalRegion(context, "city"),
                          );
                        }
                      },
                      child: Container(
                          height: 60,
                          width: double.infinity,
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.grey),
                              color: Colors.grey[100]),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  basket['nameCity'] == null
                                      ? "Masukan Kota Anda"
                                      : basket['nameCity'],
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.grey[600])),
                            ],
                          )),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Kecamatan",
                                  style: TextStyle(
                                    fontSize: 16.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (basket['idCity'] == null) {
                                    Fluttertoast.showToast(
                                        msg: "Pilih Kota Anda Terlebih Dahulu",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIos: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white);
                                  } else {
                                    showBarModalBottomSheet(
                                      expand: false,
                                      context: context,
                                      backgroundColor: Colors.transparent,
                                      builder: (context) =>
                                          modalRegion(context, "subdistrict"),
                                    );
                                  }
                                },
                                child: Container(
                                    height: 60,
                                    width: double.infinity,
                                    padding: EdgeInsets.all(16),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(color: Colors.grey),
                                        color: Colors.grey[100]),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                            basket['nameSubdistrict'] == null
                                                ? "Masukan Kecamatan Anda"
                                                : basket['nameSubdistrict'],
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                color: Colors.grey[600])),
                                      ],
                                    )),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 120,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Kode Pos",
                                  style: TextStyle(
                                    fontSize: 16.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                // color: Colors.white,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: new TextFormField(
                                  controller: _kodePos,
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    hintText: "Kode Pos",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  // validator: (val) {
                                  //   if (val.isEmpty) {
                                  //     return "Kode Pos tidak boleh kosong";
                                  //   } else {
                                  //     return null;
                                  //   }
                                  // },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text("No Ponsel",
                        style: TextStyle(
                          fontSize: 16.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      // color: Colors.white,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: new TextFormField(
                        controller: _noPonsel,
                        decoration: new InputDecoration(
                          // labelText: "No Ponsel",
                          hintText: "Isi No Ponsel Anda",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (val) {
                          if (val.isEmpty) {
                            return "No Ponsel tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.phone,
                        style: new TextStyle(
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    SizedBox(
                        height: 60,
                        width: double.infinity,
                        child: RaisedButton(
                          color: ColorPalette.themeIcon,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(60),
                          ),
                          child: Center(
                            child: isLoading
                                ? Text("Simpan",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold))
                                : Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.white),
                                    ),
                                  ),
                          ),
                          onPressed: () async {
                            if (_getLat == null ||
                                _getLng == null ||
                                basket['idProvince'] == null ||
                                basket['nameProvince'] == null ||
                                basket['idCity'] == null ||
                                basket['nameCity'] == null ||
                                basket['idSubdistrict'] == null ||
                                basket['nameSubdistrict'] == null) {
                              Fluttertoast.showToast(
                                  msg: "Terjadi kesalahan lengkapi data",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white);
                            } else {
                              print(_detailAlamat.text);
                              print(_namaAlamat.text);
                              print(_kodePos.text);
                              print(_noPonsel.text);
                              print(basket['idProvince']);
                              print(basket['nameProvince']);
                              print(basket['idCity']);
                              print(basket['nameCity']);
                              print(basket['idSubdistrict']);
                              print(basket['nameSubdistrict']);
                              CHttp _chttp =
                                  Provider.of<CHttp>(context, listen: false);
                              AddressViewModel addressVM =
                                  AddressViewModel(http: _chttp);
                              if (validates()) {
                                setState(() {
                                  isLoading = false;
                                });
                                await addressVM
                                    .postAddress(
                                        _detailAlamat.text,
                                        _namaAlamat.text,
                                        basket['nameProvince'],
                                        basket['nameCity'],
                                        basket['nameSubdistrict'],
                                        basket['idProvince'],
                                        basket['idCity'],
                                        basket['idSubdistrict'],
                                        _kodePos.text,
                                        _noPonsel.text,
                                        _getLat.toString(),
                                        _getLng.toString())
                                    .then((address) {
                                  if (address != null) {
                                    setState(() {
                                      basket.addAll({
                                        'nameProvince': null,
                                        'idProvince': null,
                                        'nameCity': null,
                                        'idCity': null,
                                        'nameSubdistrict': null,
                                        'idSubdistrict': null,
                                      });
                                      isLoading = true;
                                    });
                                    Fluttertoast.showToast(
                                        msg: "Tambah Alamat Berhasil",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIos: 1,
                                        backgroundColor: ColorPalette.btnGreen,
                                        textColor: Colors.white);
                                    Navigator.pop(context, true);
                                  } else {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    Fluttertoast.showToast(
                                        msg:
                                            "Terjadi kesalahan saat berkomunikasi dengan server",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIos: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white);
                                  }
                                });
                              }
                            }
                          },
                        ))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget modalRegion(BuildContext context, String typeInput) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    RegionViewModel regionVM = RegionViewModel(http: _chttp);
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Text(
                          typeInput == "city"
                              ? "Pilih Kota Anda"
                              : typeInput == "province"
                                  ? "Pilih Provinsi Anda"
                                  : typeInput == "subdistrict"
                                      ? "Pilih Kecamatan Anda"
                                      : "Masukan $typeInput",
                          style: TextStyle(
                              fontSize: 16.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    typeInput == "province"
                        ? Expanded(
                            flex: 40,
                            child: FutureBuilder(
                              future: regionVM.fetchProvince(),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  final RegionModel regionModel = snapshot.data;
                                  return ListView.separated(
                                    padding: EdgeInsets.all(16),
                                    shrinkWrap: true,
                                    itemCount: regionModel.data.length,
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        onTap: () {
                                          setState(() {
                                            basket.addAll({
                                              'nameProvince': regionModel
                                                  .data[index].province,
                                              'idProvince': regionModel
                                                  .data[index].provinceId,
                                              'nameCity': null,
                                              'idCity': null,
                                              'nameSubdistrict': null,
                                              'idSubdistrict': null,
                                            });
                                          });
                                          Navigator.pop(context);
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 8, bottom: 8),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                regionModel
                                                    .data[index].province,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.black),
                                              ),
                                              basket['idProvince'] ==
                                                      regionModel.data[index]
                                                          .provinceId
                                                  ? Icon(
                                                      Icons.check_circle,
                                                      color:
                                                          ColorPalette.btnGreen,
                                                    )
                                                  : Icon(
                                                      Icons.lens_outlined,
                                                      color: Colors.black,
                                                    )
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                    separatorBuilder: (context, index) {
                                      return Divider(
                                        thickness: 1,
                                      );
                                    },
                                  );
                                }
                                return loadingCategory();
                              },
                            ),
                          )
                        : typeInput == "city"
                            ? Expanded(
                                flex: 40,
                                child: FutureBuilder(
                                  future:
                                      regionVM.fetchCity(basket['idProvince']),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      final RegionModel regionModel =
                                          snapshot.data;
                                      return ListView.separated(
                                        padding: EdgeInsets.all(16),
                                        shrinkWrap: true,
                                        itemCount: regionModel.data.length,
                                        itemBuilder: (context, index) {
                                          return InkWell(
                                            onTap: () {
                                              setState(() {
                                                basket.addAll({
                                                  'nameCity': regionModel
                                                      .data[index].cityName,
                                                  'idCity': regionModel
                                                      .data[index].cityId,
                                                  'nameSubdistrict': null,
                                                  'idSubdistrict': null,
                                                });
                                              });
                                              Navigator.pop(context);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8, bottom: 8),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    regionModel
                                                        .data[index].cityName,
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black),
                                                  ),
                                                  basket['idCity'] ==
                                                          regionModel
                                                              .data[index]
                                                              .cityId
                                                      ? Icon(
                                                          Icons.check_circle,
                                                          color: ColorPalette
                                                              .btnGreen,
                                                        )
                                                      : Icon(
                                                          Icons.lens_outlined,
                                                          color: Colors.black,
                                                        )
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                        separatorBuilder: (context, index) {
                                          return Divider(
                                            thickness: 1,
                                          );
                                        },
                                      );
                                    }
                                    return loadingCategory();
                                  },
                                ),
                              )
                            : Expanded(
                                flex: 40,
                                child: FutureBuilder(
                                  future: regionVM
                                      .fetchSubdistrict(basket['idCity']),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      final RegionSubdistrictModel regionModel =
                                          snapshot.data;
                                      return ListView.separated(
                                        padding: EdgeInsets.all(16),
                                        shrinkWrap: true,
                                        itemCount: regionModel.data.length,
                                        itemBuilder: (context, index) {
                                          return InkWell(
                                            onTap: () {
                                              setState(() {
                                                basket.addAll({
                                                  'nameSubdistrict': regionModel
                                                      .data[index]
                                                      .subdistrictName,
                                                  'idSubdistrict': regionModel
                                                      .data[index]
                                                      .subdistrictId,
                                                });
                                              });
                                              Navigator.pop(context);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8, bottom: 8),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    regionModel.data[index]
                                                        .subdistrictName,
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black),
                                                  ),
                                                  basket['idSubdistrict'] ==
                                                          regionModel
                                                              .data[index]
                                                              .subdistrictId
                                                      ? Icon(
                                                          Icons.check_circle,
                                                          color: ColorPalette
                                                              .btnGreen,
                                                        )
                                                      : Icon(
                                                          Icons.lens_outlined,
                                                          color: Colors.black,
                                                        )
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                        separatorBuilder: (context, index) {
                                          return Divider(
                                            thickness: 1,
                                          );
                                        },
                                      );
                                    }
                                    return loadingCategory();
                                  },
                                ),
                              ),
                  ],
                )),
          ]),
    );
  }

  Widget loadingCategory() {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: 10,
        padding: EdgeInsets.all(16),
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 250,
                        height: 10,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 200,
                        height: 10,
                      ),
                    ),
                  ],
                )),
                Icon(
                  Icons.lens_outlined,
                  color: Colors.black,
                )
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            thickness: 1,
          );
        },
      ),
    );
  }
}

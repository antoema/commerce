import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-home/widget/home_widget.dart';
import 'package:provider/provider.dart';
import 'package:pekik_app/view/widgets/custom_expansion_tile.dart' as custom;

class PaymentPage extends StatefulWidget {
  final String paymentCode;
  final String paymentGuide;
  final double paymentAdminFee;
  final double amount;

  PaymentPage({
    Key key,
    @required this.paymentCode,
    @required this.paymentGuide,
    @required this.paymentAdminFee,
    @required this.amount,
  }) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(hours: 12));
    _controller.forward();
  }

  Future<bool> _onWillPop(BuildContext context) async {
    return Navigator.push(context, new MaterialPageRoute(builder: (context) {
      return Home();
    }));
  }

  @override
  dispose() {
    _controller.dispose(); // you need this
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return _onWillPop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: ColorPalette.themeIcon),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text(
            "Cara Pembayaran",
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
        ),
        body: Stack(children: [
          ListView(physics: BouncingScrollPhysics(), children: [
            Column(children: [
              Container(
                padding: EdgeInsets.only(
                    left: 16, right: 45.0, top: 16.0, bottom: 16.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Total Tagihan",
                      style: TextStyle(fontSize: 16),
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(ConnexistHelper.formatCurrency(widget.amount),
                            style: TextStyle(fontSize: 16))
                      ],
                    )),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 16.0, right: 45.0, bottom: 16.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Biaya Admin",
                      style: TextStyle(fontSize: 16),
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                            ConnexistHelper.formatCurrency(
                                widget.paymentAdminFee),
                            style: TextStyle(fontSize: 16))
                      ],
                    )),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Divider(
                    thickness: 2.0,
                  )),
              Container(
                padding: EdgeInsets.only(
                    left: 16.0, right: 16.0, bottom: 16.0, top: 16.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Total Pembayaran",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                            ConnexistHelper.formatCurrency(
                                widget.amount + widget.paymentAdminFee),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16))
                      ],
                    )),
                    InkWell(
                      onTap: () {
                        Clipboard.setData(ClipboardData(
                                text: (widget.amount + widget.paymentAdminFee)
                                    .toString()))
                            .then((result) {
                          showFloatingFlushbar(context);
                        });
                      },
                      child: Container(
                          margin: EdgeInsets.only(left: 8),
                          child: Image.asset(
                            "assets/copy.png",
                            width: 20,
                            height: 20,
                            color: ColorPalette.fontColor,
                          )),
                    ),
                  ],
                ),
              ),
            ]),
            Container(child: _viewOtomatis()),
          ]),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 70,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SizedBox(
                    height: 55,
                    width: double.infinity,
                    child: RaisedButton(
                        color: ColorPalette.themeIcon,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(55),
                        ),
                        child: Center(
                          child: Text("Selesaikan Pembayaran",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                        onPressed: () {
                          return Navigator.push(context,
                              new MaterialPageRoute(builder: (context) {
                            return Home();
                          }));
                        }),
                  )))
        ]),
      ),
    );
  }

  void showFloatingFlushbar(BuildContext context) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.blue, Colors.blueAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      // All of the previous Flushbars could be dismissed by swiping down
      // now we want to swipe to the sides
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      // The default curve is Curves.easeOut
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: 'This is a floating Flushbar',
      message: 'Copied to Clipboard',
      duration: Duration(seconds: 2),
    )..show(context);
  }

  Widget _viewOtomatis() {
    Map<String, dynamic> _basket =
        Provider.of<Map<String, dynamic>>(context, listen: false);
    return Card(
      elevation: 2,
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 80),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5.0, top: 25, left: 10, right: 10),
            child: Text(
                "Transfer ke Nomor " + _basket["nama_bank"] + " berikut ini :",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: "Proppins",
                  fontSize: 16.0,
                  color: ColorPalette.fontColor,
                  fontWeight: FontWeight.bold,
                )),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 5.0),
            child: Text(
              widget.paymentCode,
              style: TextStyle(
                  fontFamily: "Proppins",
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10.0),
            width: 90,
            height: 40,
            child: Card(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(2.0),
                  side: BorderSide(width: 1, color: ColorPalette.themeIcon)),
              child: InkWell(
                child: Center(
                  child: Text(
                    'SALIN',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: ColorPalette.themeIcon,
                    ),
                  ),
                ),
                onTap: () {
                  Clipboard.setData(ClipboardData(text: widget.paymentCode))
                      .then((result) {
                    showFloatingFlushbar(context);
                  });
                },
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 5.0, left: 16, right: 16),
            child: Text(
              widget.paymentGuide,
              // maxLines: 3,
              style: TextStyle(
                fontFamily: "Proppins",
                fontSize: 15.0,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 5.0, top: 5.0),
            child: Text("Batas Pembayaran",
                style: TextStyle(
                  fontFamily: "Proppins",
                  fontSize: 16.0,
                  color: ColorPalette.fontColor,
                  fontWeight: FontWeight.bold,
                )),
          ),
          Container(
            margin: EdgeInsets.only(left: 10, right: 10, bottom: 10.0),
            child: Countdown(
              animation: StepTween(
                begin: 7200,
                end: 0,
              ).animate(_controller),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 16),
            child: custom.ExpansionTile(
              headerBackgroundColor: ColorPalette.themeIcon,
              iconColor: Colors.white,
              initiallyExpanded: true,
              title: Text(
                "Petunjuk Pembayaran",
                style: TextStyle(color: Colors.white),
              ),
              children: <Widget>[
                ListTile(
                  title: Html(data: "${_basket["guide"]}"),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class Countdown extends AnimatedWidget {
  Countdown({Key key, this.animation}) : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context) {
    Duration clockTimer = Duration(seconds: animation.value);

    String timerText =
        '${clockTimer.inHours.remainder(24).toString()} jam ${clockTimer.inMinutes.remainder(60).toString()} menit ${(clockTimer.inSeconds.remainder(60) % 60).toString().padLeft(2, '0')} detik';

    return Text(
      "$timerText",
      style: TextStyle(
        fontSize: 25,
        color: Colors.black,
      ),
    );
  }
}

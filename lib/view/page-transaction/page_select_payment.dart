import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/bloc/bank-bloc/bank_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/bank.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-transaction/page_payment.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:pekik_app/view/widgets/custom_expansion_tile.dart' as custom;

class SelectPaymentPage extends StatefulWidget {
  const SelectPaymentPage({Key key}) : super(key: key);

  @override
  _SelectPaymentPageState createState() => _SelectPaymentPageState();
}

class _SelectPaymentPageState extends State<SelectPaymentPage> {
  BankBloc bankBloc = BankBloc();
  ProgressDialog pr;

  String idx;
  var _bankController;
  bool load = false;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

  @override
  void initState() {
    super.initState();
    bankBloc.add(GetBank());
    initializing();
  }

  Future<void> onRefresh() async {
    setState(() {
      bankBloc.add(GetBank());
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  void initializing() async {
    androidInitializationSettings =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    iosInitializationSettings = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (payload) async {
        onSelectNotification(payload);
      },
    );
  }

  void _showNotificationsAfterSecond(String noVA, String idTrx) async {
    await notificationAfterSec(noVA, idTrx);
  }

  Future<void> notificationAfterSec(String noVA, String idTrx) async {
    Map<String, dynamic> basket =
        Provider.of<Map<String, dynamic>>(context, listen: false);
    var timeDelayed = DateTime.now().add(Duration(seconds: 2));
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('second channel ID', 'General', 'General',
            priority: Priority.High,
            importance: Importance.Max,
            styleInformation: BigTextStyleInformation(
              'Segera selesaikan pembayaran Anda melalui ' +
                  basket["nama_bank"] +
                  ' dengan Transfer ke Nomor ' +
                  noVA,
              htmlFormatBigText: true,
              contentTitle: '<b>Transaksi Berhasil</b>',
              htmlFormatContentTitle: true,
            ),
            ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails(
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );

    NotificationDetails notificationDetails =
        NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.schedule(
        1,
        'Transaksi Berhasil',
        'Segera selesaikan pembayaran Anda melalui ' +
            basket["nama_bank"] +
            ' dengan Transfer ke Nomor ' +
            noVA,
        timeDelayed,
        notificationDetails,
        payload: idTrx);
    load = true;
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              print("");
            },
            child: Text("Okay")),
      ],
    );
  }

  Future onSelectNotification(String payload) async {
    if (load == true) {
      Map<String, dynamic> basket =
          Provider.of<Map<String, dynamic>>(context, listen: false);
      debugPrint('notification payload: ' + payload);
      load = false;
      basket.addAll({"information": payload});
      print(payload);
      // Navigator.pushNamed(context, "/transaction-warung");
    }
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(
        message: ' Mohon Tunggu...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Pilih Pembayaran",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildListBank(),
    );
  }

  Widget _buildListBank() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => bankBloc,
        child: BlocListener<BankBloc, BankState>(
          listener: (context, state) {
            if (state is BankError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<BankBloc, BankState>(
            builder: (context, state) {
              if (state is BankInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is BankLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is BankLoaded) {
                return _buildBank(context, state.bankModel);
              }
              if (state is BankError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildBank(BuildContext context, BankModel bankModel) {
    Map<String, dynamic> _basket =
        Provider.of<Map<String, dynamic>>(context, listen: false);
    return Stack(
      children: [
        ListView(
          shrinkWrap: true,
          children: [
            Card(
              margin: EdgeInsets.only(left: 12, right: 12, bottom: 70, top: 12),
              child: custom.ExpansionTile(
                initiallyExpanded: true,
                headerBackgroundColor: ColorPalette.themeIcon,
                iconColor: Colors.white,
                title: Row(
                  children: <Widget>[
                    Icon(
                      Icons.account_balance_wallet,
                      color: Colors.white,
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          "Pilih Cara Pembayaran",
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                ),
                children: <Widget>[
                  ExpansionTile(
                    initiallyExpanded: true,
                    title: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/atm.png",
                          width: 25,
                          height: 25,
                          // color: ColorPalette.themeColor,
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 5),
                            child: Text(
                              "TRANSFER",
                              style: TextStyle(color: Colors.grey),
                            )),
                      ],
                    ),
                    children: <Widget>[

                      ...bankModel.data
                          .where((element) =>
                      element.channel != 'QRISPAY' &&
                          element.channel != 'KKWP')
                          .map((bank) {
                        return Row(
                          children: [
                            Expanded(
                              child: Container(
                                  height: 70,
                                  margin: EdgeInsets.only(
                                      left: 16, right: 16, bottom: 8, top: 8),
                                  child: Card(
                                    elevation: 2.0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                        side: BorderSide(
                                            width: 1,
                                            color: ColorPalette.themeIcon)),
                                    color: idx == bank.channel
                                        ? ColorPalette.themeIcon
                                        : Colors.white,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          print("tapped");
                                          print(bank.channel);
                                          idx = bank.channel;

                                          _bankController =
                                              TextEditingController(
                                                  text: bank.name);

                                          _basket.addAll({
                                            "nama_bank": bank.name,
                                            "guide": bank.guide
                                          });
                                        });
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(4.0)),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: [
                                                ClipRRect(
                                                  child: CachedNetworkImage(
                                                    imageUrl:
                                                    bank.paymentLogo == null
                                                        ? ''
                                                        : bank.paymentLogo,
                                                    height: 30,
                                                    fit: BoxFit.cover,
                                                    placeholder: (context,
                                                        url) =>
                                                        Center(
                                                            child:
                                                            CircularProgressIndicator()),
                                                    errorWidget: (context, url,
                                                        error) =>
                                                        Center(
                                                            child: Image.asset(
                                                              "assets/default_image.png",
                                                              height: 20,
                                                              fit: BoxFit.cover,
                                                            )),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                  EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    bank.name,
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          ],
                        );
                      }),
                      ListTile(
                        title: LayoutBuilder(
                            builder: (context, constraints) {
                              return Container(
                                  height: 70,
                                  margin: EdgeInsets.only( bottom: 8),
                                  child: Card(
                                    elevation: 2.0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(8.0),
                                        side: BorderSide(
                                            width: 1,
                                            color: ColorPalette.themeIcon)),
                                    color: Colors.white,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          print("bankdki");
                                          Fluttertoast.showToast(
                                              msg: "VA Dalam Proses Integrasi",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIos: 2,
                                              backgroundColor: Colors.green,
                                              textColor: Colors.white);
                                        });
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(4.0)),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: [
                                                ClipRRect(
                                                    child: Container(
                                                      padding: EdgeInsets.only(top: 5),
                                                      child: Image.asset(
                                                        "assets/bank_dki.png",
                                                        height: 11,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    )
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    "BANK DKI",
                                                    style: TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ));
                            }
                        ),
                      ),
                    ],
                  ),
                  /*ExpansionTile(
                    initiallyExpanded: true,
                    title: Row(
                      children: <Widget>[
                        Image.asset(
                          "assets/atm.png",
                          width: 25,
                          height: 25,
                          // color: ColorPalette.themeColor,
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 5),
                            child: Text(
                              "E-Wallet",
                              style: TextStyle(color: Colors.grey),
                            )),
                      ],
                    ),
                    children: <Widget>[
                      Container(
                          height: 70,
                          margin: EdgeInsets.only(
                              left: 16, right: 16, bottom: 8, top: 8),
                          child: Card(
                            elevation: 2.0,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.circular(8.0),
                                side: BorderSide(
                                    width: 1,
                                    color: ColorPalette.themeIcon)),
                            color: idx == 'e-wallet'
                                ? ColorPalette.themeIcon
                                : Colors.white,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  print("tapped");
                                  idx = 'e-wallet';
                                  print(idx);
                                  _bankController =
                                      TextEditingController(
                                          text: 'e-wallet');
                                  *//*print("tapped");
                                  print(bank.channel);
                                  idx = bank.channel;

                                  _bankController =
                                      TextEditingController(
                                          text: bank.name);

                                  _basket.addAll({
                                    "nama_bank": bank.name,
                                    "guide": bank.guide
                                  });*//*
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.circular(4.0)),
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          'assets/ic_coin.png',
                                          width: 25,
                                        ),
                                        Container(
                                          margin:
                                          EdgeInsets.only(left: 10),
                                          child: Text(
                                            ConnexistHelper.formatCurrency(0.toDouble()),
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ))
                    ],
                  ),*/
                ],
              ),
            )
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 60.0,
            color: Colors.white,
            padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
            width: double.infinity,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(60)),
              color: _bankController == null
                  ? Colors.grey[200]
                  : ColorPalette.themeIcon,
              onPressed: _bankController == null
                  ? () {
                      Fluttertoast.showToast(
                          msg: "Anda belum memilih metode pembayaran",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIos: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white);
                    }
                  : () async {
                      print(idx);
                      CHttp _chttp = Provider.of<CHttp>(context, listen: false);
                      TransactionViewModel transactionViewModel =
                          TransactionViewModel(http: _chttp);
                      pr.show();
                      await transactionViewModel
                          .postPayTransaction(idx)
                          .then((value) {
                        if (value != null) {
                          pr.hide();
                          _showNotificationsAfterSecond(
                              value.data.paymentCode, value.data.refNo);
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return PaymentPage(
                                paymentCode: value.data.paymentCode,
                                paymentGuide: value.data.paymentGuide,
                                paymentAdminFee:
                                    value.data.paymentAdminFee.toDouble(),
                                amount: value.data.amount.toDouble());
                          }));
                        } else if (idx == 'e-wallet') {
                          pr.hide();
                          Fluttertoast.showToast(
                              msg: "Pembayaran E-wallet",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.green,
                              textColor: Colors.white);
                        } else {
                          pr.hide();
                          Fluttertoast.showToast(
                              msg: "Terjadi kesalahan mohon ulangi transaksi",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white);
                        }
                      });
                    },
              child: Text(
                "Bayar",
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color:
                        _bankController == null ? Colors.grey : Colors.white),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

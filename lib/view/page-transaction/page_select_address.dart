import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/bloc/address-bloc/address_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/address.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/view/page-transaction/page_add_address.dart';
import 'package:pekik_app/view/page-transaction/page_edit_address.dart';
import 'package:pekik_app/viewmodel/address_vm.dart';
import 'package:provider/provider.dart';

class SelectAddressPage extends StatefulWidget {
  @override
  _SelectAddressPageState createState() => _SelectAddressPageState();
}

class _SelectAddressPageState extends State<SelectAddressPage> {
  AddressBloc addressBloc = AddressBloc();

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    addressBloc.add(GetAddress(http: _chttp));
    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      addressBloc.add(GetAddress(http: _chttp));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, true);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: ColorPalette.themeIcon),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text(
            "Pilih Alamat",
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
        ),
        body: _buildListAddress(),
      ),
    );
  }

  Widget _buildListAddress() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => addressBloc,
        child: BlocListener<AddressBloc, AddressState>(
          listener: (context, state) {
            if (state is AddressError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<AddressBloc, AddressState>(
            builder: (context, state) {
              if (state is AddressInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is AddressLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is AddressLoaded) {
                return _buildList(context, state.addressModel);
              }
              if (state is AddressError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, AddressModel addressModel) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    AddressViewModel addressVM = AddressViewModel(http: _chttp);
    if (addressModel.data.length == 0) {
      return Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/ic_address.png',
                    color: ColorPalette.btnYellow,
                    width: 120,
                  ),
                  SizedBox(height: 25),
                  Container(
                    child: Text(
                      "Wah, Anda belum mengisi alamat",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(height: 5),
                  Container(
                    child: Text(
                      "Yuk, isi alamat anda dengan menambah alamat baru",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16, color: ColorPalette.fontColor),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 80,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SizedBox(
                      height: 60,
                      width: double.infinity,
                      child: RaisedButton(
                        color: ColorPalette.themeIcon,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: Center(
                          child: Text("Tambah Alamat Baru",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return AddAddressPage();
                          })).then((nilai) => nilai ? onRefresh() : null);
                        },
                      ))))
        ],
      );
    }

    return Stack(
      children: [
        ListView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.only(bottom: 100, left: 16, right: 16, top: 16),
          scrollDirection: Axis.vertical,
          itemCount: addressModel.data.length,
          itemBuilder: (context, index) {
            return Card(
              elevation: 2,
              margin: EdgeInsets.only(bottom: 10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: InkWell(
                borderRadius: BorderRadius.circular(10),
                onTap: () async {
                  await addressVM
                      .postSetDefaultAddress(addressModel.data[index].id)
                      .then((value) {
                    if (value != null) {
                      Fluttertoast.showToast(
                          msg: "Set Default Berhasil",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 1,
                          backgroundColor: ColorPalette.btnGreen,
                          textColor: Colors.white);
                      onRefresh();
                      Navigator.pop(context, true);
                    } else {
                      Fluttertoast.showToast(
                          msg: "Set Default Gagal",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white);
                    }
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(addressModel.data[index].name,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(height: 5),
                                Text(addressModel.data[index].phoneNumber,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    )),
                                // SizedBox(height: 3),
                                Text(addressModel.data[index].address,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    )),
                              ],
                            ),
                          ),
                          SizedBox(width: 8),
                          addressModel.data[index].defaultLocation == true
                              ? Icon(Icons.check_circle,
                                  color: ColorPalette.btnGreen)
                              : Container()
                        ],
                      ),
                      SizedBox(height: 14),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return EditAddressPage(
                              dataAddressModel: addressModel.data[index],
                            );
                          })).then((val) => val ? onRefresh() : null);
                        },
                        child: Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                width: 2,
                                color: Colors.grey[350],
                              )),
                          child: Center(
                            child: Text("Ubah Alamat",
                                style: TextStyle(
                                  color: ColorPalette.fontColor,
                                  fontSize: 14,
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: 80,
                width: double.infinity,
                padding:
                    EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 8),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: SizedBox(
                    height: 60,
                    width: double.infinity,
                    child: RaisedButton(
                      color: ColorPalette.themeIcon,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(60),
                      ),
                      child: Center(
                        child: Text("Tambah Alamat Baru",
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return AddAddressPage();
                        })).then((nilai) => nilai ? onRefresh() : null);
                      },
                    ))))
      ],
    );
  }
}

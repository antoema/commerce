import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/bloc/expedition-bloc/expedition_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/expedition.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:pekik_app/view/widgets/custom_expansion_tile.dart' as custom;

class ExpeditionPage extends StatefulWidget {
  final String cartId;

  ExpeditionPage({
    Key key,
    @required this.cartId,
  }) : super(key: key);

  @override
  _ExpeditionPageState createState() => _ExpeditionPageState();
}

class _ExpeditionPageState extends State<ExpeditionPage> {
  ExpeditionBloc expeditionBloc = ExpeditionBloc();

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    expeditionBloc.add(GetExpedition(http: _chttp, cartId: widget.cartId));
    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      expeditionBloc.add(GetExpedition(http: _chttp, cartId: widget.cartId));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Pilih Jasa Pengiriman",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildExpedition(),
    );
  }

  Widget _buildExpedition() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => expeditionBloc,
        child: BlocListener<ExpeditionBloc, ExpeditionState>(
          listener: (context, state) {
            if (state is ExpeditionError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<ExpeditionBloc, ExpeditionState>(
            builder: (context, state) {
              if (state is ExpeditionInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is ExpeditionLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is ExpeditionLoaded) {
                return _buildViewExpedition(context, state.expeditionModel);
              }
              if (state is ExpeditionError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildViewExpedition(
      BuildContext context, ExpeditionModel expeditionModel) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    if (expeditionModel.data.length == 0) {
      return Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/ic_courier.png',
              width: 128,
              height: 128,
            ),
            SizedBox(height: 25),
            Container(
              child: Text(
                "Ups, belum ada kurir \nSilahkan coba ganti/tambah alamat anda",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      );
    }
    return ListView.builder(
      itemCount: expeditionModel.data.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        return expeditionModel.data[index].costs.length != 0
            ? custom.ExpansionTile(
                initiallyExpanded: true,
                headerBackgroundColor: ColorPalette.themeIcon,
                title: Text(expeditionModel.data[index].name,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold)),
                children: expeditionModel.data[index].costs
                    .asMap()
                    .map((key, kurir) => MapEntry(
                        key,
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: ListView(
                            physics: BouncingScrollPhysics(),
                            shrinkWrap: true,
                            children: [
                              ...kurir.cost.map((cost) {
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ListTile(
                                      contentPadding: EdgeInsets.all(0),
                                      onTap: () async {
                                        await transactionVM
                                            .postSetExpedition(
                                                widget.cartId,
                                                expeditionModel
                                                    .data[index].code,
                                                expeditionModel
                                                    .data[index].name,
                                                kurir.service,
                                                cost.value.toString())
                                            .then((value) {
                                          if (value != null) {
                                            Fluttertoast.showToast(
                                                msg: "Set Pengiriman Berhasil",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIos: 1,
                                                backgroundColor:
                                                    ColorPalette.btnGreen,
                                                textColor: Colors.white);
                                            onRefresh();
                                            Navigator.pop(context, true);
                                          } else {
                                            Fluttertoast.showToast(
                                                msg: "Set Pengiriman Gagal",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                timeInSecForIos: 1,
                                                backgroundColor: Colors.red,
                                                textColor: Colors.white);
                                          }
                                        });
                                      },
                                      title: Container(
                                        width: double.infinity,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(height: 8),
                                            Text(kurir.service,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                            SizedBox(height: 5),
                                            Text(
                                                "(" +
                                                    ConnexistHelper
                                                        .formatCurrency(cost
                                                            .value
                                                            .toDouble()) +
                                                    ")",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15,
                                                )),
                                            SizedBox(height: 5),
                                            Column(
                                              children: [
                                                if(cost.note == 'hours')...[
                                                  Text(
                                                      "Estimasi Tiba " +
                                                          cost.etd.replaceAll(
                                                              RegExp(r"Hours"), "") +
                                                          " Jam",
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                      ))
                                                ] else if(cost.value == 0) ...[
                                                  Text(
                                                      "Estimasi Tiba " +
                                                          cost.etd.replaceAll(
                                                              RegExp(r"hours"), "") +
                                                          " Jam",
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                      ))
                                                  ] else ... [
                                                  Text(
                                                      "Estimasi Tiba " +
                                                          cost.etd.replaceAll(
                                                              RegExp(r"HARI"), "") +
                                                          " Hari",
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                      )),
                                                ]
                                              ],
                                            ),
                                            SizedBox(height: 8),
                                          ],
                                        ),
                                      ),
                                    ),
                                    expeditionModel.data[index].costs.length ==
                                            key + 1
                                        ? Container()
                                        : Divider(
                                            thickness: 1,
                                          )
                                  ],
                                );
                              }).toList(),
                            ],
                          ),
                        )))
                    .values
                    .toList(),
              )
            : Container();
      },
    );
  }
}

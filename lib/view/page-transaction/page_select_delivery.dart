import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/bloc/checkout-bloc/checkout_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/checkout.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-transaction/page_select_address.dart';
import 'package:pekik_app/view/page-transaction/page_expedition.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class SelectDeliveryPage extends StatefulWidget {
  const SelectDeliveryPage({Key key}) : super(key: key);

  @override
  _SelectDeliveryPageState createState() => _SelectDeliveryPageState();
}

class _SelectDeliveryPageState extends State<SelectDeliveryPage> {
  CheckoutBloc checkoutBloc = CheckoutBloc();

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    checkoutBloc.add(GetCheckout(http: _chttp));
    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      checkoutBloc.add(GetCheckout(http: _chttp));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Pilih Pengiriman",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildCheckout(),
    );
  }

  Widget _buildCheckout() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => checkoutBloc,
        child: BlocListener<CheckoutBloc, CheckoutState>(
          listener: (context, state) {
            if (state is CheckoutError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<CheckoutBloc, CheckoutState>(
            builder: (context, state) {
              if (state is CheckoutInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is CheckoutLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is CheckoutLoaded) {
                return _buildViewCheckout(context, state.checkoutModel);
              }
              if (state is CheckoutError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildViewCheckout(BuildContext context, CheckoutModel checkoutModel) {
    return Stack(
      children: [
        ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Alamat Pengiriman",
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return SelectAddressPage();
                              })).then((val) => val ? onRefresh() : null);
                            },
                            child: Text(
                              "Pilih Alamat Lain",
                              style: TextStyle(
                                  fontSize: 14,
                                  color: ColorPalette.themeIcon,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 4),
                      Divider(
                        thickness: 2,
                        color: Colors.grey[100],
                      ),
                      SizedBox(height: 4),
                      checkoutModel.data.shippingAddress == null
                          ? Text("Silahkan pilih alamat pengiriman Anda.",
                              style: TextStyle(
                                color: ColorPalette.fontColor,
                                fontSize: 12,
                              ))
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(checkoutModel.data.shippingAddress.name,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(height: 5),
                                Text(
                                    checkoutModel
                                        .data.shippingAddress.phoneNumber,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    )),
                                SizedBox(height: 3),
                                Text(checkoutModel.data.shippingAddress.address,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    )),
                                // SizedBox(height: 4),
                              ],
                            ),
                    ],
                  ),
                ),
                Divider(
                  thickness: 12,
                  color: Colors.grey[100],
                ),
                ...checkoutModel.data.carts
                    .asMap()
                    .map((index, data) => MapEntry(
                        index,
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Pesanan  " + (index + 1).toString(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Container(
                                          height: 35,
                                          width: 35,
                                          padding: EdgeInsets.all(2),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(40),
                                              color: ColorPalette.themeIcon),
                                          child: Center(
                                              child: Icon(Icons.store,
                                                  color: Colors.white))),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(data.tenant.tenantName,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              data.tenant.tenantType ==
                                                      'official'
                                                  ? Image.asset(
                                                      'assets/ic_official_store.png',
                                                      width: 18,
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                          Text(data.tenant.city,
                                              style: TextStyle(
                                                color: ColorPalette.fontColor,
                                                fontSize: 12,
                                              )),
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  ...data.items.map((dataProduct) {
                                    return Container(
                                      margin: EdgeInsets.only(top: 8),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: 60,
                                            height: 60,
                                            child: Stack(
                                              children: [
                                                Container(
                                                    width: double.infinity,
                                                    height: double.infinity,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12),
                                                    ),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12),
                                                      child: CachedNetworkImage(
                                                        imageUrl: dataProduct
                                                            .productImage
                                                            .split(',')
                                                            .first,
                                                        fit: BoxFit.cover,
                                                        placeholder: (context,
                                                                url) =>
                                                            Center(
                                                                child: Shimmer
                                                                    .fromColors(
                                                          baseColor:
                                                              Colors.grey[300],
                                                          highlightColor:
                                                              Colors.grey[100],
                                                          child: Container(
                                                            color: Colors.white,
                                                            width:
                                                                double.infinity,
                                                            height:
                                                                double.infinity,
                                                          ),
                                                        )),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Center(
                                                                child:
                                                                    Image.asset(
                                                          "assets/default_image.png",
                                                          fit: BoxFit.cover,
                                                        )),
                                                      ),
                                                    )),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            width: 8,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                dataProduct.productName.length >
                                                        75
                                                    ? Text(
                                                        dataProduct.productName
                                                                .substring(
                                                                    0, 80) +
                                                            "...",
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          fontSize: 15.0,
                                                        ),
                                                      )
                                                    : Text(
                                                        dataProduct.productName,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          fontSize: 15.0,
                                                        ),
                                                      ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                    dataProduct.qty.toString() +
                                                        " barang ",
                                                    style: TextStyle(
                                                      fontSize: 12.0,
                                                      color: ColorPalette
                                                          .fontColor,
                                                    )),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                    ConnexistHelper
                                                        .formatCurrency(
                                                            dataProduct.price
                                                                .toDouble()),
                                                    style: TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  }).toList(),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      if (checkoutModel.data.shippingAddress !=
                                          null) {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) {
                                          return ExpeditionPage(
                                            cartId: data.id,
                                          );
                                        })).then(
                                            (val) => val ? onRefresh() : null);
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: "Anda belum memilih alamat",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white);
                                      }
                                    },
                                    child: Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(16),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border:
                                              Border.all(color: Colors.grey),
                                          color: Colors.white,
                                        ),
                                        child: data.expedition == null
                                            ? Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                      child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Icon(Icons.local_shipping,
                                                          color: ColorPalette
                                                              .btnGreen),
                                                      SizedBox(
                                                        width: 15,
                                                      ),
                                                      Text("Pilih Pengiriman",
                                                          style: TextStyle(
                                                            fontSize: 15.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ))
                                                    ],
                                                  )),
                                                  SizedBox(
                                                    width: 8,
                                                  ),
                                                  Icon(Icons.arrow_forward_ios,
                                                      color: Colors.grey)
                                                ],
                                              )
                                            : Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Expanded(
                                                          child: Text(
                                                              data.expedition
                                                                  .expeditionService,
                                                              style: TextStyle(
                                                                fontSize: 15.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ))),
                                                      SizedBox(
                                                        width: 8,
                                                      ),
                                                      Icon(
                                                          Icons
                                                              .arrow_forward_ios,
                                                          color: Colors.grey)
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Divider(
                                                    thickness: 1,
                                                    color: Colors.grey[100],
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Expanded(
                                                          child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: [
                                                            Text(
                                                                data.expedition
                                                                    .expeditionName,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      15.0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                            SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                                ConnexistHelper.formatCurrency(data
                                                                    .expedition
                                                                    .expeditionCost
                                                                    .toDouble()),
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        15.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    color: ColorPalette
                                                                        .fontColor)),
                                                            // SizedBox(
                                                            //   height: 5,
                                                            // ),
                                                            // Text(
                                                            //     "Estimasi tiba " +
                                                            //         data.deliveryCost
                                                            //             .estimateDays
                                                            //             .replaceAll(
                                                            //                 RegExp(
                                                            //                     r"HARI"),
                                                            //                 "") +
                                                            //         " Hari",
                                                            //     style: TextStyle(
                                                            //         fontSize:
                                                            //             14.0,
                                                            //         color: ColorPalette
                                                            //             .fontColor)),
                                                          ])),
                                                    ],
                                                  ),
                                                ],
                                              )),
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                ],
                              ),
                            ),
                            Divider(
                              thickness: 12,
                              color: Colors.grey[100],
                            ),
                          ],
                        )))
                    .values
                    .toList(),
                Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(
                        top: 8, bottom: 95, left: 16, right: 16),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: Text("Ringakasan Belanja",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Total Harga",
                                      style: TextStyle(color: Colors.black)),
                                  Container(
                                    child: Text(
                                        ConnexistHelper.formatCurrency(
                                            checkoutModel.data.totalPrice
                                                .toDouble()),
                                        style: TextStyle(color: Colors.black)),
                                  )
                                ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 8),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Ongkos Kirim",
                                      style: TextStyle(color: Colors.black)),
                                  Container(
                                    child: Text(
                                        ConnexistHelper.formatCurrency(
                                            checkoutModel.data.expeditionCost
                                                .toDouble()),
                                        style: TextStyle(color: Colors.black)),
                                  )
                                ]),
                          ),
                        ])),
              ],
            )
          ],
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: 70,
                width: double.infinity,
                padding:
                    EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 4), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                          height: 60,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Total Tagihan",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: ColorPalette.fontColor,
                                ),
                              ),
                              Text(
                                  ConnexistHelper.formatCurrency(checkoutModel
                                      .data.totalAmount
                                      .toDouble()),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        bool active = false;
                        for (int i = 0;
                            i < checkoutModel.data.carts.length;
                            i++) {
                          if (checkoutModel.data.carts[i].expedition != null) {
                            active = true;
                          } else {
                            active = false;

                            break;
                          }
                        }
                        active
                            ? Navigator.pushNamed(context, '/select-payment')
                            : Fluttertoast.showToast(
                                msg: "Anda belum memilih metode pengiriman",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                      },
                      child: Container(
                        // width: 150,
                        padding: EdgeInsets.only(left: 25, right: 25),
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(60),
                            color: ColorPalette.themeIcon),
                        child: Center(
                          child: Text(
                            "Pilih Pembayaran",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                )))
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Tentang Kami",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: [
          Container(
            height: 180,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.asset(
                      "assets/background.png",
                      width: double.infinity,
                      height: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Center(
                    child: Container(
                      width: 128,
                      height: 128,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(128)),
                      child: Image.asset(
                        "assets/logo_umkm.png",
                        width: 128,
                        height: 128,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            elevation: 4,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            margin: EdgeInsets.only(left: 16, right: 16, bottom: 20),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset(
                        "assets/logo_umkm.png",
                        height: 60,
                        width: 60,
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Text("Tentang")),
                    ],
                  ),
                  Divider(
                    thickness: 1.5,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            "Pemerintah Provinsi DKI Jakarta menghadirkan Jakpreneur sebagai platform kreasi, fasilitasi, dan kolaborasi pengembangan UMKM melalui ekosistem kewirausahaan, seperti start-up, institusipendidikan, maupun institusi pembiayaan.\n"),
                        Text(
                            "Jakpreneur dapat berbentuk kerja sama jangka panjang maupun bentuk kegiatan lainnya yang berpotensi untuk mengembangkan  keterampilan dan kemandirian berusaha dengan cara kolaboratif antara Pemerintah Provinsi DKI Jakarta, dunia pendidikan, dunia usaha, masyarakat dan/atau lembaga dan/atau pihak lainnya.\n"),
                        Text(
                            "Jak merupakan brand identity / gambaran identitas dari Kota Jakarta, sementara Preneur diambil dari kata entrepreneurship yang merupakan fokus dan subyek dari brand ini.\n"),
                        Text(
                            "Mengenai logo Jakpreneur, logo bunga melambangkan sebuah perkembangan. Bagian tengah dalam logo bunga terinspirasi dari tombol start yang merupakan langkah awal untuk memulai sesuatu. Tombol start itu kemudian tumbuh membentuk bunga mekar ke segala arah yang menggambarkan usaha yang semakin berkembang hingga ke puncaknya.\n"),
                        Text(
                            "Dengan pilihan warna putih, oranye, dan hitam untuk menampilkan nuansa semangat, optimisme, kuat, serta kemakmuran. Untuk bisa menyampaikan semangat ini kepada masyarakat, perlu adanya branding yang membumi (dekat dengan masyarakat), catchy, dan ikonik.\n"),
                        Text(
                            "Dengan Mendaftar Online melalui website jakpreneur.jakarta.go.id dan membawa dokumen yang asli ke Kecamatan atau ke tempat Instansi Pemprov DKI Jakarta yang mengadakan pelatihan tersebut.\n"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

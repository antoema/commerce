import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/wallet.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-wallet/page_withdrawal.dart';
import 'package:provider/provider.dart';

class DetailWalletPage extends StatefulWidget {
  final WalletModel walletModel;
  const DetailWalletPage({Key key, @required this.walletModel})
      : super(key: key);

  @override
  _DetailWalletPageState createState() => _DetailWalletPageState();
}

class _DetailWalletPageState extends State<DetailWalletPage> {
  @override
  Widget build(BuildContext context) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Detail Wallet",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: [
          Card(
            elevation: 2,
            margin: EdgeInsets.only(left: 16, right: 16, top: 10),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Expanded(
                      child: Row(
                    children: [
                      Image.asset(
                        'assets/ic_coin.png',
                        width: 25,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        'Total Saldo Aktif',
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      )
                    ],
                  )),
                  Text(
                    widget.walletModel.data == null
                        ? ConnexistHelper.formatCurrency(0.toDouble())
                        : ConnexistHelper.formatCurrency(
                            widget.walletModel.data.balance.toDouble()),
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: _auth.currentUser.data.role != 'basic' ? 16 : 0,
          ),
          _auth.currentUser.data.role != 'basic'
              ? GestureDetector(
                  onTap: () {
                    if (widget.walletModel.data != null) {
                      if (widget.walletModel.data.balance < 50000) {
                        Fluttertoast.showToast(
                            msg: "Penarikan Saldo Minimum Rp.50.000",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white);
                      } else {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return WithdrawalPage();
                        }));
                      }
                    } else {
                      Fluttertoast.showToast(
                          msg: "Penarikan Saldo Minimum Rp.50.000",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white);
                    }
                  },
                  child: Container(
                    height: 50,
                    margin: EdgeInsets.only(left: 16, right: 16),
                    width: double.infinity,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        color: ColorPalette.themeIcon,
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text(
                        'Tarik Saldo',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                )
              : Container(),
          SizedBox(
            height: 16,
          ),
          Divider(
            thickness: 1,
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Text(
              'Riwayat Transaksi',
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          _emptyTransaction()
        ],
      ),
    );
  }

  Widget _emptyTransaction() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.receipt_long,
            size: 120,
            color: ColorPalette.btnYellow,
          ),
          SizedBox(height: 25),
          Container(
            child: Text(
              "Wah, Belum ada transaksi",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 5),
          Container(
            child: Text(
              "Yuk, isi transaksi dengan pesanan kesukaanmu",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16, color: Colors.grey[450]),
            ),
          ),
        ],
      ),
    );
  }
}

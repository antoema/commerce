import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/wallet.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-wallet/page_detail_wallet.dart';
import 'package:pekik_app/viewmodel/wallet_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class WalletPage extends StatelessWidget {
  const WalletPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    WalletViewModel walletVM = WalletViewModel(http: _chttp);
    return FutureBuilder(
      future: walletVM.fetchMyWallet(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final WalletModel walletModel = snapshot.data;
          if (walletModel.data == null) {
            return Card(
              elevation: 2,
              margin: EdgeInsets.only(left: 16, right: 16, top: 16),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Container(
                padding: EdgeInsets.all(16),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Expanded(
                        child: GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return DetailWalletPage(
                            walletModel: walletModel,
                          );
                        }));
                      },
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/ic_coin.png',
                            width: 25,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  ConnexistHelper.formatCurrency(0.toDouble()),
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Saldo',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: ColorPalette.fontColor),
                                    ),
                                    // SizedBox(
                                    //   width: 2,
                                    // ),
                                    // Icon(
                                    //   Icons.arrow_forward_ios,
                                    //   color: Colors.black,
                                    //   size: 14,
                                    // )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
                    SizedBox(
                      width: 16,
                    ),
                    Container(
                      height: 35,
                      width: 2,
                      color: Colors.grey[400],
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                        child: GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return DetailWalletPage(walletModel: walletModel);
                        }));
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.history,
                            color: Colors.black,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              'Klik & Cek Riwayat',
                              style: TextStyle(
                                  fontSize: 14, color: ColorPalette.themeIcon),
                            ),
                          ),
                        ],
                      ),
                    )),
                  ],
                ),
              ),
            );
          }
          return Card(
            elevation: 2,
            margin: EdgeInsets.only(left: 16, right: 16, top: 16),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Container(
              padding: EdgeInsets.all(16),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return DetailWalletPage(walletModel: walletModel);
                      }));
                    },
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/ic_coin.png',
                          width: 25,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                ConnexistHelper.formatCurrency(
                                    walletModel.data.balance.toDouble()),
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Saldo',
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: ColorPalette.fontColor),
                                  ),
                                  // SizedBox(
                                  //   width: 2,
                                  // ),
                                  // Icon(
                                  //   Icons.arrow_forward_ios,
                                  //   color: Colors.black,
                                  //   size: 14,
                                  // )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
                  SizedBox(
                    width: 16,
                  ),
                  Container(
                    height: 35,
                    width: 2,
                    color: Colors.grey[400],
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return DetailWalletPage(walletModel: walletModel);
                      }));
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.history,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Text(
                            'Klik & Cek Riwayat',
                            style: TextStyle(
                                fontSize: 14, color: ColorPalette.themeIcon),
                          ),
                        ),
                      ],
                    ),
                  )),
                ],
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Container();
        }
        return _loadingWallet();
      },
    );
  }

  Widget _loadingWallet() {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: EdgeInsets.all(16),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 140,
                      height: 15.0,
                      margin: EdgeInsets.only(left: 8, right: 8, bottom: 8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                    ),
                    Row(
                      children: [
                        Container(
                          width: 15,
                          height: 15.0,
                          margin: EdgeInsets.only(left: 8, right: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                        ),
                        Container(
                          width: 80,
                          height: 15.0,
                          margin: EdgeInsets.only(left: 8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Container(
                height: 35,
                width: 2,
                color: ColorPalette.fontColor,
              ),
              SizedBox(
                width: 16,
              ),
              Expanded(
                  child: Row(
                children: [
                  Container(
                    width: 15,
                    height: 15.0,
                    margin: EdgeInsets.only(left: 8, right: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                  ),
                  Container(
                    width: 80,
                    height: 15.0,
                    margin: EdgeInsets.only(left: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.white),
                  ),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/response_bank.dart';
import 'package:pekik_app/model/user_bank.dart';
import 'package:pekik_app/model/wallet_bank.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/viewmodel/wallet_vm.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:http/http.dart' as http;

class AddBankPage extends StatefulWidget {
  const AddBankPage({Key key}) : super(key: key);

  @override
  _AddBankPageState createState() => _AddBankPageState();
}

class _AddBankPageState extends State<AddBankPage> {
  final _nomorRekening = TextEditingController();
  final _search = TextEditingController();
  final formKey = new GlobalKey<FormState>();
  bool _isLoading = true;
  bool _isLoadingPost = true;
  String namaPemilik = '';
  String namaBank = '';

  List<DataUserBank> list = [];
  List<DataUserBank> search = [];

  TextEditingController controller = TextEditingController();
  ScrollController bankController = ScrollController();

  Future fetchData() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    list.clear();
    try {
      http.Response response = await http.get(
          'https://cxapi.connexist.com/ppob-service/banklist',
          headers: {"x-access-token": "${_auth.currentUser.accessToken}"});
      UserBank cm = UserBank.fromJson(json.decode(response.body));
      List<DataUserBank> search = cm.data;
      setState(() {
        list.addAll(search);
      });
    } catch (error) {
      print(error);
    }
  }

  @override
  void initState() {
    fetchData();
    _nomorRekening.addListener(() {
      setState(() {});
    });
    _search.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  bool validates() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  onSearch(String text) async {
    search.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }
    list.forEach((bank) {
      if (bank.bankName.toLowerCase().contains(text.toLowerCase()))
        search.add(bank);
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    WalletViewModel walletVM = WalletViewModel(http: _chttp);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Tambah Rekening",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: Stack(
        children: [
          Form(
            key: formKey,
            child: ListView(
              padding:
                  EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 90),
              children: [
                Text("Nama Bank",
                    style: TextStyle(
                      fontSize: 16.0,
                    )),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () {
                    showBarModalBottomSheet(
                      expand: false,
                      context: context,
                      backgroundColor: Colors.transparent,
                      builder: (BuildContext context) {
                        return StatefulBuilder(
                          builder: (context, setState) => modalBank(context),
                        );
                      },
                    );
                  },
                  child: Container(
                      height: 60,
                      width: double.infinity,
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey),
                          color: Colors.grey[100]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              basket['namaWalletBank'] == null
                                  ? "Masukan Nama Bank"
                                  : basket['namaWalletBank'],
                              style: TextStyle(
                                  fontSize: 16.0, color: Colors.grey[600])),
                        ],
                      )),
                ),
                SizedBox(
                  height: 15,
                ),
                Text("Nomor Rekening",
                    style: TextStyle(
                      fontSize: 16.0,
                    )),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        // color: Colors.white,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0)),
                        child: new TextFormField(
                          controller: _nomorRekening,
                          decoration: new InputDecoration(
                            // labelText: "No Ponsel",
                            hintText: "Masukan No Rekening",
                            fillColor: Colors.white,
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType: TextInputType.number,
                          style: new TextStyle(
                            fontFamily: 'Poppins',
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (basket['idWalletBank'] == null) {
                          Fluttertoast.showToast(
                              msg: "Anda belum mengisi nama bank",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white);
                        } else if (_nomorRekening.text == '') {
                          Fluttertoast.showToast(
                              msg: "Anda belum mengisi nomor rekening",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIos: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white);
                        } else {
                          setState(() {
                            _isLoading = false;
                          });
                          await walletVM
                              .fetchDataBank(
                                  basket['idWalletBank'],
                                  _nomorRekening.text,
                                  50000,
                                  _auth.currentUser.data.phoneNumber)
                              .then((value) {
                            if (value != null) {
                              setState(() {
                                _isLoading = true;
                                if (value.data.namaPelanggan == '') {
                                  Fluttertoast.showToast(
                                      msg: value.data.ket,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIos: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white);
                                } else if (value.data.namaPelanggan != null) {
                                  namaPemilik = value.data.namaPelanggan;
                                  namaBank = value.data.namaBank;
                                }
                              });
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Terjadi kesalahan saat terhubung ke server",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white);
                            }
                          });
                        }
                      },
                      child: Container(
                        height: 65,
                        width: 100,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: _nomorRekening.text == ''
                                ? ColorPalette.backgroundSplash
                                : ColorPalette.themeIcon),
                        child: Center(
                          child: _isLoading
                              ? Text(
                                  'Periksa',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: _nomorRekening.text == ''
                                          ? ColorPalette.fontColor
                                          : Colors.white),
                                )
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                                ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                namaPemilik == ''
                    ? Container()
                    : Text("Nama Pemilik Rekening",
                        style: TextStyle(
                          fontSize: 16.0,
                        )),
                SizedBox(
                  height: 10,
                ),
                namaPemilik == ''
                    ? Container()
                    : Row(
                        children: [
                          Container(
                            height: 25,
                            width: 25,
                            decoration: BoxDecoration(
                                color: ColorPalette.themeIcon,
                                borderRadius: BorderRadius.circular(20)),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(namaPemilik,
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: ColorPalette.fontColor,
                                  fontWeight: FontWeight.bold)),
                        ],
                      )
              ],
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 75,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SizedBox(
                      height: 60,
                      width: double.infinity,
                      child: RaisedButton(
                        color: _nomorRekening.text == ''
                            ? ColorPalette.backgroundSplash
                            : ColorPalette.themeIcon,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: Center(
                          child: _isLoadingPost
                              ? Text("Simpan",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: _nomorRekening.text == ''
                                          ? ColorPalette.fontColor
                                          : Colors.white,
                                      fontWeight: FontWeight.bold))
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                                ),
                        ),
                        onPressed: () async {
                          if (basket['idWalletBank'] == null) {
                            Fluttertoast.showToast(
                                msg: "Anda belum mengisi nama bank",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else if (_nomorRekening.text == '') {
                            Fluttertoast.showToast(
                                msg: "Anda belum mengisi nomor rekening",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else if (namaPemilik == '') {
                            Fluttertoast.showToast(
                                msg: "Silahkan klik periksa terlebih dahulu",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else {
                            setState(() {
                              _isLoadingPost = false;
                            });
                            await walletVM
                                .postUserBank(
                                    _nomorRekening.text,
                                    basket['namaWalletBank'],
                                    basket['idWalletBank'],
                                    namaPemilik)
                                .then((value) {
                              if (value != null) {
                                //print(namaPemilik);
                                setState(() {
                                  basket.addAll({
                                    'namaWalletBank': null,
                                    'idWalletBank': null,
                                  });
                                  _isLoadingPost = true;
                                });
                                Fluttertoast.showToast(
                                    msg: "Tambah Rekening Berhasil",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIos: 1,
                                    backgroundColor: ColorPalette.btnGreen,
                                    textColor: Colors.white);
                                Navigator.pop(context, true);
                              } else {
                                setState(() {
                                  _isLoadingPost = true;
                                });
                                Fluttertoast.showToast(
                                    msg:
                                        "Terjadi kesalahan saat berkomunikasi dengan server",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white);
                              }
                            });
                          }
                        },
                      ))))
        ],
      ),
    );
  }

  Widget modalBank(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, top: 16, right: 16),
                      child: Text("Pilih Bank",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 8, bottom: 8, left: 8, right: 8),
                      child: _searchCard(),
                    ),
                    search.length != 0 || _search.text.isNotEmpty
                        ? withSearch()
                        : withoutSearch(),
                  ],
                )),
          ]),
    );
  }

  Widget loadingBank() {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: 10,
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 250,
                        height: 10,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 200,
                        height: 10,
                      ),
                    ),
                  ],
                )),
                Icon(
                  Icons.lens_outlined,
                  color: Colors.black,
                )
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            thickness: 1,
          );
        },
      ),
    );
  }

  Widget withoutSearch() {
    print('withoutSearch');
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    WalletViewModel walletViewModel = WalletViewModel(http: _chttp);
    return Expanded(
      flex: 40,
      child: FutureBuilder<WalletBank>(
        future: walletViewModel.fetchListBank(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final WalletBank walletBank = snapshot.data;
            print(walletBank);
            return ListView.separated(
              shrinkWrap: true,
              padding: EdgeInsets.all(16),
              itemCount: walletBank.data.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    setState(() {
                      basket.addAll({
                        'namaWalletBank': walletBank.data[index].bankName,
                        'idWalletBank': walletBank.data[index].bankCode
                      });
                      _nomorRekening.text = '';
                      namaPemilik = '';
                    });
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          walletBank.data[index].bankName,
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                        basket['idWalletBank'] ==
                                walletBank.data[index].bankCode
                            ? Icon(
                                Icons.check_circle,
                                color: ColorPalette.btnGreen,
                              )
                            : Icon(
                                Icons.lens_outlined,
                                color: Colors.black,
                              )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider(
                  thickness: 1,
                );
              },
            );
          } else if (snapshot.hasError) {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/ic_failure.png',
                      width: 150,
                    ),
                    SizedBox(height: 25),
                    Container(
                      child: Text(
                        "Ups, Koneksi Gagal",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      'Terjadi kesalahan saat berkomunikasi dengan server',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
            );
          }
          return loadingBank();
        },
      ),
    );
  }

  Widget withSearch() {
    print('withSearch');
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    print(search);
    print('ada');
    return Expanded(
      flex: 40,
      child: ListView.separated(
        padding: EdgeInsets.all(16),
        controller: bankController,
        itemCount: search.length,
        itemBuilder: (context, index) {
          final data = search[index];
          return InkWell(
            onTap: () {
              setState(() {
                basket.addAll({
                  'namaWalletBank': data.bankName,
                  'idWalletBank': data.bankCode
                });
                _nomorRekening.text = '';
                namaPemilik = '';
              });
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    data.bankName,
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  basket['idWalletBank'] == data.bankCode
                      ? Icon(
                    Icons.check_circle,
                    color: ColorPalette.btnGreen,
                  )
                      : Icon(
                    Icons.lens_outlined,
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            thickness: 1,
          );
        },
      ),
    );
  }

  Widget _searchCard() => Container(
        height: 45.0,
        width: double.infinity,
        margin: EdgeInsets.only(right: 16),
        child: CupertinoTextField(
          controller: controller,
          keyboardType: TextInputType.text,
          placeholder: 'Cari Bank',
          placeholderStyle: TextStyle(
            color: Color(0xffC4C6CC),
            fontSize: 14.0,
            fontFamily: 'Poppins',
          ),
          onChanged: onSearch,
          style: TextStyle(fontFamily: 'Poppins'),
          prefix: Padding(
            padding: const EdgeInsets.fromLTRB(9.0, 6.0, 9.0, 6.0),
            child: Icon(
              Icons.search,
              color: Color(0xffC4C6CC),
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Color(0xffF0F1F5),
          ),
        ),
      );
}

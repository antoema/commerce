import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-home/page_home.dart';
import 'package:pekik_app/view/page-home/widget/home_widget.dart';
import 'package:pekik_app/viewmodel/wallet_vm.dart';
import 'package:provider/provider.dart';
import 'package:pekik_app/utils/api_helper.dart';

class CheckoutWidgetPage extends StatefulWidget {
  const CheckoutWidgetPage({Key key}) : super(key: key);

  @override
  _CheckoutWidgetPageState createState() => _CheckoutWidgetPageState();
}

class _CheckoutWidgetPageState extends State<CheckoutWidgetPage> {
  bool _isLoading = true;
  String namaBank = '';
  String numberBank = '';
  String namaPemilik = '';

  @override
  Widget build(BuildContext context) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    WalletViewModel walletVM = WalletViewModel(http: _chttp);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Checkout",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 4),
                SizedBox(height: 4),
                SizedBox(height: 4),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(basket['dataBank']['namaBank'],
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
                    SizedBox(height: 5),
                    Text(basket['dataBank']['numberBank'],
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        )),
                    SizedBox(height: 3),
                    Text("a/n " + basket['dataBank']['namaPemilik'],
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        )),
                    Divider(
                      thickness: 2,
                      color: Colors.grey[100],
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Nominal",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                          Text(
                              ConnexistHelper.formatCurrency(
                                  basket['dataCheckout']['nominal'].toDouble()),
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ]),
                    SizedBox(height: 5),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Biaya Admin",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                          Text(
                              ConnexistHelper.formatCurrency(
                                  basket['dataCheckout']['admin'].toDouble()),
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ]),
                    SizedBox(height: 5),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Total",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                          Text(
                              ConnexistHelper.formatCurrency(
                                  basket['dataCheckout']['total'].toDouble()),
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ]),
                    // SizedBox(height: 4),
                  ],
                ),
              ],
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 75,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SizedBox(
                      height: 60,
                      width: double.infinity,
                      child: RaisedButton(
                        color: Colors.deepOrange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: Center(
                          child: _isLoading
                              ? Text("Kirim Sekarang",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold))
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                                ),
                        ),
                        onPressed: () async {
                          setState(() {
                            _isLoading = false;
                          });
                          await walletVM
                              .postWithdrawPay(basket['dataBank']['idUserBank'].toString(),
                                  basket['dataCheckout']['nominal'].toString())
                              .then((value) {
                            if (value != null) {
                              setState(() {
                                _isLoading = true;
                              });
                              Fluttertoast.showToast(
                                  msg: "Tarik saldo berhasil",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIos: 1,
                                  backgroundColor: ColorPalette.btnGreen,
                                  textColor: Colors.white);
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return Home();
                              }));
                            } else {
                              setState(() {
                                _isLoading = true;
                              });
                              Fluttertoast.showToast(
                                  msg: "Terjadi kesalahan saat berkomunikasi dengan server",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white);
                            }
                          });
                        },
                      ))))
        ],
      ),
    );
  }
}

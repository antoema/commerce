import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/response_wd_checkout.dart';
import 'package:pekik_app/view/page-wallet/page_bank_account.dart';
import 'package:pekik_app/view/page-wallet/page_checkout_confirm.dart';
import 'package:pekik_app/view/widgets/widget_coming_soon.dart';
import 'package:pekik_app/viewmodel/wallet_vm.dart';
import 'package:provider/provider.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/model/checkout.dart';

class WithdrawalPage extends StatefulWidget {
  const WithdrawalPage({Key key}) : super(key: key);

  @override
  _WithdrawalPageState createState() => _WithdrawalPageState();
}

class _WithdrawalPageState extends State<WithdrawalPage> {
  final _nominal = TextEditingController();
  bool _isLoading = true;
  String namaBank = '';
  String numberBank = '';
  String namaPemilik = '';

  @override
  void initState() {
    _nominal.addListener(() {
      setState(() {});
    });
    _getRequest();
    super.initState();
  }

  _getRequest() {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    if (basket['dataBank'] != null) {
      setState(() {
        namaBank = basket['dataBank']['namaBank'];
        numberBank = basket['dataBank']['numberBank'];
        namaPemilik = basket['dataBank']['namaPemilik'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    WalletViewModel walletVM = WalletViewModel(http: _chttp);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Penarikan Saldo",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: Stack(
        children: [
          ListView(
            padding: EdgeInsets.only(bottom: 75),
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Kirim Ke",
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return BankAccountPage();
                            })).then((val) => val ? _getRequest() : null);
                          },
                          child: Text(
                            "Pilih Bank Lain",
                            style: TextStyle(
                                fontSize: 14,
                                color: ColorPalette.themeIcon,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 4),
                    Divider(
                      thickness: 2,
                      color: Colors.grey[100],
                    ),
                    SizedBox(height: 4),
                    basket['dataBank'] == null
                        ? Text("Silahkan pilih rekening Anda.",
                            style: TextStyle(
                              color: ColorPalette.fontColor,
                              fontSize: 12,
                            ))
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(namaBank,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 5),
                              Text(numberBank,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                              SizedBox(height: 3),
                              Text("a/n " + namaPemilik,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                              // SizedBox(height: 4),
                            ],
                          ),
                  ],
                ),
              ),
              Divider(
                thickness: 12,
                color: Colors.grey[100],
              ),
              Container(
                // color: Colors.white,
                width: double.infinity,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0)),
                child: new TextFormField(
                  controller: _nominal,
                  decoration: new InputDecoration(
                    // labelText: "No Ponsel",
                    hintText: "Masukan Nominal (Min Rp. 50.000)",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: new BorderSide(),
                    ),
                  ),
                  keyboardType: TextInputType.number,
                  style: new TextStyle(
                    fontFamily: 'Poppins',
                  ),
                ),
              ),
            ],
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 75,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SizedBox(
                      height: 60,
                      width: double.infinity,
                      child: RaisedButton(
                        color: _nominal.text == ''
                            ? ColorPalette.backgroundSplash
                            : ColorPalette.themeIcon,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: Center(
                          child: _isLoading
                              ? Text("Tarik Saldo",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: _nominal.text == ''
                                          ? ColorPalette.fontColor
                                          : Colors.white,
                                      fontWeight: FontWeight.bold))
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                                ),
                        ),
                        onPressed: () async {
                          setState(() {
                            _isLoading = false;
                          });
                          if (_nominal.text == '') {
                            setState(() {
                              _isLoading = true;
                            });
                            Fluttertoast.showToast(
                                msg: "Anda belum mengisi nominal",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else if (int.parse(_nominal.text) < 50000) {
                            setState(() {
                              _isLoading = true;
                            });
                            Fluttertoast.showToast(
                                msg: "Penarikan Saldo Minimum Rp.50.000",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else if(basket['dataBank'] == null) {
                            setState(() {
                              _isLoading = true;
                            });
                            Fluttertoast.showToast(
                                msg: "Silahkan pilih rekening Anda.",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white);
                          } else {
                            print(basket['dataBank']['idUserBank']);
                            setState(() {
                              _isLoading = true;
                            });
                            await walletVM
                                .postWithdrawCheckout(
                                    basket['dataBank']['idUserBank'],
                                    _nominal.text)
                                .then((value) {
                              print('cek luar');
                              print(value);
                              if (value != null) {
                                print('ce tengah');
                                print(value);
                                print('=========');
                                if(value.toString() == '{"error":"Not enough balance"}'){
                                  print('cek dalem');
                                  print(value);
                                  setState(() {
                                    _isLoading = true;
                                  });
                                  Fluttertoast.showToast(
                                      msg: 'Saldo tidak mencukupi',
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIos: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white);
                                } else {
                                  print("cekcek1");
                                  //ResponseWdCheckout v = ResponseWdCheckout.fromJson(json.decode(json.encode(value.toString())));
                                  ResponseWdCheckout v = ResponseWdCheckout.fromJson(value);
                                  print("cekcek");
                                  print(v.data.nominal);
                                  //List<ResponseWdCheckout> list = v.map((val) =>  ResponseWdCheckout.fromJson(val)).toList();
                                  setState(() {
                                    _isLoading = true;
                                    basket.addAll({
                                      "dataCheckout": {
                                        "nominal": v.data.nominal,
                                        "admin": v.data.admin,
                                        "total": v.data.totalAmount
                                      }
                                    });
                                  });
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                        return CheckoutWidgetPage();
                                      }));
                                }
                              } else {
                                setState(() {
                                  _isLoading = true;
                                });
                                Fluttertoast.showToast(
                                    msg: "Sistem sedang dalam perbaikan. Silakan coba beberapa saat lagi",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white);
                              }
                            }); catchError(onError) {
                              print(onError);
                            }
                          }
                        },
                      ))))
        ],
      ),
    );
  }
}

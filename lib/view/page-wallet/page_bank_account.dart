import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pekik_app/bloc/user-bank-bloc/user_bank_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/user_bank.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/view/page-wallet/page_add_bank.dart';
import 'package:pekik_app/viewmodel/wallet_vm.dart';
import 'package:provider/provider.dart';

class BankAccountPage extends StatefulWidget {
  const BankAccountPage({Key key}) : super(key: key);

  @override
  _BankAccountPageState createState() => _BankAccountPageState();
}

class _BankAccountPageState extends State<BankAccountPage> {
  UserBankBloc userBankBloc = UserBankBloc();

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    userBankBloc.add(GetUserBank(http: _chttp));
    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      userBankBloc.add(GetUserBank(http: _chttp));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: ColorPalette.themeIcon),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text(
            "Rekening Bank",
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
        ),
        body: _buildListUserBank());
  }

  Widget _buildListUserBank() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => userBankBloc,
        child: BlocListener<UserBankBloc, UserBankState>(
          listener: (context, state) {
            if (state is UserBankError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<UserBankBloc, UserBankState>(
            builder: (context, state) {
              if (state is UserBankInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is UserBankLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is UserBankLoaded) {
                return _buildList(context, state.userBank);
              }
              if (state is UserBankError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildList(BuildContext context, UserBank userBank) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    WalletViewModel walletVM = WalletViewModel(http: _chttp);
    if (userBank.data.length == 0) {
      return Stack(
        children: [
          _emptyRekeningBank(),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 75,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SizedBox(
                      height: 60,
                      width: double.infinity,
                      child: RaisedButton(
                        color: ColorPalette.themeIcon,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: Center(
                          child: Text("Tambah Rekening",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return AddBankPage();
                          })).then((val) => val ? onRefresh() : null);
                        },
                      ))))
        ],
      );
    }
    return Stack(
      children: [
        ListView.builder(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 75, top: 16),
          itemCount: userBank.data.length,
          itemBuilder: (context, index) {
            return Card(
              elevation: 2,
              margin: EdgeInsets.only(bottom: 10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: InkWell(
                borderRadius: BorderRadius.circular(10),
                onTap: () async {
                  setState(() {
                    basket.addAll({
                      "dataBank": {
                        "idUserBank": userBank.data[index].id,
                        "namaBank": userBank.data[index].bankName,
                        "numberBank": userBank.data[index].bankNumber,
                        "namaPemilik": userBank.data[index].accName
                      }
                    });
                  });
                  Navigator.pop(context, true);
                },
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(userBank.data[index].bankName,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                                SizedBox(height: 5),
                                Text(userBank.data[index].bankNumber,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    )),
                                // SizedBox(height: 3),
                                Text("a/n " + userBank.data[index].accName,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 14),
                      GestureDetector(
                        onTap: () async {
                          await walletVM
                              .deleteUserBank(userBank.data[index].id)
                              .then((value) {
                            if (value != null) {
                              Fluttertoast.showToast(
                                  msg: "Hapus Rekening Berhasil",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 1,
                                  backgroundColor: ColorPalette.btnGreen,
                                  textColor: Colors.white);
                              onRefresh();
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "Terjadi kesalahan saat berkomunikasi dengan server",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white);
                            }
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                width: 2,
                                color: Colors.grey[350],
                              )),
                          child: Center(
                            child: Text("Hapus Rekening",
                                style: TextStyle(
                                  color: ColorPalette.fontColor,
                                  fontSize: 14,
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                height: 75,
                width: double.infinity,
                padding:
                    EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: SizedBox(
                    height: 60,
                    width: double.infinity,
                    child: RaisedButton(
                      color: ColorPalette.themeIcon,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(60),
                      ),
                      child: Center(
                        child: Text("Tambah Rekening",
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return AddBankPage();
                        })).then((val) => val ? onRefresh() : null);
                      },
                    ))))
      ],
    );
  }

  Widget _emptyRekeningBank() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/ic_bank_account.png',
              color: ColorPalette.btnYellow,
              width: 120,
            ),
            SizedBox(height: 25),
            Container(
              child: Text(
                "Wah, belum ada rekening bank",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: 5),
            Container(
              child: Text(
                "Yuk, tambahkan rekening baru",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, color: ColorPalette.fontColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/view/page-wallet/page_pilih_pembayaran_topup.dart';
import 'package:provider/provider.dart';

class TopUpPage extends StatefulWidget {
  @override
  _TopUpPageState createState() => _TopUpPageState();
}

class _TopUpPageState extends State<TopUpPage> {
  Color color;
  Color colorFont;
  String idx;
  // String _idProduct = "9aa56191-1ea7-4be4-bf17-7d391c04e78b";
  var _nominalController;
  String topupType;

  List<Map<String, Object>> topUpSaldo = [
    {"id": 1, "saldo": "100000"},
    {"id": 2, "saldo": "250000"},
    {"id": 3, "saldo": "500000"},
    {"id": 4, "saldo": "1000000"}
  ];

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> _basket =
        Provider.of<Map<String, dynamic>>(context, listen: false);
    CHttp _chttp = Provider.of(context, listen: false);
    Auth _auth = _chttp.auth;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        title: Text("Topup saldoku",
            style: TextStyle(
                fontWeight: FontWeight.bold, color: ColorPalette.themeIcon)),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              ListView(
                shrinkWrap: true,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(
                            left: 5, right: 5, top: 5, bottom: 70),
                        child: GridView.count(
                            crossAxisCount: 3,
                            shrinkWrap: true,
                            childAspectRatio: 1,
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            children: List.generate(
                                topUpSaldo.length,
                                (index) => Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                              margin: EdgeInsets.all(5.0),
                                              child: Card(
                                                elevation: 2.0,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    side: BorderSide(
                                                        width: 1,
                                                        color: ColorPalette
                                                            .themeIcon)),
                                                color: idx ==
                                                        topUpSaldo[index]["id"]
                                                            .toString()
                                                    ? ColorPalette.themeIcon
                                                    : color,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      print("tapped");
                                                      print(topUpSaldo[index]
                                                          ["id"]);
                                                      idx = topUpSaldo[index]
                                                              ["id"]
                                                          .toString();

                                                      _nominalController =
                                                          TextEditingController(
                                                              text: topUpSaldo[
                                                                      index]
                                                                  ["saldo"]);
                                                      _basket.addAll({
                                                        "nominal":
                                                            topUpSaldo[index]
                                                                ["saldo"]
                                                      });
                                                    });
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10.0),
                                                    width: 100.0,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(4.0)),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Column(
                                                          children: [
                                                            Center(
                                                                child: Text(
                                                              NumberFormat(
                                                                      "###,000",
                                                                      "id_ID")
                                                                  .format(int.parse(
                                                                      topUpSaldo[
                                                                              index]
                                                                          [
                                                                          "saldo"])),
                                                              style: TextStyle(
                                                                  color: idx ==
                                                                          topUpSaldo[index]["id"]
                                                                              .toString()
                                                                      ? Colors
                                                                          .white
                                                                      : colorFont,
                                                                  fontSize:
                                                                      16.0),
                                                            )),
                                                            // Center(
                                                            //     child: Text(
                                                            //   "Bayar Rp.${_productPulsa.data[index].price}",
                                                            //   style: TextStyle(fontSize: 10.0, color: idx == _productPulsa.data[index].productId ? Colors.white : colorFont),
                                                            //   textAlign: TextAlign.center,
                                                            // )),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )),
                                        ),
                                      ],
                                    ))),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 70.0,
              color: Colors.white,
              padding: EdgeInsets.only(
                  left: 10.0, right: 10.0, bottom: 10.0, top: 5.0),
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(70)),
                color: _nominalController == null
                    ? Colors.grey[200]
                    : ColorPalette.themeIcon,
                onPressed: _nominalController == null
                    ? () {
                        Fluttertoast.showToast(
                            msg: "Anda belum memilih nominal",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIos: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white);
                      }
                    : () {
                        // setState(() {
                        //   topupType = "topup-saldo";
                        //   print("coba print type");
                        //   print(topupType);
                        // });
                        // _basket.addAll({
                        //   "nominal": topUpSaldo[int.parse(idx) - 1]["nominal"],
                        //   "type": "topup.wallet",
                        //   "qty": 1,
                        //   "phoneNumber": _auth.currentUser.data.phoneNumber,
                        //   "productId": topUpSaldo[int.parse(idx) - 1]
                        //       ["product_id"],
                        //   "topup_type": topupType,
                        // });
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return PilihPembayaranTopUpPage();
                        }));
                      },
                child: Text(
                  "Pilih Cara Pembayaran",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: _nominalController == null
                          ? Colors.grey
                          : Colors.white),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/address.dart';
import 'package:pekik_app/model/cart_badge.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-home/widget/widget_banner.dart';
import 'package:pekik_app/view/page-home/widget/widget_category.dart';
import 'package:pekik_app/view/page-public-product/page_public_product_filter.dart';
import 'package:pekik_app/view/page-transaction/page_cart.dart';
import 'package:pekik_app/view/page-transaction/page_select_address.dart';
import 'package:pekik_app/view/page-wallet/page_wallet.dart';
import 'package:pekik_app/viewmodel/address_vm.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  String _currentAddress;
  String _currentAddressView;
  CartBadgesModel cartBadgesModel;
  bool isActive = true;
  AddressModel addressModel;
  bool isAlamat = true;

  @override
  void initState() {
    _getCurrentLocation();
    _getRequest();
    _getAddress();
    super.initState();
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.medium)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.thoroughfare} ${place.subThoroughfare}, ${place.locality}, ${place.postalCode}, ${place.administrativeArea}, ${place.country}";
        _currentAddressView =
            "${place.locality}, ${place.administrativeArea}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  _getRequest() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    if (_auth.currentUser != null) {
      await transactionVM.fetchCartBadge().then((value) {
        if (value != null) {
          setState(() {
            cartBadgesModel = value;
            isActive = false;
          });
        } else {
          setState(() {
            isActive = true;
          });
        }
      });
    }
  }

  _getAddress() async {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    AddressViewModel addressVM = AddressViewModel(http: _chttp);
    if (_auth.currentUser != null) {
      await addressVM.fetchAddressAll().then((alamat) {
        if (alamat != null) {
          setState(() {
            addressModel = alamat;

            isAlamat = false;
          });
        } else {
          setState(() {
            isAlamat = false;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);

    if (_currentPosition != null) {
      print("ada posisi: $_currentAddress");
      basket.addAll({
        "geo-position": {
          'address': _currentAddress,
          'addressView': _currentAddressView,
          'lat': _currentPosition.latitude,
          'long': _currentPosition.longitude,
        }
      });
      print("data latitude");
      print(basket['geo-position']['lat']);
    } else {
      basket.addAll({
        "geo-position": {
          'lat': -6.2761853,
          'long': 106.8144653,
        }
      });
    }
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        titleSpacing: 0,
        elevation: 1,
        backgroundColor: Colors.white,
        title: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, "/search")
                      .then((val) => val ? _getRequest() : null);
                },
                child: Container(
                  padding: EdgeInsets.all(10),
                  height: 45,
                  decoration: BoxDecoration(
                      color: Color(0xffF0F1F5),
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(width: 0.5, color: Color(0xffC4C6CC))),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.search,
                        color: ColorPalette.themeIcon,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Cari Produk atau Toko",
                        style: TextStyle(
                          color: Color(0xffC4C6CC),
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        actions: [
          // Padding(
          //   padding: EdgeInsets.only(left: 16),
          //   child: GestureDetector(
          //     onTap: () {
          //       Navigator.push(context, MaterialPageRoute(builder: (context) {
          //         return ComingSoonWidget();
          //       }));
          //     },
          //     child: Icon(
          //       Icons.chat_outlined,
          //       color: Colors.black,
          //     ),
          //   ),
          // ),
          isActive
              ? Padding(
                  padding: EdgeInsets.only(left: 12, right: 16),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return CartPage();
                      })).then((val) => val ? _getRequest() : null);
                    },
                    child: Icon(
                      Icons.shopping_cart_outlined,
                      color: Colors.black,
                    ),
                  ),
                )
              : _shoppingCartBadge()
        ],
      ),
      body: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(bottom: 60),
        children: [
          _auth.currentUser == null
              ? Container()
              : isAlamat
                  ? Container()
                  : addressModel.data.length == 0
                      ? Container()
                      : Padding(
                          padding: const EdgeInsets.only(
                              left: 16, right: 16, top: 16),
                          child: Row(
                            children: [
                              Icon(
                                Icons.location_on_outlined,
                                color: Colors.grey[400],
                                size: 25,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return SelectAddressPage();
                                  })).then((val) => val ? _getAddress() : null);
                                },
                                child: Row(
                                  children: [
                                    Text('Dikirim ke '),
                                    ...addressModel.data
                                        .where((element) =>
                                            element.defaultLocation == true)
                                        .map((address) {
                                      return Text(
                                        address.name,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      );
                                    }),
                                    SizedBox(
                                      width: 2,
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.black,
                                      size: 15,
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
          _auth.currentUser == null ? Container() : WalletPage(),
          CategoryWidget(),
          SizedBox(
            height: 8,
          ),
          BannerWidget(),
          SizedBox(
            height: 8,
          ),
          _productFilter('Produk Terbaru', 'latests'),
          SizedBox(
            height: 8,
          ),
          Stack(
            children: [
              Image.asset(
                'assets/background.png',
                height: 220,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              _productFilter('Produk Termurah', 'cheapests'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _productFilter(String title, String path) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    PublicProductViewModel productVm = PublicProductViewModel();
    return FutureBuilder<ProductModel>(
      future: productVm.fetchProductHome(path),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final ProductModel productModel = snapshot.data;
          return productModel.data.length == 0
              ? Container()
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            title,
                            style: TextStyle(
                                // fontFamily: "Caveat",
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold
                                // color: kKepasarTeal
                                ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return PublicProductFilterPage(
                                    title: title, path: path);
                              })).then((val) => val ? _getRequest() : null);
                            },
                            child: Text(
                              "Lihat Semua",
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 320,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        padding: EdgeInsets.all(16),
                        physics: BouncingScrollPhysics(),
                        itemCount: productModel.data.length + 1,
                        itemBuilder: (context, index) {
                          if (productModel.data.length == index) {
                            return Container(
                              width: 160,
                              child: Card(
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(10.0),
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return PublicProductFilterPage(
                                          title: title, path: path);
                                    })).then(
                                        (val) => val ? _getRequest() : null);
                                  },
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 3,
                                              blurRadius: 3,
                                              offset: Offset(0,
                                                  2), // changes position of shadow
                                            ),
                                          ],
                                          borderRadius:
                                              BorderRadius.circular(50),
                                        ),
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: ColorPalette.themeIcon,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 16,
                                      ),
                                      Text(
                                        'Lihat Semua',
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.black),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                          return Container(
                            width: 160,
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: InkWell(
                                borderRadius: BorderRadius.circular(10.0),
                                onTap: () {
                                  basket.addAll({
                                    "idProduct": productModel.data[index].id
                                  });
                                  Navigator.pushNamed(context, "/detail-produk")
                                      .then(
                                          (val) => val ? _getRequest() : null);
                                },
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    AspectRatio(
                                      aspectRatio: 7 / 5.8,
                                      child: Container(
                                        // height: imageHeight,
                                        // width: imageWidth,
                                        child: ClipRRect(
                                          child: CachedNetworkImage(
                                            imageUrl: productModel
                                                .data[index].productImage
                                                .split(',')
                                                .first,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) =>
                                                Center(
                                                    child: Shimmer.fromColors(
                                              baseColor: Colors.grey[300],
                                              highlightColor: Colors.grey[100],
                                              child: Container(
                                                color: Colors.white,
                                                width: double.infinity,
                                                height: double.infinity,
                                              ),
                                            )),
                                            errorWidget:
                                                (context, url, error) => Center(
                                                    child: Image.asset(
                                              "assets/default_image.png",
                                              fit: BoxFit.cover,
                                              width: double.infinity,
                                            )),
                                          ),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10.0),
                                              topRight: Radius.circular(10.0)),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.all(12.0),
                                      alignment: Alignment.centerLeft,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${productModel.data[index].productName}",
                                            maxLines: 2,
                                            style: TextStyle(fontSize: 16.0),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            ConnexistHelper.formatCurrency(
                                                productModel
                                                    .data[index].productPrice
                                                    .toDouble()),
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "${productModel.data[index].tenant.city}",
                                            maxLines: 1,
                                            style: TextStyle(
                                              fontSize: 12.0,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          productModel.data[index].tenant
                                                      .tenantType ==
                                                  'official'
                                              ? Row(
                                                  children: [
                                                    Image.asset(
                                                      'assets/ic_official_store.png',
                                                      width: 20,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      "Official Store",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 14.0,
                                                          color: ColorPalette
                                                              .btnGreen),
                                                    ),
                                                  ],
                                                )
                                              : productModel.data[index]
                                                          .productStock <
                                                      1
                                                  ? Text(
                                                      "Stok Habis",
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 12.0,
                                                          color: Colors.red),
                                                    )
                                                  : productModel.data[index]
                                                              .productStock <
                                                          3
                                                      ? Text(
                                                          "Stok Hampir Habis",
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: 12.0,
                                                              color:
                                                                  Colors.red),
                                                        )
                                                      : Text(
                                                          "Stok Tersedia",
                                                          maxLines: 1,
                                                          style: TextStyle(
                                                              fontSize: 12.0,
                                                              color:
                                                                  Colors.black),
                                                        ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                );
        }
        return _loadingList();
      },
    );
  }

  Widget _loadingList() {
    return Container(
      height: 320,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 16),
            child: Shimmer.fromColors(
              baseColor: Colors.grey[300],
              highlightColor: Colors.grey[100],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 15,
                    width: 150,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                  ),
                  Container(
                    height: 15,
                    width: 80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 280,
            child: ListView.builder(
              padding: EdgeInsets.all(16),
              physics: ScrollPhysics(),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: 6,
              itemBuilder: (context, index) {
                return Container(
                  width: 160,
                  child: Card(
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 140,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(15),
                                  topLeft: Radius.circular(15),
                                ),
                                color: Colors.white),
                          ),
                          SizedBox(height: 10),
                          Container(
                            width: 150,
                            height: 15.0,
                            margin:
                                EdgeInsets.only(left: 8, right: 8, bottom: 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white),
                          ),
                          Container(
                            width: 100,
                            height: 15.0,
                            margin: EdgeInsets.only(left: 8, right: 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _shoppingCartBadge() {
    return cartBadgesModel.data.shoppingCartBadges == 0
        ? Padding(
            padding: EdgeInsets.only(left: 12, right: 16),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CartPage();
                })).then((val) => val ? _getRequest() : null);
              },
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Colors.black,
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.only(left: 4, right: 4),
            child: Badge(
              position: BadgePosition.topEnd(top: 3, end: 6),
              animationDuration: Duration(milliseconds: 300),
              animationType: BadgeAnimationType.slide,
              badgeContent: Text(
                cartBadgesModel.data.shoppingCartBadges.toString(),
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
              child: IconButton(
                  icon: Icon(Icons.shopping_cart_outlined, color: Colors.black),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CartPage();
                    })).then((val) => val ? _getRequest() : null);
                  }),
            ),
          );
  }
}

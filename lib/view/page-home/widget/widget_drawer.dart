import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/content_view.dart';
import 'package:pekik_app/view/widgets/widget_detail_image.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    Map<String, dynamic> _basket =
        Provider.of<Map<String, dynamic>>(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);

    print("object");
    print(_auth.currentUser.data.avatarUrl);

    return Drawer(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              width: double.infinity,
              height: 270,
              child: Stack(
                children: [
                  Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/background.png"),
                            fit: BoxFit.cover,
                          ),
                          color: Colors.white)),
                  Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return DetailImagePage(
                              imageUrl: _auth.currentUser.data.avatarUrl,
                            );
                          }));
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 50, bottom: 20),
                          child: Stack(children: [
                            Center(
                              child: Container(
                                  width: 140,
                                  height: 140,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(150),
                                      child: CachedNetworkImage(
                                        imageUrl: _auth.currentUser == null
                                            ? ''
                                            : _auth.currentUser.data
                                                        .avatarUrl ==
                                                    null
                                                ? ''
                                                : _auth
                                                    .currentUser.data.avatarUrl,
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) => Center(
                                            child: Shimmer.fromColors(
                                          baseColor: Colors.grey[300],
                                          highlightColor: Colors.grey[100],
                                          child: Container(
                                            color: Colors.white,
                                            width: double.infinity,
                                            height: double.infinity,
                                          ),
                                        )),
                                        errorWidget: (context, url, error) =>
                                            Image.asset(
                                                "assets/default_profile.png"),
                                      )),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  )),
                            ),
                          ]),
                        ),
                      ),
                      Center(
                        child: _auth.currentUser == null
                            ? Text("Nama Pengguna",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold))
                            : Text(_auth.currentUser.data.name,
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                      ),
                      Center(
                        child: _auth.currentUser == null
                            ? Text("Ex : 081282154871",
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white))
                            : Text(_auth.currentUser.data.phoneNumber,
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white)),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 270),
            child: ListView(
              children: [
                _auth.currentUser == null
                    ? Container()
                    : ListTile(
                        title: Container(
                          margin: EdgeInsets.only(left: 16, right: 16),
                          child: Row(
                            children: [
                              Icon(
                                Icons.person_outline,
                                color: ColorPalette.themeIcon,
                                size: 35,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15),
                                child: Text("Profil",
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                    )),
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          if (_auth.currentUser == null) {
                            Navigator.pushNamed(context, "/login");
                          } else {
                            Navigator.pushNamed(context, "/profile");
                          }
                        },
                      ),
                _auth.currentUser == null
                    ? Container()
                    : _auth.currentUser.data.role == 'seller'
                        ? ListTile(
                            title: Container(
                              margin: EdgeInsets.only(left: 16, right: 16),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.store_outlined,
                                    color: ColorPalette.themeIcon,
                                    size: 35,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 15),
                                    child: Text("Toko Saya",
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.black,
                                        )),
                                  )
                                ],
                              ),
                            ),
                            onTap: () {
                              if (_auth.currentUser == null) {
                                Navigator.pushNamed(context, "/login");
                              } else {
                                Navigator.pushNamed(context, "/toko-saya");
                              }
                            },
                          )
                        : Container(),
                // _auth.currentUser.data == null
                //     ? Container()
                //     : _auth.currentUser.data.role == 'seller'
                //         ? ListTile(
                //             title: Container(
                //               margin: EdgeInsets.only(left: 16, right: 16),
                //               child: Row(
                //                 children: [
                //                   Icon(
                //                     Icons.receipt_outlined,
                //                     color: ColorPalette.themeIcon,
                //                     size: 35,
                //                   ),
                //                   Container(
                //                     margin: EdgeInsets.only(left: 15),
                //                     child: Text("Penjualan",
                //                         style: TextStyle(
                //                           fontSize: 18,
                //                           color: Colors.black,
                //                         )),
                //                   )
                //                 ],
                //               ),
                //             ),
                //             onTap: () {
                //               if (_auth.currentUser == null) {
                //                 Navigator.pushNamed(context, "/login");
                //               } else {
                //                 Navigator.pushNamed(context, "/pesanan");
                //               }
                //             },
                //           )
                //         : Container(),
                // _auth.currentUser == null
                //     ? Container()
                //     : ListTile(
                //         title: Container(
                //           margin: EdgeInsets.only(left: 16, right: 16),
                //           child: Row(
                //             children: [
                //               Icon(
                //                 Icons.store,
                //                 color: ColorPalette.themeIcon,
                //                 size: 35,
                //               ),
                //               Container(
                //                 margin: EdgeInsets.only(left: 15),
                //                 child: _basket["outletId"] == null
                //                     ? Text("Buka Toko",
                //                         style: TextStyle(
                //                             fontSize: 18,
                //                             color: ColorPalette.themeIcon,
                //                             fontWeight: FontWeight.bold))
                //                     : Text("Toko Saya",
                //                         style: TextStyle(
                //                             fontSize: 18,
                //                             color: ColorPalette.themeIcon,
                //                             fontWeight: FontWeight.bold)),
                //               )
                //             ],
                //           ),
                //         ),
                //         onTap: () {
                //           if (_auth.currentUser == null) {
                //             Navigator.pushNamed(context, "/login");
                //           } else {
                //             _basket.addAll({
                //               "name": _auth.currentUser.user.fullName,
                //               "noHp": _auth.currentUser.user.phone,
                //               "email": _auth.currentUser.user.email
                //             });
                //             _basket["outletId"] == null
                //                 ? Navigator.pushNamed(
                //                     context, "/pilih-paket-register")
                //                 :
                //                 // Navigator.pushNamed(context, "/buat-akun-merchant");
                //                 Navigator.pushNamed(context, "/home-merchant");
                //           }
                //         },
                //       ),
                Container(
                  margin:
                      EdgeInsets.only(left: 35, right: 16, top: 16, bottom: 16),
                  child: Text("Pusat Bantuan",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                // ListTile(
                //   title: Container(
                //     margin: EdgeInsets.only(left: 16, right: 16),
                //     child: Row(
                //       children: [
                //         Icon(
                //           Icons.settings_outlined,
                //           color: ColorPalette.themeIcon,
                //           size: 35,
                //         ),
                //         Container(
                //           margin: EdgeInsets.only(left: 15),
                //           child: Text("Pengaturan",
                //               style: TextStyle(
                //                 fontSize: 18,
                //                 color: Colors.black,
                //               )),
                //         )
                //       ],
                //     ),
                //   ),
                //   onTap: () {
                //     if (_auth.currentUser == null) {
                //       Navigator.pushNamed(context, "/login");
                //     } else {
                //       Navigator.pushNamed(context, "/setting-account");
                //     }
                //   },
                // ),
                // ListTile(
                //   title: GestureDetector(
                //     onTap: () {
                //       Navigator.pushNamed(context, "/favorite-product");
                //     },
                //     child: Container(
                //       margin: EdgeInsets.only(left: 16, right: 16),
                //       child: Row(
                //         children: [
                //           Icon(
                //             Icons.favorite,
                //             color: ColorPalette.themeIcon,
                //             size: 35,
                //           ),
                //           Container(
                //             margin: EdgeInsets.only(left: 15),
                //             child: Text("Favorite",
                //                 style: TextStyle(
                //                     fontSize: 18,
                //                     color: ColorPalette.themeIcon,
                //                     fontWeight: FontWeight.bold)),
                //           )
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                // ListTile(
                //   title: Container(
                //     margin: EdgeInsets.only(left: 16, right: 16),
                //     child: Row(
                //       children: [
                //         Icon(
                //           Icons.qr_code,
                //           color: ColorPalette.themeIcon,
                //           size: 35,
                //         ),
                //         Container(
                //           margin: EdgeInsets.only(left: 15),
                //           child: Text("Kode QR Saya",
                //               style: TextStyle(
                //                   fontSize: 18,
                //                   color: ColorPalette.themeIcon,
                //                   fontWeight: FontWeight.bold)),
                //         )
                //       ],
                //     ),
                //   ),
                // ),
                ListTile(
                  title: Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      children: [
                        Icon(
                          Icons.assignment_late_outlined,
                          color: ColorPalette.themeIcon,
                          size: 35,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          child: Text("Term of Service",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              )),
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => MyWebview(
                              title: "Term Of Service",
                              url: "https://connexist.com/term-of-sevice",
                            )));
                  },
                ),
                ListTile(
                  title: Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      children: [
                        Icon(
                          Icons.flag_outlined,
                          color: ColorPalette.themeIcon,
                          size: 35,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          child: Text("Bantuan",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              )),
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => MyWebview(
                              title: "Bantuan",
                              url: "https://connexist.com/mobile-bantuan",
                            )
                        // BantuanPage()
                        ));
                  },
                ),
                // ListTile(
                //   title: Container(
                //     margin: EdgeInsets.only(left: 16, right: 16),
                //     child: Row(
                //       children: [
                //         Icon(
                //           Icons.info_outline,
                //           color: ColorPalette.themeIcon,
                //           size: 35,
                //         ),
                //         Container(
                //           margin: EdgeInsets.only(left: 15),
                //           child: Text("Tentang",
                //               style: TextStyle(
                //                 fontSize: 18,
                //                 color: Colors.black,
                //               )),
                //         )
                //       ],
                //     ),
                //   ),
                //   onTap: () {
                //     Navigator.of(context).push(MaterialPageRoute(
                //         builder: (BuildContext context) => AboutPage()));
                //   },
                // ),
                ListTile(
                  title: Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      children: [
                        Icon(
                          Icons.logout,
                          color: ColorPalette.themeIcon,
                          size: 35,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          child: Text(
                              _auth.currentUser == null ? "Masuk" : "Keluar",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              )),
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    _basket.clear();
                    _chttp.auth.logout();
                    Navigator.pushNamed(context, "/login");
                  },
                ),
                Container(
                    margin: EdgeInsets.only(top: 50),
                    child: Center(child: Text("poweredBy :"))),
                Container(
                    margin: EdgeInsets.only(bottom: 10, top: 5),
                    child: Image.asset(
                      'assets/logo_cx.png',
                      height: 37,
                      width: 82,
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}

class Constants {
  Constants._();
  static const double padding = 35;
  static const double avatarRadius = 45;
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/view/page-ppob/page_ppob.dart';
import 'package:pekik_app/view/page-product-by-category/page_product_by_category.dart';
import 'package:pekik_app/view/page-public-product/page_public_all_category.dart';
import 'package:pekik_app/view/widgets/widget_coming_soon.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:shimmer/shimmer.dart';

class CategoryWidget extends StatefulWidget {
  const CategoryWidget({Key key}) : super(key: key);

  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    PublicProductViewModel productVM = PublicProductViewModel();
    return FutureBuilder(
      future: productVM.fetchCategoryPublic(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final CategoryProductModel categoryProductModel = snapshot.data;
          return Container(
            height: 150,
            child: ListView.builder(
              padding: EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 0),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 6 + 1,
              itemBuilder: (context, index) {
                if (index == 6) {
                  return Container(
                    width: 80,
                    child: Column(
                      children: <Widget>[
                        InkWell(
                          borderRadius: BorderRadius.circular(15),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return PublicAllCategoryPage();
                            }));
                          },
                          child: Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(22),
                            ),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/ic_semua.png",
                                    width: 50,
                                    height: 50,
                                    fit: BoxFit.contain,
                                  )
                                ]),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return PublicAllCategoryPage();
                            }));
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 2),
                            child: Text('Lihat Semua',
                                textAlign: TextAlign.center,
                                maxLines: 2,
                                style: TextStyle(
                                    fontSize: 12, fontFamily: "Poppins")),
                          ),
                        )
                      ],
                    ),
                  );
                }
                return Container(
                  width: 80,
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        borderRadius: BorderRadius.circular(15),
                        onTap: () {
                          if (categoryProductModel.data[index].categoryType ==
                              'official') {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ProductByCategoryPage(
                                  title: categoryProductModel
                                      .data[index].categoryName,
                                  tenantType: categoryProductModel
                                      .data[index].categoryType,
                                  tenantId: '',
                                  idCategory: '');
                            }));
                          } else if (categoryProductModel
                                  .data[index].categoryType ==
                              'ppob') {
                            /*Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return PPOBPage();
                            }));*/
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ComingSoonWidget();
                            }));
                          } else {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ProductByCategoryPage(
                                  title: categoryProductModel
                                      .data[index].categoryName,
                                  tenantType: '',
                                  tenantId: '',
                                  idCategory:
                                      categoryProductModel.data[index].id);
                            }));
                          }
                        },
                        child: Container(
                          height: 70,
                          width: 70,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(22),
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CachedNetworkImage(
                                  imageUrl: categoryProductModel
                                      .data[index].categoryImage,
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.contain,
                                  placeholder: (context, url) => Center(
                                      child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.grey[100],
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: Colors.white),
                                    ),
                                  )),
                                  errorWidget: (context, url, error) => Center(
                                      child: Image.asset(
                                    "assets/default_image.png",
                                    width: 50,
                                    height: 50,
                                    fit: BoxFit.contain,
                                  )),
                                )
                              ]),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if (categoryProductModel.data[index].categoryType ==
                              'official') {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ProductByCategoryPage(
                                  title: categoryProductModel
                                      .data[index].categoryName,
                                  tenantType: categoryProductModel
                                      .data[index].categoryType,
                                  tenantId: '',
                                  idCategory: '');
                            }));
                          } else if (categoryProductModel
                                  .data[index].categoryType ==
                              'ppob') {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return PPOBPage();
                            }));
                          } else {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ProductByCategoryPage(
                                  title: categoryProductModel
                                      .data[index].categoryName,
                                  tenantType: '',
                                  tenantId: '',
                                  idCategory:
                                      categoryProductModel.data[index].id);
                            }));
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Text(
                              categoryProductModel.data[index].categoryName,
                              textAlign: TextAlign.center,
                              maxLines: 3,
                              style: TextStyle(
                                  fontSize: 12, fontFamily: "Poppins")),
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          );
        }
        return loadingListCategory();
      },
    );
  }

  Widget loadingListCategory() {
    return Container(
      height: 110,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: 7,
        itemBuilder: (context, index) {
          return Container(
            width: 80,
            child: Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(60),
                      color: Colors.white,
                    ),
                    width: 50,
                    height: 50,
                  ),
                ),
                SizedBox(height: 8),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                    ),
                    width: 60,
                    height: 15,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

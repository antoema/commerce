import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/view/widgets/widget_coming_soon.dart';
import 'package:pekik_app/viewmodel/public_product_vm.dart';
import 'package:shimmer/shimmer.dart';

class MenuWidget extends StatefulWidget {
  @override
  _MenuWidgetState createState() => _MenuWidgetState();
}

class _MenuWidgetState extends State<MenuWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _getCategory();
  }

  Widget _getCategory() {
    PublicProductViewModel productVM = PublicProductViewModel();
    return FutureBuilder(
      future: productVM.fetchCategoryPublic(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final CategoryProductModel categoryProductModel = snapshot.data;
          return Container(
            height: 130,
            child: ListView.builder(
              padding: EdgeInsets.all(10),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: categoryProductModel.data.length,
              itemBuilder: (context, index) {
                return Container(
                  width: 80,
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        borderRadius: BorderRadius.circular(15),
                        onTap: () {
                          if (categoryProductModel.data[index].categoryType ==
                              'official') {
                            // Navigator.push(context,
                            //     MaterialPageRoute(builder: (context) {
                            //   return OfficialStorePage();
                            // }));
                          } else {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ComingSoonWidget();
                            }));
                          }
                        },
                        child: Container(
                          height: 70,
                          width: 70,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(22),
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CachedNetworkImage(
                                  imageUrl: categoryProductModel
                                      .data[index].categoryImage,
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.contain,
                                  placeholder: (context, url) => Center(
                                      child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.grey[100],
                                    child: Container(
                                      color: Colors.white,
                                      width: double.infinity,
                                      height: double.infinity,
                                    ),
                                  )),
                                  errorWidget: (context, url, error) => Center(
                                      child: Image.asset(
                                    "assets/default_image.png",
                                    width: 50,
                                    height: 50,
                                    fit: BoxFit.contain,
                                  )),
                                )
                              ]),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return ComingSoonWidget();
                          }));
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Text(
                              categoryProductModel.data[index].categoryName,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: 12, fontFamily: "Poppins")),
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          );
        }
        return loadingListCategory();
      },
    );
  }

  Widget loadingListCategory() {
    return Container(
      height: 130,
      child: ListView.builder(
        padding: EdgeInsets.all(10),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: 7,
        itemBuilder: (context, index) {
          return Container(
            width: 80,
            child: Column(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                    ),
                    width: 60,
                    height: 60,
                  ),
                ),
                SizedBox(height: 8),
                Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                    ),
                    width: 60,
                    height: 15,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
  // Widget _getDataMenu() {
  //   return Column(
  //     children: [
  //       Container(
  //         margin: EdgeInsets.only(left: 12, bottom: 10, right: 12, top: 15),
  //         child: Row(
  //           children: <Widget>[
  //             Expanded(
  //               child: Column(
  //                 children: <Widget>[
  //                   InkWell(
  //                     borderRadius: BorderRadius.circular(10),
  //                     onTap: () {
  //                       Navigator.push(context,
  //                           MaterialPageRoute(builder: (context) {
  //                         return ComingSoonWidget();
  //                       }));
  //                     },
  //                     child: Container(
  //                       height: 65,
  //                       width: 65,
  //                       child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.center,
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: <Widget>[
  //                             Image.asset(
  //                               "assets/ic_tagihan.png",
  //                             ),
  //                           ]),
  //                     ),
  //                   ),
  //                   Stack(overflow: Overflow.visible, children: [
  //                     Container(
  //                         margin: EdgeInsets.only(top: 10),
  //                         child: Column(
  //                           children: [
  //                             Text("Bayar", style: TextStyle(fontSize: 14)),
  //                           ],
  //                         )),
  //                     Positioned(
  //                       top: 20,
  //                       right: -8,
  //                       child: Container(
  //                           margin: EdgeInsets.only(top: 10),
  //                           child: Column(
  //                             children: [
  //                               Text("Tagihan", style: TextStyle(fontSize: 14)),
  //                             ],
  //                           )),
  //                     )
  //                   ]),
  //                 ],
  //               ),
  //             ),
  //             Expanded(
  //               child: Column(
  //                 children: <Widget>[
  //                   InkWell(
  //                     borderRadius: BorderRadius.circular(10),
  //                     onTap: () {
  //                       Navigator.push(context,
  //                           MaterialPageRoute(builder: (context) {
  //                         return ComingSoonWidget();
  //                       }));
  //                     },
  //                     child: Container(
  //                       height: 65,
  //                       width: 65,
  //                       child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.center,
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: <Widget>[
  //                             Image.asset(
  //                               "assets/ic_jual.png",
  //                             ),
  //                           ]),
  //                     ),
  //                   ),
  //                   Container(
  //                     margin: EdgeInsets.only(top: 10),
  //                     child: Text("Jual/Beli", style: TextStyle(fontSize: 14)),
  //                   )
  //                 ],
  //               ),
  //             ),
  //             Expanded(
  //               child: Column(
  //                 children: <Widget>[
  //                   InkWell(
  //                     borderRadius: BorderRadius.circular(10),
  //                     onTap: () {
  //                       Navigator.push(context,
  //                           MaterialPageRoute(builder: (context) {
  //                         return ComingSoonWidget();
  //                       }));
  //                     },
  //                     child: Container(
  //                       // padding: EdgeInsets.all(18),
  //                       height: 65,
  //                       width: 65,
  //                       child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.center,
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: <Widget>[
  //                             Image.asset(
  //                               "assets/ic_pesan.png",
  //                             ),
  //                           ]),
  //                     ),
  //                   ),
  //                   Container(
  //                     margin: EdgeInsets.only(top: 10),
  //                     child: Text("Pesan", style: TextStyle(fontSize: 14)),
  //                   )
  //                 ],
  //               ),
  //             ),
  //             Expanded(
  //               child: Column(
  //                 children: <Widget>[
  //                   InkWell(
  //                     borderRadius: BorderRadius.circular(10),
  //                     onTap: () {
  //                       Navigator.push(context,
  //                           MaterialPageRoute(builder: (context) {
  //                         return OfficialStorePage();
  //                       }));
  //                     },
  //                     child: Container(
  //                       height: 65,
  //                       width: 65,
  //                       child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.center,
  //                           mainAxisAlignment: MainAxisAlignment.center,
  //                           children: <Widget>[
  //                             Image.asset(
  //                               "assets/ic_store.png",
  //                             ),
  //                           ]),
  //                     ),
  //                   ),
  //                   Stack(overflow: Overflow.visible, children: [
  //                     Container(
  //                         margin: EdgeInsets.only(top: 10),
  //                         child: Column(
  //                           children: [
  //                             Text("Official", style: TextStyle(fontSize: 14)),
  //                           ],
  //                         )),
  //                     Positioned(
  //                       top: 20,
  //                       right: 5,
  //                       child: Container(
  //                           margin: EdgeInsets.only(top: 10),
  //                           child: Column(
  //                             children: [
  //                               Text("Store", style: TextStyle(fontSize: 14)),
  //                             ],
  //                           )),
  //                     )
  //                   ]),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ],
  //   );
  // }
}

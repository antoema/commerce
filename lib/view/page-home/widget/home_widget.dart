import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/view/page-home/page_home.dart';
import 'package:pekik_app/view/page-home/widget/widget_drawer.dart';
import 'package:pekik_app/view/page-public-product/page_public_product.dart';
import 'package:pekik_app/view/page-transaction/page_transaction.dart';
import 'package:pekik_app/view/widgets/push_notification_service.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  PageController _pageController;

  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    PublicProductPage(),
    TransactionPage()
  ];

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            contentTextStyle: TextStyle(
              fontFamily: 'Poppins',
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
            content: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: Constants.padding,
                      top: Constants.avatarRadius + Constants.padding,
                      right: Constants.padding,
                      bottom: Constants.padding),
                  margin: EdgeInsets.only(top: Constants.avatarRadius),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(Constants.padding),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "Keluar Aplikasi ?",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Colors.black),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Anda akan keluar dari aplikasi",
                        style: TextStyle(fontSize: 14, color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pop(false);
                                },
                                child: Container(
                                  height: 45,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                      border:
                                          Border.all(color: Colors.grey[400])),
                                  child: Center(
                                    child: Text(
                                      "Tidak",
                                      style: TextStyle(
                                          color: ColorPalette.themeIcon),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  SystemNavigator.pop();
                                },
                                child: Container(
                                  height: 45,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: ColorPalette.themeIcon),
                                  child: Center(
                                    child: Text(
                                      "Ya",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: Constants.padding,
                  right: Constants.padding,
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: Constants.avatarRadius,
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(
                            Radius.circular(Constants.avatarRadius)),
                        child: Image.network(
                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQdt12mgKthASs1d-8dTOXZ6Xhoy281I5hkuA&usqp=CAU")),
                  ),
                ),
              ],
            ),
          ),
        )) ??
        false;
  }

  Future<void> initDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      await _handleDeepLink(dynamicLink);
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    _handleDeepLink(data);
  }

  // bool _deeplink = true;
  _handleDeepLink(PendingDynamicLinkData data) async {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    final Uri deeplink = data.link;
    if (deeplink != null) {
      print(deeplink.path);
      basket.addAll({"idProduct": deeplink.path.replaceAll('/', '')});
      Navigator.pushNamed(context, "/detail-produk");
    }
  }

  @override
  void initState() {
    _pageController = PageController();
    initDynamicLinks();
    PushNotificationHelper(this.context);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        endDrawer: DrawerWidget(),
        body: SafeArea(
          child: Stack(
            children: [
              PageView(
                physics: new NeverScrollableScrollPhysics(),
                controller: _pageController,
                onPageChanged: (value) {
                  setState(() {
                    print(value);
                    _currentIndex = value;
                  });
                },
                children: _children,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 60,
                  // padding: EdgeInsets.all(5),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        top: BorderSide(width: 0.5, color: Colors.grey[400])),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                _pageController.animateToPage(0,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.fastLinearToSlowEaseIn);
                              },
                              child: Column(
                                children: [
                                  ImageIcon(
                                    AssetImage("assets/ic_home.png"),
                                    size: 25,
                                    color: _currentIndex == 0
                                        ? ColorPalette.btnRed
                                        : Colors.black,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    'Home',
                                    style: TextStyle(
                                      color: _currentIndex == 0
                                          ? ColorPalette.btnRed
                                          : Colors.black,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                _pageController.animateToPage(1,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.fastLinearToSlowEaseIn);
                              },
                              child: Column(
                                children: [
                                  ImageIcon(
                                    AssetImage("assets/ic_official.png"),
                                    size: 25,
                                    color: _currentIndex == 1
                                        ? ColorPalette.btnRed
                                        : Colors.black,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text('Produk',
                                      style: TextStyle(
                                        color: _currentIndex == 1
                                            ? ColorPalette.btnRed
                                            : Colors.black,
                                      ))
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                _pageController.animateToPage(2,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.fastLinearToSlowEaseIn);
                              },
                              child: Column(
                                children: [
                                  ImageIcon(
                                    AssetImage("assets/ic_transaction.png"),
                                    size: 25,
                                    color: _currentIndex == 2
                                        ? ColorPalette.btnRed
                                        : Colors.black,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    'Transaksi',
                                    style: TextStyle(
                                      color: _currentIndex == 2
                                          ? ColorPalette.btnRed
                                          : Colors.black,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: StatefulBuilder(
                              builder: (context, setState) {
                                return _auth.currentUser == null
                                    ? InkWell(
                                        onTap: () {
                                          Navigator.pushNamed(
                                              context, "/login");
                                        },
                                        child: Column(
                                          children: [
                                            ImageIcon(
                                              AssetImage(
                                                  "assets/ic_login_outlined.png"),
                                              size: 25,
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text('Masuk')
                                          ],
                                        ),
                                      )
                                    : InkWell(
                                        onTap: () {
                                          Scaffold.of(context).openEndDrawer();
                                        },
                                        child: Column(
                                          children: [
                                            ImageIcon(
                                              AssetImage("assets/ic_more.png"),
                                              size: 25,
                                            ),
                                            SizedBox(
                                              height: 4,
                                            ),
                                            Text('Lainnya')
                                          ],
                                        ),
                                      );
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ), // new
        // bottomNavigationBar: BottomNavigationBar(
        //   selectedItemColor: ColorPalette.themeRed,
        //   onTap: onTabTapped,
        //   // elevation: 15,
        //   // backgroundColor: Colors.white70, // new
        //   currentIndex: _currentIndex,
        //   type: BottomNavigationBarType.fixed, // new
        //   items: [
        //     new BottomNavigationBarItem(
        //       icon: ImageIcon(
        //         AssetImage("assets/ic_home.png"),
        //         size: 25,
        //       ),
        //       title: Text('Home'),
        //     ),
        //     new BottomNavigationBarItem(
        //       icon: ImageIcon(
        //         AssetImage("assets/ic_official.png"),
        //         size: 25,
        //       ),
        //       title: Text('Official'),
        //     ),
        //     new BottomNavigationBarItem(
        //       icon: ImageIcon(
        //         AssetImage("assets/ic_shopping.png"),
        //         size: 25,
        //       ),
        //       title: Text('Keranjang'),
        //     )
        //   ],
        // ),
      ),
    );
  }
}

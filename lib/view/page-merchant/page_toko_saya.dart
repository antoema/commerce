import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';
import 'package:pekik_app/bloc/merchant-bloc/merchant_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/merchant.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/content_view.dart';
import 'package:pekik_app/view/page-merchant/page_ubah_toko.dart';
import 'package:pekik_app/view/page-wallet/page_wallet.dart';
import 'package:pekik_app/view/page_about.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class TokoSayaPage extends StatefulWidget {
  @override
  _TokoSayaPageState createState() => _TokoSayaPageState();
}

class _TokoSayaPageState extends State<TokoSayaPage> {
  MerchantBloc merchantBloc = MerchantBloc();
  var accessToken = '';
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    version: 'Unknown',
  );

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    merchantBloc.add(GetMerchant(http: _chttp));
    super.initState();
    _initPackageInfo();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      merchantBloc.add(GetMerchant(http: _chttp));
    });
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Toko Saya",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildMerchant(),
    );
  }

  Widget _buildMerchant() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => merchantBloc,
        child: BlocListener<MerchantBloc, MerchantState>(
          listener: (context, state) {
            if (state is MerchantError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<MerchantBloc, MerchantState>(
            builder: (context, state) {
              if (state is MerchantInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is MerchantLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is MerchantLoaded) {
                return _build(context, state.merchantModel);
              }
              if (state is MerchantError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _build(BuildContext context, MerchantModel merchantModel) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  width: 65,
                  height: 65,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(45)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(45),
                    child: CachedNetworkImage(
                      imageUrl: merchantModel.data.tenantLogo,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          Image.asset("assets/default_toko.jpg"),
                    ),
                  )),
              SizedBox(width: 15),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        merchantModel.data.tenantType == 'official'
                            ? Image.asset(
                                'assets/ic_official_store.png',
                                width: 20,
                              )
                            : Container(),
                        SizedBox(
                          width: merchantModel.data.tenantType == 'official'
                              ? 5
                              : 0,
                        ),
                        merchantModel.data.tenantName.length > 25
                            ? Text(
                                merchantModel.data.tenantName.substring(0, 25) +
                                    "...",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold))
                            : Text(merchantModel.data.tenantName,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold)),
                      ],
                    ),
                    SizedBox(height: 4),
                    Text(merchantModel.data.tenantAddress,
                        style: TextStyle(color: Colors.black, fontSize: 14)),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return UbahTokoPage(merchantModel: merchantModel);
                  })).then((val) => val ? onRefresh() : null);
                },
                child: Icon(
                  Icons.edit_outlined,
                  color: Colors.black,
                ),
              )
            ],
          ),
        ),
        WalletPage(),
        SizedBox(
          height: 9,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Text("Produk",
              style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.bold)),
        ),
        InkWell(
          onTap: () {
            Navigator.pushNamed(context, "/pesanan");
          },
          child: Container(
              margin: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(right: 16),
                      child: Icon(Icons.receipt_outlined,
                          size: 30, color: Colors.grey[600])),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Transaksi Penjualan",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Colors.black),
                      ),
                      Text(
                        "Cek transaksi tokomu, ubah status dan urus resi disini",
                        style: TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                    ],
                  ),
                ],
              )),
        ),
        Divider(),
        InkWell(
          onTap: () {
            basket.addAll({'tenantId': merchantModel.data.id});
            Navigator.pushNamed(context, "/tambah-barang");
          },
          child: Container(
              margin: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(right: 16),
                      child: Icon(Icons.add_circle_outline,
                          size: 30, color: Colors.grey[600])),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Tambah Barang",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Colors.black),
                      ),
                      Text(
                        "Upload foto barang jualanmu lalu lengkapi detailnya",
                        style: TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                    ],
                  ),
                ],
              )),
        ),
        Divider(),
        InkWell(
          onTap: () {
            basket.addAll({'tenantId': merchantModel.data.id});
            Navigator.pushNamed(context, "/daftar-produk");
          },
          child: Container(
              margin: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(right: 16),
                      child: Icon(
                        Icons.list_alt,
                        color: Colors.grey[600],
                        size: 30,
                      )),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Daftar Produk",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Colors.black),
                      ),
                      Text(
                        "Lihat semua barang yang dijual di tokomu",
                        style: TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                    ],
                  ),
                ],
              )),
        ),
        SizedBox(
          height: 16,
        ),
        Divider(
          thickness: 10,
        ),
        SizedBox(
          height: 16,
        ),
        Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Seputar Pekik JKT",
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => AboutPage()));
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.info_outline,
                      color: Colors.grey[600],
                      size: 30,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Text("Tentang",
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.w700))
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => MyWebview(
                            title: "Term Of Service",
                            url: "https://connexist.com/term-of-sevice",
                          )));
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.assignment_late_outlined,
                      color: Colors.grey[600],
                      size: 30,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Text("Term of Service",
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.w700))
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => MyWebview(
                            title: "Bantuan",
                            url: "https://connexist.com/mobile-bantuan",
                          )
                      // BantuanPage()
                      ));
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.flag_outlined,
                      color: Colors.grey[600],
                      size: 30,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Text("Bantuan",
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.w700))
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () => Platform.isIOS
                    ? _launchURL(
                        "https://apps.apple.com/id/app/pekik-jkt/id1571444736")
                    : _launchURL(
                        'https://play.google.com/store/apps/details?id=com.connexist.pekik_app'),
                child: Row(
                  children: [
                    Icon(
                      Icons.stars_outlined,
                      color: Colors.grey[600],
                      size: 30,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                      child: Text("Ulas Aplikasi",
                          style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                              fontWeight: FontWeight.w700)),
                    ),
                    Text(
                      "PEKIK JKT v ${_packageInfo.version}",
                      style: TextStyle(color: Colors.grey, fontSize: 14),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/merchant_vm.dart';
import 'package:pekik_app/viewmodel/upload_image.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class TambahBarangPage extends StatefulWidget {
  @override
  _TambahBarangPageState createState() => _TambahBarangPageState();
}

class _TambahBarangPageState extends State<TambahBarangPage> {
  final formKey = new GlobalKey<FormState>();
  File f1;
  final _namaBarang = TextEditingController();
  final _harga = TextEditingController();
  final _stok = TextEditingController();
  final _minimum = TextEditingController();
  final _berat = TextEditingController();
  final _dimensi = TextEditingController();
  final _deskripsi = TextEditingController();
  final _satuan = TextEditingController();

  bool isLoading = true;
  List<Asset> images = List<Asset>();
  List<File> files = [];

  bool validates() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void tambahProduk() async {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    MerchantViewModel merchantViewModel = MerchantViewModel(http: _chttp);
    List imagList = [];
    if (validates()) {
      if (files.length == 0) {
        Fluttertoast.showToast(
            msg: "Anda belum memilih gambar produk",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      } else if (basket['idCategory'] == null) {
        Fluttertoast.showToast(
            msg: "Anda belum memilih kategori produk",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      } else if (_deskripsi.text == '') {
        Fluttertoast.showToast(
            msg: "Anda belum mengisi deskripsi produk",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      } else {
        setState(() {
          isLoading = false;
        });
        for (int i = 0; i < files.length; i++) {
          File file = File(files[i].path);
          await ImageUploader.uploadImage(file).then((res) {
            setState(() {
              imagList.add(res.data.path);
            });
          });
        }
        await merchantViewModel
            .postProduct(
          _namaBarang.text,
          _deskripsi.text,
          imagList
              .toString()
              .replaceAll('[', '')
              .replaceAll(']', '')
              .replaceAll(' ', ''),
          _harga.text,
          _dimensi.text,
          _berat.text,
          _satuan.text,
          _stok.text,
          _minimum.text,
          basket['tenantId'],
          basket['idCategory'],
        )
            .then((product) {
          if (product != null) {
            setState(() {
              isLoading = true;
            });
            Fluttertoast.showToast(
                msg: "Tambah Produk Berhasil",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                backgroundColor: ColorPalette.btnGreen,
                textColor: Colors.white);
            Navigator.pop(context, true);
          } else {
            setState(() {
              isLoading = true;
            });
            Fluttertoast.showToast(
                msg: "Terjadi kesalahan saat terhubung ke server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white);
          }
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _deskripsi.addListener(() {
      setState(() {});
    });
  }

  void uploadPic() async {
    List<Asset> resultList = List<Asset>();
    List<File> before = [];
    resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: false,
        selectedAssets: images,
        materialOptions: MaterialOptions(
            actionBarColor: "#FF7700", statusBarColor: "#FF7700"));
    resultList.forEach((imageAsset) async {
      final filePath =
          await FlutterAbsolutePath.getAbsolutePath(imageAsset.identifier);
      File tempFile = File(filePath);
      setState(() {
        images = resultList;
        before.add(tempFile);
        files = before.toSet().toList();
      });
    });
  }

  // void _pickImage() async {
  //   final imageSource = await showDialog<ImageSource>(
  //       // context: context,
  //       builder: (context) => AlertDialog(
  //             title: Text(
  //               "Pilih sumber gambar",
  //               style:
  //                   TextStyle(fontWeight: FontWeight.bold, color: Colors.teal),
  //             ),
  //             actions: <Widget>[
  //               MaterialButton(
  //                 child: Text(
  //                   "Camera",
  //                   style: TextStyle(color: Colors.blueAccent),
  //                 ),
  //                 onPressed: () => Navigator.pop(context, ImageSource.camera),
  //               ),
  //               MaterialButton(
  //                 child: Text(
  //                   "Gallery",
  //                   style: TextStyle(color: Colors.blueAccent),
  //                 ),
  //                 onPressed: () => Navigator.pop(context, ImageSource.gallery),
  //               )
  //             ],
  //           ));

  //   if (imageSource != null) {
  //     final file =
  //         await ImagePicker.pickImage(source: imageSource, maxHeight: 720);
  //     if (file != null) {
  //       setState(() => f1 = file);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    return WillPopScope(
      onWillPop: () {
        basket.addAll({
          "idCategory": null,
          "nameCategory": null,
        });
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: ColorPalette.themeIcon,
          ),
          title: Text(
            "Tambah Produk",
            style: TextStyle(
                color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
          ),
        ),
        body: ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(16),
          children: [
            Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Kategori Produk",
                      style: TextStyle(
                        fontSize: 18.0,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      showBarModalBottomSheet(
                        expand: false,
                        context: context,
                        backgroundColor: Colors.transparent,
                        builder: (context) => modalCategory(context),
                      );
                    },
                    child: Container(
                        height: 60,
                        width: double.infinity,
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.grey),
                            color: Colors.grey[100]),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                basket['nameCategory'] == null
                                    ? "Masukan Kategori Produk"
                                    : basket['nameCategory'],
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.grey[600])),
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Nama Produk",
                      style: TextStyle(
                        fontSize: 18.0,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    // color: Colors.white,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0)),
                    child: new TextFormField(
                      controller: _namaBarang,
                      decoration: new InputDecoration(
                        // labelText: "No Ponsel",
                        hintText: "Masukan Nama Produk",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      validator: (val) {
                        if (val.isEmpty) {
                          return "Nama produk tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                        fontFamily: 'Poppins',
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Harga",
                                style: TextStyle(
                                  fontSize: 18.0,
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: new TextFormField(
                                controller: _harga,
                                decoration: new InputDecoration(
                                  // labelText: "No Ponsel",
                                  hintText: "1",
                                  fillColor: Colors.white,
                                  prefixIcon: Container(
                                    width: 50,
                                    child: Center(
                                      child: Text(
                                        "Rp",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Harga tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                style: new TextStyle(
                                  fontFamily: 'Poppins',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: 120,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Satuan",
                                style: TextStyle(
                                  fontSize: 18.0,
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: new TextFormField(
                                controller: _satuan,
                                decoration: new InputDecoration(
                                  // labelText: "No Ponsel",
                                  hintText: "ex: KG",
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Satuan tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.text,
                                style: new TextStyle(
                                  fontFamily: 'Poppins',
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Container(
                        width: 120,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Stok",
                                style: TextStyle(
                                  fontSize: 18.0,
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: new TextFormField(
                                controller: _stok,
                                decoration: new InputDecoration(
                                  // labelText: "No Ponsel",
                                  hintText: "1",
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Stok tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                style: new TextStyle(
                                  fontFamily: 'Poppins',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Berat",
                                style: TextStyle(
                                  fontSize: 18.0,
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: new TextFormField(
                                controller: _berat,
                                decoration: new InputDecoration(
                                  // labelText: "No Ponsel",
                                  hintText: "1",
                                  fillColor: Colors.white,
                                  suffixIcon: Container(
                                    // padding: EdgeInsets.only(right: 16),
                                    width: 80,
                                    child: Center(
                                      child: Text(
                                        "Gram",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Berat tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                style: new TextStyle(
                                  fontFamily: 'Poppins',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Container(
                        width: 150,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Dimensi",
                                style: TextStyle(
                                  fontSize: 18.0,
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: new TextFormField(
                                controller: _dimensi,
                                decoration: new InputDecoration(
                                  // labelText: "No Ponsel",
                                  hintText: "ex: 10x10x10",
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Dimensi tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.text,
                                style: new TextStyle(
                                  fontFamily: 'Poppins',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Minimum Order",
                              style: TextStyle(
                                fontSize: 18.0,
                              )),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            // color: Colors.white,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0)),
                            child: new TextFormField(
                              controller: _minimum,
                              decoration: new InputDecoration(
                                // labelText: "No Ponsel",
                                hintText: "1",
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return "Minimum Order tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              style: new TextStyle(
                                fontFamily: 'Poppins',
                              ),
                            ),
                          ),
                        ],
                      ))
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Deskripsi",
                      style: TextStyle(
                        fontSize: 18.0,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      showBarModalBottomSheet(
                        expand: false,
                        context: context,
                        backgroundColor: Colors.transparent,
                        builder: (context) => modalDescription(context),
                      );
                    },
                    child: Container(
                        height: 120,
                        width: double.infinity,
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.grey),
                            color: Colors.grey[100]),
                        child: Text(
                            _deskripsi.text == ''
                                ? "Masukan Deskripsi Produk Anda"
                                : _deskripsi.text,
                            style: TextStyle(
                                fontSize: 16.0, color: Colors.grey[600]))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Gambar Barang",
                      style: TextStyle(
                        fontSize: 18.0,
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  // Container(
                  //   height: 100,
                  //   padding: EdgeInsets.all(10),
                  //   decoration: BoxDecoration(
                  //       border: Border.all(color: Colors.grey[600]),
                  //       borderRadius: BorderRadius.circular(10)),
                  //   child: Row(
                  //     children: [
                  //       GestureDetector(
                  //         onTap: _pickImage,
                  //         child: Container(
                  //           height: 80,
                  //           width: 80,
                  //           decoration: BoxDecoration(
                  //               borderRadius: BorderRadius.circular(10),
                  //               border: Border.all(color: Colors.grey[400]),
                  //               color: Colors.grey[350]),
                  //           child: Center(
                  //             child: f1 == null
                  //                 ? Icon(
                  //                     Icons.camera_alt,
                  //                     color: Colors.grey[600],
                  //                     size: 35,
                  //                   )
                  //                 : ClipRRect(
                  //                     borderRadius: BorderRadius.circular(10),
                  //                     child: FadeInImage(
                  //                         fit: BoxFit.cover,
                  //                         height: double.infinity,
                  //                         width: double.infinity,
                  //                         image: FileImage(f1),
                  //                         placeholder: AssetImage(
                  //                             "assets/default_image.png")),
                  //                   ),
                  //           ),
                  //         ),
                  //       ),
                  //       SizedBox(
                  //         width: 10,
                  //       ),
                  //       Expanded(
                  //           child: Column(
                  //         crossAxisAlignment: CrossAxisAlignment.start,
                  //         mainAxisAlignment: MainAxisAlignment.center,
                  //         children: [
                  //           Text("Upload Gambar Barang",
                  //               style: TextStyle(
                  //                 fontSize: 16,
                  //               )),
                  //           SizedBox(
                  //             height: 5,
                  //           ),
                  //           Text(
                  //               "Ukuran minimal 300x300px berformat JPG atau PNG",
                  //               style: TextStyle(
                  //                   fontSize: 14, color: Colors.grey[600])),
                  //         ],
                  //       ))
                  //     ],
                  //   ),
                  // ),
                  Container(
                    height: 100,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey[600]),
                        borderRadius: BorderRadius.circular(10)),
                    child: files.length == 0
                        ? Row(
                            children: [
                              GestureDetector(
                                onTap: uploadPic,
                                child: Container(
                                  height: 80,
                                  width: 80,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border:
                                          Border.all(color: Colors.grey[400]),
                                      color: Colors.grey[350]),
                                  child: Center(
                                    child: files.length == 0
                                        ? Icon(
                                            Icons.camera_alt,
                                            color: Colors.grey[600],
                                            size: 35,
                                          )
                                        : ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: FadeInImage(
                                                fit: BoxFit.cover,
                                                height: double.infinity,
                                                width: double.infinity,
                                                image: FileImage(files.first),
                                                placeholder: AssetImage(
                                                    "assets/default_profile.png")),
                                          ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Upload Gambar Barang",
                                      style: TextStyle(
                                        fontSize: 16,
                                      )),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                      "Maksimum 5 gambar, ukuran minimal 300x300px berformat JPG atau PNG",
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.grey[600])),
                                ],
                              ))
                            ],
                          )
                        : ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: files.length + 1,
                            itemBuilder: (context, index) {
                              if (index < files.length) {
                                return Container(
                                  height: 80,
                                  width: 80,
                                  margin: EdgeInsets.only(right: 4),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      border:
                                          Border.all(color: Colors.grey[400]),
                                      color: Colors.grey[350]),
                                  child: Center(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: FadeInImage(
                                          fit: BoxFit.cover,
                                          height: double.infinity,
                                          width: double.infinity,
                                          image: FileImage(files[index]),
                                          placeholder: AssetImage(
                                              "assets/default_profile.png")),
                                    ),
                                  ),
                                );
                              } else {
                                return GestureDetector(
                                  onTap: () {
                                    if (files.length < 5) {
                                      uploadPic();
                                    } else if (files.length >= 5) {
                                      setState(() {
                                        print("ajeg");
                                        files.clear();
                                        images.clear();
                                      });
                                    }
                                  },
                                  child: Container(
                                    height: 80,
                                    width: 80,
                                    margin: EdgeInsets.only(right: 4),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border:
                                            Border.all(color: Colors.grey[400]),
                                        color: files.length < 5
                                            ? Colors.grey[350]
                                            : Colors.red),
                                    child: Center(
                                      child: Icon(
                                        files.length < 5
                                            ? Icons.camera_alt
                                            : Icons.delete,
                                        color: files.length < 5
                                            ? Colors.grey[600]
                                            : Colors.white,
                                        size: 35,
                                      ),
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  GestureDetector(
                    onTap: () async {
                      tambahProduk();
                    },
                    child: Container(
                      height: 55,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(55),
                          color: ColorPalette.themeIcon),
                      child: Center(
                        child: isLoading
                            ? Text(
                                'Submit',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              )
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                ),
                              ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget modalDescription(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                margin: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Masukan Deskripsi",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text('Submit',
                                style: TextStyle(
                                    color: ColorPalette.themeIcon,
                                    fontSize: 16)),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    // SizedBox(height: 8),
                    Container(
                      decoration: BoxDecoration(
                          // color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: new TextFormField(
                        controller: _deskripsi,
                        autocorrect: true,
                        autofocus: true,
                        maxLines: null,
                        decoration: new InputDecoration(
                          hintText: "Masukan Deskripsi Produk Anda",
                          // fillColor: Colors.white,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                        ),
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Deskripsi tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.multiline,
                        style: new TextStyle(
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                  ],
                )),
          ]),
    );
  }

  Widget modalCategory(BuildContext context) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    MerchantViewModel merchantVM = MerchantViewModel(http: _chttp);
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, top: 16, right: 16),
                      child: Text("Pilih Kategori Produk",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    Expanded(
                      flex: 40,
                      child: FutureBuilder(
                        future: merchantVM.fetchCategoryAll(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            final CategoryProductModel categoryProductModel =
                                snapshot.data;
                            return ListView.separated(
                              shrinkWrap: true,
                              padding: EdgeInsets.all(16),
                              itemCount: categoryProductModel.data.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    setState(() {
                                      basket.addAll({
                                        'nameCategory': categoryProductModel
                                            .data[index].categoryName,
                                        'idCategory':
                                            categoryProductModel.data[index].id,
                                      });
                                    });
                                    Navigator.pop(context);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8, bottom: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          categoryProductModel
                                              .data[index].categoryName,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black),
                                        ),
                                        basket['idCategory'] ==
                                                categoryProductModel
                                                    .data[index].id
                                            ? Icon(
                                                Icons.check_circle,
                                                color: ColorPalette.btnGreen,
                                              )
                                            : Icon(
                                                Icons.lens_outlined,
                                                color: Colors.black,
                                              )
                                      ],
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return Divider(
                                  thickness: 1,
                                );
                              },
                            );
                          }
                          return loadingCategory();
                        },
                      ),
                    ),
                  ],
                )),
          ]),
    );
  }

  Widget loadingCategory() {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: 10,
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 250,
                        height: 10,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 200,
                        height: 10,
                      ),
                    ),
                  ],
                )),
                Icon(
                  Icons.lens_outlined,
                  color: Colors.black,
                )
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            thickness: 1,
          );
        },
      ),
    );
  }
}

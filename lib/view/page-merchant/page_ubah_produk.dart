import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/model/category_single_product.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/viewmodel/merchant_vm.dart';
import 'package:pekik_app/viewmodel/upload_image.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class UbahProdukPage extends StatefulWidget {
  final ResultProduct product;
  UbahProdukPage(this.product);
  @override
  _UbahProdukPageState createState() => _UbahProdukPageState();
}

class _UbahProdukPageState extends State<UbahProdukPage> {
  String published;
  bool toggleValue;

  final formKey = new GlobalKey<FormState>();
  File f1;
  String _namaBarang;
  String _harga;
  String _stok;
  String _minimum;
  String _berat;
  String _deskripsi;
  String _satuan;
  String _dimensi;

  bool isLoading = true;
  List<Asset> images = List<Asset>();

  List<File> files = [];

  @override
  void initState() {
    super.initState();
  }

  // void _pickImage() async {
  //   final imageSource = await showDialog<ImageSource>(
  //       context: context,
  //       builder: (context) => AlertDialog(
  //             title: Text(
  //               "Pilih sumber gambar",
  //               style:
  //                   TextStyle(fontWeight: FontWeight.bold, color: Colors.teal),
  //             ),
  //             actions: <Widget>[
  //               MaterialButton(
  //                 child: Text(
  //                   "Camera",
  //                   style: TextStyle(color: Colors.blueAccent),
  //                 ),
  //                 onPressed: () => Navigator.pop(context, ImageSource.camera),
  //               ),
  //               MaterialButton(
  //                 child: Text(
  //                   "Gallery",
  //                   style: TextStyle(color: Colors.blueAccent),
  //                 ),
  //                 onPressed: () => Navigator.pop(context, ImageSource.gallery),
  //               )
  //             ],
  //           ));

  //   if (imageSource != null) {
  //     final file =
  //         await ImagePicker.pickImage(source: imageSource, maxHeight: 720);
  //     if (file != null) {
  //       setState(() => f1 = file);
  //     }
  //   }
  // }

  void uploadPic() async {
    List<Asset> resultList = List<Asset>();
    List<File> before = [];
    resultList = await MultiImagePicker.pickImages(
        maxImages: 5,
        enableCamera: false,
        selectedAssets: images,
        materialOptions: MaterialOptions(
            actionBarColor: "#FF7700", statusBarColor: "#FF7700"));
    resultList.forEach((imageAsset) async {
      final filePath =
          await FlutterAbsolutePath.getAbsolutePath(imageAsset.identifier);
      File tempFile = File(filePath);
      setState(() {
        images = resultList;
        before.add(tempFile);
        files = before.toSet().toList();
      });
    });
  }

  Widget build(BuildContext context) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    return WillPopScope(
      onWillPop: () {
        basket.addAll({
          "idCategory": null,
          "nameCategory": null,
        });
        Navigator.pop(context);
      },
      child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(
              color: ColorPalette.themeIcon,
            ),
            title: Text(
              "Ubah Produk",
              style: TextStyle(
                  color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
            ),
          ),
          body: ListView(
            physics: BouncingScrollPhysics(),
            padding: EdgeInsets.all(16),
            children: [
              Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Kategori Produk",
                        style: TextStyle(
                          fontSize: 18.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    getSingleCategory(),
                    SizedBox(
                      height: 15,
                    ),
                    Text("Nama Produk",
                        style: TextStyle(
                          fontSize: 18.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      // color: Colors.white,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: new TextFormField(
                        initialValue: widget.product.productName,
                        decoration: new InputDecoration(
                          // labelText: "No Ponsel",
                          hintText: "Masukan Nama Produk",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Nama produk tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (newValue) => _namaBarang = newValue,
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Harga",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                // color: Colors.white,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: new TextFormField(
                                  initialValue:
                                      widget.product.productPrice.toString(),
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    hintText: "0",
                                    fillColor: Colors.white,
                                    prefixIcon: Container(
                                      width: 50,
                                      child: Center(
                                        child: Text(
                                          "Rp",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Harga tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onSaved: (newValue) => _harga = newValue,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 120,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Satuan",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                // color: Colors.white,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: new TextFormField(
                                  initialValue:
                                      widget.product.productMeasurementUnit,
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    hintText: "ex: KG",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  onSaved: (newValue) => _satuan = newValue,
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Satuan tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Container(
                          width: 120,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Stok",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                // color: Colors.white,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: new TextFormField(
                                  initialValue:
                                      widget.product.productStock.toString(),
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    hintText: "0",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  onSaved: (newValue) => _stok = newValue,
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Stok tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Berat",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                // color: Colors.white,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: new TextFormField(
                                  initialValue:
                                      widget.product.productWeight.toString(),
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    hintText: "0",
                                    fillColor: Colors.white,
                                    suffixIcon: Container(
                                      // padding: EdgeInsets.only(right: 16),
                                      width: 80,
                                      child: Center(
                                        child: Text(
                                          "Gram",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  onSaved: (newValue) => _berat = newValue,
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Berat tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Container(
                          width: 150,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Dimensi",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                // color: Colors.white,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: new TextFormField(
                                  initialValue:
                                      widget.product.productDimensions,
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    hintText: "ex: 10x10x10",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Dimensi produk tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onSaved: (newValue) => _dimensi = newValue,
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Minimum Order",
                                style: TextStyle(
                                  fontSize: 18.0,
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              // color: Colors.white,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: new TextFormField(
                                initialValue:
                                    widget.product.minimumOrder.toString(),
                                decoration: new InputDecoration(
                                  // labelText: "No Ponsel",
                                  hintText: "0",
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                onSaved: (newValue) => _minimum = newValue,
                                validator: (val) {
                                  if (val.isEmpty) {
                                    return "Minimum Order tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                style: new TextStyle(
                                  fontFamily: 'Poppins',
                                ),
                              ),
                            ),
                          ],
                        ))
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text("Deskripsi",
                        style: TextStyle(
                          fontSize: 18.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      // color: Colors.white,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: new TextFormField(
                        initialValue: widget.product.productDesc,
                        maxLines: null,
                        maxLength: null,
                        decoration: new InputDecoration(
                          // labelText: "No Ponsel",
                          hintText: "Masukan Deskripsi",
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Deskripsi tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (newValue) => _deskripsi = newValue,
                        keyboardType: TextInputType.multiline,
                        style: new TextStyle(
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text("Gambar Barang",
                        style: TextStyle(
                          fontSize: 18.0,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    // Container(
                    //   height: 100,
                    //   padding: EdgeInsets.all(10),
                    //   decoration: BoxDecoration(
                    //       border: Border.all(color: Colors.grey[600]),
                    //       borderRadius: BorderRadius.circular(10)),
                    //   child: Row(
                    //     children: [
                    //       GestureDetector(
                    //         onTap: _pickImage,
                    //         child: Container(
                    //           height: 80,
                    //           width: 80,
                    //           decoration: BoxDecoration(
                    //               borderRadius: BorderRadius.circular(10),
                    //               border: Border.all(color: Colors.grey[400]),
                    //               color: Colors.grey[350]),
                    //           child: Center(
                    //             child: f1 == null
                    //                 ? ClipRRect(
                    //                     borderRadius: BorderRadius.circular(10),
                    //                     child: CachedNetworkImage(
                    //                       imageUrl: widget.product.productImage,
                    //                       width: double.infinity,
                    //                       height: double.infinity,
                    //                       fit: BoxFit.cover,
                    //                       placeholder: (context, url) => Center(
                    //                           child: Shimmer.fromColors(
                    //                         baseColor: Colors.grey[300],
                    //                         highlightColor: Colors.grey[100],
                    //                         child: Container(
                    //                           color: Colors.white,
                    //                           width: double.infinity,
                    //                           height: double.infinity,
                    //                         ),
                    //                       )),
                    //                       errorWidget: (context, url, error) =>
                    //                           Image.asset(
                    //                               "assets/default_image.png"),
                    //                     ),
                    //                   )
                    //                 : ClipRRect(
                    //                     borderRadius: BorderRadius.circular(10),
                    //                     child: FadeInImage(
                    //                         fit: BoxFit.cover,
                    //                         height: double.infinity,
                    //                         width: double.infinity,
                    //                         image: FileImage(f1),
                    //                         placeholder: AssetImage(
                    //                             "assets/default_image.png")),
                    //                   ),
                    //           ),
                    //         ),
                    //       ),
                    //       SizedBox(
                    //         width: 10,
                    //       ),
                    //       Expanded(
                    //           child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.start,
                    //         mainAxisAlignment: MainAxisAlignment.center,
                    //         children: [
                    //           Text("Upload Gambar Barang",
                    //               style: TextStyle(
                    //                 fontSize: 16,
                    //               )),
                    //           SizedBox(
                    //             height: 5,
                    //           ),
                    //           Text(
                    //               "Ukuran minimal 300x300px berformat JPG atau PNG",
                    //               style: TextStyle(
                    //                   fontSize: 14, color: Colors.grey[600])),
                    //         ],
                    //       ))
                    //     ],
                    //   ),
                    // ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[600]),
                          borderRadius: BorderRadius.circular(10)),
                      child: files == null || files.length == 0
                          ? ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: widget.product.productImage
                                      .split(',')
                                      .length +
                                  1,
                              itemBuilder: (context, index) {
                                if (index <
                                    widget.product.productImage
                                        .split(',')
                                        .length) {
                                  return Container(
                                    height: 80,
                                    width: 80,
                                    margin: EdgeInsets.only(right: 4),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border:
                                            Border.all(color: Colors.grey[400]),
                                        color: Colors.grey[350]),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: CachedNetworkImage(
                                          imageUrl: widget.product.productImage
                                              .split(',')[index],
                                          width: double.infinity,
                                          height: double.infinity,
                                          fit: BoxFit.cover,
                                          placeholder: (context, url) =>
                                              CircularProgressIndicator(),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                                  "assets/default_image.png"),
                                        ),
                                      ),
                                    ),
                                  );
                                } else {
                                  return GestureDetector(
                                    onTap: () {
                                      uploadPic();
                                    },
                                    child: Container(
                                      height: 80,
                                      width: 80,
                                      margin: EdgeInsets.only(right: 4),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Colors.grey[400]),
                                          color: Colors.grey[350]),
                                      child: Center(
                                        child: Icon(
                                          Icons.camera_alt,
                                          color: Colors.grey[600],
                                          size: 35,
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              },
                            )
                          : ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: files.length + 1,
                              itemBuilder: (context, index) {
                                if (index < files.length) {
                                  return Container(
                                    height: 80,
                                    width: 80,
                                    margin: EdgeInsets.only(right: 4),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border:
                                            Border.all(color: Colors.grey[400]),
                                        color: Colors.grey[350]),
                                    child: Center(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: FadeInImage(
                                            fit: BoxFit.cover,
                                            height: double.infinity,
                                            width: double.infinity,
                                            image: FileImage(files[index]),
                                            placeholder: AssetImage(
                                                "assets/default_profile.png")),
                                      ),
                                    ),
                                  );
                                } else {
                                  return GestureDetector(
                                    onTap: () {
                                      if (files.length < 5) {
                                        uploadPic();
                                      } else if (files.length >= 5) {
                                        setState(() {
                                          print("ajeg");
                                          files.clear();
                                          images.clear();
                                        });
                                      }
                                    },
                                    child: Container(
                                      height: 80,
                                      width: 80,
                                      margin: EdgeInsets.only(right: 4),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Colors.grey[400]),
                                          color: files.length < 5
                                              ? Colors.grey[350]
                                              : Colors.red),
                                      child: Center(
                                        child: Icon(
                                          files.length < 5
                                              ? Icons.camera_alt
                                              : Icons.delete,
                                          color: files.length < 5
                                              ? Colors.grey[600]
                                              : Colors.white,
                                          size: 35,
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    GestureDetector(
                      onTap: () async {
                        final form = formKey.currentState;
                        List imagList = [];
                        if (files != null || files.length != 0) {
                          if (form.validate()) {
                            form.save();
                            setState(() {
                              isLoading = false;
                            });
                            CHttp _chttp =
                                Provider.of<CHttp>(context, listen: false);
                            MerchantViewModel merchantViewModel =
                                MerchantViewModel(http: _chttp);
                            for (int i = 0; i < files.length; i++) {
                              File file = File(files[i].path);
                              await ImageUploader.uploadImage(file).then((res) {
                                setState(() {
                                  imagList.add(res.data.path);
                                });
                              });
                            }
                            await merchantViewModel
                                .postEditProduct(
                              widget.product.id,
                              _namaBarang,
                              _deskripsi,
                              _harga,
                              _dimensi,
                              _berat,
                              _satuan,
                              _minimum,
                              _stok,
                              basket['tenantId'],
                              basket['idCategory'] == null
                                  ? widget.product.productCategoryId
                                  : basket['idCategory'],
                              imagList
                                  .toString()
                                  .replaceAll('[', '')
                                  .replaceAll(']', '')
                                  .replaceAll(' ', ''),
                            )
                                .then((product) {
                              if (product != null) {
                                setState(() {
                                  isLoading = true;
                                  basket.addAll({
                                    "idCategory": null,
                                    "nameCategory": null,
                                  });
                                });
                                Fluttertoast.showToast(
                                    msg: "Ubah Produk Berhasil",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: ColorPalette.btnGreen,
                                    textColor: Colors.white);
                                Navigator.pop(context, true);
                              } else {
                                setState(() {
                                  isLoading = true;
                                  basket.addAll({
                                    "idCategory": null,
                                    "nameCategory": null,
                                  });
                                });
                                Fluttertoast.showToast(
                                    msg:
                                        "Terjadi kesalahan saat terhubung ke server",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white);
                              }
                            });
                          }
                        } else {
                          if (form.validate()) {
                            form.save();
                            setState(() {
                              isLoading = false;
                            });
                            CHttp _chttp =
                                Provider.of<CHttp>(context, listen: false);
                            MerchantViewModel merchantViewModel =
                                MerchantViewModel(http: _chttp);
                            await merchantViewModel
                                .postEditProduct(
                              widget.product.id,
                              _namaBarang,
                              _deskripsi,
                              _harga,
                              _dimensi,
                              _berat,
                              _satuan,
                              _minimum,
                              _stok,
                              basket['tenantId'],
                              basket['idCategory'] == null
                                  ? widget.product.productCategoryId
                                  : basket['idCategory'],
                            )
                                .then((product) {
                              if (product != null) {
                                setState(() {
                                  isLoading = true;
                                  basket.addAll({
                                    "idCategory": null,
                                    "nameCategory": null,
                                  });
                                });
                                Fluttertoast.showToast(
                                    msg: "Ubah Produk Berhasil",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: ColorPalette.btnGreen,
                                    textColor: Colors.white);
                                Navigator.pop(context, true);
                              } else {
                                setState(() {
                                  isLoading = true;
                                  basket.addAll({
                                    "idCategory": null,
                                    "nameCategory": null,
                                  });
                                });
                                Fluttertoast.showToast(
                                    msg:
                                        "Terjadi kesalahan saat terhubung ke server",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 1,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white);
                              }
                            });
                          }
                        }
                      },
                      child: Container(
                        height: 55,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(55),
                            color: ColorPalette.themeIcon),
                        child: Center(
                          child: isLoading
                              ? Text(
                                  'Simpan',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                )
                              : Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                                ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }

  Widget getSingleCategory() {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    MerchantViewModel merchantVM = MerchantViewModel(http: _chttp);
    return FutureBuilder(
      future: merchantVM.fetchSingleCategory(widget.product.productCategoryId),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final CategorySingleProductModel singleProductModel = snapshot.data;
          return GestureDetector(
            onTap: () {
              showBarModalBottomSheet(
                expand: false,
                context: context,
                backgroundColor: Colors.transparent,
                builder: (context) => modalCategory(context),
              );
            },
            child: Container(
                height: 60,
                width: double.infinity,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.grey),
                    color: Colors.grey[100]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        basket['nameCategory'] == null
                            ? singleProductModel.data.categoryName
                            : basket['nameCategory'],
                        style:
                            TextStyle(fontSize: 16.0, color: Colors.grey[600])),
                  ],
                )),
          );
        }
        return Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            width: double.infinity,
            height: 60,
          ),
        );
      },
    );
  }

  Widget modalCategory(BuildContext context) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    MerchantViewModel merchantVM = MerchantViewModel(http: _chttp);
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Text("Pilih Kategori Produk",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    Expanded(
                      flex: 40,
                      child: FutureBuilder(
                        future: merchantVM.fetchCategoryAll(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            final CategoryProductModel categoryProductModel =
                                snapshot.data;
                            return ListView.separated(
                              shrinkWrap: true,
                              padding: EdgeInsets.all(16),
                              itemCount: categoryProductModel.data.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    setState(() {
                                      basket.addAll({
                                        'nameCategory': categoryProductModel
                                            .data[index].categoryName,
                                        'idCategory':
                                            categoryProductModel.data[index].id,
                                      });
                                    });
                                    Navigator.pop(context);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8, bottom: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          categoryProductModel
                                              .data[index].categoryName,
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black),
                                        ),
                                        basket['idCategory'] ==
                                                categoryProductModel
                                                    .data[index].id
                                            ? Icon(
                                                Icons.check_circle,
                                                color: ColorPalette.btnGreen,
                                              )
                                            : Icon(
                                                Icons.lens_outlined,
                                                color: Colors.black,
                                              )
                                      ],
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return Divider(
                                  thickness: 1,
                                );
                              },
                            );
                          }
                          return loadingCategory();
                        },
                      ),
                    ),
                  ],
                )),
          ]),
    );
  }

  Widget loadingCategory() {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: 10,
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 8, top: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 250,
                        height: 10,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                        ),
                        width: 200,
                        height: 10,
                      ),
                    ),
                  ],
                )),
                Icon(
                  Icons.lens_outlined,
                  color: Colors.black,
                )
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            thickness: 1,
          );
        },
      ),
    );
  }
}

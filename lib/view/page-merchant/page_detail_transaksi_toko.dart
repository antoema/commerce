import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/bloc/transaction_detail-seller-bloc/transaction_detail_seller_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/tracking_courier.dart';
import 'package:pekik_app/model/transaction_single.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-transaction/page_tracking_courier.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class DetailTransaksiTokoPage extends StatefulWidget {
  @override
  _DetailTransaksiTokoPageState createState() =>
      _DetailTransaksiTokoPageState();
}

class _DetailTransaksiTokoPageState extends State<DetailTransaksiTokoPage> {
  TransactionDetailSellerBloc transactionDetailBloc =
      TransactionDetailSellerBloc();
  final formKey = new GlobalKey<FormState>();
  final _noResi = TextEditingController();

  bool isLoading = true;

  @override
  void initState() {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    transactionDetailBloc.add(
        GetTransactionDetailSeller(http: _chttp, idTrx: basket['idTransaksi']));
    _noResi.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    transactionDetailBloc.add(
        GetTransactionDetailSeller(http: _chttp, idTrx: basket['idTransaksi']));
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Detail Transaksi",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildTransactionDetail(),
    );
  }

  Widget _buildTransactionDetail() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => transactionDetailBloc,
        child: BlocListener<TransactionDetailSellerBloc,
            TransactionDetailSellerState>(
          listener: (context, state) {
            if (state is TransactionDetailSellerError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<TransactionDetailSellerBloc,
              TransactionDetailSellerState>(
            builder: (context, state) {
              if (state is TransactionDetailSellerInitial) {
                return Center(child: CircularProgressIndicator());
              } else if (state is TransactionDetailSellerLoading) {
                return Center(child: CircularProgressIndicator());
              }
              if (state is TransactionDetailSellerLoaded) {
                return _buildViewDetail(context, state.transactionSingleModel);
              }
              if (state is TransactionDetailSellerError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildViewDetail(
      BuildContext context, TransactionSingleModel transactionSingleModel) {
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    return Stack(
      children: [
        ListView(
          children: [
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Status",
                        style: TextStyle(
                          color: ColorPalette.fontColor,
                          fontSize: 12,
                        )),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(transactionSingleModel.data.status.toUpperCase(),
                            style: TextStyle(
                              color: ColorPalette.themeIcon,
                              fontSize: 14,
                            )),
                        GestureDetector(
                          onTap: () {
                            if (transactionSingleModel.data.wayBill == null) {
                              Fluttertoast.showToast(
                                  msg: "No Resi belum ada",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white);
                            } else {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return TrackingCourierPage(
                                    idTrx: transactionSingleModel.data.id,
                                    status: transactionSingleModel.data.status);
                              }));
                            }
                          },
                          child: Text("Lihat",
                              style: TextStyle(
                                color: ColorPalette.themeIcon,
                                fontSize: 14,
                              )),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 2,
                      color: Colors.grey[100],
                    ),
                    SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Tanggal Pembelian",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                            )),
                        Text(
                            ConnexistHelper.formatDate(
                                transactionSingleModel.data.createdAt),
                            style: TextStyle(
                              color: ColorPalette.fontColor,
                              fontSize: 14,
                            )),
                      ],
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 2,
                      color: Colors.grey[100],
                    ),
                    SizedBox(height: 8),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("No Invoice",
                            style: TextStyle(
                              color: ColorPalette.fontColor,
                              fontSize: 12,
                            )),
                        SizedBox(height: 4),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(transactionSingleModel.data.invoiceNumber,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                )),
                            // GestureDetector(
                            //   onTap: () {},
                            //   child: Text("Lihat",
                            //       style: TextStyle(
                            //         color: ColorPalette.themeIcon,
                            //         fontSize: 14,
                            //       )),
                            // ),
                          ],
                        ),
                      ],
                    ),
                  ],
                )),
            Divider(
              thickness: 12,
              color: Colors.grey[100],
            ),
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Toko: ",
                            style: TextStyle(
                              color: ColorPalette.fontColor,
                              fontSize: 14,
                            )),
                        SizedBox(width: 2),
                        Text(transactionSingleModel.data.tenant.tenantName,
                            style: TextStyle(
                              color: ColorPalette.fontColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            )),
                      ],
                    ),
                    SizedBox(height: 8),
                    ...transactionSingleModel.data.items.map((dataProduct) {
                      return Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: 60,
                                    height: 60,
                                    child: Stack(
                                      children: [
                                        Container(
                                            width: double.infinity,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                            ),
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(12),
                                              child: CachedNetworkImage(
                                                imageUrl: dataProduct
                                                    .productImage
                                                    .split(',')
                                                    .first,
                                                fit: BoxFit.cover,
                                                placeholder: (context, url) =>
                                                    Center(
                                                        child:
                                                            Shimmer.fromColors(
                                                  baseColor: Colors.grey[300],
                                                  highlightColor:
                                                      Colors.grey[100],
                                                  child: Container(
                                                    color: Colors.white,
                                                    width: double.infinity,
                                                    height: double.infinity,
                                                  ),
                                                )),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Center(
                                                            child: Image.asset(
                                                  "assets/default_image.png",
                                                  fit: BoxFit.cover,
                                                )),
                                              ),
                                            )),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        dataProduct.productName.length > 75
                                            ? Text(
                                                dataProduct.productName
                                                        .substring(0, 80) +
                                                    "...",
                                                maxLines: 2,
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                ),
                                              )
                                            : Text(
                                                dataProduct.productName,
                                                maxLines: 2,
                                                style: TextStyle(
                                                  fontSize: 14.0,
                                                ),
                                              ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                            dataProduct.qty.toString() +
                                                " barang ",
                                            style: TextStyle(
                                              fontSize: 12.0,
                                              color: ColorPalette.fontColor,
                                            )),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                            ConnexistHelper.formatCurrency(
                                                dataProduct.price.toDouble()),
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8),
                              Divider(
                                thickness: 2,
                                color: Colors.grey[100],
                              ),
                              SizedBox(height: 8),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Total Belanja",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          )),
                                      SizedBox(height: 8),
                                      Text(
                                          ConnexistHelper.formatCurrency(
                                              dataProduct.price.toDouble() *
                                                  dataProduct.qty.toDouble()),
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12,
                                          )),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }).toList()
                  ],
                )),
            Divider(
              thickness: 12,
              color: Colors.grey[100],
            ),
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Detail Pengiriman",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        )),
                    SizedBox(height: 15),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text("Kurir Pengiriman",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ),
                        Expanded(
                          child: Text(
                              transactionSingleModel
                                      .data.expedition.expeditionName
                                      .toUpperCase() +
                                  " - " +
                                  transactionSingleModel
                                      .data.expedition.expeditionService,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ),
                      ],
                    ),
                    SizedBox(height: 15),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text("No. Resi",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  transactionSingleModel.data.wayBill == null
                                      ? "Belum ada No. Resi"
                                      : transactionSingleModel.data.wayBill,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                  )),
                              SizedBox(height: 15),
                              transactionSingleModel.data.wayBill == null
                                  ? Container()
                                  : GestureDetector(
                                      onTap: () {
                                        Clipboard.setData(ClipboardData(
                                                text: transactionSingleModel
                                                    .data.wayBill))
                                            .then((result) {
                                          showFloatingFlushbar2(context);
                                        });
                                      },
                                      child: Text("Salin No. Resi",
                                          style: TextStyle(
                                            color: ColorPalette.themeIcon,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14,
                                          )),
                                    )
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 15),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text("Alamat Pengiriman",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              )),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(transactionSingleModel.data.destination.name,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                              SizedBox(height: 4),
                              Text(
                                  transactionSingleModel
                                      .data.destination.phoneNumber,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                              SizedBox(height: 4),
                              Text(
                                  transactionSingleModel
                                      .data.destination.address,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                              SizedBox(height: 4),
                              Text(transactionSingleModel.data.destination.city,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                              SizedBox(height: 4),
                              Text(
                                  transactionSingleModel
                                      .data.destination.postalCode
                                      .toString(),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
            Divider(
              thickness: 12,
              color: Colors.grey[100],
            ),
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Informasi Pembayaran",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        )),
                    SizedBox(height: 8),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Total Harga Barang",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                )),
                            Container(
                              child: Text(
                                  ConnexistHelper.formatCurrency(
                                      transactionSingleModel.data.totalPrice
                                          .toDouble()),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                            )
                          ]),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Ongkos Kirim",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                )),
                            Container(
                              child: Text(
                                  ConnexistHelper.formatCurrency(
                                      transactionSingleModel
                                          .data.expedition.expeditionCost
                                          .toDouble()),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                  )),
                            )
                          ]),
                    ),
                  ],
                )),
            Divider(
              thickness: 2,
              color: Colors.grey[100],
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: Container(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Total bayar",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                          )),
                      Container(
                        child: Text(
                            ConnexistHelper.formatCurrency(
                                transactionSingleModel.data.totalAmount
                                    .toDouble()),
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                      )
                    ]),
              ),
            ),
            SizedBox(
              height: transactionSingleModel.data.status == "received"
                  ? 70
                  : transactionSingleModel.data.status == "packing"
                      ? 150
                      : 10,
            )
          ],
        ),
        transactionSingleModel.data.status == "received"
            ? Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    height: 70,
                    width: double.infinity,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 4), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () async {
                              setState(() {
                                isLoading = false;
                              });
                              await transactionVM
                                  .postPackingTransaction(basket['idTransaksi'])
                                  .then((value) {
                                if (value != null) {
                                  setState(() {
                                    isLoading = true;
                                  });
                                  Fluttertoast.showToast(
                                      msg: "Konfirmasi pesanan berhasil",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIos: 1,
                                      backgroundColor: ColorPalette.btnGreen,
                                      textColor: Colors.white);
                                  Navigator.pop(context);
                                } else {
                                  setState(() {
                                    isLoading = true;
                                  });
                                  Fluttertoast.showToast(
                                      msg:
                                          "Konfirmasi pesanan gagal, coba ulangi kembali",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIos: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white);
                                }
                              });
                            },
                            child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(60),
                                  color: ColorPalette.themeIcon),
                              child: Center(
                                child: isLoading
                                    ? Text(
                                        "Konfirmasi Pesanan",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      )
                                    : SizedBox(
                                        height: 20.0,
                                        width: 20.0,
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white),
                                        ),
                                      ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )))
            : transactionSingleModel.data.status == "packing"
                ? Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        height: 150,
                        width: double.infinity,
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 4), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Form(
                          key: formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(60.0)),
                                child: new TextFormField(
                                  controller: _noResi,
                                  decoration: new InputDecoration(
                                    // labelText: "No Ponsel",
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: Icon(
                                        Icons.local_shipping,
                                        color: Colors.grey[400],
                                      ),
                                    ),
                                    hintText: "Masukan No Resi Pengiriman",
                                    fillColor: Colors.white,
                                    border: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(60.0),
                                      borderSide: new BorderSide(),
                                    ),
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "No Resi pengiriman tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                              SizedBox(height: 8),
                              GestureDetector(
                                onTap: () async {
                                  print(_noResi.text);
                                  if (_noResi.text == '') {
                                    Fluttertoast.showToast(
                                        msg: "No Resi, tidak boleh kosong",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIos: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white);
                                  } else {
                                    setState(() {
                                      isLoading = false;
                                    });
                                    await transactionVM
                                        .postShippingTransaction(
                                            basket['idTransaksi'], _noResi.text)
                                        .then((value) {
                                      if (value != null) {
                                        setState(() {
                                          isLoading = true;
                                        });
                                        Fluttertoast.showToast(
                                            msg: "Input No Resi Berhasil",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 1,
                                            backgroundColor:
                                                ColorPalette.btnGreen,
                                            textColor: Colors.white);
                                        Navigator.pop(context);
                                      } else {
                                        setState(() {
                                          isLoading = true;
                                        });
                                        Fluttertoast.showToast(
                                            msg:
                                                "Input No Resi, coba ulangi kembali",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white);
                                      }
                                    });
                                  }
                                },
                                child: Container(
                                  height: 60,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(60),
                                      color: _noResi.text == ''
                                          ? Colors.grey[350]
                                          : ColorPalette.themeIcon),
                                  child: Center(
                                    child: isLoading
                                        ? Text(
                                            "Submit",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16),
                                          )
                                        : SizedBox(
                                            height: 20.0,
                                            width: 20.0,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Colors.white),
                                            ),
                                          ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )))
                : Container()
      ],
    );
  }

  void showFloatingFlushbar2(BuildContext context) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.blue, Colors.blueAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      // All of the previous Flushbars could be dismissed by swiping down
      // now we want to swipe to the sides
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      // The default curve is Curves.easeOut
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      // title: 'This is a floating Flushbar',
      message: 'Copied to Clipboard',
      duration: Duration(seconds: 2),
    )..show(context);
  }

  Widget modalTracking(BuildContext context, String idTrx) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    return SafeArea(
      bottom: false,
      child: ListView(
          shrinkWrap: true,
          controller: ModalScrollController.of(context),
          children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.90,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Text("Detail Status Pengiriman",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16)),
                    ),
                    SizedBox(height: 8),
                    Divider(
                      thickness: 1,
                    ),
                    SizedBox(height: 8),
                    Expanded(
                      flex: 40,
                      child: FutureBuilder<TrackingCourierModel>(
                        future: transactionVM.fetchTrackingCourier(idTrx),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            final TrackingCourierModel trackingCourierModel =
                                snapshot.data;
                            return ListView(
                              padding: EdgeInsets.only(left: 16, right: 16),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: [
                                Timeline.builder(
                                    shrinkWrap: true,
                                    itemBuilder: (context, index) {
                                      return TimelineModel(
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 8, bottom: 8),
                                            child: Card(
                                              elevation: 0,
                                              // margin: EdgeInsets.symmetric(vertical: 16.0),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0)),
                                              clipBehavior: Clip.antiAlias,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(0),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    // Image.network(doodle.doodle),
                                                    const SizedBox(
                                                      height: 8.0,
                                                    ),
                                                    Text(trackingCourierModel
                                                        .data
                                                        .manifest[index]
                                                        .cityName),
                                                    const SizedBox(
                                                      height: 8.0,
                                                    ),
                                                    Text(trackingCourierModel
                                                        .data
                                                        .manifest[index]
                                                        .manifestDescription),
                                                    const SizedBox(
                                                      height: 8.0,
                                                    ),
                                                    Row(children: [
                                                      Text(DateFormat(
                                                              'dd MMMM yyyy')
                                                          .format(
                                                              trackingCourierModel
                                                                  .data
                                                                  .manifest[
                                                                      index]
                                                                  .manifestDate)),
                                                      Text(trackingCourierModel
                                                          .data
                                                          .manifest[index]
                                                          .manifestTime),
                                                    ]),
                                                    const SizedBox(
                                                      height: 8.0,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                          iconBackground:
                                              ColorPalette.themeIcon,
                                          position: TimelineItemPosition.right,
                                          isFirst: index == 0,
                                          isLast: index ==
                                              trackingCourierModel
                                                  .data.manifest.length,
                                          // iconBackground: doodle.iconBackground,
                                          icon: Icon(
                                            Icons.local_shipping,
                                            color: Colors.white,
                                          ));
                                    },
                                    itemCount: trackingCourierModel
                                        .data.manifest.length,
                                    physics: BouncingScrollPhysics(),
                                    position: TimelinePosition.Left)
                              ],
                            );
                          }
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      ),
                    ),
                  ],
                )),
          ]),
    );
  }
}

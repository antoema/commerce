import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pekik_app/bloc/product-bloc/product_bloc.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/view/page-merchant/page_tambah_barang.dart';
import 'package:pekik_app/view/page-merchant/page_ubah_produk.dart';
import 'package:pekik_app/view/widgets/widget_loading_list.dart';
import 'package:pekik_app/viewmodel/merchant_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class DaftarProdukPage extends StatefulWidget {
  @override
  _DaftarProdukPageState createState() => _DaftarProdukPageState();
}

class _DaftarProdukPageState extends State<DaftarProdukPage>
    with TickerProviderStateMixin {
  ProductBloc productBloc = ProductBloc();
  TabController tabController;
  ScrollController productController = ScrollController();
  AnimationController animcontroller;
  Animation<Offset> offset;

  var _isVisible;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    productBloc.add(GetProduct(http: _chttp));

    _isVisible = true;

    animcontroller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 2.0))
        .animate(animcontroller);
    productController.addListener(() {
      if (productController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds 
             */
          print("**** $_isVisible up"); //Move IO away from setState
          setState(() {
            _isVisible = false;
            animcontroller.forward();
          });
        }
      } else {
        if (productController.position.userScrollDirection ==
            ScrollDirection.forward) {
          if (_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds 
               */
            print("**** $_isVisible down"); //Move IO away from setState
            setState(() {
              _isVisible = true;

              animcontroller.reverse();
            });
          }
        }
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    productController.dispose();
    super.dispose();
  }

  Future<void> onRefresh() async {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    setState(() {
      productBloc.add(GetProduct(http: _chttp));
    });
  }

  void showFloatingFlushbar(String msg) {
    Flushbar(
      // aroundPadding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10.0),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red, Colors.redAccent],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      duration: Duration(seconds: 2),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom == 0.0;
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: showFab
          ? SlideTransition(
              position: offset,
              child: FloatingActionButton(
                backgroundColor: ColorPalette.themeIcon,
                child: Center(
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TambahBarangPage(),
                      )).then((value) => value ? onRefresh() : null);
                },
              ),
            )
          : null,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Daftar Produk",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _buildMerchant(),
    );
  }

  Widget _buildMerchant() {
    return RefreshIndicator(
      onRefresh: () => onRefresh(),
      child: BlocProvider(
        create: (context) => productBloc,
        child: BlocListener<ProductBloc, ProductState>(
          listener: (context, state) {
            if (state is ProductError) {
              return showFloatingFlushbar(state.message);
            }
          },
          child: BlocBuilder<ProductBloc, ProductState>(
            builder: (context, state) {
              if (state is ProductInitial) {
                return LoadingListWidget();
              } else if (state is ProductLoading) {
                return LoadingListWidget();
              }
              if (state is ProductLoaded) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TabBar(
                      isScrollable: true,
                      indicatorColor: ColorPalette.themeIcon,
                      labelColor: Colors.black,
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.bold, fontFamily: 'Muli'),
                      tabs: [
                        Tab(text: "Produk Dijual"),
                        Tab(text: "Tidak Dijual"),
                      ],
                      controller: tabController,
                    ),
                    Expanded(
                        flex: 40,
                        child: TabBarView(
                            controller: tabController,
                            children: <Widget>[
                              _productForSale(context, state.productModel),
                              _productNotForSale(context, state.productModel)
                            ])),
                  ],
                );
              }
              if (state is ProductError) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/ic_failure.png',
                          width: 150,
                        ),
                        SizedBox(height: 25),
                        Container(
                          child: Text(
                            "Ups, Koneksi Gagal",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          state.message,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: 120,
                          height: 45,
                          child: RaisedButton(
                            color: ColorPalette.themeIcon,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            onPressed: () {
                              onRefresh();
                            },
                            child: Center(
                              child: Text(
                                "Coba Lagi",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _productForSale(BuildContext context, ProductModel productModel) {
    var isActive =
        productModel.data.where((element) => element.isActive == true).toList();
    if (isActive.length == 0) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(bottom: 30),
                child: Center(
                  child: Image.asset(
                    "assets/ic_empty.png",
                    width: 128,
                  ),
                )),
            Container(
              child: Center(
                child: Text("Produk belum ada",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.black)),
              ),
            ),
          ],
        ),
      );
    }
    return ListView(
      controller: productController,
      children: [
        ...isActive.where((element) => element.isActive == true).map((data) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  showBarModalBottomSheet(
                    expand: false,
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (context) => modalViewProduct(context, data),
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 16,
                        right: 16,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)),
                            child: Container(
                              height: 70.0,
                              width: 70.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child: CachedNetworkImage(
                                  imageUrl: data.productImage.split(',').first,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => Center(
                                      child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.grey[100],
                                    child: Container(
                                      color: Colors.white,
                                      width: double.infinity,
                                      height: double.infinity,
                                    ),
                                  )),
                                  errorWidget: (context, url, error) =>
                                      Image.asset(
                                    'assets/default_image.png',
                                    height: 60.0,
                                    width: 60.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              data.productName.length > 50
                                  ? Text(
                                      data.productName.substring(0, 50) + '...',
                                      maxLines: 2,
                                      style: TextStyle(fontSize: 16),
                                    )
                                  : Text(
                                      data.productName,
                                      style: TextStyle(fontSize: 16),
                                    ),
                              SizedBox(height: 5),
                              Text(
                                  ConnexistHelper.formatCurrency(
                                      data.productPrice.toDouble()),
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 10),
                              Text('STOK : ${data.productStock}',
                                  style: TextStyle(
                                    fontSize: 14,
                                  ))
                            ],
                          )),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 16,
                        right: 16,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () async {
                                print("object");
                                if (data.isActive == true) {
                                  CHttp _chttp = Provider.of<CHttp>(context,
                                      listen: false);
                                  MerchantViewModel merchantViewModel =
                                      MerchantViewModel(http: _chttp);
                                  await merchantViewModel
                                      .postEditPublish(data.id, false)
                                      .then((value) {
                                    if (value != null) {
                                      onRefresh();
                                      Fluttertoast.showToast(
                                          msg: "Set Tidak Dijual Berhasil",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor:
                                              ColorPalette.btnGreen,
                                          textColor: Colors.white);
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "Terjadi Kesalahan, Set Tidak Dijual Gagal",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white);
                                    }
                                  });
                                } else {
                                  CHttp _chttp = Provider.of<CHttp>(context,
                                      listen: false);
                                  MerchantViewModel merchantViewModel =
                                      MerchantViewModel(http: _chttp);
                                  await merchantViewModel
                                      .postEditPublish(data.id, true)
                                      .then((value) {
                                    if (value != null) {
                                      onRefresh();
                                      Fluttertoast.showToast(
                                          msg: "Set Dijual Berhasil",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor:
                                              ColorPalette.btnGreen,
                                          textColor: Colors.white);
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "Terjadi Kesalahan, Set Dijual Gagal",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white);
                                    }
                                  });
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: Color(0xffF0F1F5)),
                                child: Center(
                                  child: Text(
                                    data.isActive
                                        ? 'Set Tidak Dijual'
                                        : 'Set Dijual',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return UbahProdukPage(data);
                                })).then((value) => value ? onRefresh() : null);
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: Color(0xffF0F1F5)),
                                child: Center(
                                  child: Text(
                                    'Ubah',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                showBarModalBottomSheet(
                                  expand: false,
                                  context: context,
                                  backgroundColor: Colors.transparent,
                                  builder: (context) =>
                                      modalViewProduct(context, data),
                                );
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: Color(0xffF0F1F5)),
                                child: Center(
                                  child: Text(
                                    'Lihat Info',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
              )
            ],
          );
        }).toList()
      ],
    );
  }

  Widget _productNotForSale(BuildContext context, ProductModel productModel) {
    var isActive = productModel.data
        .where((element) => element.isActive == false)
        .toList();
    if (isActive.length == 0) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(bottom: 30),
                child: Center(
                  child: Image.asset(
                    "assets/ic_empty.png",
                    width: 128,
                  ),
                )),
            Container(
              child: Center(
                child: Text("Produk belum ada",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.black)),
              ),
            ),
          ],
        ),
      );
    }
    return ListView(
      controller: productController,
      children: [
        ...isActive.where((element) => element.isActive == false).map((data) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  showBarModalBottomSheet(
                    expand: false,
                    context: context,
                    backgroundColor: Colors.transparent,
                    builder: (context) => modalViewProduct(context, data),
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 16,
                        right: 16,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)),
                            child: Container(
                              height: 70.0,
                              width: 70.0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child: CachedNetworkImage(
                                  imageUrl: data.productImage.split(',').first,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => Center(
                                      child: Shimmer.fromColors(
                                    baseColor: Colors.grey[300],
                                    highlightColor: Colors.grey[100],
                                    child: Container(
                                      color: Colors.white,
                                      width: double.infinity,
                                      height: double.infinity,
                                    ),
                                  )),
                                  errorWidget: (context, url, error) =>
                                      Image.asset(
                                    'assets/default_image.png',
                                    height: 60.0,
                                    width: 60.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              data.productName.length > 50
                                  ? Text(
                                      data.productName.substring(0, 50) + '...',
                                      maxLines: 2,
                                      style: TextStyle(fontSize: 16),
                                    )
                                  : Text(
                                      data.productName,
                                      style: TextStyle(fontSize: 16),
                                    ),
                              SizedBox(height: 5),
                              Text(
                                  ConnexistHelper.formatCurrency(
                                      data.productPrice.toDouble()),
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold)),
                              SizedBox(height: 10),
                              Text('STOK : ${data.productStock}',
                                  style: TextStyle(
                                    fontSize: 14,
                                  ))
                            ],
                          )),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 16,
                        right: 16,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: () async {
                                print("object");
                                if (data.isActive == true) {
                                  CHttp _chttp = Provider.of<CHttp>(context,
                                      listen: false);
                                  MerchantViewModel merchantViewModel =
                                      MerchantViewModel(http: _chttp);
                                  await merchantViewModel
                                      .postEditPublish(data.id, false)
                                      .then((value) {
                                    if (value != null) {
                                      onRefresh();
                                      Fluttertoast.showToast(
                                          msg: "Set Tidak Dijual Berhasil",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor:
                                              ColorPalette.btnGreen,
                                          textColor: Colors.white);
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "Terjadi Kesalahan, Set Tidak Dijual Gagal",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white);
                                    }
                                  });
                                } else {
                                  CHttp _chttp = Provider.of<CHttp>(context,
                                      listen: false);
                                  MerchantViewModel merchantViewModel =
                                      MerchantViewModel(http: _chttp);
                                  await merchantViewModel
                                      .postEditPublish(data.id, true)
                                      .then((value) {
                                    if (value != null) {
                                      onRefresh();
                                      Fluttertoast.showToast(
                                          msg: "Set Dijual Berhasil",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor:
                                              ColorPalette.btnGreen,
                                          textColor: Colors.white);
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "Terjadi Kesalahan, Set Dijual Gagal",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white);
                                    }
                                  });
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: Color(0xffF0F1F5)),
                                child: Center(
                                  child: Text(
                                    data.isActive
                                        ? 'Set Tidak Dijual'
                                        : 'Set Dijual',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return UbahProdukPage(data);
                                })).then((value) => value ? onRefresh() : null);
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: Color(0xffF0F1F5)),
                                child: Center(
                                  child: Text(
                                    'Ubah',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                showBarModalBottomSheet(
                                  expand: false,
                                  context: context,
                                  backgroundColor: Colors.transparent,
                                  builder: (context) =>
                                      modalViewProduct(context, data),
                                );
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: Color(0xffF0F1F5)),
                                child: Center(
                                  child: Text(
                                    'Lihat Info',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
              )
            ],
          );
        }).toList()
      ],
    );
  }

  Widget modalViewProduct(BuildContext context, ResultProduct productData) {
    return SafeArea(
      bottom: false,
      child: Stack(
        children: [
          ListView(
              shrinkWrap: true,
              controller: ModalScrollController.of(context),
              children: [
                Container(
                    height: MediaQuery.of(context).size.height * 0.90,
                    margin: EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Container(
                            height: 300,
                            width: double.infinity,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: CachedNetworkImage(
                                imageUrl:
                                    productData.productImage.split(',').first,
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Center(
                                    child: Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[100],
                                  child: Container(
                                    color: Colors.white,
                                    width: double.infinity,
                                    height: double.infinity,
                                  ),
                                )),
                                errorWidget: (context, url, error) => Center(
                                    child: Image.asset(
                                  "assets/default_image.png",
                                  height: double.infinity,
                                  width: double.infinity,
                                  fit: BoxFit.cover,
                                )),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          productData.productName,
                          style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        productData.productDesc.length > 150
                            ? Text(
                                productData.productDesc.substring(0, 150) +
                                    "...",
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              )
                            : Text(
                                productData.productDesc,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          ConnexistHelper.formatCurrency(
                              productData.productPrice.toDouble()),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ],
                    )),
              ]
              // ListTile.divideTiles(
              //   context: context,
              //   tiles: List.generate(
              //       100,
              //       (index) => ListTile(
              //             title: Text('Item'),
              //           )),
              // ).toList(),
              ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 65,
                  width: double.infinity,
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    // boxShadow: [
                    //   BoxShadow(
                    //     color: Colors.grey.withOpacity(0.5),
                    //     spreadRadius: 5,
                    //     blurRadius: 7,
                    //     offset: Offset(0, 4), // changes position of shadow
                    //   ),
                    // ],
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () async {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return UbahProdukPage(productData);
                            })).then((value) => value ? onRefresh() : null);
                          },
                          child: Container(
                            height: 55,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(55),
                                color: ColorPalette.themeIcon),
                            child: Center(
                              child: Text(
                                "Ubah Produk",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )))
        ],
      ),
    );
  }
}

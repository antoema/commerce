import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/color_pallete.dart';
import 'package:pekik_app/model/transaction_by_status.dart';
import 'package:pekik_app/utils/api_helper.dart';
import 'package:pekik_app/utils/auth.dart';
import 'package:pekik_app/utils/helper.dart';
import 'package:pekik_app/viewmodel/transaction_vm.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class TransaksiTokoPage extends StatefulWidget {
  const TransaksiTokoPage({Key key}) : super(key: key);

  @override
  _TransaksiTokoPageState createState() => _TransaksiTokoPageState();
}

class _TransaksiTokoPageState extends State<TransaksiTokoPage>
    with TickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 5, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    BaseAuth _auth = Provider.of<Auth>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPalette.themeIcon),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text(
          "Daftar Transaksi",
          style: TextStyle(
              color: ColorPalette.themeIcon, fontWeight: FontWeight.bold),
        ),
      ),
      body: _auth.currentUser == null
          ? Padding(
              padding: const EdgeInsets.only(
                  left: 16, right: 16, bottom: 60, top: 16),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/ic_login.png',
                      width: 128,
                      height: 128,
                    ),
                    SizedBox(height: 25),
                    Container(
                      child: Text(
                        "Wah, Kamu belum masuk",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      child: Text(
                        "Silahkan masuk terlebih dahulu untuk melanjutkan transaksi",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 16, color: Colors.grey[450]),
                      ),
                    ),
                    SizedBox(height: 25),
                    Container(
                        height: 65,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          // boxShadow: [
                          //   BoxShadow(
                          //     color: Colors.grey.withOpacity(0.5),
                          //     spreadRadius: 5,
                          //     blurRadius: 7,
                          //     offset: Offset(0, 4), // changes position of shadow
                          //   ),
                          // ],
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context, "/login");
                                },
                                child: Container(
                                  height: 55,
                                  margin: EdgeInsets.only(left: 25, right: 25),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(55),
                                      color: ColorPalette.themeIcon),
                                  child: Center(
                                    child: Text(
                                      "Masuk",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            )
          : Column(
              children: [
                TabBar(
                  isScrollable: true,
                  indicatorColor: ColorPalette.themeIcon,
                  labelColor: Colors.black,
                  tabs: [
                    Tab(text: "Pesanan Baru"),
                    Tab(text: "Siap Dikirim"),
                    Tab(text: "Sedang Dikirim"),
                    Tab(text: "Sampai Tujuan"),
                    Tab(text: "Dibatalkan"),
                  ],
                  controller: tabController,
                ),
                Expanded(
                  flex: 40,
                  child:
                      TabBarView(controller: tabController, children: <Widget>[
                    _transactionSellerByStatus('received'),
                    _transactionSellerByStatus('packing'),
                    _transactionSellerByStatus('shipping'),
                    _transactionSellerByStatus('delivered'),
                    _transactionSellerByStatus('canceled'),
                  ]),
                ),
                // getToken()
              ],
            ),
    );
  }

  Widget _transactionSellerByStatus(String status) {
    CHttp _chttp = Provider.of<CHttp>(context, listen: false);
    Map<String, dynamic> basket = Provider.of(context, listen: false);
    TransactionViewModel transactionVM = TransactionViewModel(http: _chttp);
    return FutureBuilder<TransactionByStatusModel>(
      future: transactionVM.fetchSellerTransactionByStatus(status),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final TransactionByStatusModel transactionPaid = snapshot.data;
          if (transactionPaid == null) {
            return _emptyTransaction();
          } else if (transactionPaid.data.length == 0) {
            return _emptyTransaction();
          }

          return ListView.builder(
            padding: EdgeInsets.all(16),
            itemCount: transactionPaid.data.length,
            itemBuilder: (context, index) {
              return Card(
                elevation: 2,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.only(bottom: 10),
                child: InkWell(
                  borderRadius: BorderRadius.circular(10),
                  onTap: () {
                    basket.addAll(
                        {'idTransaksi': transactionPaid.data[index].id});
                    Navigator.pushNamed(context, '/detail-transaksi');
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(Icons.shopping_bag,
                                    color: ColorPalette.themeIcon),
                                SizedBox(width: 8),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Belanja",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                        )),
                                    SizedBox(height: 2),
                                    Text(
                                        ConnexistHelper.formatDate(
                                            transactionPaid
                                                .data[index].createdAt),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                        )),
                                  ],
                                )
                              ],
                            )),
                            Text(
                                transactionPaid.data[index].status
                                    .toUpperCase(),
                                style: TextStyle(
                                  color: ColorPalette.themeIcon,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                ))
                          ],
                        ),
                        SizedBox(height: 5),
                        Divider(
                          thickness: 2,
                          color: Colors.grey[100],
                        ),
                        SizedBox(height: 5),
                        Container(
                          margin: EdgeInsets.only(bottom: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 45,
                                height: 45,
                                child: Stack(
                                  children: [
                                    Container(
                                        width: double.infinity,
                                        height: double.infinity,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        ),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          child: CachedNetworkImage(
                                            imageUrl: transactionPaid
                                                .data[index]
                                                .items
                                                .first
                                                .productImage
                                                .split(',')
                                                .first,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) =>
                                                Center(
                                                    child: Shimmer.fromColors(
                                              baseColor: Colors.grey[300],
                                              highlightColor: Colors.grey[100],
                                              child: Container(
                                                color: Colors.white,
                                                width: double.infinity,
                                                height: double.infinity,
                                              ),
                                            )),
                                            errorWidget:
                                                (context, url, error) => Center(
                                                    child: Image.asset(
                                              "assets/default_image.png",
                                              fit: BoxFit.cover,
                                            )),
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    transactionPaid.data[index].items.first
                                                .productName.length >
                                            75
                                        ? Text(
                                            transactionPaid.data[index].items
                                                    .first.productName
                                                    .substring(0, 80) +
                                                "...",
                                            maxLines: 2,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )
                                        : Text(
                                            transactionPaid.data[index].items
                                                .first.productName,
                                            maxLines: 2,
                                            style: TextStyle(
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                        transactionPaid
                                                .data[index].items.first.qty
                                                .toString() +
                                            " barang",
                                        style: TextStyle(
                                          fontSize: 12.0,
                                          color: ColorPalette.fontColor,
                                        )),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 5),
                        transactionPaid.data[index].items.length > 1
                            ? Text(
                                "+" +
                                    (transactionPaid.data[index].items.length -
                                            1)
                                        .toString() +
                                    " produk lainnya",
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: ColorPalette.fontColor,
                                ))
                            : Container(),
                        SizedBox(height: 20),
                        Text("Total Belanja",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                            )),
                        SizedBox(height: 5),
                        Text(
                            ConnexistHelper.formatCurrency(transactionPaid
                                .data[index].totalPrice
                                .toDouble()),
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                            )),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        }
        return loadingList();
      },
    );
  }

  Widget _emptyTransaction() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.receipt_long,
            size: 120,
            color: ColorPalette.btnYellow,
          ),
          SizedBox(height: 25),
          Container(
            child: Text(
              "Wah, Belum ada transaksi",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 5),
          Container(
            child: Text(
              "Yuk, isi transaksi dengan pesanan kesukaanmu",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16, color: Colors.grey[450]),
            ),
          ),
        ],
      ),
    );
  }

  Widget loadingList() {
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.all(16),
        shrinkWrap: true,
        itemCount: 3,
        itemBuilder: (context, index) {
          return Card(
            elevation: 2,
            margin: EdgeInsets.only(bottom: 8),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Row(
                            children: [
                              Container(
                                  height: 25,
                                  width: 25,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white)),
                              SizedBox(width: 8),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      height: 10,
                                      width: 80,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: Colors.white)),
                                  SizedBox(height: 2),
                                  Container(
                                      height: 10,
                                      width: 60,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                          color: Colors.white)),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                            height: 10,
                            width: 80,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.white)),
                      ],
                    ),
                    SizedBox(height: 5),
                    Divider(
                      thickness: 2,
                    ),
                    SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            height: 45,
                            width: 45,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white)),
                        SizedBox(width: 8),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                height: 10,
                                width: 120,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                            SizedBox(height: 5),
                            Container(
                                height: 10,
                                width: 80,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: Colors.white)),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 30),
                    Container(
                      width: 80,
                      height: 10.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                    ),
                    SizedBox(height: 5),
                    Container(
                      width: 60,
                      height: 10.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';

class ColorPalette {
  static const themeColor = Color(0xFF11304A);
  static const btnBlue = Color(0xFF0096DB);
  static const btnYellow = Color(0xFFFFC344);
  static const btnGreen = Color(0xFF1EC600);
  static const btnBack = Color(0xFFD1E1CF);
  static const btnRed = Color(0xffFF2800);
  static const btnAction = Color(0xffFFAE70);
  static const iconColor = Color(0xFF11304A);
  static const bgColor = Color(0xffC6F0FE);
  static const backgroundSplash = Color(0xFFe6e7e9);
  static const buttonFb = Color(0xff367FC0);
  static const orangeColor = Color(0xFFFF2800);
  static const greenChangeLocation = Color(0xff0DB299);
  static const bgWinningScreen2 = Color(0xfff56D5D);
  static const themeRed = Color(0xffFF6969);
  static const themeIcon = Color(0xffE95703);
  static const fontColor = Color(0xFF707070);
  static const btnGrey = Color(0xFFF8F8F8);
}

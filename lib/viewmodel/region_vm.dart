import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/region.dart';
import 'package:pekik_app/model/region_subdistrict.dart';
import 'package:pekik_app/utils/api_helper.dart';

class RegionViewModel {
  final CHttp http;
  RegionViewModel({@required this.http});

  Future<RegionModel> fetchProvince() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/region/province');
      print(_res);
      final RegionModel regionModel = RegionModel.fromJson(_res.data);
      return regionModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<RegionModel> fetchCity(String idProvince) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.get(
          '$baseUrl/commerce-service/region/city',
          queryParameters: {'provinceId': idProvince});
      print(_res);
      final RegionModel regionModel = RegionModel.fromJson(_res.data);
      return regionModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<RegionSubdistrictModel> fetchSubdistrict(String idCity) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.get(
          '$baseUrl/commerce-service/region/subdistrict',
          queryParameters: {'cityId': idCity});
      print(_res);
      final RegionSubdistrictModel regionSubdistrictModel =
          RegionSubdistrictModel.fromJson(_res.data);
      return regionSubdistrictModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }
}

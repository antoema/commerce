// import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/model/category_single_product.dart';
import 'package:pekik_app/model/merchant.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/model/product_single.dart';
import 'package:pekik_app/utils/api_helper.dart';
// import 'package:path/path.dart';

class MerchantViewModel {
  final CHttp http;
  MerchantViewModel({@required this.http});

  Future<MerchantModel> fetchMerchant() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/mytenant');
      print(_res);
      final MerchantModel merchantModel = MerchantModel.fromJson(_res.data);
      return merchantModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ProductModel> fetchProductAll() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/myproduct');
      print(_res);
      final ProductModel productModel = ProductModel.fromJson(_res.data);
      return productModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postEditMerchant(
      String idMerchant,
      String address,
      String name,
      String province,
      String city,
      String subDistrict,
      String idProvince,
      String idCity,
      String idSubDistrict,
      String postalCode,
      String phone,
      String latitude,
      String longitude,
      [String tenantLogo]) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.put('$baseUrl/commerce-service/tenant/$idMerchant',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'tenant_address': address,
            'tenant_name': name,
            'province': province,
            'city': city,
            'sub_district': subDistrict,
            'province_id': idProvince,
            'city_id': idCity,
            'sub_district_id': idSubDistrict,
            'postal_code': postalCode,
            'tenant_phone': phone,
            'tenant_lat': latitude,
            'tenant_long': longitude,
            'tenant_logo': tenantLogo,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CategoryProductModel> fetchCategoryAll() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/category');
      print(_res);
      final CategoryProductModel categoryProductModel =
          CategoryProductModel.fromJson(_res.data);
      return categoryProductModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CategorySingleProductModel> fetchSingleCategory(
      String idCategory) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/category/$idCategory');
      print(_res);
      final CategorySingleProductModel categorySingleProductModel =
          CategorySingleProductModel.fromJson(_res.data);
      return categorySingleProductModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ProductSingleModel> postProduct(
      String name,
      String description,
      String img,
      String price,
      String dimention,
      String weight,
      String measurement,
      String stock,
      String minimum,
      String tenantId,
      String idCategory) async {
    final Dio _dio = await http.getClient();

    try {
      final Response _res = await _dio.post('$baseUrl/commerce-service/product',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'product_name': name,
            'product_desc': description,
            'product_image': img,
            'product_price': price,
            'product_dimensions': dimention,
            'product_weight': weight,
            'product_measurement_unit': measurement,
            'product_stock': stock,
            'minimum_order': minimum,
            'tenant_id': tenantId,
            'product_category_id': idCategory
          });
      print(_res);
      final ProductSingleModel productSingleModel =
          ProductSingleModel.fromJson(_res.data);
      return productSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err.response.data);
      print(err);
      return null;
    }
  }

  Future<ProductSingleModel> postEditProduct(
    String id,
    String name,
    String description,
    String price,
    String dimention,
    String weight,
    String measurement,
    String minimumOrder,
    String stock,
    String tenantId,
    String idCategory, [
    String image = '',
  ]) async {
    final Dio _dio = await http.getClient();

    Map<String, dynamic> _data = {
      'product_name': name,
      'product_desc': description,
      'product_price': price,
      'product_dimensions': dimention,
      'product_weight': weight,
      'product_measurement_unit': measurement,
      'minimum_order': minimumOrder,
      'product_stock': stock,
      'tenant_id': tenantId,
      'product_category_id': idCategory
    };

    if (image.trim().length > 0) {
      _data.addAll({"product_image": image});
    }

    try {
      final Response _res = await _dio.put(
          '$baseUrl/commerce-service/product/$id',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: _data);
      print(_res);
      final ProductSingleModel productSingleModel =
          ProductSingleModel.fromJson(_res.data);
      return productSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ProductSingleModel> postEditPublish(String id, bool isActive) async {
    final Dio _dio = await http.getClient();

    try {
      final Response _res = await _dio.put(
          '$baseUrl/commerce-service/product/$id',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {'is_active': isActive.toString()});
      print(_res);
      final ProductSingleModel productSingleModel =
          ProductSingleModel.fromJson(_res.data);
      return productSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }
}

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/image_upload.dart';

class ImageUploader {
  static Future<ImageUploadModel> uploadImage(File file) async {
    Dio _dio = Dio();
    // String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(file.path),
    });
    // print(formData.files[0].value.length);
    try {
      Response _res = await _dio.post("$baseUrl/media-service/media/upload",
          data: formData);

      if (_res.statusCode != 200) {
        print("error");
        throw (_res.statusMessage);
      }
      print(_res);
      ImageUploadModel _img = ImageUploadModel.fromJson(_res.data);
      return _img;
    } catch (err) {
      print(err);
      return null;
    }
  }
}

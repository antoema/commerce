import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/address.dart';
import 'package:pekik_app/utils/api_helper.dart';

class AddressViewModel {
  final CHttp http;
  AddressViewModel({@required this.http});

  Future<AddressModel> fetchAddressAll() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/shippingaddress');
      print(_res);
      final AddressModel addressModel = AddressModel.fromJson(_res.data);
      return addressModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postAddress(
      String address,
      String name,
      String province,
      String city,
      String subDistrict,
      String idProvince,
      String idCity,
      String idSubDistrict,
      String postalCode,
      String phone,
      String latitude,
      String longitude) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.post('$baseUrl/commerce-service/shippingaddress',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'address': address,
            'name': name,
            'province': province,
            'city': city,
            'sub_district': subDistrict,
            'province_id': idProvince,
            'city_id': idCity,
            'sub_district_id': idSubDistrict,
            'postal_code': postalCode,
            'phone_number': phone,
            'latitude': latitude,
            'longitude': longitude,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postSetDefaultAddress(String idAddress) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.put('$baseUrl/commerce-service/shippingaddress/$idAddress',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'default_location': true,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err.response.data);
      print(err);
      return null;
    }
  }

  Future postEditAddress(
      String idAddress,
      String address,
      String name,
      String province,
      String city,
      String subDistrict,
      String idProvince,
      String idCity,
      String idSubDistrict,
      String postalCode,
      String phone,
      String latitude,
      String longitude) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.put('$baseUrl/commerce-service/shippingaddress/$idAddress',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'address': address,
            'name': name,
            'province': province,
            'city': city,
            'sub_district': subDistrict,
            'province_id': idProvince,
            'city_id': idCity,
            'sub_district_id': idSubDistrict,
            'postal_code': postalCode,
            'phone_number': phone,
            'latitude': latitude,
            'longitude': longitude,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }
}

import 'package:dio/dio.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/banner.dart';
import 'package:pekik_app/model/category_product.dart';
import 'package:pekik_app/model/product.dart';
import 'package:pekik_app/model/product_single.dart';
import 'package:pekik_app/model/tenant_single.dart';

class PublicProductViewModel {
  PublicProductViewModel();

  Future<ProductModel> fetchProductAll() async {
    final Dio _dio = Dio();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/public/products');
      print(_res);
      final ProductModel productModel = ProductModel.fromJson(_res.data);
      return productModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ProductSingleModel> fetchSingleProduct(String idProduct) async {
    final Dio _dio = Dio();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/public/product/$idProduct');
      print(_res);
      final ProductSingleModel productSingleModel =
          ProductSingleModel.fromJson(_res.data);
      return productSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CategoryProductModel> fetchCategoryProduct() async {
    final Dio _dio = Dio();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/public/category');
      print(_res);
      final CategoryProductModel categoryProductModel =
          CategoryProductModel.fromJson(_res.data);
      return categoryProductModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ProductModel> fetchProductHome(String path) async {
    final Dio _dio = Dio();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/public/products/$path');
      print(_res);
      final ProductModel productModel = ProductModel.fromJson(_res.data);
      return productModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ProductModel> fetchProductFilter(String tenantType, String tenantId,
      String idCategory, String qName) async {
    final Dio _dio = Dio();
    try {
      final Response _res = await _dio.get(
          '$baseUrl/commerce-service/public/products/filter',
          queryParameters: {
            'tenantType': tenantType,
            'tenantId': tenantId,
            'categoryId': idCategory,
            'q': qName,
          });
      print(_res);
      final ProductModel productModel = ProductModel.fromJson(_res.data);
      return productModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TenantSingleModel> fetchTenantFilter(
      String tenantId, String qName) async {
    final Dio _dio = Dio();
    try {
      final Response _res = await _dio.get(
          '$baseUrl/commerce-service/public/tenants/filter',
          queryParameters: {
            'tenantId': tenantId,
            'q': qName,
          });
      print(_res);
      final TenantSingleModel tenantSingleModel =
          TenantSingleModel.fromJson(_res.data);
      return tenantSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<BannerModel> fetchBannerAll() async {
    final Dio _dio = Dio();
    try {
      final Response _res = await _dio.get('$baseUrl/banner-service/banners');
      print(_res);
      final BannerModel bannerModel = BannerModel.fromJson(_res.data);
      return bannerModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CategoryProductModel> fetchCategoryPublic() async {
    final Dio _dio = Dio();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/public/category');
      print(_res);
      final CategoryProductModel categoryProductModel =
          CategoryProductModel.fromJson(_res.data);
      return categoryProductModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }
}

import 'package:dio/dio.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/response_bank.dart';
import 'package:pekik_app/model/response_wd_checkout.dart';
import 'package:pekik_app/model/user_bank.dart';
import 'package:pekik_app/model/wallet.dart';
import 'package:pekik_app/model/wallet_bank.dart';
import 'package:pekik_app/utils/api_helper.dart';

class WalletViewModel {
  final CHttp http;
  WalletViewModel({this.http});

  Future<WalletModel> fetchMyWallet() async {
    final Dio _dio = await http.getClient();
    try {
      Response _res = await _dio.get('$baseUrl/wallet-service/mydeposit');
      print(_res);
      final WalletModel walletModel = WalletModel.fromJson(_res.data);
      return walletModel;
    } catch (err) {
      print(err.request.headers);
      print(err);
      return err;
    }
  }

  Future<WalletBank> fetchListBank() async {
    final Dio _dio = await http.getClient();
    try {
      Response _res = await _dio.get('$baseUrl/ppob-service/banklist');
      print(_res);
      final WalletBank walletBank = WalletBank.fromJson(_res.data);
      return walletBank;
    } catch (err) {
      print(err.request.headers);
      print(err);
      return err;
    }
  }

  Future<ResponseBank> fetchDataBank(
      String bankCode, String bankNumber, int nominal, String phone) async {
    final Dio _dio = await http.getClient();
    try {
      Response _res =
          await _dio.post('$baseUrl/ppob-service/bankaccount', data: {
        "bank_code": bankCode,
        "bank_number": bankNumber,
        "nominal": nominal,
        "phone": phone
      });
      print(_res);
      final ResponseBank responseBank = ResponseBank.fromJson(_res.data);
      return responseBank;
    } catch (err) {
      print(err.request.headers);
      print(err);
      return err;
    }
  }

  Future<UserBank> fetchUserBank() async {
    final Dio _dio = await http.getClient();
    try {
      Response _res = await _dio.get('$baseUrl/ppob-service/user/bankacc');
      print(_res);
      final UserBank userBank = UserBank.fromJson(_res.data);
      return userBank;
    } catch (err) {
      print(err.request.headers);
      print(err);
      return err;
    }
  }

  Future postUserBank(String bankNumber, String bankName, String bankCode,
      String accName) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.post('$baseUrl/ppob-service/user/bankacc',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'bank_number': bankNumber,
            'bank_name': bankName,
            'bank_code': bankCode,
            'acc_name': accName,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future deleteUserBank(String id) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.delete('$baseUrl/ppob-service/user/bankacc/$id');
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postWithdrawCheckout(String userBankId, String nominal ) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
      await _dio.post('$baseUrl/ppob-service/cashout/inquiry',
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
          ),
          data: {
            'userbank_id': userBankId,
            'nominal': nominal
          });
      print(_res);
      /*final ResponseWdCheckout responseWdCheckout = ResponseWdCheckout.fromJson(_res.data);
      return responseWdCheckout;*/
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      print(err.response);
      return err.response;
    }
  }

  Future postWithdrawPay(String userBankId, String nominal ) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
      await _dio.post('$baseUrl/ppob-service/cashout/pay',
          options: Options(
            contentType: Headers.formUrlEncodedContentType,
          ),
          data: {
            'userbank_id': userBankId,
            'nominal': nominal
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return err;
    }
  }
}

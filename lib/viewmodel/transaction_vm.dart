import 'package:dio/dio.dart';
import 'package:pekik_app/constant/constants.dart';
import 'package:pekik_app/model/bank.dart';
import 'package:pekik_app/model/cart.dart';
import 'package:pekik_app/model/cart_badge.dart';
import 'package:pekik_app/model/checkout.dart';
import 'package:pekik_app/model/expedition.dart';
import 'package:pekik_app/model/payment.dart';
import 'package:pekik_app/model/tracking_courier.dart';
import 'package:pekik_app/model/transaction_by_status.dart';
import 'package:pekik_app/model/transaction_single.dart';
import 'package:pekik_app/model/transaction_waiting.dart';
import 'package:pekik_app/utils/api_helper.dart';

class TransactionViewModel {
  final CHttp http;
  TransactionViewModel({this.http});

  Future<CartModel> fetchCartAll() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/shoppingcart');
      print(_res);
      final CartModel cartModel = CartModel.fromJson(_res.data);
      return cartModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CartModel> postCart(String idProduct) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/shoppingcart',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {'product_id': idProduct});
      print(_res);
      final CartModel cartModel = CartModel.fromJson(_res.data);
      return cartModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CartModel> postIncrementCart(String idProduct, String idCart) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/shoppingcart/increment',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'product_id': idProduct,
            'cart_id': idCart,
          });
      print(_res);
      final CartModel cartModel = CartModel.fromJson(_res.data);
      return cartModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CartModel> postDecrementCart(String idProduct, String idCart) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/shoppingcart/decrement',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'product_id': idProduct,
            'cart_id': idCart,
          });
      print(_res);
      final CartModel cartModel = CartModel.fromJson(_res.data);
      return cartModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CartModel> postRemoveItemCart(String idCart, String idItemCart) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/shoppingcart/delete',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'cart_id': idCart,
            'cart_item_id': idItemCart,
          });
      print(_res);
      final CartModel cartModel = CartModel.fromJson(_res.data);
      return cartModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CartModel> postEmptyCart() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/shoppingcart/empty',
          options: Options(contentType: Headers.formUrlEncodedContentType));
      print(_res);
      final CartModel cartModel = CartModel.fromJson(_res.data);
      return cartModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CartBadgesModel> fetchCartBadge() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/shoppingcart/badges');
      print(_res);
      final CartBadgesModel cartBadgesModel =
          CartBadgesModel.fromJson(_res.data);
      return cartBadgesModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<CheckoutModel> fetchCheckout() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/shoppingcart/checkout');
      print(_res);
      final CheckoutModel checkoutModel = CheckoutModel.fromJson(_res.data);
      return checkoutModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<ExpeditionModel> fetchExpedition(String idCart) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.get(
        '$baseUrl/commerce-service/shoppingcart/selectexpedition',
        queryParameters: {'cartId': idCart},
      );
      print(_res);
      final ExpeditionModel expeditionModel =
          ExpeditionModel.fromJson(_res.data);
      return expeditionModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postSetExpedition(
      String idCart,
      String expeditionCode,
      String expeditionName,
      String expeditionService,
      String expeditionCost) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/shoppingcart/chooseexpedition',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'cart_id': idCart,
            'expedition_code': expeditionCode,
            'expedition_name': expeditionName,
            'expedition_service': expeditionService,
            'expedition_cost': expeditionCost,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<BankModel> fetchBank() async {
    final Dio _dio = Dio();
    try {
      final Response _res = await _dio
          .get('http://167.172.68.215:8087/payment-service/winpay/channels');
      print(_res);
      final BankModel bankModel = BankModel.fromJson(_res.data);
      return bankModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<PaymentModel> postPayTransaction(String paymentChannel) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.post('$baseUrl/commerce-service/shoppingcart/submit',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'payment_channel': paymentChannel,
          });
      print(_res);
      final PaymentModel paymentModel = PaymentModel.fromJson(_res.data);
      return paymentModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TransactionWaitingModel> fetchTransactionWaiting() async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/transaction/waiting');
      print(_res.data);
      final TransactionWaitingModel transactionWaitingModel =
          TransactionWaitingModel.fromJson(_res.data);
      return transactionWaitingModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TransactionByStatusModel> fetchTransactionByStatus(
      String status) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio
          .get('$baseUrl/commerce-service/transaction?status=$status');
      print(_res);
      final TransactionByStatusModel transactionByStatusModel =
          TransactionByStatusModel.fromJson(_res.data);
      return transactionByStatusModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TransactionSingleModel> fetchTransactionSingle(String idTrx) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.get('$baseUrl/commerce-service/transaction/detail/$idTrx');
      print(_res);
      final TransactionSingleModel transactionSingleModel =
          TransactionSingleModel.fromJson(_res.data);
      return transactionSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TrackingCourierModel> fetchTrackingCourier(String idTrx) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio
          .get('$baseUrl/commerce-service/transaction/tracking?trxId=$idTrx');
      print(_res);
      final TrackingCourierModel trackingCourierModel =
          TrackingCourierModel.fromJson(_res.data);
      return trackingCourierModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TransactionByStatusModel> fetchSellerTransactionByStatus(
      String status) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio
          .get('$baseUrl/commerce-service/transaction/seller?status=$status');
      print(_res);
      final TransactionByStatusModel transactionByStatusModel =
          TransactionByStatusModel.fromJson(_res.data);
      return transactionByStatusModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future<TransactionSingleModel> fetchSellerTransactionSingle(
      String idTrx) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio
          .get('$baseUrl/commerce-service/transaction/seller/detail/$idTrx');
      print(_res);
      final TransactionSingleModel transactionSingleModel =
          TransactionSingleModel.fromJson(_res.data);
      return transactionSingleModel;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postDoneTransaction(String idTrx) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res =
          await _dio.post('$baseUrl/commerce-service/transaction/done',
              options: Options(
                contentType: Headers.formUrlEncodedContentType,
              ),
              data: {
            'trxId': idTrx,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err.response.data);
      print(err);
      return null;
    }
  }

  Future postPackingTransaction(String idTrx) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/transaction/seller/packing',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'trxId': idTrx,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }

  Future postShippingTransaction(String idTrx, String wayBill) async {
    final Dio _dio = await http.getClient();
    try {
      final Response _res = await _dio.post(
          '$baseUrl/commerce-service/transaction/seller/shipping',
          options: Options(contentType: Headers.formUrlEncodedContentType),
          data: {
            'trxId': idTrx,
            'wayBill': wayBill,
          });
      print(_res);
      return _res.data;
    } on DioError catch (err) {
      print(err.request.headers);
      print(err);
      return null;
    }
  }
}
